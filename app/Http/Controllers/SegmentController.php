<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Segment;
use App\Models\Theme;
use App\Models\Base;

class SegmentController extends Controller {



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $segments = \DB::table('segments')->get();
        $bases = \DB::table('bases')->select('id', 'nom')->get();
        return view('segment.index_new', ['segments' => $segments, 'bases' => $bases])
            ->withMenu('campagnes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $themes = array();
        $departements=array();
        $campagnes=array();
        $sexe = '';
        $agemin = '';
        $agemax = '';


        $all_themes = \DB::table('themes')->select('id','nom')->get();
        foreach($all_themes as $t)
        {
            $themes[$t->id]=$t->nom;
        }

        $all_departements = \DB::table('segments_departements')->get();
        foreach ($all_departements as $d) {
          $departements[intval($d->departement_code)] = $d->departement_code . ' - ' . $d->departement_nom;
        }

        $campagnes = \DB::table('campagnes')->select('id','ref')
            ->orderBy('id', 'desc')
            ->get();

        return view('segment.create_new')
            ->withThemes($themes)
            ->withDepartements($departements)
            ->withCampagnes($campagnes)
            ->with('statut', '0')
            ->with('selected_departements', array())
            ->with('selected_campagnes', array())
            ->with('selected_themes', array())
            ->with('from', '')
            ->with('to', '')
            ->with('sexe', $sexe)
            ->with('agemin', $agemin)
            ->with('agemax', $agemax)
            ->withMenu('campagnes');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $message = '';

        $fields = array_except(\Input::all(), '_token');

        $segment = new Segment();
        $segment->nom = $fields['nom'];
        $segment->created_at = \Carbon\Carbon::now();
        $segment->updated_at = \Carbon\Carbon::now();
        $segment->save();

        if(! empty($fields['statut']) ){

            $operator = '!=';
            $value = '0';

            if(!empty($fields['from'])){
                $operator = '>';
                $value = date("Y-m-d", strtotime($fields['from']));
            }

            \DB::table('segments_conditions')->insert([
                'segment_id'=>$segment->id,
                'condition_type' => $fields['statut'],
                'condition_column' => 'date_active',
                'condition_operator' => $operator,
                'condition_value' => $value,

            ]);

            if(!empty($fields['to'])){
                $operator = '<';
                $value = date("Y-m-d", strtotime($fields['to']));

                \DB::table('segments_conditions')->insert([
                    'segment_id'=>$segment->id,
                    'condition_type' => $fields['statut'],
                    'condition_column' => 'date_active',
                    'condition_operator' => $operator,
                    'condition_value' => $value,

                ]);
            }
        }

        if(isset($fields['themes'])) {
            foreach ($fields['themes'] as $theme_id)
            {

                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'theme',
                    'condition_column' => 'theme_id',
                    'condition_operator' => '=',
                    'condition_value' => $theme_id,

                ]);
            }
        }

        if(isset($fields['departements'])) {
            foreach ($fields['departements'] as $geo_dep)
            {

                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'geoloc',
                    'condition_column' => 'departement',
                    'condition_operator' => '=',
                    'condition_value' => $geo_dep,

                ]);
            }
        }

        if(isset($fields['campagnes'])) {
            foreach ($fields['campagnes'] as $camp_id)
            {
                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'campagnes',
                    'condition_column' => 'campagne_id',
                    'condition_operator' => '=',
                    'condition_value' => $camp_id,

                ]);
            }
        }

        // var_dump($fields['sexe']);
        // die();
        if(isset($fields['sexe'])) {
          if($fields['sexe'] != '-'){

            \DB::table('segments_conditions')->insert([
            'segment_id' => $segment->id,
            'condition_type' => 'sexe',
            'condition_column' => 'civilite',
            'condition_operator' => '=',
            'condition_value' => $fields['sexe'],
            ]);
          }
        }


        if(isset($fields['agemin'])) {
          if($fields['agemin'] != '-'){

            $operator = '<';

            \DB::table('segments_conditions')->insert([
            'segment_id' => $segment->id,
            'condition_type' => 'age',
            'condition_column' => 'datenaissance',
            'condition_operator' => $operator,
            'condition_value' => $fields['agemin'],
            ]);
          }
        }

        if(isset($fields['agemax'])) {
          if($fields['agemax'] != '-'){

            $operator = '>';

            \DB::table('segments_conditions')->insert([
            'segment_id' => $segment->id,
            'condition_type' => 'age',
            'condition_column' => 'datenaissance',
            'condition_operator' => $operator,
            'condition_value' => $fields['agemax'],
            ]);
          }
        }


        return \redirect('segment')
            ->withMessage($message)
            ->withMenu('campagnes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $all_themes = \DB::table('themes')
            ->select('id','nom')
            ->get();

        $campagnes = \DB::table('campagnes')
            ->select('id','ref')
            ->orderBy('id', 'desc')
            ->get();

        $themes = array();
        $departements=array();
        $selected_themes = $selected_departements = $selected_campagnes = array();
        $statut = '0';
        $from = $to = '';

        $segment = \DB::table('segments')
            ->where('id', $id)
            ->first();

        foreach($all_themes as $t)
        {
            $themes[$t->id]=$t->nom;
        }

        $selected_th = \DB::table('segments_conditions')
            ->where('segment_id', $segment->id)
            ->where('condition_type', 'theme')
            ->get();

        foreach($selected_th as $sth)
        {
            $selected_themes[$sth->condition_value] = $sth->condition_value;
        }

        $all_departements = \DB::table('segments_departements')->get();
        foreach ($all_departements as $d) {
          $departements[intval($d->departement_code)] = $d->departement_code . ' - ' . $d->departement_nom;
        }

        $selected_dep = \DB::table('segments_conditions')
            ->where('segment_id', $segment->id)
            ->where('condition_type', 'geoloc')
            ->get();

        foreach($selected_dep as $std)
        {
            $selected_departements[$std->condition_value] = $std->condition_value;
        }

        $selected_th = \DB::table('segments_conditions')
            ->where('segment_id', $segment->id)
            ->where('condition_type', 'theme')
            ->get();

        foreach($selected_th as $sth)
        {
            $selected_themes[$sth->condition_value] = $sth->condition_value;
        }

        $selected_ca = \DB::table('segments_conditions')
            ->where('segment_id', $segment->id)
            ->where('condition_type', 'campagnes')
            ->get();

        foreach($selected_ca as $stc)
        {
            $selected_campagnes[$stc->condition_value] = $stc->condition_value;
        }

        $statut_o = \DB::table('segments_conditions')
            ->where('segment_id', $segment->id)
            ->where('condition_type', 'ouvreurs')
            ->get();

        $statut_c = \DB::table('segments_conditions')
            ->where('segment_id', $segment->id)
            ->where('condition_type', 'cliqueurs')
            ->get();

        $statut_i = \DB::table('segments_conditions')
            ->where('segment_id', $segment->id)
            ->where('condition_type', 'inactifs')
            ->get();

        if(!empty($statut_o)) {
            $statut = 'ouvreurs';
            foreach($statut_o as $s)
            {
                if($s->condition_operator == '>'){
                    $from = date("Y-m-d", strtotime($s->condition_value));
                }
                if($s->condition_operator == '<'){
                    $to = date("Y-m-d", strtotime($s->condition_value));
                }
            }
        } elseif(!empty($statut_c)) {
            $statut = 'cliqueurs';
            foreach($statut_c as $s)
            {
                if($s->condition_operator == '>'){
                    $from = date("Y-m-d", strtotime($s->condition_value));
                }
                if($s->condition_operator == '<'){
                    $to = date("Y-m-d", strtotime($s->condition_value));
                }
            }
        } elseif(!empty($statut_i)) {
            $statut = 'inactifs';
            foreach($statut_i as $s)
            {
                if($s->condition_operator == '>'){
                    $from = date("Y-m-d", strtotime($s->condition_value));
                }
                if($s->condition_operator == '<'){
                    $to = date("Y-m-d", strtotime($s->condition_value));
                }
            }
        }

        $sexe_segment = \DB::table('segments_conditions')->select('condition_value')->where('segment_id', $segment->id)->where('condition_type', 'sexe')->first();
        $age_segment_min = \DB::table('segments_conditions')->where('segment_id', $segment->id)->where('condition_operator', '<')->where('condition_type', 'age')->first();
        $age_segment_max = \DB::table('segments_conditions')->where('segment_id', $segment->id)->where('condition_operator', '>')->where('condition_type', 'age')->first();

        if($sexe_segment == null){
          $sexe = '';
        } else {
          $sexe = $sexe_segment->condition_value;
        }

        if($age_segment_min == null){
          $agemin = '';
        } else {
          $agemin = $age_segment_min->condition_value;

        }

        if($age_segment_max == null){
          $agemax = '';
        } else {
          $agemax = $age_segment_max->condition_value;

        }

        // var_dump($agemin);
        // var_dump($agemax);

        return view('segment.edit')
            ->withSegment($segment)
            ->withThemes($themes)
            ->withDepartements($departements)
            ->withCampagnes($campagnes)
            ->with('statut', $statut)
            ->with('selected_departements', $selected_departements)
            ->with('selected_themes', $selected_themes)
            ->with('selected_campagnes', $selected_campagnes)
            ->with('from', $from)
            ->with('to', $to)
            ->with('sexe',$sexe)
            ->with('agemin',$agemin)
            ->with('agemax',$agemax)
            ->withMenu('campagnes');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $fields = array_except(\Input::all(), '_token');

        $segment = Segment::find($id);
        $segment->nom = $fields['nom'];


        if(! empty($fields['statut']) ){

            $operator = '!=';
            $value = '0';

            \DB::table('segments_conditions')->where('segment_id', $segment->id)->delete();

            if(!empty($fields['from'])){
                $operator = '>';
                $value = date("Y-m-d", strtotime($fields['from']));
            }

            \DB::table('segments_conditions')->insert([
                'segment_id'=>$segment->id,
                'condition_type' => $fields['statut'],
                'condition_column' => 'date_active',
                'condition_operator' => $operator,
                'condition_value' => $value,

            ]);

            if(!empty($fields['to'])){
                $operator = '<';
                $value = date("Y-m-d", strtotime($fields['to']));

                \DB::table('segments_conditions')->insert([
                    'segment_id'=>$segment->id,
                    'condition_type' => $fields['statut'],
                    'condition_column' => 'date_active',
                    'condition_operator' => $operator,
                    'condition_value' => $value,

                ]);
            }
        }

        if(isset($fields['themes'])) {
            foreach ($fields['themes'] as $theme_id)
            {
                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'theme',
                    'condition_column' => 'theme_id',
                    'condition_operator' => '=',
                    'condition_value' => $theme_id,

                ]);
            }
        }

        if(isset($fields['departements'])) {

            \DB::table('segments_conditions')->where('segment_id', $segment->id)->where('condition_type', 'geoloc')->delete();

            foreach ($fields['departements'] as $geo_dep)
            {
                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'geoloc',
                    'condition_column' => 'departement',
                    'condition_operator' => '=',
                    'condition_value' => $geo_dep,

                ]);
            }
        }

        if(isset($fields['campagnes'])) {
            foreach ($fields['campagnes'] as $camp_id)
            {

                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'campagnes',
                    'condition_column' => 'campagne_id',
                    'condition_operator' => '=',
                    'condition_value' => $camp_id,

                ]);
            }
        }

        if(isset($fields['sexe'])) {
            if($fields['sexe'] != '-') {

              \DB::table('segments_conditions')->where('segment_id', $segment->id)->where('condition_type', 'sexe')->delete();

                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'sexe',
                    'condition_column' => 'civilite',
                    'condition_operator' => '=',
                    'condition_value' => $fields['sexe'],

                ]);
        }
        }

        if(isset($fields['agemin'])){
          if($fields['agemin'] != '-') {

              \DB::table('segments_conditions')->where('segment_id', $segment->id)->where('condition_operator', '<')->where('condition_type', 'age')->delete();

                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'age',
                    'condition_column' => 'datenaissance',
                    'condition_operator' => '<',
                    'condition_value' => $fields['agemin'],
                ]);
            }
        }

        if(isset($fields['agemax'])){
          if($fields['agemax'] != '-') {

            \DB::table('segments_conditions')->where('segment_id', $segment->id)->where('condition_operator', '>')->where('condition_type', 'age')->delete();
                \DB::table('segments_conditions')->insert([
                    'segment_id' => $segment->id,
                    'condition_type' => 'age',
                    'condition_column' => 'datenaissance',
                    'condition_operator' => '>',
                    'condition_value' => $fields['agemax'],

                ]);
        }
      }


        $segment->updated_at = \Carbon\Carbon::now();

        $segment->save();

        return \redirect('segment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Segment::find($id)->delete();
        \DB::table('segments_conditions')->where('segment_id',$id)->delete();
        \DB::table('plannings_segments')->where('segment_id',$id)->delete();

        return \redirect('segment');
    }

    public function ajaxShowVolume()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        /* Création de la requête */
        \Log::info('ajaxShowVolume - Début création de la requête');

        $pdo = \DB::connection()->getPdo();

        $base_id = \Input::get('base_id');
        $segment_id = \Input::get('segment_id');

        //Dans le cas où il n'y a pas de base associé = toutes bases confondues
        if(empty($base_id)){
            $bases = \DB::table('bases')
                ->select('id')
                ->where('is_active', 1)
                ->get();

            $base_ids = array_pluck($bases,'id');

            $base_query = "base_id IN (";
            foreach ($bases as $bid) {
                $base_query .= "$bid->id,";
            }
            $base_query = substr($base_query, 0, -1) . ")";
        } else {
            $base_query = "base_id = $base_id";
            $base_ids = [$base_id];
        }

        $count = 0;
        $geoloc_query = "";
        $age_query = "";
        $civil_query = "";

        //AUTRES CRITERES RAJOUTES : ouvreurs / cliqueurs / thématique / geoloc
        $critQuery = \DB::table('segments_conditions')
            ->where('segment_id', $segment_id);

        $critQuery2 = clone $critQuery;
        $critQuery3 = clone $critQuery;
        $critQuery4 = clone $critQuery;
        $critQuery5 = clone $critQuery;
        $critQuery6 = clone $critQuery;
        $critQuery7 = clone $critQuery;
        $critQuery8 = clone $critQuery;

        //Si segment avec ouvreurs
        $conditions_ouv = $critQuery->where('condition_type','ouvreurs')->get();
        //Si segment avec cliqueurs
        $conditions_clic = $critQuery2->where('condition_type','cliqueurs')->get();
        //Si segment avec themes
        $conditions_theme = $critQuery3->where('condition_type', 'theme')->get();
        //Si segment avec geoloc
        $conditions_geoloc = $critQuery4->where('condition_type','geoloc')->get();
        //Si segment avec sexe
        $conditions_civilite = $critQuery5->where('condition_type','sexe')->get();
        //Si segment avec sexe
        $conditions_age = $critQuery6->where('condition_type','age')->get();
        //Si segment inactifs
        $conditions_inactif = $critQuery7->where('condition_type','inactifs')->get();

        $selected_campagne_id = $critQuery8->where('condition_type','campagnes')->get();

        if(isset($conditions_geoloc) and count($conditions_geoloc) > 0) {
            $all_geoloc = array_pluck($conditions_geoloc, 'condition_value');

            $geoloc_query .= ' and (';
            foreach($all_geoloc as $ag)
            {
                $geoloc_query .= 'departement like "'.$ag.'%" or ';
            }
            $geoloc_query = substr($geoloc_query, 0, -4).")";
        }

        if(isset($conditions_civilite) && !empty($conditions_civilite)) {
            \Log::info('ajaxShowVolume - In sexe condition');

            foreach ($conditions_civilite as $civil)
            {
              $civil_query .= " AND (civilite = " . $civil->condition_value . " ) ";
            }
            \Log::info('ajaxShowVolume - Fin sexe condition');
        }


        if(isset($conditions_age) && !empty($conditions_age)) {
            \Log::info('ajaxShowVolume - In age condition');
            // Simple pour le moment
            $conditiondusegmentmin = \DB::table('segments_conditions')
                    ->where('segment_id', $segment_id)
                    ->where('condition_operator','<')
                    ->first();

            $conditiondusegmentmax = \DB::table('segments_conditions')
                    ->where('segment_id', $segment_id)
                    ->where('condition_operator','>')
                    ->first();

            $yearnow = date('Y');

            $conditiondusegmentminc = $yearnow - $conditiondusegmentmin->condition_value;
            $conditiondusegmentmaxc = $yearnow - $conditiondusegmentmax->condition_value;

            $age_query .= " AND (datenaissance < " . $conditiondusegmentminc . " AND datenaissance > " . $conditiondusegmentmaxc . " ) ";

            \Log::info('ajaxShowVolume - Fin age condition');
        }
        \Log::info('ajaxShowVolume - After if sexe');

        if(isset($conditions_theme) && !empty($conditions_theme)) {
            \Log::info('ajaxShowVolume - In  thème condition');
            $campagne_ids_query = \DB::table('campagnes')
                ->select('id')
                ->whereIn('base_id', $base_ids)
                ->whereIn('theme_id', array_pluck($conditions_theme,'condition_value'));
//            \Log::info(json_encode(array_pluck($conditions_theme,'condition_value')));
            $campagne_ids_query2 = clone $campagne_ids_query;
        }
        \Log::info('ajaxShowVolume - After if thèmes');

        if(isset($conditions_ouv) && !empty($conditions_ouv)){
            \Log::info('ajaxShowVolume - In  ouvertures condition');
            $ouvreursQuery = \DB::table('ouvertures')
                ->select('destinataire_id');

            foreach($conditions_ouv as $co)
            {
//                echo '\n'.$co->condition_column.' - '. $co->condition_operator.' - '. $co->condition_value;
                $ouvreursQuery->where($co->condition_column, $co->condition_operator, $co->condition_value) ;
                if(empty($conditions_theme)) {
                    continue;
                }
                $campagne_ids_query->where('created_at', $co->condition_operator, $co->condition_value);
            }
            if(!empty($campagne_ids_query)){
                $campagne_ids = $campagne_ids_query->get();
                $ouvreursQuery->whereIn('campagne_id', array_pluck($campagne_ids, 'id'));
            }
//            select id from campagnes where theme_id in (180) and created_at > '2016-08-01 00:00:00' and created_at < '2016-08-30 00:00:00'
            $toSql = $ouvreursQuery->toSql();
            /* Autre requête plus précise */
//            $sql = "SELECT COUNT(*) as max_tok FROM tokens WHERE base_id = $base_id and campagne_id = NULL and destinataire_id IN ($toSql)";
            $sql = "SELECT COUNT(distinct mail) as max_tok, fai_id FROM destinataires WHERE $base_query and statut < 3 and id IN ($toSql)$geoloc_query GROUP BY fai_id";
//            \Log::info("SQL -- $sql");
//            var_dump($campagne_ids);
            $stmt = $pdo->prepare($sql);
            $bindings = $ouvreursQuery->getBindings();
            $stmt->execute($bindings);
            $response = $stmt->fetchAll();
//            $count = $response['max_tok'];
//            \Log::info(json_encode([$response['max_tok'], $sql]));

        } elseif(isset($conditions_clic) && !empty($conditions_clic)){
            \Log::info('ajaxShowVolume - In  clics condition');
            $cliqueursQuery = \DB::table('clics')->select('destinataire_id');

            foreach($conditions_clic as $cc)
            {
                $cliqueursQuery->where($cc->condition_column, $cc->condition_operator, $cc->condition_value);
                if(empty($conditions_theme)) {
                    continue;
                }
                $campagne_ids_query2->where('created_at', $cc->condition_operator, $cc->condition_value);
            }
            if(!empty($campagne_ids_query2)){
                $campagne_ids = $campagne_ids_query2->get();
                $cliqueursQuery->whereIn('campagne_id', array_pluck($campagne_ids, 'id'));
            }

            $toSql = $cliqueursQuery->toSql();
            /* Autre requête plus précise */
//            $sql = "SELECT COUNT(*) as max_tok FROM tokens WHERE base_id = $base_id and campagne_id = NULL and destinataire_id IN ($toSql)";
            $sql = "SELECT COUNT(distinct mail) as max_tok, fai_id FROM destinataires WHERE $base_query and statut < 3 and id IN ($toSql)$geoloc_query GROUP BY fai_id";
//            \Log::info("SQL -- $sql");
//            var_dump($campagne_ids);
            $stmt = $pdo->prepare($sql);
            $bindings = $cliqueursQuery->getBindings();
            $stmt->execute($bindings);
            $response = $stmt->fetchAll();
//            $count = $response['max_tok'];
//            \Log::info(json_encode([$response['max_tok'], $sql]));
        } else { //juste la geoloc
            $ouvreursQuery = '';
            $cliqueursQuery = '';

            if(!empty($conditions_inactif)){
                $ouvreursQuery = " AND id NOT IN (SELECT distinct destinataire_id FROM ouvertures WHERE";
                $cliqueursQuery = " AND id NOT IN (SELECT distinct destinataire_id FROM clics WHERE";

                $conditions_ouv = $conditions_inactif;

                foreach($conditions_ouv as $k => $co)
                {
                    if($k != 0){
                        $ouvreursQuery .= " AND";
                        $cliqueursQuery .= " AND";
                    }
                    $ouvreursQuery .= " $co->condition_column $co->condition_operator '$co->condition_value'";
                    $cliqueursQuery .= " $co->condition_column $co->condition_operator '$co->condition_value'";
                    if(empty($conditions_theme)) {
                        continue;
                    }
                    $campagne_ids_query->where('created_at', $co->condition_operator, $co->condition_value);
                }

                if(!empty($campagne_ids_query)){
                    $campagne_ids = $campagne_ids_query->get();
//                $ouvreursQuery->whereIn('campagne_id', array_pluck($campagne_ids, 'id'));
                    if(count($campagne_ids) > 0) {
                        $ouvreursQuery .= " AND campagne_id IN (" . implode(',', array_pluck($campagne_ids, 'id')) . ")";
                        $cliqueursQuery .= " AND campagne_id IN (" . implode(',', array_pluck($campagne_ids, 'id')) . ")";
                    }
                } else if (!empty($selected_campagne_id)) {
                    $ouvreursQuery .= " AND campagne_id IN (" . implode(',', array_pluck($selected_campagne_id, 'condition_value')) . ")";
                    $cliqueursQuery .= " AND campagne_id IN (" . implode(',', array_pluck($selected_campagne_id, 'condition_value')) . ")";
                }
                $ouvreursQuery .= ') ';
                $cliqueursQuery .= ') ';
            }

            $sql = "SELECT COUNT(distinct mail) as max_tok, fai_id FROM destinataires WHERE $base_query and statut < 3$geoloc_query $age_query $civil_query $ouvreursQuery $cliqueursQuery GROUP BY fai_id";
            \Log::info("SQL -- $sql");
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            $response = $stmt->fetchAll();
//            $count = $response['max_tok'];
        }
        \Log::info('ajaxShowVolume - After if ouv/clic');

        //select count(*) from tokens
        // where departement like '%$departement'
        // and destinataire_id in (select destinataire_id from ouvertures ou clics where created_at > '2016-09-27 00:00:00')
        // ou array_intersect entre 2 tableaux recuperant les de
        //stinataire_id des tokens potentiels
        \Log::info('ajaxShowVolume - Fin traitement');
        \Log::info(json_encode($response));
        return $response;
    }
}
