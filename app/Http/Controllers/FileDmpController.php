<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs\BaseImportDico;

class FileDmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function upload(){
      return view('dmp.upload.uploadDico');
    }


    public function inject_file(){
      \Log::info("Call Inject File");
      $name = \Input::file('file')->getClientOriginalName();
      \Log::info("Name :" . $name);
      \Input::file('file')->move(storage_path() . '/listes/', $name);
      \Log::info("MOVE OK GO DISPATCH");
      $this->dispatch (new BaseImportDico(\Input::file('file')->getClientOriginalName()));
      \Log::info("DISPATCH DISPATCH OK");
      return "OK";
    }

    public function displayDico(){
      $dico = \DB::table('dmp_dicomail')->paginate(100);

      return view('dmp.data.showDico')
      ->with('dico',$dico);

    }


}
