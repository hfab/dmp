<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Editor;
use App\Models\Campagne;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AdminDmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getStatsEditors()
    {
        $editors = Editor::all();
        $dateIn = date('Y-m-d',strtotime('-30 days'));
        $dateOut = date('Y-m-d');
        $nbMatched = $nbCookie = $editorsName = [];
        $tabEditorMatched = $tabEditorCookie = [];

        foreach($editors as $editor){
          // sinon doublons, à opty
          $datePeriod = [];
            $tempMatched = $tempCookie = [];
            array_push($editorsName,$editor->name);
            for($index = 30;$index>0;$index--){
                $object = new \stdClass();
                $date = date('Y-m-d',strtotime("-$index days"));
                array_push($datePeriod,"$date");
                $match = DB::select("SELECT COUNT(id) AS nbMatched
                    FROM dmp_matched
                    WHERE updated_at like '$date %'
                    AND editor_id = $editor->id");
                array_push($tempMatched,$match[0]->nbMatched);
                $cookie = DB::select("SELECT COUNT(id) AS nbCookie
                    FROM dmp_cookies
                    WHERE updated_at like '$date %'
                    AND editor_id = $editor->id");
                array_push($tempCookie,$cookie[0]->nbCookie);
            }
            array_push($nbMatched,array($editor->name=>$tempMatched));
            array_push($nbCookie,array($editor->name=>$tempCookie));
        }

        return view('dmp.management.statsEditors')
            ->with("dateIn",$dateIn)
            ->with("dateOut",$dateOut)
            ->with("matched",$nbMatched)
            ->with("period",$datePeriod)
            ->with("editorsName",$editorsName)
            ->with("cookies",$nbCookie);
    }

    public function getStatsPartners()
    {
        $partners = DB::table('dmp_partnersites')->get();
        $dateIn = date('Y-m-d',strtotime('-30 days'));
        $dateOut = date('Y-m-d');
        $tabPartner = $tabCampaign = [];
        foreach($partners as $partner){
            $object = new \stdClass();
            $object->name = $partner->url;
            //Selection des campagnes venant d'un partenaire
            $campaignsPartner = Campagne::whereIn('id',function($query) use ($dateIn,$dateOut){
                $query->select('campagne_id')
                    ->from('dmp_history')
                    ->where('relaunch_at','<',$dateIn)
                    //->where('dmp_partnersite_id',$partner->id)
                    ->where('relaunch_at','>',$dateOut);
            })->get();
            //Nombre de campagnes lancées par partenaire
            $object->nbCampaigns = count($campaignsPartner);
            array_push($tabPartner,$object);
        }

        $campaigns = Campagne::whereIn('id',function($query) use ($dateIn,$dateOut){
            $query->select('campagne_id')
                ->from('dmp_history')
                ->where('relaunch_at','<',$dateOut)
                ->where('relaunch_at','>',$dateIn);
        })->get();

        //Nombre de mails relancés par campagne
        foreach($campaigns as $campaign){
            $statsCampaign = new \stdClass();
            $nbAddresses  = DB::select("SELECT COUNT(destinataire_id) AS nbDest
                FROM dmp_history
                WHERE campagne_id = $campaign->id
                AND relaunch_at > $dateIn
                AND relaunch_at < $dateOut");
            $statsCampaign->nom = $campaign->ref;
            $statsCampaign->number = $nbAddresses->nbDest;
            array_push($tabCampaign,$statsCampaign);
        }

        return view("dmp.management.statsPartner")
            ->with("partners",$tabPartner)
            ->with("campaigns",$tabCampaign)
            ->with("dateIn",$dateIn)
            ->with("dateOut",$dateOut);
    }

    public function getNdd()
    {
        $allNdd = DB::table('dmp_ndd')->get();
        return view('dmp.management.listNdd')
            ->with('listNdd',$allNdd);
    }

    public function createNdd()
    {
        $nddName = \Input::get('nddName');
        if(empty($nddName)){
            echo json_encode(['status'=>"Error","message"=>"Erreur:Le nom de domaine choisi est vide."]);
            exit;
        }
            DB::table('dmp_ndd')->insert([
                "editor_id"=>0,
                "ndd"=>$nddName,
                "status"=>"ready"
            ]);
        echo json_encode(['status'=>"Ok","message"=>"Nom de domaine créé"]);
        exit;
    }


    /**
     *TODO: Faire appel à cet fonction en ajax. Doit returner toutes les campagnes pour un partner(annonceur)
     *
     *
     */
    public function getPartnerCampagne()
    {
        $partnerId = \Input::get('partnerId');
        $campagnes = Campagne::where('dmp_partnersite_id',$partnerId)->get();
        return view('dmp.management.partnerCampagne')
            ->with('campagne',$campagnes);
    }
    /**
     *Affiche toutes les campagnes des annonceurs
     *
     *
     */
    public function getCampaignsPartners()
    {
        $partners = DB::table('dmp_partnersites')->get();
        $campagnes = Campagne::where('dmp_partnersite_id','!=',0)->paginate(15);
        return view('dmp.management.listCampaings')
            ->with('partners',$partners)
            ->with('campagnes',$campagnes);
    }


    public function getStatsPartnerSite(){

      $dateIn = date('Y-m-d',strtotime('-30 days'));
      $dateOut = date('Y-m-d');
      $partnerSite = DB::table('dmp_partnersites')
      ->select('id','url')
      ->get();
      $temphit = $nbHit = [];
      $partnerName = [];
      foreach ($partnerSite as $pS) {
        // $datePeriod = [];
        $dateP = [];
        array_push($partnerName,$pS->url);
        $hitndd = DB::table('dmp_callscript')
        ->where('domain','LIKE','%'.$pS->url.'%')
        ->get();
        for($index = 30;$index>0;$index--){
          $date = date('Y-m-d',strtotime("-$index days"));
          array_push($dateP,"$date");
          $hitndd = DB::table('dmp_callscript')
          ->where('domain','LIKE','%'.$pS->url.'%')
          ->where('created_at','LIKE',$date.'%')
          ->first();
           if(count($hitndd) === 0){

            array_push($temphit,0);
          } else {
            array_push($temphit,$hitndd->nbcall);
          }
        }
        array_push($nbHit,array($pS->url=>$temphit));
        $temphit = [];
      }

      return view('dmp.management.statsPartnerSite')
          ->with("dateIn",$dateIn)
          ->with("dateOut",$dateOut)
          ->with("period",$dateP)
          ->with("nbHit",$nbHit);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
