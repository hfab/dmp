<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\Editor;
use App\Models\User;

class EditorDmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $editors = Editor::all();
        $usersByEditor = [];
        foreach($editors as $editor){
            $countUser = User::where('editor_id',$editor->id)->count();
            $object = new \StdClass();
            $object->nom = $editor->name;
            $object->number = $countUser;
            $object->domains = $editor->domain;
            array_push($usersByEditor,$object);
        }
        return view('dmp.management.listEditor')
            ->with('editors',$usersByEditor);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dmp.management.createEditor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $editorName = \Input::get('editor');
        $editorDomains = \Input::get('editorDomains');
        \Log::info("[EditorDmpController@store] editorDomains : ".json_encode(Input::get('editorDomains')));
        $domains = "";
        $index = -1;
        foreach($editorDomains as $domain){
            $checkDomain = DB::table('dmp_ndd')->where('ndd',$domain)->first();
            if($index++){
                $index++;
                if($index == count($editorDomains)-1){
                      $domains = $domains.$domain;
                }else{
                      $domains = $domains."$domain | ";
                }
            }
        }

        if(!empty($editorName)){
            //\Log::info(\Input::all());
            $newEditorID = DB::table('editors')->insertGetId([
                "name"=>$editorName,
                "domain"=>$domains
            ]);
            foreach($editorDomains as $domain){
                DB::table('dmp_ndd')->where('ndd',$domain)->update(['editor_id'=>$newEditorID]);
            }
            echo json_encode(['status'=>'Ok']);
            exit;
        }
        echo json_encode(['status'=>'Failed','message'=>"Erreur: le nom de l\'editeur est vide"]);
        exit;
    }

    public function getStats($id)
    {
        //TODO: Ajouter une vérification sur l'utilisateur authentifié
        //(Super Admin ou membre de chez l'éditeur)
        $editor = Editor::find(User::find($id)->editor_id);
        \Log::info('[EditorDmpController@getStats] user:'.$id);
        \Log::info('[EditorDmpController@getStats] editor :'.$editor->id);
        $dateIn = date('Y-m-d',strtotime('-30 days'));
        $nbMatched = $nbCookie = $nbRelaunched = [];
        $dateOut = date('Y-m-d',strtotime('-1 days'));
        for($index =30;$index>0;$index--){
          $date = date('Y-m-d',strtotime("-$index days"));

          $object2 = new \stdClass();
          $match = DB::select("SELECT COUNT(id) AS nbMatched
              FROM dmp_matched
              WHERE updated_at LIKE '$date %'
              AND editor_id = $editor->id");

          $object2->date = $date;
          $object2->number = $match[0]->nbMatched;
          array_push($nbMatched,$object2);

          $object = new \stdClass();
          $cookie = DB::select("SELECT COUNT(id) AS nbCookie
              FROM dmp_cookies
              WHERE updated_at LIKE '$date %'
              AND editor_id = $editor->id");

          $object->date = $date;
          $object->number = $cookie[0]->nbCookie;
          array_push($nbCookie,$object);

          // à changer de place
          $ndd = \DB::table('dmp_ndd')
          ->where('editor_id',$editor->id)
          ->get();

          $object3 = new \stdClass();
          $object3->date = $date;


          $relanceday = DB::select("SELECT COUNT(id) AS nbRelaunch
              FROM dmp_relaunch
              WHERE relaunch_at LIKE '$date %'");
              // mettre par quand on aura une campagne pour chacun
              // AND editor_id = $editor->id");
          $object3->number = $relanceday[0]->nbRelaunch;
          array_push($nbRelaunched,$object3);


        }

        $sitepartenaires = \DB::table('dmp_partnersites')
        ->get();

        $hitbysitebyeditor = array();
        foreach ($sitepartenaires as $st) {
          $matchedhitsite = \DB::table('dmp_matched')
          ->where('editor_id',$editor->id)
          ->where('base_site_id',$st->id)
          ->count();
          $hitbysitebyeditor[] = array('id' => $st->id, 'name' => $st->url, 'count' => $matchedhitsite);
        }

        // ++ pages parcourues
        return view('dmp.editors.stats')
            ->with('matched',$nbMatched)
            ->with('dateIn',$dateIn)
            ->with('dateOut',$dateOut)
            ->with('cookie',$nbCookie)
            ->with('relaunch',$nbRelaunched)
            ->with('ndd',$ndd)
            ->with('editorName',$editor->name)
            ->with('hitbysitebyeditor',$hitbysitebyeditor);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function gest_ndd($id)
    {
        // liste des ndd

        $user = User::find(\Auth::User()->id);
        if($user->id == $id){

          // echo $user->editor_id;
          $ndd = \DB::table('dmp_ndd')->where('editor_id',$user->editor_id)->get();
          // var_dump($ndd);

          return view('dmp.editors.ndd')
          ->with('ndd',$ndd);

        }



    }
}
