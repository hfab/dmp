<?php

namespace App\Http\Controllers;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Classes\Routeurs\RouteurSmessage;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Http\Controllers\Controller;

use App\Models\Campagne;
use App\Models\CampagneRouteur;
use App\Models\Planning;
use App\Models\Routeur;
use App\Models\Sender;
use Illuminate\Routing\Route;

use Illuminate\Support\Facades\Artisan;

class PlanningController extends Controller
{

    public function show_used_senders($id)
    {
        $planning = Planning::find($id);

        if (empty($planning))
        {
            return redirect('/planning');
        }

        $campagnes_routeurs = \DB::table('campagnes_routeurs')
            ->select('sender_id', 'taskid', 'listid')
            ->where('planning_id', $planning->id)
            ->get();

        $quelrouteur = Routeur::find($planning->routeur_id);

        $namespace = "App\\Classes\\Routeurs\\";
//        $routeurclasse = 'Routeur';
        $routeur = null;

        if(!empty($quelrouteur)) {
            $routeurclasse = "{$namespace}Routeur$quelrouteur->nom";
            $routeur = new $routeurclasse();
        }

        $segments = array();

        if(!empty($routeur)) {
            foreach ($campagnes_routeurs as $cr)
            {
                $infosender = Sender::find($cr->sender_id);
                $segments[$infosender->nom] = "";
                if(method_exists($routeur, 'checkStatutImport')) {
                    $segments[$infosender->nom] = $routeur->checkStatutImport($infosender, $cr->taskid);
                }
            }
        }

        return view('planning.show_used_senders')
            ->withMenu('planning')
            ->with([
                'planning' => $planning,
                'segments' => $segments
        ]);
    }


    function edit_mindbaz($campagne_id)
    {
        $mindbaz = new RouteurMindbaz();
        $campagne = Campagne::find($campagne_id);
        $routeur = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->first();

        $selected_lists = array();
        $selected_excluded_lists = array();

        $plannings = \DB::table('plannings')
            ->where('campagne_id', $campagne_id)
            ->where('routeur_id', $routeur->id)
            ->get();

        foreach($plannings as $p)
        {
            $campagnes_routeurs = \DB::table('campagnes_routeurs')
                ->select('listid', 'sender_id')
                ->where('planning_id', $p->id)
                ->get();

            if(!$campagnes_routeurs){
                $selected_lists[$p->id] = 0;
                continue;
            }
            foreach($campagnes_routeurs as $cr)
            {
                $sender = Sender::find($cr->sender_id);
                if (!isset($selected_lists[$p->id])) {
                    $selected_lists[$p->id] = array();
                }
                $selected_lists[$p->id][] = $mindbaz->get_list($sender, $cr->listid);
            }

            $excluded_lists = \DB::table('excluded_lists')
                ->select('list_id', 'sender_id')
                ->where('planning_id', $p->id)
                ->get();

            foreach($excluded_lists as $el)
            {
                $sender = Sender::find($el->sender_id);
                if (!isset($selected_excluded_lists[$p->id])) {
                    $selected_excluded_lists[$p->id] = array();
                }
                $selected_excluded_lists[$p->id][] = $mindbaz->get_list($sender, $el->list_id);
            }
        }

        $mindbaz = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->first();

        $senders = \DB::table('senders')
            ->where('routeur_id', $mindbaz->id)
            ->get();

        return view('planning.edit_mindbaz')
            ->with('routeur', $mindbaz)
            ->with('campagne', $campagne)
            ->with('plannings', $plannings)
            ->with('senders',   $senders)
            ->with('selected_lists', $selected_lists)
            ->with('selected_excluded_lists', $selected_excluded_lists)
            ->with('today', date('Y-m-d'));
    }

    function schedule_mindbaz($campagne_id)
    {
        $dates_campagnes = $heures_campagnes = array();

        $campagne = Campagne::find($campagne_id);

        $already = \Input::get('already');

        $list_ids = \Input::get('list_ids');

        $dates_campagnes = \Input::get('dates_campagnes');

        $heures_campagnes = \Input::get('heures_campagnes');

        $routeur = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->first();

        $routeur_id = $routeur->id;

        $planning_existants = Planning::where('campagne_id',$campagne->id)
            ->where('routeur_id', $routeur_id)
            ->get();

        //Par défaut : branche principale pour Mindbaz
        $branche_principal = \DB::table('branches')
            ->where('name', 'Principale')
            ->first();

        $branche_id = $branche_principal->id;
        $volume_global = 0;

        $sender_ids = \Input::get('senders_ids');

        // partie protection en dur
        $types =  \Input::get('types');

        $excluded_lists = \Input::get('excluded_list_ids');

        $newplannifs = array();
        $mindbaz = new RouteurMindbaz();

        foreach($already as $k => $plan)
        {
            $sender = Sender::find($sender_ids[$k]);
            $planning = Planning::find($already[$k]);
            $ts = \Carbon\Carbon::now();

            if(!$planning && $already[$k]==0) {
                $planning = new Planning();
                $planning->campagne_id = $campagne_id;
                $planning->routeur_id = $routeur_id;
                $planning->tokens_at = $ts;
                $planning->is_valid = 0;
                $planning->nb_trials = 4; //pas de reel "segmentation" -> sinon passe dans la boucle CampagnePrepare
            }

            $planning->volume = $volume_global;
            $planning->date_campagne = $dates_campagnes[$k];
            $planning->time_campagne = $heures_campagnes[$k];
            $planning->type = $types[$k];
            $planning->branch_id = $branche_id;

            $planning->save();

            if( !isset($list_ids[$k]) )
            {
                $planning->delete();
                \Log::error("[PlanningController] : list_id empty list_ids[k] NOT ISSET P$planning->id/C$planning->campagne_id ".json_encode($list_ids));
                continue;
            }

            $existing_cid = \DB::table('campagnes_routeurs')
                ->select('cid_routeur')
                ->where('planning_id', $planning->id)
                ->first();

            if(!empty($existing_cid)){
                $cid_routeur = $existing_cid->cid_routeur;
            } else {
                // /!\ : ordre important car pour la creation de la campagne
                // on recupere les listes à exclure
                foreach($excluded_lists[$k] as $el)
                {
                    \DB::table('excluded_lists')
                        ->insert([
                            'planning_id' => $planning->id,
                            'list_id' => $el,
                            'sender_id' => $sender_ids[$k],
                            'created_at' => $ts,
                            'updated_at' => $ts
                        ]);
                }
                $cid_routeur = $mindbaz->create_campagne($campagne, $sender, $planning, $list_ids[$k]);
                foreach($list_ids[$k] as $onelist)
                {
                    \DB::table('campagnes_routeurs')
                        ->insert([
                            'campagne_id' => $campagne_id,
                            'sender_id' => $sender_ids[$k],
                            'cid_routeur' => $cid_routeur,
                            'listid' => $onelist,
                            'planning_id' => $planning->id,
                            'created_at' => $ts,
                            'updated_at' => $ts
                        ]);
                }
                $newplannifs++;
            }

            if($cid_routeur == false ){
                $planning->delete();
                \Log::error("[PlanningController] : create_campagne NO CID ROUTEUR CREATED P$planning->id/C$planning->campagne_id");
                continue;
            }
        }
        return redirect('campagne/'.$campagne->id.'/mindbaz_planning');
    }

    function calculatespamscore($id)
    {
        $scores_sa = array();
        $scores_vr = array();

        $planning = Planning::find($id);

        $routeur = Routeur::where('nom', 'Mindbaz')->first();
        $mindbaz = new RouteurMindbaz();

        $average_spamscore_vr = 0;
        $average_spamscore_sa = 0;

        if(!empty($planning) && $planning->routeur_id == $routeur->id){
            $campagnes_routeurs = \DB::table('campagnes_routeurs')
                ->where('planning_id', $planning->id)
                ->limit(1)
                ->get();

            if(empty($campagnes_routeurs)){
                \Log::error("PlanningController@calculatespamscore : ".json_encode($campagnes_routeurs));
                return ['spamassassin' => $average_spamscore_sa, 'vaderetro' => $average_spamscore_vr];
            }

            foreach($campagnes_routeurs as $cr) {
                $sender = Sender::find($cr->sender_id);
                $ss = $mindbaz->calculate_spamscore($sender, $cr->cid_routeur);
                $scores_sa[] = $ss[0];
                $scores_vr[] = $ss[1];
            }

//            $average_spamscore_sa = ceil( array_sum($scores_sa) / count($scores_sa) );
//            $average_spamscore_vr = ceil( array_sum($scores_vr) / count($scores_vr) );
        }
        return ['spamassassin' => $scores_sa, 'vaderetro' => $scores_vr];
    }

    function set_valid($id)
    {
        $planning = Planning::find($id);
        if(empty($planning)){
            return 'false';
        }

        $mindbaz = new RouteurMindbaz();
        $campagnes_routeurs = \DB::table('campagnes_routeurs')
            ->where('planning_id', $planning->id)
            ->limit(1)
            ->get();

        foreach($campagnes_routeurs as $cr)
        {
            $sender = Sender::find($cr->sender_id);
            $sent_or_not = $mindbaz->schedule_campaign($sender, $cr->cid_routeur, "$planning->date_campagne $planning->time_campagne");
            if(!$sent_or_not){
                return 'false';
            }
        }

        $now = date('Y-m-d H:i:s');
        $planning->is_valid = true;
        $planning->segments_at = $now;
        $planning->save();

        return 'true';
    }

    function send_bat_mb($planning_id)
    {
        $cr = \DB::table('campagnes_routeurs')
            ->select('cid_routeur')
            ->where('planning_id', $planning_id)
            ->first();

        if(!empty($cr)) {
            $mindbaz = new RouteurMindbaz();
            $sent = $mindbaz->send_bat_mindbaz($cr->cid_routeur);
            if($sent){
                return 'true';
            }
        }
        return 'false';
    }

    function delete()
    {
        $planning_id = \Input::get('planning_id');
        $planning = Planning::find($planning_id);

        if(empty($planning_id) or empty($planning)){
            \Log::error("PlanningController@delete : $planning_id empty");
            return 'false';
        }

        $campagnes_routeurs = CampagneRouteur::select('id', 'cid_routeur', 'sender_id')
            ->where('planning_id', $planning_id)
            ->get();

        $mindbaz = new RouteurMindbaz();

        foreach($campagnes_routeurs as $cr)
        {
            $sender = Sender::find($cr->sender_id);
            $deleted = $mindbaz->delete_campaign($sender, $cr->cid_routeur);
            if(!$deleted){
                \Log::error("PlanningController@delete : $planning_id not deleted");
                return 'false';
            }
            $cr->delete();
        }

        \DB::table('excluded_lists')
            ->where('planning_id', $planning_id)
            ->delete();

        $planning->delete();
        return 'true';
    }

    function list_planning_defaut(){

      $config_defaut = \DB::table('planning_defaut')->select('config_id')->distinct()->get();
      // $users = DB::table('users')->distinct()->get();
      // var_dump($config_defaut);

      return view('planning.listdefaut')->with('config_defaut',$config_defaut);

    }

    function set_defaut(){

      $lesfais = \DB::table('fais')->get();
      return view('planning.setdefaut')->with('lesfais',$lesfais);

    }

    function set_defaut_store(){

      // var_dump(\Input::all());
      $postall = \Input::all();
      var_dump($postall);
      $config = \DB::table('planning_defaut')->max('config_id');
      var_dump($config);
      // die();
      if(is_null($config)){

        $config_id = 1;
      } else {
        $config_id = $config + 1;

      }

    foreach ($postall as $fai_id_key => $pfai) {
      // var_dump($fai_id);
      // var_dump($pfai);
      // $config_id

      if(is_int($fai_id_key)){

        \DB::table('planning_defaut')->insert(
          ['config_id' => $config_id, 'fai_id' => $fai_id_key, 'volume' => $pfai,'volume_total' => null]
        );

      }

      if($fai_id_key == 'volume_total'){

        \DB::table('planning_defaut')->insert(
          ['config_id' => $config_id, 'fai_id' => null, 'volume' => null,'volume_total' => $pfai]
        );

      }

    }

    die();
    $today = date("Y-m-d H:i:s");
    $dataset[] = [
    'colonne1' => $dataforeach1,
    'colonne2' => $dataforeach2,
    'created_at' => $today,
    'updated_at' => $today
    ];



    }

    function send($id){
        $planning = Planning::find($id);

        if(!empty($planning) && empty($planning->sent_at)){
            $command = "campagne:send_".strtolower($planning->routeur->nom);
//            var_dump($command);
            Artisan::call( $command, ['planning_id' => $planning->id]);
        }

        redirect("/planning/".$planning->id."/senders");

    }
}
