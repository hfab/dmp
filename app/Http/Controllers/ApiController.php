<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Destinataire;

class ApiController extends Controller
{


  public function ve_api(){

    $prenom = null;
    $nom = null;
    $ville = '';
    $civilite = null;
    $adresse = null;
    $datenaissance = null;
    $departement = 0;

    $input = \Input::all();

    if(isset($input['webservice_key'])){

      $key_check = \DB::table('api')
      ->select('id','api_login_info','api_key','api_call','base_id')
      ->where('api_key',$input['webservice_key'])
      ->first();

      if($key_check == null){
        return $this->reponse_api(null, 0,'Api Key Error');
      }

    } else {
      return $this->reponse_api(null, 0,'Api Key Not found');
    }

    foreach ($input as $k => $i) {

      // phone number ??

      // reference_civility // 1 homme // 2 Madame // 3 Mademoiselle

      // civilité, nom, prénom, email, code postal, date de naissance ,ville


      if($k == 'first_name'){
        $prenom = $i;
      }

      if($k == 'last_name'){
        $nom = $i;
      }

      if($k == 'city'){
        $ville = $i;
      }

      if($k == 'reference_civility'){
        $civilite = $i;
      }

      if($k == 'address'){
        $adresse = $i;
      }

      if($k == 'birth_date'){
        $datenaissance = $i;
      }

      if($k == 'zipcode'){
        $departement = $i;
      }

      if($k == 'civilite'){
        $civilite = $i;
      }
    }

    if(isset($input['email'])){
      $bdd_mail = $this->search_email($input['email']);
    }

    if($bdd_mail === false){

      $fai = Destinataire::getFai($input['email']);
      $insert = \DB::table('destinataires')
        ->insert([
            'mail' => $input['email'],
            'hash' => md5($input['email']),
            'base_id' => $key_check->base_id,
            'fai_id' => $fai->id,
            'origine' => $key_check->api_login_info,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),

            'prenom' => $prenom,
            'nom' => $nom,
            'ville' => $ville,
            'civilite' => $civilite,
            'adresse' => $adresse,
            'datenaissance' => $datenaissance,
            'departement' => $departement

          ]);

      if($insert == true){
        $desti = \DB::table('destinataires')
              ->select('id')
              ->where('mail',$input['email'])
              ->first();

        $call = $key_check->api_call + 1;
        \DB::table('api')
        ->where('id', $key_check->id)
        ->update(['api_call' => $call]);

        return $this->reponse_api($desti->id,1,'Success');
      }
    } else {
      return $this->reponse_api(null, 0,'Duplicate');
    }
  }


  function search_email($email){
    // renvoie true si l'adresse est trouvée
    // false si pas trouvé
    $email = trim($email);
    if(filter_var($email, FILTER_VALIDATE_EMAIL)){
      $resultat = \DB::table('destinataires')
      ->select('mail')
      // ->where('base_id',2)
      ->where('mail',$email)
      ->first();

    } else {
      return 'error';
    }
    if($resultat !== NULL){
      return true;
    } else {
      return false;
    }
  }

  function reponse_api($id_lead,$status_code,$status_label){

    if($id_lead == null){
      $array_response = array(
        'status_code' => $status_code,
        'status_label' => $status_label
      );
    } else {
      $array_response = array(
        'id_lead' => $id_lead,
        'status_code' => $status_code,
        'status_label' => $status_label
      );
    }
    return response()->json(
      $array_response
    );
  }



}
