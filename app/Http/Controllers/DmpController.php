<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\Planning;
use App\Models\Fai;
use App\Models\Theme;
use App\Models\Destinataire;
use App\Models\Base;
use App\Models\Campagne;
use Auth;
use Session;

class DmpController extends Controller
{

    function url_exists($url)
    {
        if (!$fp = curl_init($url)) return false;
        return true;
    }

    //On génére le cookie sur le domaine de la DMP à l'ouverture du mail
    public function redirectGenCookie($ndd_id,$editor_id,$hash)
    {
        \Log::info('GET PIXEL BY '. $hash .' - MD5');
        $toBeHashed = "DMP-SC2CAP-VISITOR";
        $cookieName = md5($toBeHashed);
        $lifetime = 3600 * 24 * 365;
        $data = array("hsh"=>$hash,
            "edt"=>$editor_id);
        //On encapsule le hash et l'editor_id pour plus de facilité de lecture
        // $token = serialize(json_encode($data));
        $token = json_encode($data);

        \Log::info(var_dump($token));

        \Log::info("[DmpController@redirectGetCookie] Cookie name -> $cookieName");
        $ndd = \DB::table('dmp_ndd')->where('id',$ndd_id)->first();
        \Log::info(json_encode($ndd));

        if(!empty($ndd))
        {
            $newhit = $ndd->hit + 1;
           \DB::table('dmp_ndd')->where('id',$ndd_id)->update(['hit'=>$newhit]);
        }

        if(isset($_COOKIE[$cookieName])){

            $emptyOrNot = \DB::table('dmp_cookies')->where('hash',$hash)->where('editor_id',$editor_id)->first();

            if(is_null($emptyOrNot)){
                \DB::table('dmp_cookies')->where('hash',$hash)->insert([
                    'hash'=>$hash,
                    'editor_id'=>$editor_id,
                    'token' => $token,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                    //'base_id'=>$base_id,
                ]);
            }

            \DB::table('dmp_cookies')->where('hash',$hash)->where('editor_id',$editor_id)->update([
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
        }
        if(!isset($_COOKIE[$cookieName])){
            \DB::table('dmp_cookies')->where('hash',$hash)->insert([
                'hash'=>$hash,
                'editor_id'=>$editor_id,
                'token' => $token,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
                //'base_id'=>$base_id,
            ]);
        }
        setcookie($cookieName,$token,time()+$lifetime,'/');
        exit;
    }

    //Vouée à disparaitre
    public function redirect()
    {
        $ip = $_SERVER['SERVER_ADDR'];
        $now = \Carbon\Carbon::now();
        //clctv
        //        $link = Input::get('lid');
        $link = Input::get('to');
        $visitor_id = Input::get('vsd');
        //        $partner_id = Input::get('prd');
        // base <-> partenaire si on fait un TOR DMP ?
        $base_id = Input::get('bsd');
        //        $campaign_id = Input::get('cid');
        $planning_id = Input::get('pld');
        $decoded_link = urldecode($link);

        \Log::info("DmpController@redirect == Request from $ip / VISITOR $visitor_id / BASE $base_id / PLANNING $planning_id / LINK $link ($decoded_link)");

        $cookie_name = "";
        $token = "";
        $details = "";

        if (!empty($visitor_id)) {
            $to_be_hashed = "TOR-VISITOR";
            $cookie_name = md5($to_be_hashed);
            $max_expire_time_duration = 3600 * 24 * 365;

            $token = Hash::make($to_be_hashed);

            \Log::info('DmpController@redirect ==  COOKIE NAME BEFORE HASH - ' . $to_be_hashed);
            \Log::info('DmpController@redirect ==  COOKIE NAME - ' . $cookie_name);

            //            setcookie($cookie_name, $token, time() + $max_expire_time_duration, '/');
            setrawcookie($cookie_name, $token, time() + $max_expire_time_duration, '/');

            if (isset($_COOKIE[$cookie_name])) {
                \Log::info('DmpController@redirect == Cookie already exists UPDATE' . $token);
                \DB::table('dmp_cookies')->where('token', $token)->update(
                    [
                        'to' => $link,
                        //                        'partner_id' => $partner_id,
                        'base_id' => $base_id, // base_id ou partner_id
                        'planning_id' => $planning_id,

                        'destinataire_id' => $visitor_id,
                        'updated_at' => "$now",
                    ]
                );
                //                exit;
            } else {
                \Log::info('DmpController@redirect == Cookie does not exists INSERT ' . $token);
                \DB::table('dmp_cookies')->insert(
                    [
                        'token' => $token,
                        'to' => $link,
                        //                        'partner_id' => $partner_id,
                        'base_id' => $base_id, // base_id ou partner_id
                        'planning_id' => $planning_id,
                        'destinataire_id' => $visitor_id,

                        'created_at' => "$now",
                        'updated_at' => "$now",
                    ]
                );
            }
            //            exit;
        }
        // dmpid=cpdcsn
        // co=125307
        // timestamp=1491822761
        // redirect=https%3A%2F%2Fasset.cpdcsn.com%2Fcollect.img.php%3Fdmpid%3Dcpdcsn%26m%3D{$Member.email|md5}%26p%3Dcpd15%26lp%3Dcpd15%26timestamp%3D1491822761

        if (!empty($decoded_link) and $this->url_exists($decoded_link) !== false) {
            \Log::info("DmpController@redirect == redirect to $decoded_link");
            //            exit;
            return Redirect::to($decoded_link);
        }

        \Log::info("DmpController@redirect == EXIT");
        exit;
    }

    /**
     *Récupère m
     *
     *
     */
    public function getVisitorData()
    {
        // $data = json_decode(unserialize(urldecode(Input::get('token'))));

        // window.location.href
        // \Input::get('ndd');
        // \Log::info("Before Access Control");
         header('Access-Control-Allow-Origin: *');
        // \Log::info("DATA -- " . json_decode(urldecode(Input::get('token')));
        $data = json_decode(\Input::get('token'));
        \Log::info('[DmpController@getVisitorData] Input:'.json_encode(\Input::all()));

        \Log::info("DATA_TOKEN -- ".json_encode(\Input::get('token')));
        // \Log::info("DATA -- ".json_encode($data));

        $ajaxCallFrom = $data->origin;
        // parse url casse la var
        // $host = parse_url($ajaxCallFrom,PHP_URL_HOST);
        // \Log::info("HOST -- ".var_dump($host));
        $partnerSite = \DB::table('dmp_partnersites')->where('url','LIKE',"%$data->origin%")->first();
        $destinataireInfo = \DB::table('dmp_cookies')->where('hash',$data->hsh)
            ->where('editor_id',$data->edt)
            ->first();

        // test du link en param
        if(\Input::get('by_link') !== null){
          \Log::info("DATA_LINK -- ".json_encode(\Input::get('by_link')));

          \DB::table('dmp_tracking')->insert([
              'hash'=> $data->hsh,
              'partnersite'=> $partnerSite->id,
              'editor_id' => $data->edt,
              'link'=> \Input::get('by_link'),
              'updated_at'=>date('Y-m-d H:i:s'),
              'created_at'=>date('Y-m-d H:i:s')
          ]);
        }

        \Log::info("DATA -- hsh: ".$data->hsh.' edt:'.$data->edt." origin_url: ".$data->origin);
        \Log::info("partnerSite -- " . json_encode($partnerSite));
        \Log::info("destinataireInfo -- " . json_encode($destinataireInfo));

        if(empty($partnerSite) ||empty($destinataireInfo)){
            \Log::info('[DmpController@getVisitorData] partnersite or cookie\'s value empty');
            exit;
        }

        $theme = Theme::find($partnerSite->theme_id);
        $originUrl = substr($partnerSite->url,0,-1);
        //On autorise le site partenaire à faire des requêtes AJAX CROSS DOMAIN
        $destinataireExistence = \DB::table('dmp_matched')
        ->where('destinataire_hash',$data->hsh)
        ->where('base_site_id', $partnerSite->id)
        ->first();

        if(empty($destinataireExistence)){
            $newMatchedId = \DB::table('dmp_matched')->insertGetId([
                'destinataire_hash'=>$data->hsh,
                'editor_id'=>$data->edt,
                'theme_id'=>$theme->id,
                'base_site_id'=>$partnerSite->id,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
            $newDestinataire = \DB::table('dmp_matched')->where('id',$newMatchedId)->first();
            if(\Input::get('has_ordered') == 1){
                $value = $newDestinataire->nb_has_ordered++;
                \DB::table('dmp_matched')->where('id',$newMatchedId)->update(['nb_has_ordered'=>$value]);
            }
            /*
            if(\Input::get('has_ordered') == 0){
                $value = $newDestinataire->nb_has_ordered++;
                \DB::table('dmp_matched')->where('id',$newMatchedId)->update(['nb_has_ordered'=>$value]);
            }
            */
        }

        $hasOrdered = false;
        if(\Input::get('has_ordered') == 1){
            $hasOrdered = true;
        }

        $count_records = \DB::table('dmp_visitors')
        ->where('destinataire_hash',$data->hsh)
        ->where('base_site_id',$partnerSite->id)
        ->where('theme_id', $theme->id)
        ->where('editor_id',$data->edt)
        ->where('visited_at','LIKE',date('Y-m-d').'%')
        ->count();

        \Log::info("counter -- " . $count_records);

        if($count_records === 0){

        \DB::table('dmp_visitors')->insert([
            'destinataire_hash'=>$data->hsh,
            'visited_at'=>date('Y-m-d H:i:s'),
            'base_site_id'=>$partnerSite->id,
            'theme_id'=>$theme->id,
            'editor_id'=>$data->edt,
            'has_ordered'=>$hasOrdered
        ]);
        }

        //TODO:A améliorer
        if(!empty($destinataireExistence)){
            if(\Input::get('has_ordered') == 1){
                $destinataireExistence->nb_has_ordered++;
                \DB::table('dmp_matched')
                    ->where('destinataire_hash',$data->hsh)
                    ->where('editor_id',$data->edt)
                    ->where('base_site_id',$partnerSite->id)
                    ->update(['nb_has_ordered' => $destinataireExistence->nb_has_ordered++]);
                // $destinataireExistence->save();
            }
            if(\Input::get('has_ordered') == 0){
                $destinataireExistence->nb_has_not_ordered++;
                \DB::table('dmp_matched')
                    ->where('destinataire_hash',$data->hsh)
                    ->where('editor_id',$data->edt)
                    ->where('base_site_id',$partnerSite->id)
                    ->update(['nb_has_not_ordered' => $destinataireExistence->nb_has_not_ordered++]);
                // $destinataireExistence->save();
            }
        }
        echo json_encode(['status'=>'ok']);
        exit;
    }

    public function getVisitor()
    {
        $ip = $_SERVER['SERVER_ADDR'];

        $has_ordered = Input::get('has_ordered');
        $token = Input::get('token');
        $ajax_call_from = $_SERVER["HTTP_REFERER"];
        $host_from = parse_url($ajax_call_from, PHP_URL_HOST);

        \Log::info("@getVisitor == Request from $ip / $host_from ($ajax_call_from) (token : $token ; has_ordered : $has_ordered)");

        $now = \Carbon\Carbon::now();
        $which_site = \DB::table('dmp_partnersites')
            ->where('url', 'LIKE', "%$host_from%")
            ->first();

        $get_tokens_infos = \DB::table('dmp_cookies')
            ->where('token', $token)
            ->first();

        if(empty($which_site) || empty($get_tokens_infos)){
            \Log::warning('@getVisitor == which_site '.json_encode($which_site).' - get_tokens_infos '.json_encode($get_tokens_infos));
            \Log::warning("@getVisitor == variable(s) empty");
            exit;
        }

        $theme = DB::select("SELECT id from themes,plannings,campagnes where plannings.id = $get_tokens_infos->planning_id and plannings.campagne_id = campagnes.id and campagnes.theme_id = themes.id;");
        $origin_url = substr($which_site->url, 0, -1);
        //Autoriser le site partenaire à faire une requête AJAX / CROSS DOMAIN
        header("Access-Control-Allow-Origin: $origin_url");
        \Log::info("@getVisitor == Access-Control-Allow-Origin: $origin_url");

        $destiExistence = \DB::table('dmp_matched')
            ->where('destinataire_id',$get_tokens_infos->destinataire_id)
            ->first();
        if(empty($destiExistence)){
            \DB::table('dmp_matched')->insert([
                //                'partner_site_id' => $get_tokens_infos->base_id,
                'base_site_id' => $which_site->id,
                'destinataire_id' => $get_tokens_infos->destinataire_id,
                'has_ordered' => $has_ordered,
                'created_at' => $now,
                'updated_at' => $now,
                'planning_id'=>$get_tokens_infos->planning_id,
                'theme_id'=>$theme->id,
            ]);
        }

        if(!empty($destiExistence)){
            $destiExistence->nb_has_ordered++;
            $destiExistence->save();
        }
        \Log::info("@getVisitor == status => ok");
        echo json_encode(['status'=>'ok']);
        exit;
    }

    public function exemple()
    {
        return view('dmp.testSendCookies');
    }

    public function getCookie()
    {
        return view('dmp.getCookie');
    }

    public function getPlannings()
    {
        $dmpBranch = DB::table('branches')->where('name','DMP')->orWhere('name','dmp')->first();
        if(empty($dmpBranch)){
            $message = "Aucune branche dmp trouvé.";
            return view('dmp.plannings')
                ->with('message',$message);
        }

        $bases = Base::where('is_active',1)->get();
        $tables = DB::select('SHOW TABLES');
        $tablesNames = [];
        $database = "Tables_in_".env('DB_DATABASE');
        foreach($tables as $table){
            array_push($tablesNames,$table->$database);
        }
        //Objectif: Renvoyer un tableau de type clé/valeur (thème=>nb_plannings_lancés)
        //TODO:Récupérer tous les thèmes
        //TODO:Faire le compte du nombre de plannings effectué depuis le lancement de la dmp en fonction du thème
        //TODO:Est-ce important de savoir quelle a été sollicité le plus ?
        //TODO:Renvoyer à la vue

        // PARTIE THEME RELANCE
        // var nb_jour a faire
        $t = DB::table('themes')->get();

        foreach ($t as $theme) {
            $r = DB::select("select count(id) as counter from destinataires
                where id in (select destinataire_id from dmp_history
                where campagne_id in (select id from campagnes
                where theme_id = $theme->id))");
            $a_themeVolume[] = array('name' => $theme->nom, 'count' => $r[0]->counter);
        }

        $datein = date('Y-m-d 00:00:00',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y'))));
        $plannings = Planning::where("branch_id",$dmpBranch->id)
            ->whereNotNull('sent_at')
            ->with('campagne')
            ->orderBy('sent_at','desc')
            ->where('sent_at','>',$datein)
            ->paginate(15);

        //Nb de campagnes relancées par base
        $allCampaigns = [];
        foreach($bases as $base){
            $nbCampagne = Campagne::where('base_id',$base->id)
                ->whereIn('id',function($query){
                    $query->select("campagne_id")
                        ->from('dmp_history')
                        ->where('relaunch_at','>',date('Y-m-d 00:00:00',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y')))))
                        ->where('relaunch_at','<',date('Y-m-d H:i:s'));
                })->count();
            $object = new \StdClass();
            $object->nom = $base->nom;
            $object->number = $nbCampagne;
            array_push($allCampaigns,$object);
        }
        //Nb de mails relancés par bases sur la période.
        $allAddresses = [];
        foreach($bases as $base){
            $nbDest = Destinataire::where('base_id',$base->id)
                ->whereIn('id',function($query){
                    $query->select("destinataire_id")
                        ->from('dmp_history')
                        ->where('relaunch_at','>',date('Y-m-d 00:00:00',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y')))))
                        ->where('relaunch_at','<',date('Y-m-d H:i:s'));
                })->count();
            $object = new \StdClass();
            $object->nom = $base->nom;
            $object->number = $nbDest;
            array_push($allAddresses,$object);
        }

        if(empty($plannings)){
            $message = "Pas de plannings retrouvés.";
            return view('dmp.plannings')
                ->with('message',$message);
        }
        //Nb @ relancés en fonction de la campagne
        $mailsCampaign = [];
        $campaigns = Campagne::whereIn('id',function($query){
            $query->select("campagne_id")
                ->from("dmp_history")
                ->where('relaunch_at','>',date('Y-m-d 00:00:00',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y')))))
                ->where('relaunch_at','<',date('Y-m-d H:i:s'));
        })->get();

        \Log::info("Stats DMP:".count($campaigns));
        foreach($campaigns as $campaign){
            \Log::info("Stats DMP: $campaign->id");
            $nbAddresses = DB::select("SELECT count(destinataire_id) as nbDest FROM dmp_history
                WHERE campagne_id = $campaign->id
                AND relaunch_at > '".date('Y-m-d 00:00:00',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y'))))."'
                AND relaunch_at < '".date('Y-m-d H:i:s')."';");
            $object = new \StdClass();
            $object->nom = $campaign->ref;
            $object->number = $nbAddresses[0]->nbDest;
            array_push($mailsCampaign,$object);
        }
        $mailsMatched= [];
        //Nb @ matched par bases
        foreach($bases as $base){
            $nbDestBase = Destinataire::where('base_id',$base->id)->whereIn('id',function($query){
                $query->select('destinataire_hash')
                    ->from('dmp_matched');
            })->count();
            $object = new \StdClass();
            $object->nom = $base->nom;
            $object->number = $nbDestBase;
            array_push($mailsMatched,$object);

        }
        // PARTIE PRESSION MAIL
        $a_date = array('in' => date('Y-m-d',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y')))), 'out' => date('Y-m-d'));

        $users = DB::table('dmp_history')
            ->select(DB::raw('count(*) as user_count, destinataire_id'))
            ->groupBy('destinataire_id')
            ->get();

        $a_countPression = array();
        foreach ($users as $u) {
            $a_countPression[$u->user_count][] = $u->user_count;
        }
        foreach ($a_countPression as $key => $value) {
            $countPression[$key][] = count($a_countPression[$key]);
        }

        if(empty($plannings)){
            $message = "Pas de plannings retrouvés.";
            return view('dmp.management.plannings')
                ->with('message',$message);
        }
        $a_date = array('in' => date('d-m-Y',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y')))), 'out' => date('d-m-Y'));
        return view('dmp.management.plannings')
            ->with('plannings',$plannings)
            ->with('a_date', $a_date)
            ->with('a_themeVolume', $a_themeVolume)
            ->with('countPression', $countPression)
            ->with('addresses',$allAddresses)
            ->with('campaigns',$allCampaigns)
            ->with('nbMailBases',$mailsMatched)
            ->with('mailsCampaign',$mailsCampaign);
    }



    public function homeDmp(){
        //TODO:Récupérer les 5 dernières relances ainsi que le nom des campagnes
        //Le nombre d'adresses relancées aujourd'hui
        $dmpBranch = DB::table('branches')->where('name','DMP')->orWhere('name','dmp')->first();
        if(empty($dmpBranch)){
            $message = "Aucune branche dmp trouvé.";
            return view('dmp.management.homeDmp')
                ->with('message',$message);
        }
        $plannings =	Planning::where("branch_id",$dmpBranch->id)
            ->whereNotNull('sent_at')
            ->orderBy('sent_at')
            ->take(5)->get();

        $nbUsersRelauched = DB::table('dmp_history')
            ->where('relaunch_at','like',date('Y-m-d'))
            ->count();
        $nbPlanningsDmpToday = Planning::where('branch_id',$dmpBranch->id)
            ->where('sent_at','like',date('Y-m-d'))
            ->count();
        return view('dmp.management.homeDmp')
            ->with('plannings',$plannings)
            ->with('nbPlanningsRelaunched',$nbPlanningsDmpToday)
            ->with('nbUsers',$nbUsersRelauched);
    }

    public function getVolumeRelaunched()
    {
        //TODO:Affichage des adresses relancées par jour depuis lancement dmp pour historique
        //avec la campagne
    }

    public function management()
    {
        $fais = DB::table('fais')->get();
        return view('dmp.management.management')
            ->with('fais',$fais);
        //Pour gérer les
        //->with('')
    }

    public function getAddresses()
    {
        $themes = Theme::where('etat_actif',1)->get();
        if(empty($themes)){
            return view('dmp.management.addresses');
        }
        $tables = DB::select('SHOW TABLES');
        $tablesNames = [];
        $database = "Tables_in_".env('DB_DATABASE');
        foreach($tables as $table){
            array_push($tablesNames,$table->$database);
        }
        $bases = [];
        foreach($themes as $theme){
            if($this->checkTable("dmp_".$theme->getThemeName(),$tablesNames)){
                $newObject = new \stdClass();
                $dmpThemeName = "dmp_".$theme->getThemeName();
                $nbDestinataires = DB::table($dmpThemeName)->count();
                $newObject->name = $dmpThemeName;
                $newObject->nbDestinataires = $nbDestinataires;
                $newObject->theme_id = $theme->id;
                array_push($bases,$newObject);
            }
        }
        return view('dmp.management.addresses')
            ->with('dmpBases',$bases);
    }

    function checkTable($str,$arr){
        for($index = 0 ;$index<count($arr);$index++){
            if($str == $arr[$index]){
                return true;
            }
        }
        return false;
    }
    //Function appeler via de l'ajax
    public function changeFaiPression($id)
    {
        $fai = Fai::find($id);
        $quota = Input::get("newQuota");
        \Log::info(Input::all());
        if(empty($quota)){
            echo json_encode(["status"=>"error","message"=>"Champs vide"]);
            exit;
        }
        $fai->dmp_pression = $quota;
        $fai->save();

        echo json_encode(["status"=>"Ok","message"=>"Modification enregistrée"]);
        exit;
    }

    public function exportTheme($theme_id)
    {

          header("Content-type: application/force-download");
          $themeName = Theme::find($theme_id)->getThemeName();
          $date = date('Ymd');
          header("Content-Disposition: attachment; filename=base-dmp_$themeName-$date.txt");
          $addresses = DB::select("SELECT destinataire_hash FROM dmp_$themeName;");
          $content = "";
          foreach($addresses as $addr){
            $content .= "$addr->destinataire_hash\n";
          }
          print $content;
          exit;
    }

    public function getPixel()
    {
        $editor_id = Auth::User()->editor_id;
        $domains = DB::table('dmp_ndd')->where('editor_id',$editor_id)
                                       ->get();
        $pixels = "";
        foreach($domains as $domain)
        {
            $dmp_link = 'https://'.getenv('CLIENT_URL').'/rdrct/'."$domain->id/".$editor_id.'/[emailmd5]';
            $pixel = '<img src="'.$dmp_link.'" style="width:1px;height:1px;display:none">';
            \Log::info("[DmpController@getPixel] $pixel");
            $pixels .="$pixel\n";
        }
        \Log::info("[DmpController@getPixel] $pixels");
        return view('dmp.editors.getPixel')
            ->with('pixel',$pixels);
    }


    public function getCall($nddsource){

      header('Access-Control-Allow-Origin: *');
      // \Log::info("..!![HIT]!!.. $nddsource");

      // TODO / tout mettre dans la même rqt ajax avant
      // lecture de la table
      $nb_callscript = DB::table('dmp_callscript')
      ->where('created_at','LIKE',date('Y-m-d').'%')
      ->where('domain',$nddsource)
      ->first();

      if($nb_callscript == null){
        \DB::table('dmp_callscript')->insert([
            'nbcall'=>0,
            'created_at'=>date('Y-m-d H:i:s'),
            'domain'=>$nddsource,
        ]);

      }else{

        \DB::table('dmp_callscript')
        ->where('created_at','LIKE',date('Y-m-d').'%')
        ->where('domain',$nddsource)
        ->update([
          'nbcall'=> $nb_callscript->nbcall + 1,
          'updated_at'=>date('Y-m-d H:i:s'),
          'domain'=>$nddsource,
        ]);

      }

    }

    public function changeDelay()
    {
        //TODO:Change Cron to the command campagne:dmp_prepare
    }
    //    public function getCookie()
    //    {
    //        header("Access-Control-Allow-Origin: http://www.voyaneo.com");
    //        $to_be_hashed = "TOR-VISITOR";
    //        $cookie_name = md5($to_be_hashed);
    //        $cookie_value = "";
    //        if(isset($_COOKIE[$cookie_name]))
    //        {
    //            $cookie_value = $_COOKIE[$cookie_name];
    //        }
    ////        header("Location: http://voyaneo.com?&tkv=$cookie_name");
    //        \Log::info("COOKIE VALUE -- ".$cookie_value);
    ////        echo $cookie_value;
    //        return $cookie_value;
    //    }

}
//http://asset.cpdcsn.com/collect_visite.img.php?dmpid=cpdcsn&co=125307&timestamp=1491822761&redirect=https%3A%2F%2Fasset.cpdcsn.com%2Fcollect.img.php%3Fdmpid%3Dcpdcsn%26m%3D{$Member.email|md5}%26p%3Dcpd15%26lp%3Dcpd15%26timestamp%3D1491822761
//
//http://asset.cpdcsn.com/collect.img.php?dmpid=cpdcsn&m=AAAAAAAA&p=slsdata01&lp
