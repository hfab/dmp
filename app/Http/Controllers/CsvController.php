<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
// fab1 pour le crawldb
// use DB;

use App\Http\Controllers\DB;
use File;

use App\Models\Base;
use App\Models\Campagne;
use App\Models\Theme;

class CsvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $campagnes = Campagne::orderBy('created_at', 'desc')->limit(25)->get();

        // return view("campagne.index")
                        // ->withMenu('campagnes')
                        // ->withCampagnes($campagnes);
		
		// $bases = Base::orderBy('created_at', 'desc')->limit(25)->get();
		
		return view("csv.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	
	public function choixextrairebase() {
		
		$bases = Base::orderBy('nom', 'asc')->limit(25)->get();
		
		return view("csv.base")->withBases($bases);
	}
	
	public function extrairebase(Request $request) {
		
		$baseid = $request->input('baseid');
				
		$bases = Base::orderBy('nom', 'asc')->limit(25)->get();
		$basenom = \DB::table('bases')->select('nom')->where('id', $baseid)->first();
		
		$export = \DB::table('destinataires')->select('mail')->where('base_id', $baseid)->get();
				
		fopen(storage_path() . '/csv/' . $basenom->nom . '_' . date('Y-m-d') . '.csv', 'w+');
				
		$fp = fopen(storage_path() . '/csv/' . $basenom->nom . '_' . date('Y-m-d') . '.csv', 'w');
		
		foreach ($export as $email) {
			 // fputcsv($fp, $email, ',');
			  // fputcsv($fp, $email->mail);
			 
			 fputs($fp, $email->mail . ',');			 
		}

		fclose($fp);
		
		return response()->download(storage_path() . '/csv/' . $basenom->nom . '_' . date('Y-m-d') . '.csv');		
		return view("csv.base")->withBases($bases);
	}	
	
	public function choixextrairecampagne() {
		
		$bases = Base::orderBy('nom', 'asc')->limit(25)->get();
		$campagnes = Campagne::orderBy('nom', 'asc')->limit(25)->get();
		
		return view("csv.campagne")->withBases($bases)->withCampagnes($campagnes);
	}
	
	public function extrairecampagne(Request $request) {
		
		$bases = Base::orderBy('nom', 'asc')->limit(25)->get();
		$campagnes = Campagne::orderBy('nom', 'asc')->limit(25)->get();
		
		// $baseid = $request->input('baseid');
		$campagneid = $request->input('campagneid');
		$campagnenom = \DB::table('campagnes')->select('nom')->where('id', $campagneid)->first();
		$baseid = \DB::table('campagnes')->select('base_id')->where('id', $campagneid)->first();
		// var_dump($campagneid);
		// var_dump($baseid->base_id);
		
		$export = \DB::table('destinataires')->select('mail')->where('base_id', $baseid->base_id)->get();
		
		fopen(storage_path() . '/csv/' . $campagnenom->nom . '_' . date('Y-m-d') . '.csv', 'w+');
				
		$fp = fopen(storage_path() . '/csv/' . $campagnenom->nom . '_' . date('Y-m-d') . '.csv', 'w+');
		
		foreach ($export as $email) {
			 // fputcsv($fp, $email, ',');
			  // fputcsv($fp, $email->mail);
			 
			 fputs($fp, $email->mail . ',');			 
		}

		fclose($fp);
		
		
		return response()->download(storage_path() . '/csv/' . $campagnenom->nom . '_' . date('Y-m-d') . '.csv');
		return view("csv.campagne")->withBases($bases)->withCampagnes($campagnes);
	}
	
	public function choixextrairetheme() {
		
		return view("csv.theme");
	}
	
	public function extrairetheme() {
		
		echo '';
		// return view("csv.campagne");
	}
	
	public function choixextraireperiode() {
		
		echo 'extraireperiode';
	}
	
	public function extraireperiode() {
		
		echo 'extraireperiode';
	}
	
	
}
