<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\Destinataire;
use App\Models\Campagne;
use App\Models\Partner;
use App\Models\Theme;
use App\Models\User;

class PartnerDmpController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
        $partners = Partner::paginate(15);
        $themes = Theme::all();
        return view('dmp.management.partners')
                    ->with('themes',$themes)
                    ->with('partners',$partners);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	//Est appelé en ajax
	public function store(Request $request)
	{
		$partnerUrl = Input::get('partnerSite');
		$partnerSubNetworkUrl = Input::get('subPartner');
        $partnerName = Input::get('partnerName');
        $partnerTheme = Input::get('partnerTheme');
		\Log::info(Input::all());
		if(empty($partnerUrl) || empty($partnerName)){
			echo json_encode([
				'status'=>'error',
				'message'=>'Erreur:L\'url du partenaire doit être rempli'
			]);
		}
		$partnerId = DB::table('dmp_partnersites')->insertGetId([
			"url"=>$partnerUrl,
			"subDomain"=>$partnerSubNetworkUrl,
			"created_at"=>date('Y-m-d H:i:s'),
            "updated_at"=>date('Y-m-d H:i:s'),
            "name"=>$partnerName,
            "theme_id"=>$partnerTheme
		]);
		\Log::info($partnerId);
		echo json_encode(["status"=>"Ok",'message'=>"Partenaire enregistré","id"=>$partnerId,"url"=>$partnerUrl,"subDomain"=>$partnerSubNetworkUrl,"name"=>$partnerName]);
		exit;
    }

    public function getStats()
    {
       //TODO:Nb d'@ relancées en fonction de la campagne
        $mailsByCampaign = [];
        $dateIn = date('Y-m-d',strtotime('-30 days'));
        $dateOut = date('Y-m-d');
        $partner = DB::table('dmp_partnersites')->where('id',User::find(\Auth::User()->id)->dmp_partnersite_id)->first();
        if(empty($partner))
        {
            \Log::error('[PartnerDmpController@getStats] No partner found.');
            return \redirect('/');
        }
        $campaigns = Campagne::where('dmp_partnersite_id',$partner->id)->whereIn('id',function($query){
                $query->select("campagne_id")
                        ->from("dmp_history")
                        ->where("relaunch_at",">",date('Y-m-d 00:00:00',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y')))))
                        ->where('relaunch_at','<',date('Y-m-d H:i:s'));
       })->get();

        foreach($campaigns as $campaign){
            $nbAddresses = DB::select("SELECT COUNT(destinataire_id) AS nbDest
                                                                FROM dmp_history
                                                                WHERE campagne_id = $campaign->id
                                                                 AND relaunch_at >'".date('Y-m-d 00:00:00',strtotime("-30 day",mktime(0,0,0,date('m'),date('d'),date('Y')))).
                                                                "' AND relaunch_at <'".date('Y-m-d H:i:s')."';");
            $object = new \StdClass();
            $object->nom = $campaign->ref;
            $object->number = $nbAddresses[0]->nbDest;
            array_push($mailsByCampaign,$object);
       }

        return view('dmp.partners.stats')
                ->with('dateIn',$dateIn)
                ->with('dateOut',$dateOut)
                ->with('mails',$mailsByCampaign);
    }

    /**
     *
     *
     *
     */
    public function getScriptsForWebSite()
    {
        $importScript = '<script src="https://'.getenv('CLIENT_URL',false).'/dmp/js/partner/partner.js"></script>'."\n".'gCD()';
        $scriptConfPage = "ho('8a704337bac01056fdb5ffdef3d6c57e', 1);";

        return view('dmp.partners.getScript')
                    ->with('importScript',$importScript)
                    ->with('scriptConfPage',$scriptConfPage);
    }
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$domain = Input::get('newDomain');
		$subDomain = Input::get('newSubDomain');
        $nameDomain = Input::get('newNameDomain');
		if(!empty($domain)){
			DB::table('dmp_partnersites')->where('id',$id)->update([
				"url"=>$domain,
                "subDomain"=>$subDomain,
                "name"=>$nameDomain
			]);
			json_encode(["status"=>"ok",
				"message"=>"Partenaire mis à jour."]);
			exit;
		}

		json_encode([
			"status"=>"error",
			"message"=>"Erreur impossible de mettre à jour ce partenaire."]);
		exit;
	}

    /**
     * Get all campaings from one partner
     *
     *
     */
    public function getPartnerCampaign($id)
    {
        $campagnes = Campagne::where('dmp_partnersite_id',$id)->get();
        return view('dmp.partners.list')
                    ->with('campagnes',$campagnes);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy()
	{
		if(!empty(\Input::get('id'))){
			DB::table('dmp_partnersites')->where('id',\Input::get('id'))->delete();
			json_encode(["status"=>"ok","message"=>"Partenaire supprimé"]);
			exit;
		}
		json_encode(["status"=>"error","message"=>"Erreur impossible de supprimer ce partenaire."]);
		exit;
	}
}
