<?php namespace App\Http\Controllers;

use App\Classes\Browser;
use App\Models\Campagne;
use App\Models\Ouverture;
use App\Models\Destinataire;

class OuvertureController extends Controller
{
    public function store($tor_id, $campagne_id)
    {
//        \App\Helpers\Profiler::start('ouverture');

//        for($i=0;$i<500;$i++) {

        if (!is_numeric($campagne_id)) {
            die('cmp');
        }

        $result = \BrowserDetect::toArray();
        $result = array_except($result, 'isBot');

        $result['destinataire_id'] = $tor_id;
        $result['campagne_id'] = $campagne_id;
        $result['ip'] = $_SERVER['REMOTE_ADDR'];
        $result['created_at'] = date('Y-m-d H:i:s');
        $result['updated_at'] = date('Y-m-d H:i:s');

        $result['date_active'] = date('Y-m-d');

        \DB::table('ouvertures')->insert($result);

//        }
//        \App\Helpers\Profiler::report('ouverture');
    }
}
