<?php

namespace App\Http\Middleware;

use Closure;

class EditorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      // prévoir le cas pas log

      if (\Auth::User()->user_group->name != 'Editeur') {
          return redirect('/');
      }
      return $next($request);
    }
}
