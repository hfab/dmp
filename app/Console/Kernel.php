<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /*
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        //Back up only necessary files commands
        'App\Console\Commands\ServerDump',
        'App\Console\Commands\ServerImport',
        'App\Console\Commands\ServerCheck',

        // DEV
        'App\Console\Commands\TestCommand', // not used in code

        //DMP
        // 'App\Console\Commands\RegroupMail',
        // 'App\Console\Commands\SimulateDmpMatched',
        // 'App\Console\Commands\CleanDmpMatched',
        // 'App\Console\Commands\DmpSendCampaignMaildrop',
        // 'App\Console\Commands\DmpSendCampaignMindbaz',
        // 'App\Console\Commands\DmpPrepareCampaign',
        'App\Console\Commands\CreateDmpServer',


        'App\Console\Commands\ExtractEmailOuvreurs',
        'App\Console\Commands\ExtractBddJson',


        'App\Console\Commands\relaunchCampagneMatchDay',
        'App\Console\Commands\relaunchGo',

        'App\Console\Commands\DmpUnsubscribeMindbaz',
        'App\Console\Commands\DmpUnsubscribeDico',

        'App\Console\Commands\BaseImportMindbazChoixSender',
        'App\Console\Commands\ImportDicoMailFile',

        'App\Console\Commands\DmpRelaunchGlobal',

        'App\Console\Commands\CampagneUnsubscribeMaildrop',
        'App\Console\Commands\BaseImportDesinscrits'



    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // \Log::info('Test - Kernel->schedule');

        // PARTIE DMP


        // FIN PARTIE DMP

        if (\Schema::hasTable('settings')) {
            $clean_relaunch = \DB::table('settings')
                ->where('parameter', 'clean_relaunch')
                ->first();
        }

        if (env('APP_ENV') == 'prod') {

            $heure = date('H');

            //On génère les tokens (important que ça se fasse après la phase de desinscription)
            /*
            if($heure > 4) {

                if (empty($clean_relaunch) or $clean_relaunch->value == 1) {
                    $schedule->command('tokens:manage')
                        ->hourly()
                        ->withoutOverlapping();
                }

                $schedule->command('planning:check')
                    ->everyThirtyMinutes()
                    ->withoutOverlapping();

                // Pour préparer les campagnes à envoyer (tokens+segments)
                $schedule->command('campagne:prepare')
                    ->cron('* * * * *')
                    ->withoutOverlapping();

                //Pour lancer les envois des campagnes segmentées
                $schedule->command('campagne:launch_send')
                    ->cron('* * * * *')
                    ->withoutOverlapping();
            }

            */

            //Vérifie si il y a un problème de serveur (disque, mysql, nginx)
            $schedule->command('server:check')
                ->hourly()
                ->withoutOverlapping();

            $schedule->command('relaunch:global')
                ->hourly()
                ->withoutOverlapping();

        }
    }
}
