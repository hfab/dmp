<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

class TokensReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'On supprime tous les tokens qui ont plus de 1 semaine.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ajd = date('Ymd');

        $today = date('Y-m-d');
        $oneweek = date('Y-m-d',strtotime ( '-8 days' , strtotime ( $today )));

        $yesterday = date('Ymd',strtotime ( '-1 day' , strtotime ( $today )));

        print_r("\nTokensReset -- start");

        //OLD QUERY
//        \DB::query("DELETE FROM tokens where date_active < '$twoweeks'");
        // On copie la structure de la table tokens
        $var = \DB::statement("SET sql_mode = ''");

        $var = \DB::statement("CREATE TABLE tokens_new_$ajd LIKE tokens");
        \Log::info('[TokensReset] : 1 Creation nouvelle table tokens_new '.json_encode($var));
        print_r("\nCreation nouvelle table");
        print_r($var);

        //On ré-insère uniquement les tokens à garder dans la nouvelle table
        $var = \DB::statement("INSERT INTO tokens_new_$ajd SELECT * FROM tokens WHERE date_active > '$oneweek' and campagne_id IS NOT NULL");
        \Log::info('[TokensReset] : 2 Insertion nouvelle table tokens_new '.json_encode($var));
        print_r("\nInsertion nouvelle table");
        print_r($var);

        // On renomme l'ancienne
        $var = \DB::statement("ALTER TABLE tokens RENAME tokens_$ajd");
        \Log::info("[TokensReset] : 3 Renommage ancienne table tokens en tokens_$ajd ".json_encode($var));
        print_r("\nRenommage ancienne table");
        print_r($var);

        //On renomme le nouveau
        $var = \DB::statement("ALTER TABLE tokens_new_$ajd RENAME tokens");
        \Log::info("[TokensReset] : 4 Renommage nouvelle table tokens_new en tokens ".json_encode($var));
        print_r("\nRenommage nouvelle table");
        print_r($var);

        $var = \DB::statement("DROP TABLE IF EXISTS  tokens_$yesterday");
        \Log::info("[TokensReset] : 5 Suppression de la table tokens backup de la veille tokens_$yesterday ".json_encode($var));
        print_r("\nSuppression de la table tokens backup de la veille");
        print_r($var);

        $var = \DB::statement("DROP TABLE IF EXISTS  tokens_new_$ajd");
        \Log::info("[TokensReset] : 6 Suppression de la table tokens_new_$ajd ".json_encode($var));
        print_r("\nSuppression de la table tokens tokens_new_$ajd");
        print_r($var);

        $var = \DB::statement("DROP TABLE IF EXISTS  tokens_new_$yesterday");
        \Log::info("[TokensReset] : 7 Suppression de la table tokens_new_$yesterday ".json_encode($var));
        print_r("\nSuppression de la table tokens tokens_new_$yesterday");
        print_r($var);

        print_r("\nTokensReset -- end");
    }
}
