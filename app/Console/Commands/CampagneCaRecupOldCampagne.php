<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CampagneCaRecupOldCampagne extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ca:old';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les old campagne routés ce mois';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle()
      {
        $today = date("Y-m-d H:i:s");
        $moisCompta = date("m");
        $moisdernier = date("m")-1;

        $debutMoisbefore = date("Y-m-d H:i:s", mktime(0,0,0,$moisdernier,1,date("Y")));
        $finMoisbefore = date("Y-m-d H:i:s", mktime(23,59,59,$moisdernier, 31, date("Y")));

        var_dump('Date début : ' .  $debutMoisbefore);
        var_dump('Date fin : ' .  $finMoisbefore);

        $debutMois = date("Y-m-d H:i:s", mktime(0,0,0,date("m"), 1, date("Y")));
        $finMois = date("Y-m-d H:i:s", mktime(23,59,59,date("m"),31,date("Y")));

        var_dump('Search NOW - Date début : ' .  $debutMois);
        var_dump('Search NOW - Date fin : ' .  $finMois);

        $lesbases = \DB::table('bases')->get();
        $lesplatesformes = \DB::table('plateformes_affi')->get();

        foreach ($lesbases as $vb) {
             foreach ($lesplatesformes as $vp) {
                  //je recupère les campagnes du mois dernier
                  $getcampagnesMoisbefore= \DB::table('campagnes')
                  ->where('base_id', $vb->id)
                  ->where('plateforme_id', $vp->id)
                  ->whereBetween('created_at', [$debutMoisbefore,$finMoisbefore])
                  ->get();
                  //var_dump($getcampagnesMoisbefore);die();

                  foreach ($getcampagnesMoisbefore as $vc) {

                      echo 'Campagne : id['.$vc->id.'] nom['.$vc->nom.']'."\n";

                       //je cherche un plannings du mois en cours pour une campagne du mois dernier
                       $getplannings=\DB::table('plannings')
                       ->where('campagne_id', $vc->id)
                       // ptet changer ce crit
                       // ->whereBetween('created_at', [$debutMois,$finMois])
                       ->whereBetween('date_campagne', [$debutMois,$finMois])
                       ->get();
                       //si null boucle suivante
                       if($getplannings == NULL){
                            // echo 'NULL';
                            // continue;
                       } else {
                            //sinon check si nom, base_id et plateforme_id existe ds concat_ca
                            $checkconcat = \DB::table('campagnes_ca_concat')
                            ->where('nom', $vc->nom)
                            ->where('base_id', $vb->id)
                            ->where('plateforme_id',$vp->id)
                            ->first();
                            //si n'existe pas insert
                            if($checkconcat == NULL){

                              echo 'AJOUT Campagne : id['.$vc->id.'] nom['.$vc->nom.']'."\n";
                                 \DB::table('campagnes_ca_concat')->insert(
                                      ['nom'=> $vc->nom,
                                      'base_id' => $vb->id,
                                      'ca_brut' => 0,
                                      'ca_net'=> 0,
                                      'aaf'=> 0,
                                      'envoi_facture'=>0,
                                      'state'=>1,
                                      'mois_compta'=>$moisCompta,
                                      'commentaire'=> " ",
                                      'ca_volume_total'=>0,
                                      'cout_routage'=>0,
                                      'created_at' => $today,
                                      'updated_at' => $today,
                                      'plateforme_id' => $vp->id
                                 ]);

                                 // récup la ligne fraichement crée
                                 $recup = \DB::table('campagnes_ca_concat')
                                 ->where('base_id', $vb->id)
                                 ->where('plateforme_id',$vp->id)
                                 ->where('nom',$vc->nom)
                                 ->where('mois_compta',$moisCompta)
                                 ->get();

                                 foreach ($recup as $vr) {

                                      $checkrelation=\DB::table('campagne_ca_relation')
                                      ->where('id_campagnes_ca', $vr->id)
                                      ->where('base_id_ca', $vr->id)
                                      ->where('id_campagne', $vc->id)
                                      ->first();

                                      if($checkrelation == null){
                                           // reste à gerer le non rinsert
                                           \DB::table('campagne_ca_relation')->insert([
                                                'id_campagnes_ca'=> $vr->id,
                                                'base_id_ca'=> $vr->base_id,
                                                'id_campagne'=>$vc->id,
                                                'created_at' => $today,
                                                'updated_at' => $today
                                           ]);
                                      }
                                      else {
                                           echo 'existe deja en relation';
                                      }

                                 }
                            } else {
                                 // foreach ($checkconcat as $vcheck) {
                                      // var_dump($checkconcat);
                                      $checkrelation=\DB::table('campagne_ca_relation')
                                      ->where('id_campagnes_ca', $checkconcat->id)
                                      ->where('base_id_ca', $vb->id)
                                      ->where('id_campagne', $vc->id)
                                      ->first();

                                      if($checkrelation == null){
                                           // reste à gerer le non rinsert
                                           \DB::table('campagne_ca_relation')->insert([
                                                'id_campagnes_ca'=> $checkconcat->id,
                                                'base_id_ca'=> $vb->id,
                                                'id_campagne'=>$vc->id,
                                                'created_at' => $today,
                                                'updated_at' => $today
                                           ]);
                                      }
                                      else {
                                           echo 'existe deja en relation';
                                      }
                                 // }
                                 // id ligne concat
                                 // la relation id campagne mois précédent
                            }
                       }
                  }
             }
        }
   }
}
