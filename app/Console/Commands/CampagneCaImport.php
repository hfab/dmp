<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CampagneCaImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ca:init_db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rempli la table CA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // gérer mois en cours


        $today = date("Y-m-d H:i:s");
        $date_limit = date("Y-m-d H:i:s",strtotime('-15 day'));

        // $date_j = date("d",strtotime('-1 day'));
        // $date_m = date("m");


        $ca_insert = \DB::table('campagnes')
        ->where('created_at','>', $date_limit)
        // compta pas fermée / fermée à gérer

        ->get();



        foreach ($ca_insert as $campagne) {
          $moisdelacampagne = date_parse($campagne->created_at);

          var_dump($campagne->nom);
          var_dump($moisdelacampagne['month']);

          $ca_check = \DB::table('campagnes_ca')
          ->where('campagne_id', $campagne->id)
          ->where('mois_compta', $moisdelacampagne['month'])
          ->first();

          if(is_null($ca_check)){

            echo 'on peut insert';
            \DB::table('campagnes_ca')->insert(
                ['campagne_id' => $campagne->id,
                'ca_brut' => 0,
                'ca_net'=> 0,
                'aaf' => 0,
                'envoi_facture' => 0,
                'state' => 1,
                'created_at' => $today,
                'updated_at' => $today,
                'mois_compta' => $moisdelacampagne['month']]
            );
          }

        }




    }
}
