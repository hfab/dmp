<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Destinataire;
use App\Models\Sender;
use App\Models\Planning;
use App\Models\Campagne;

class DmpSendCampaignMindbaz extends Command {
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMindbaz();
    }

    protected $signature = 'campagne:dmp_send_mindbaz {planning_id}';
    protected $description = 'Send a DMP campaign with Mindbaz as router';

    /**
     * Execute the command.
     *
     * @return void
     */



    public function handle()
    {
        $planning = Planning::find($this->argument('planning_id'));
        $campaign = Campagne::find($planning->campagne_id);
        $errorMessage = "[Command:DmpSendCampaingMindbaz] Error : No planning found (planning_id:".$this->argument('planning_id').").";
        if(empty($planning)){
            \Log::info($errorMessage);
            exit();			
        }
        $errorMessage = "[Command:DmpSendCampaingMindbaz] Error : No campaign found (planning_id:$planning->id,campagne_id:$planning->campagne_id).";
        if(empty($campaign)){
            \Log::info($errorMessage);
            exit();
        }
        //On récupère le sender
        $senderId = \DB::table('planning_senders')->where('planning_id',$planning->id)->first();
        $errorMessage = "[Command:DmpSendCampaingMindbaz] Error : No sender_id found";
        if(!empty($senderId)){
            $sender = Sender::find($senderId->sender_id);
        }
        if(empty($senderId)){
            \Log::info($errorMessage);
            exit();
        }
        //On récupère le csv pour charger Mindbaz		
        $anonyme = "";
        if($planning->type == 'anonyme'){
            $anonyme = "_a";
        }
        $mindbazPath = storage_path().'mindbaz/dmp/s'.$sender->id.'_dmp_c'.$campaign->id.'_p'.$planning->id.$anonyme.'.csv';

        $destinataires = [];
        $errorMessage = "[Command:DmpSendCampaignMindbaz] Error: No sender found(planning_id:$planning->id).";
        if(!empty($sender)){
            $errorMessage = "[Command:DmpSendCampaignMindbaz] Error: No campaign found(planning_id:$planning->id,campagne_id :$planning->campagne_id).";
            if(!empty($campaign)){
                $errorMessage = "[Command:DmpSendCampaignMindbaz] Error: No listid found in database (planning_id: $planning->id).";
                $list = DB::table('dmp_planning_listid')
                    ->where('planning_id',$planning->id)->first();
                $listId = null;
                if(empty($list)){
                    \Log::info($errorMessage);
                    exit();
                }
                if(!empty($list)){
                    $message = "[Command:DmpSendCampaignMindbaz]  Planning $planning->id finished.";
                    $listId = $list->listid;
                    $cid = $this->routeur->create_campagne_light($campaign,$sender,$planning,[$listId]);
                    $this->routeur->calculate_spamscore($sender,$cid);
                    $send = false;
                    $send = $this->routeur->schedule_campaign($sender,$cid,date('Y-m-d H:i:s'));
                    //On date l'envoi de la campagne
                    $planning->sent_at = date("Y-m-d H:i:s");
                    $volume_selected = DB::table('dmp_history')
                        ->where('planning_id',$planning->id)
                        ->where('relaunch_at',date("Y-m-d H:i:s"))->count();
                    //Dans le cas où il n'y a pas de tokens disponible et l'envoi est tout de même lancé
                    if($planning->volume_selected == 0){
                        $planning->volume_selected = $volume_selected;
                    }
                    $planning->save();
                    //On retire au sender une campagne et le volume qui a été choisi
                    $sender->quota_left -= $volume_selected;
                    $sender->nb_campagnes_left--;
                    $sender->save();
                    //Office d'historique
                    DB::table('campagnes_routeurs')->insert([
                        "sender_id"=>$sender->id,
                        "listid"=>$listId,
                        "planning_id"=>$planning->id,
                        "campagne_id"=>$campaign->id,
                        "created_at"=>date('Y-m-d  H:i:s'),
                        "updated_at"=>date('Y-m-d H:i:s')
                    ]);
                    \Log::info($message);
                }
            }
            if(empty($campaign)){
                \Log::info($errorMessage);
            }
        }
        if(empty($sender)){
            \Log::info($errorMessage);
        }
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
