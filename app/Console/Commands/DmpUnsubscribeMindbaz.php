<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Routeur;
use App\Models\Sender;

class DmpUnsubscribeMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dmp:unsubscribe_mindbaz {sender_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unsubscribe DMP dicomail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $mindbaz = new RouteurMindbaz();
       $routeur = Routeur::where('nom','Mindbaz')->first();
       $senders_mindbaz = Sender::where('id', $this->argument('sender_id'))->first();
       $today = date("Ymd");
       echo "Lancement du script \n";

       $chaineUnsu = "";
       $chaineNpai = "";

       /*
       Si trop long, se baser sur les dmp_relaunch

       */

       $destinataires = \DB::table('dmp_dicomail')
       ->select('mail')
       ->where('statut', '<', 3)
       ->get();


       $destinataires = array_pluck($destinataires, 'mail');
       $counter = 0;
       // die();
       $mailschunk = array_chunk($destinataires, 1000);
       var_dump($mailschunk);
       foreach ($mailschunk as $arraychunked) {

         $chaineUnsu = '';
         $chaineNpai = '';

         $r = $mindbaz->get_subscriberid_all_npai($senders_mindbaz,$arraychunked);
         foreach ($r as $arrayresult) {

           foreach ($arrayresult as $ar) {
             $counter = $counter + 1;
            echo "C_" . $counter . "\n";
             if($ar->fld[4]->value != NULL){
               $chaineUnsu .= $ar->fld[1]->value . "\n";
             }
             if($ar->fld[7]->value == 7){
               $chaineNpai .= $ar->fld[1]->value . "\n";
             }

           }
          }

          $file = storage_path() . '/desinscrits/unsubscribe_mb_api_' . $today . '.csv';
          $fp = fopen($file,"a+");
          fwrite($fp, $chaineUnsu);
          fclose($fp);
          unset($chaineUnsu);

          $file = storage_path() . '/desinscrits/hardbounce_mb_api_' . $today . '.csv';
          $fp = fopen($file,"a+");
          fwrite($fp, $chaineNpai);
          fclose($fp);
          unset($chaineNpai);
    }

      \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command");
      \Artisan::call('dmp:import_unsubscribe', ['file' => "unsubscribe_mb_api_$today.csv"]);
      \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command OK");

      /*
      \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command");
      \Artisan::call('base:import_hbounceone', ['file' => "hardbounce_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
      \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command OK");
      */

}
}
