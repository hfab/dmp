<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Models\Routeur;

class CampagneUnsubscribePhoenix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:unsubscribe_phoenix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recupere les unsubscribe de Phoenix';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $routeur = new RouteurPhoenix();
        $today = date('Ymd');
        $one_week_ago = date('Y-m-d H:i:s', strtotime('-2 weeks'));
        $chaine = '';
        $phoenix = Routeur::where('nom','Phoenix')->first();
        $sdph = \DB::table('senders')->where('routeur_id', $phoenix->id)->get();
        $tabsender = array();
        foreach ($sdph as $v) {
          $tabsender[] = $v->id;
        }

        // deux semaines
        $information = \DB::table('campagnes_routeurs')->whereIn('sender_id', $tabsender)->where('created_at','>', $one_week_ago)->get();

        $file = storage_path() . '/desinscrits/unsubscribe_ph_' . $today . '.csv';
        $fp = fopen($file,"w+");

        foreach ($information as $lignecampagnerouteur) {
          $lacampagne = \DB::table('campagnes')->where('id', $lignecampagnerouteur->campagne_id)->first();

          $lesender = \DB::table('senders')->where('id', $lignecampagnerouteur->sender_id)->first();
          $unsubscribe = $routeur->campaign_getevent($lesender->password,'unsubscribe',$lignecampagnerouteur->cid_routeur);

          $chaine ="";
          if($unsubscribe != null){
            foreach ($unsubscribe as $api) {
              echo $api->email . "\n";
              $chaine .= $api->email . ";;$lignecampagnerouteur->campagne_id;$lignecampagnerouteur->planning_id\n";
              //  \DB::statement("UPDATE destinataires SET statut ='90' WHERE mail ='". $api->email ."'");
            }
          } else {
            echo "unsubscribe est vide \n";

          }
            fwrite($fp,$chaine);
        }

        fclose($fp);

        // $this->dispatch(new BaseImportDesinscrits('unsubscribe_ph_' . $today . '.csv'));
        \Log::info("Debut [import_desinscrits] fichier unsubscribe PH" . "[unsubscribe_ph_" . $today . ".csv]");
        \Artisan::call('base:import_desinscrits', ['file' => "unsubscribe_ph_" . $today . ".csv"]);
        \Log::info("Fin [import_desinscrits] du fichier unsubscribe PH" . "[unsubscribe_ph_" . $today . ".csv]");

        // \Log::info("Debut [insert_desinscrits] unsubscribe PH" . "[unsubscribe_ph_" . $today . ".csv]");
        // \Artisan::call('base:import_desinscrits', ['file' => "unsubscribe_ph_" . $today . ".csv"]);
        // \Log::info("Fin [insert_desinscrits] du fichier unsubscribe PH" . "[unsubscribe_ph_" . $today . ".csv]");
    }
}
