<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateDmpServer extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected $signature = 'dmp:create_server {ndd} {server_id}';
    protected $description = 'Set a dmp server with the right configuration.';
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $apiKey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjIzNTg5MjgxYTJmY2E1NGNkYmMyOGY2NDMxZDZkMjdiMGIwNjM2MWJmODFjMzcyNDI4MzlhM2JkZTE5Yjk5MjU4MDVlZWQzMmVlMjAxYzBhIn0.eyJhdWQiOiIxIiwianRpIjoiMjM1ODkyODFhMmZjYTU0Y2RiYzI4ZjY0MzFkNmQyN2IwYjA2MzYxYmY4MWMzNzI0MjgzOWEzYmRlMTliOTkyNTgwNWVlZDMyZWUyMDFjMGEiLCJpYXQiOjE1MTM2MDM3OTksIm5iZiI6MTUxMzYwMzc5OSwiZXhwIjoxNTQ1MTM5Nzk5LCJzdWIiOiIxMDE2MSIsInNjb3BlcyI6W119.Waq_Rl8y2nDDoptHpZBhneq2S9KCmPB4EmzbtZNC3s1lbfy8G6RGkE0eIDsUyKjme50ygQZR4DNVImaW2HrV7rUDz1bVXCw1UiNsLIeY0Nve_bLekO82CGLBcyn_JT5bLJfaocYikbtkYFoHDInEpKOgR2S89A7MqAhoDo5EqxwIwusAhMwupiP6kHe4rOI0xouOK1wCn-x9IiEcIAzczCDu28MJ2XXSIlo6vf4I6wXv-DlBZZ33ZvaiqoII_FCd3ZQRm0p9gkNm4FP2nFc9VoXpKEog1ToBe_NISva-fQkh4hhSf7YaxLM5j50OVmXUeB-e4fPagtwT7if3M0i6QAkYzFfw7b98qszdZhD-8xsOtXVY5VC0mjhCSBgJZGZCgRUl05xXFMjG8Jsmz31IqGPGS5Egu7jcUOyF7OIxvMTRleIkhJdPl2gvbeU7gl2WPB8ORAeJYLkRZiKYGhDst_10eT65igwglg_b73PuHynlsQw8gomCvc3w3D8jPlpzHRvdkZMRFUFUzme2o_6-3gfsMHGp6pPtpMtfi4xQREeXstKS-SxzlzmdnuWBX9BF71DoFnMd5VXEfA9ythm4jKwJkMmbhQBe89BG5mpuWOq89UGgpffKxfQc2qTdYiEFoOFYfSXH4drkGzSGplgbXaAVtlTbgKCjCkEzNNa7Y_0";
        $nddName = $this->argument('ndd');
        $serverId = $this->argument('server_id');
        $domain = "https://forge.laravel.com";
        $dbName = 'dmp';
        $header = array("Accept: application/json","Content-Type: application/json","Authorization: Bearer $apiKey");
        
        //TODO:Etapes
        //1-Créer un site sur le serveur
        //EN POST /api/v1/servers/{serverId}/sites
        $dataCreateSite = json_encode([
            "domain"=>"$nddName",
            "project_type"=>"php",
            "directory"=>"/home/forge/"]);
        $url = $domain."/api/v1/servers/$serverId/sites";
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_HTTPHEADER=>$header,
            CURLOPT_POST=>1,
            CURLOPT_POSTFIELDS=>$dataCreateSite
        );
        $resultSite = json_decode($this->executeCurlRequest($url,$options));
        print_r($resultSite);
        $siteId = $resultSite->site->id;
        echo "Site id:$siteId\n";
        //2-Cloner le repo sur le serveur
        //En POST /api/v1/servers/{serverId}/sites/{siteId}/git
        $url = $domain."/api/v1/servers/$serverId/sites/$siteId/git";
        $dataCloneGit = json_encode([
            "provider"=>"bitbucket",
            "repository"=>"hfab/dmp",
            "branch"=>"master"]);
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_HTTPHEADER=>$header,
            CURLOPT_POST=>1,
            CURLOPT_POSTFIELDS=>$dataCloneGit
        );
        $resultCloneGit = $this->executeCurlRequest($url,$options);
        sleep(60);
        //3-On met à jour le script de déploiment
        //En PUT /api/v1/servers/{serverId}/sites/{siteId}/deployment/script
        $url = $domain."/api/v1/servers/$serverId/sites/$siteId/deployment/script";
        $env = file_get_contents(public_path()."/doc/env.model");
        $env = str_replace("@title",$nddName,$env);
        $dataExploded = explode(".",$nddName);
        $value = "";
        switch(count($dataExploded)){
        case 2:
            $value = substr($dataExploded[0],0,4);
            break;
        case 3:
            $value = substr($dataExploded[1],0,4);
            break;
        }
        $env = str_replace("@servPseudoUpper",strtoupper($value),$env);
        $env = str_replace("@servPseudo",$value,$env);
        $dataDeploymentScript = json_encode(["content"=>'cd /home/forge/'."$nddName\n".'composer install'."\n".'git pull origin master'."\n".'rm .env && echo "'.$env.'" >> .env'."\n".'echo "" |sudo -S service php7.0-fpm reload']);
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_HTTPHEADER=>$header,
            CURLOPT_CUSTOMREQUEST=>"PUT",
            CURLOPT_POSTFIELDS=>$dataDeploymentScript
        );
        $this->executeCurlRequest($url,$options);
        //4-On lance le script nouvellement modifié
        //EN POST /api/v1/servers/{serverId}/sites/{siteId}/deployment/deploy
        $url = $domain."/api/v1/servers/$serverId/sites/$siteId/deployment/deploy";
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER=>$header,
            CURLOPT_POST=>1
        );
        $this->executeCurlRequest($url,$options);
        sleep(60);
        //5-Obtenir le certificat LetsEncrypt
        //EN POST /api/v1/servers/{serverId}/sites/{siteId}/certificates/letsencrypt
        $url = $domain."/api/v1/servers/$serverId/sites/$siteId/certificates/letsencrypt";
        $dataGetCert = json_encode(["domains"=>[$nddName]]);
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_HTTPHEADER=>$header,
            CURLOPT_POST=>1,
            CURLOPT_POSTFIELDS=>$dataGetCert
        );
        $resultGetCert = json_decode($this->executeCurlRequest($url,$options));
        $certId = $resultGetCert->certificate->id;
        echo "certificate Id: $certId\n";
        sleep(60);
        //6-Activer le certificat
        //EN POST /api/v1/servers/{serverId}/sites/{siteId}/certificates/{id}/activate
        $url = $domain."/api/v1/servers/$serverId/sites/$siteId/certificates/$certId/activate";
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER=>$header,
            CURLOPT_POST=>1
        );
        print_r(json_decode($this->executeCurlRequest($url,$options)));
        sleep(60);
        //7-On remet le script de déploiement comme il le faudrait
        //EN PUT /api/v1/servers/{serverId}/sites/{siteId}/deployment/script
        $url = $domain."/api/v1/servers/$serverId/sites/$siteId/deployment/script";
        $newDataDeploymentScript = json_encode(["content"=>'cd /home/forge/'."$nddName\n".'composer install'."\n".'git pull origin master'."\n".'echo "" |sudo -S service php7.0-fpm reload']);
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_HTTPHEADER=>$header,
            CURLOPT_CUSTOMREQUEST=>"PUT",
            CURLOPT_POSTFIELDS=>$newDataDeploymentScript
        );
        $this->executeCurlRequest($url,$options);
        //print_r($this->executeCurlRequest($url,$options));
        //TODO:Ouvrir un fichier model de .env
        // Faire des str_replace pour avoir la bonne configuration;
    }

    public function executeCurlRequest($url,$options)
    {
        $curl = curl_init();
        curl_setopt_array($curl,$options);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    ///TODO: A placer dans chacune des requêtes:
    // Accept: application/json
    // Content-Type: application/json
    // Authorization : Bearer API_KEY
}
