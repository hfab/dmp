<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;
use App\Classes\Routeurs\RouteurMindbaz;

use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Fai;
use App\Models\Sender;
use App\Models\CampagneRouteur;

class CampagneSegmentMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_mindbaz {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Segment et import pour maildrop facon Mindbaz';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMindbaz();
        $this->selectedsender = array();
        $this->indextabfai = array();
        $this->notcompletedfais = array();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_mindbaz')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1) {
            \Log::info('CampagneLaunchSend : MINDBAZ DISABLE');
            return 0;
        }

        $jour = date('d');
        $asc = 'asc';
        if ($jour%2 == 1) { //on alterne l'odre de sélection des senders suivant le jour (pair ou impair)
            $asc = 'desc';
        }

        $bloc = \DB::table('senders_history')->max('bloc');
        if(empty($bloc)){
            $bloc = 0;
        }
        $this->max_bloc = $bloc + 1;

        $planning = Planning::find($this->argument('planning_id'));
        $planning->nb_trials++;
        $planning->save();

        $lacampagne = Campagne::where('id',$planning->campagne_id)->first();

        $mindbaz = Routeur::where('nom','Mindbaz')->first();
        $html = $lacampagne->generateHtml($mindbaz, $planning->id);

        \Log::info("[CampagneSegmentMindbaz][P$planning->id] : Début Segmentation (Planning $planning->id/Campagne $lacampagne->id) {Trial n°$planning->nb_trials}");

        $fais = Fai::all();
        $mailsrestants = array();
        $selectedsender = array();

        $anonyme = "";

        if($planning->type == 'anonyme'){
            $html = $lacampagne->generateHtmlAnonyme($mindbaz, $planning->id);
            $anonyme = "_a";
        }

        //supprimer les fichiers de destinataires pour eviter d'avoir des fichiers lourds dans le cas où le script se répète
        exec("rm ".storage_path()."/mindbaz/*p$planning->id$anonyme*");

        $user_mono_sender = \DB::table('planning_senders')
            ->select('sender_id')
            ->where('planning_id', $planning->id)
            ->get();

        //On récupère le nombre d'@ par FAI pour la plannif
        $countFai = DB::table('tokens')
            ->select('fai_id', DB::raw('count(*) as total'))
            ->where('planning_id',$planning->id)
            ->groupBy('fai_id')
            ->get();

        //On récupère uniquement les senders Mindbaz actif
        $sendersmd_query = DB::table('senders')
            //faire un join avec la table sender_type
            ->select('id')
            ->where('routeur_id', $mindbaz->id)
            ->where('quota', '>', 0)
            ->where('quota_left', '>', 0);

        if(!empty($user_mono_sender)){ //vu qu'on selectionne directement le sender on n'a pas besoin de preciser la branche <-> eviter erreur sur branche
            $sendersmd_query->where('branch_id', $planning->branch_id);
        }

        $sendersmd = $sendersmd_query->get();

        // ******************************************
        // 1ERE ETAPE : SELECTION 'PERFECT' SENDERS
        //On récupère des senders potentiels qui peuvent envoyer
        // exactement les FAIS demandé (voire plus)
        $recupSenders = \DB::table('fai_sender')
            ->select('sender_id', \DB::raw('COUNT(fai_id) as fai_num'))
            ->where('quota_left', '>', 0)
            ->where('quota', '>', 0)
            ->whereIn('fai_id', array_pluck($countFai, 'fai_id'))
            ->whereIn('sender_id', array_pluck($sendersmd, 'id'))
            ->havingRaw('fai_num >= '.count($countFai))
            ->orderBy('sender_id', $asc)
            ->groupBy('sender_id')
            ->get();

        if(!empty($user_mono_sender)){
            $recupSenders = $user_mono_sender;
        }

        foreach($countFai as $cf) {
            $volumeFaiTotal = $cf->total;
            // On récupère les tokens PAR FAI
            $lesadressestokens = \DB::table('tokens')
                ->select('destinataire_id')
                ->where('campagne_id',$lacampagne->id)
                ->where('fai_id',$cf->fai_id)
                ->where('date_active',$planning->date_campagne)
                ->where('planning_id',$planning->id)
                ->get();

            //On récupère les senders communs à tous les fais -> fai_senders_hors_ora
            //<=> pouvant envoyer en un envoi sur plusieurs FAIS à la fois
            $fai_senders = \DB::table('fai_sender')
                ->where('fai_id',$cf->fai_id)
                ->where('quota','>',0)
                ->where('quota_left','>',0)
                ->whereIn('sender_id', array_pluck($recupSenders,'sender_id'))
                ->orderBy('sender_id',$asc)
                ->get();

            if(!isset($this->indextabfai[$cf->fai_id])){
                $this->indextabfai[$cf->fai_id] = 0;
            }

//          \Log::info("[CampagneSegmentMindbaz][P$planning->id] : FAI ID $cf->fai_id -- CHUNKSIZE $count");
            $this->write($lacampagne, $planning, $anonyme, $fai_senders, $lesadressestokens);
            $completed = $this->indextabfai[$cf->fai_id] + 1;
            if($completed != $volumeFaiTotal){
                $this->notcompletedfais[] = $cf->fai_id;
            }
        }

        // ******************************************
        // 2EME ETAPE : SI TOTALITE TOKENS NON TRAITES -> SELECTION  SENDERS 'NOT-PERFECT' ou 'OVER-QUALIFIED'
        //On récupère des senders potentiels qui peuvent envoyer
        // pas exactement les FAIS demandé (il manque un ou plusieurs FAIS des FAIS tokens sélectionné)
        if(count($this->notcompletedfais) > 0 && empty($user_mono_sender)) {

            $nbTotalFais = \DB::table('fais')->count();

            $recupSenders = \DB::table('fai_sender')
                ->select('sender_id', \DB::raw('COUNT(fai_id) as fai_num'))
                ->where('quota_left', '>', 0)
                ->where('quota', '>', 0)
                ->whereIn('fai_id', $this->notcompletedfais)
                ->whereIn('sender_id', array_pluck($sendersmd, 'id'))
                ->havingRaw('fai_num < ' . count($countFai))
                ->OrHavingRaw("fai_num > $nbTotalFais")
                ->orderBy('sender_id', $asc)
                ->groupBy('sender_id')
                ->get();

            foreach($countFai as $cf) {

                $volumeFaiTotal = $cf->total;
                // On récupère les tokens PAR FAI
                $lesadressestokens = \DB::table('tokens')
                    ->select('destinataire_id')
                    ->where('campagne_id',$lacampagne->id)
                    ->where('fai_id',$cf->fai_id)
                    ->where('date_active',$planning->date_campagne)
                    ->where('planning_id',$planning->id)
                    ->get();

                //On récupère les senders communs à tous les fais -> fai_senders_hors_ora
                //<=> pouvant envoyer en un envoi sur plusieurs FAIS à la fois
                $fai_senders = \DB::table('fai_sender')
                    ->where('fai_id',$cf->fai_id)
                    ->where('quota','>',0)
                    ->where('quota_left','>',0)
                    ->whereIn('sender_id', array_pluck($recupSenders,'sender_id'))
                    ->orderBy('sender_id',$asc)
                    ->get();

                if(!isset($this->indextabfai[$cf->fai_id])){
                    $this->indextabfai[$cf->fai_id] = 0;
                }

//          \Log::info("[CampagneSegmentMindbaz][P$planning->id] : FAI ID $cf->fai_id -- CHUNKSIZE $count");
                $this->write($lacampagne, $planning, $anonyme, $fai_senders, $lesadressestokens);
                $completed = $this->indextabfai[$cf->fai_id] + 1;
                if($completed != count($lesadressestokens)){
                    $this->notcompletedfais[] = $cf->fai_id;
                }
            }
        }

        \Log::info("[CampagneSegmentMindbaz][P$planning->id] : Début Partie Import via API");
        foreach($this->selectedsender as $sid => $sender)
        {
//            $url = 'http://' . getenv('CLIENT_URL') . '/mailexport/mindbaz/s'. $sender->id . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme . '.csv';
            $file = storage_path() . '/mindbaz/s' . $sender->id . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';
            $targetids = array();
            //Let's create the target
            $targetid = $this->routeur->create_list($sender, $file);

            if(empty($targetid)){
                \Log::error("TARGET ID EMPTY");
                return 0;
            }
            $targetids[] = $targetid;

            $cid = $this->routeur->create_campagne_light($lacampagne, $sender, $planning, $targetids);

            $this->routeur->calculate_spamscore($sender, $cid);

            \Log::info("[CampagneSegmentMindbaz][P$planning->id] before INSERT -- cid $cid");
            $ts = date('Y-m-d H:i:s');
            \DB::table('campagnes_routeurs')
                ->insert([
                    'campagne_id' => $lacampagne->id,
                    'sender_id' => $sender->id,
                    'cid_routeur' => $cid,
                    'listid' => $targetid,
                    'planning_id' => $planning->id,
                    'created_at' => $ts,
                    'updated_at' => $ts
                ]);
            \Log::info("[CampagneSegmentMindbaz][P$planning->id] after INSERT -- cid $cid");
            $sender->nb_campagnes_left--;
            $sender->save();

            $this->routeur->schedule_campaign($sender, $cid, "$planning->date_campagne $planning->time_campagne");
        }

        \Log::info("[CampagneSegmentMindbaz][P$planning->id] : Fin Partie Import via API");

        $cidasend = \DB::table('campagnes_routeurs')
            ->where('campagne_id',$lacampagne->id)
            ->where('planning_id',$planning->id)
            ->get();

        if(count($cidasend) == 0){
            \Log::error("[CampagneSegmentMindbaz][P$planning->id] : Il n'y ø de segments créés pour cette campagne (Planning $planning->id/Campagne $lacampagne->id)");
            return 0;
        }

        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->nb_trials = 0;
        $planning->save();

        \Log::info("[CampagneSegmentMindbaz][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");
    }

    public function write ($lacampagne, $planning, $anonyme, $fai_senders, $chunks) {
        $mailsrestants = array();
        $indSender = 0;
        \Log::info("[CampagneSegmentMindbaz][P$planning->id] : START WRITE");
        foreach($fai_senders as $fs) {

            $sender = Sender::find($fs->sender_id);

            if($fs->quota_left <= 0 or empty($sender)){
                continue;
            }

            if($sender->nb_campagnes_left <= 0 or $sender->quota_left <= 0 ){
                continue;
            }

            $old_index = $this->indextabfai[$fs->fai_id];
            $count = count($chunks);
            $how_many1 = $count - $old_index;
            $how_many2 = $fs->quota_left/$sender->nb_campagnes_left;
//            $how_many2 = $fs->quota/$sender->nb_campagnes;
            $how_many = min($how_many1, $how_many2);

            if($how_many <= 1){ //ca veut dire qu'on a traite tous les tokens
                break;
            }

            $left = $sender->quota_left - $how_many;
            // l'historique
            \DB::table('senders_history')
                ->insert(
                    [
                        'quota_before' => null,
                        'sender_id' => $sender->id,
                        'fai_id' => $fs->fai_id,
                        'planning_id' => $planning->id,
                        'quota_left_before' => $sender->quota_left,
                        'quota_left' => $left,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'bloc' => $this->max_bloc
                    ]
                );

            // on check si le fichier existe
            // if(file_exists())
            $file = storage_path() . '/mindbaz/s' . $sender->id . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';
            \Log::info("[CampagneSegmentMindbaz][P$planning->id] : FILE -- $file");
//            \Log::info("[CampagneSegmentMindbaz][P$planning->id] : count chunks[indSender] -- ".count($chunks[$indSender])." -- SENDER$fs->sender_id =+*.*+= FAI$fs->fai_id");
            \Log::info("[CampagneSegmentMindbaz][P$planning->id] : count indextabfai ".$this->indextabfai[$fs->fai_id]." -- SENDER$fs->sender_id =+*.*+= FAI$fs->fai_id");
            $this->write_tokens($file, $chunks, $how_many, $fs->fai_id);
            \Log::info("[CampagneSegmentMindbaz][P$planning->id] : count indextabfai ".$this->indextabfai[$fs->fai_id]." -- SENDER$fs->sender_id =+*.*+= FAI$fs->fai_id");

            $sender->quota_left = $left;
            $sender->save();

            \DB::table('fai_sender')
                ->where('fai_id',$fs->fai_id)
                ->where('sender_id',$sender->id)
                ->update(['quota_left' => $fs->quota_left - $how_many]);

            $this->selectedsender[$sender->id] = $sender;
            $indSender++;
        }
//        \Log::info("[CampagneSegmentMindbaz][P$planning->id] : Nb mails restants : ".count($mailsrestants));
//        return count($mailsrestants);
    }

    public function write_tokens($file, $tokens, $how_many, $fai_id)
    {
        if(!file_exists($file)) {
            \Log::info("[CampagneSegmentMindbaz] : FILE -- $file DOESN'T EXIST");
            $fp = fopen($file,"w");
//            $content = "tor_id" . "\n";
//            fwrite($fp,$content);
            fclose($fp);
        }

        $content = "";
        $fp = fopen($file,"a+");

        for($i=$this->indextabfai[$fai_id]; $i<count($tokens);$i++){

            if($how_many <= 0){
                break;
            }

            $desti = $tokens[$i];

            $destinataire = \DB::table('destinataires')
                ->select('id','mail')
                ->where('id', $desti->destinataire_id)
                ->first();

            $content = $content . $destinataire->id. "\r\n";
            $this->indextabfai[$fai_id]++;
            $how_many--;
        }

        \Log::info("[CampagneSegmentMindbaz] : $file CONTENT LENGTH -- ".strlen($content));

        fwrite($fp,$content);
        fclose($fp);
//        return $how_many;
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
