<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

// ajout du routeur
use App\Classes\Routeurs\RouteurSmessage;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Sender;
use App\Models\CampagneRouteur;
use App\Models\Fai;

class CampagneSegmentSmessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_smessage {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Traitement Smessage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_smessage')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : SMESSAGE DISABLE');
            return 0;
        }

        $bloc = \DB::table('senders_history')->max('bloc');
        if(empty($bloc)){
            $bloc = 0;
        }
        $this->max_bloc = $bloc + 1;

        $planning = Planning::find($this->argument('planning_id'));
        $planning->nb_trials++;
        $planning->save();

        $campagneid = $planning->campagne_id;

        $routeur = new RouteurSmessage();

        $lacampagne = Campagne::where('id',$campagneid)->first();
        $smessage = Routeur::where('nom','Smessage')->first();

        \Log::info("[CampagneSegmentSmessage][P$planning->id] : Début Segmentation (Planning $planning->id/Campagne $lacampagne->id) {Trial n°$planning->nb_trials}");

        $fais = Fai::all();

        $mailsrestants = array(); //TODO : a gerer plus tard dans le cas des restes de mails de la decoupe lineaire a dispatcher vers les senders

        $selectedsender = array();

        $user_mono_sender = \DB::table('planning_senders')
            ->select('sender_id')
            ->where('planning_id', $planning->id)
            ->get();

        $anonyme = "";
        // if ici et on creé la var
        if($planning->type == 'anonyme'){
            $anonyme = "_a";
        }

        //supprimer les fichiers de destinataires pour eviter d'avoir des fichiers lourds dans le cas où le script se répète
        exec("rm ".storage_path()."/smessage/*p$planning->id$anonyme*");

        foreach($fais as $f)
        {
            $lesadressestokens = array();
            $lesadressestokens = \DB::table('tokens')
                ->select('destinataire_id')
                ->where('fai_id',$f->id)
                ->where('campagne_id',$campagneid)
                ->where('planning_id',$planning->id)
                ->where('date_active',$planning->date_campagne)
                ->get();
            $volumeFaiTotal = count($lesadressestokens);

            //dans le foreach car pour avoir tjs a jour les senders ayant tjs un quota_left non nul
            $smessagesenders_query = Sender::where('quota_left','>',0)
                ->where('routeur_id',$smessage->id);

            if(!empty($user_mono_sender)) {
                $smessagesenders_query->whereIn('id', array_pluck($user_mono_sender, 'sender_id'));
            }

            $smessagesenders = $smessagesenders_query->get();

            $fai_senders = \DB::table('fai_sender')
                ->where('fai_id',$f->id)
                ->whereIn('sender_id',$smessagesenders->pluck('id')->all())
                ->orderByRaw('rand()')
                ->get();

            if(!$fai_senders){continue;}

            if(!empty($user_mono_sender)){
                $minQuotas = array($volumeFaiTotal);
                $minQuotas = $minQuotas + array_pluck($fai_senders, 'quota_left');
                $count = min($minQuotas);
                echo "\nFAI -- $f->id";
                var_dump($count);
                echo "\n";
            } else {
                $calcul = count($lesadressestokens) / count($fai_senders);

                $count = round($calcul)+1;
                // on prends le minimum entre le quota/sender
                if($f->quota_sender > 0) {
                    $count = min($f->quota_sender,$count);
                }
            }

            $chunks=array();
            $chunks = array_chunk($lesadressestokens,$count);
            $indSender = 0;
            \Log::info("[CampagneSegmentSmessage][P$planning->id] : FAI-$f->nom-@- ".count($lesadressestokens)." -Sender-".count($fai_senders)." -DIV-".$count." -COUNTCHUNK-".count($chunks));

            foreach($fai_senders as $fai_sender)
            {
                $sender = Sender::find($fai_sender->sender_id);
                if(!isset($chunks[$indSender])) {
                    break;
                }
                //si le sender n'a pas assez de places / jour pour envoyer on prends un autre sender
                //ou sinon on peut
                if($sender->quota_left < count($chunks[$indSender])) {
//                    array_merge($mailsrestants,$chunks[$indSender]);
                    continue;
                }

                $file = storage_path() . '/smessage/s' . $sender->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

                $this->write_tokens($file, $chunks[$indSender]);

                // l'historique
                \DB::table('senders_history')
                    ->insert(
                        [
                            'quota_before' => null,
                            'sender_id' => $sender->id,
                            'fai_id' => $fai_sender->fai_id,
                            'planning_id' => $planning->id,
                            'quota_left_before' => $sender->quota_left,
                            'quota_left' => $sender->quota_left - count($chunks[$indSender]),
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                            'bloc' => $this->max_bloc
                        ]
                    );

                $sender->quota_left -= count($chunks[$indSender]);

                \DB::table('fai_sender')
                    ->where('fai_id',$fai_sender->fai_id)
                    ->where('sender_id',$sender->id)
                    ->update(['quota_left' => $fai_sender->quota_left - count($chunks[$indSender])]);

                $sender->save();
                $selectedsender[$sender->id] = $sender;
                $indSender++;
            }
        }

        \Log::info("[CampagneSegmentSmessage][P$planning->id] : Début Partie Import via API");
        foreach($selectedsender as $sid => $sender)
        {
            if($planning->type == 'public'){
                $idcampagne = $routeur->createCampagne($sender,$lacampagne,$planning);
            } else {
                $idcampagne = $routeur->createCampagneAnonyme($sender,$lacampagne,$planning);
            }

            $url = 'http://' . getenv('CLIENT_URL') . '/mailexport/smessage/s' . $sender->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme . '.csv';
            $routeur->transfert_email($sender->nom, $sender->password, $idcampagne, $url);

        }

        $planning->nb_trials = 0;
        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->save();
        \Log::info("[CampagneSegmentSmessage][P$planning->id] : Fin Partie Import via API");

        \Log::info("[CampagneSegmentSmessage][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");
    }

    public function write_tokens($file, $tokens) {
        $fp = fopen($file,"a+");
        $content = "";
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail')
                ->where('id', $desti->destinataire_id)
                ->first();
            $content .= $destinataire->mail.";;;".$destinataire->id."\n";
        }
        fwrite($fp,$content);
        fclose($fp);
    }



    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
