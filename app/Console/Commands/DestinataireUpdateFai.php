<?php

namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use Illuminate\Console\Command;

class DestinataireUpdateFai extends Command
{
    protected $chunk_size = 10000;
    protected $chunk_size_update = 10000;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'destinataire:update_fai';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'MAJ du FAI des destinataires.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $default_fai = 6;
        $fais = \App\Models\Fai::all();
        
        \DB::table('destinataires')
            ->select('id', 'mail')
            ->where('statut', 0)
            ->chunk($this->chunk_size, function ($destinataires) use ($default_fai, $fais){
                $fai_dest_ids = array();
                foreach($destinataires as $d) {
                    $fai_id = $default_fai;
                    foreach ($fais as $k_fai) {
                        foreach($k_fai->get_domains() as $dom) {
                            if (strpos($d->mail, $dom) !== false) {
                                $fai_id = $k_fai->id;
                                break 2;
                            }
                        }
                    }

                    $fai_dest_ids[$fai_id][]=$d->id;
                    if(count($fai_dest_ids[$fai_id]) > $this->chunk_size_update){
                        $this->writeUpdates($fai_dest_ids[$fai_id], $fai_id);
                    }
                }
                foreach($fai_dest_ids as $faiid => $tabids){
                    if(count($tabids) > 0){
                        $this->writeUpdates($fai_dest_ids[$faiid], $faiid);
                    }
                }
            });

    }

    private function writeUpdates($ids, $fai_id) {
        \DB::table('destinataires')->whereIn('id', $ids)->update(['fai_id' => $fai_id]);
    }
}
