<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Sender;

use Mail;

class CampagneSendMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:send_mindbaz {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Vérifie les envois Mindbaz';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMindbaz();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_mindbaz')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : MINDBAZ DISABLE');
            return 0;
        }

        $today = date('Y-m-d');
        $now = \Carbon\Carbon::now();
        $p = Planning::find($this->argument('planning_id'));
        $campagne = Campagne::find($p->campagne_id);
        \Log::info("[CampagneSendMindbaz][P$p->id] : Début des vérifications des envois (Planning $p->id/Campagne $campagne->id)");
        if ($p->sent_at != null) {
            echo "Already sent\n";
            \Log::info("Planning $p->id (Campagne $campagne->id) : already sent");
            die();
        }

        $cidasend = \DB::table('campagnes_routeurs')
            ->where('campagne_id',$campagne->id)
            ->where('planning_id',$p->id)
            ->distinct('cid_routeur')
            ->get();

        if(count($cidasend) == 0){
            \Log::error("[CampagneSendMindbaz][P$p->id] : Il n'y ø d'envois prévus pour cette campagne (Planning $p->id/Campagne $campagne->id)");
            return 0;
        }

        foreach ($cidasend as $key => $envoi)
        {
            $sender = \DB::table('senders')->where('id',$envoi->sender_id)->first();
            $statut = $this->routeur->check_status($sender, $campagne->ref, $envoi->cid_routeur);
            if(!$statut){
                \Log::error("[CampagneSendMindbaz][P$p->id] : La campagne n'a pas encore été envoyé. (Planning $p->id/Campagne $campagne->id)");
                return 0;
            }
        }

        $p->sent_at = date('Y-m-d H:i:s');
        $p->next_campaign = 0;
        $p->save();

        \App\Models\Notification::create([
            'user_id' => 1, // /!\ User System
            'level' => 'info',
            'message' => "Campagne $campagne->ref (planning $p->id) envoyée.",
            'url' => '/campagne/'.$campagne->id.'/stats'
        ]);

        \Log::info("[CampagneSendMindbaz][P$p->id] : Fin du lancement des vérifications (Planning $p->id/Campagne $campagne->id)");
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
