<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class CheckBL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:bl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recupere une liste IP et compare avec les dnsbl';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      // tab des ndd issus des ip
      $nddtab = [];
      // faire une blade avec les domaines à la main (tracking)

      // dnsbl tab ip
      $ipbltab = array("b.barracudacentral.org",
      "bb.barracudacentral.org",
      "bl.spamcop.net",
      "zen.spamhaus.org",
      "pbl.spamhaus.org",
      "sbl.spamhaus.org",
      "sbl-xbl.spamhaus.org",
      "xbl.spamhaus.org",
      "rhsbl.sorbs.net",
      "dnsbl.sorbs.net",
      "all.spamrats.com",
      "dyna.spamrats.com"
      );


      // b.barracudacentral.org
      // bb.barracudacentral.org
      // bl.spamcop.net
      // zen.spamhaus.org
      // pbl.spamhaus.org
      // sbl.spamhaus.org
      // sbl-xbl.spamhaus.org
      // xbl.spamhaus.org

      // rhsbl.sorbs.net
      // dnsbl.sorbs.net
      // all.spamrats.com

      $nddtab = array(
        // "public.sarbl.org",
        // "multi.surbl.org",
        "dbl.spamhaus.org",
        "multi.uribl.com"
        );
      // dnsbl tab ndd
      // loffredusiecle.com.public.sarbl.org
      // lessupersoffres.fr.multi.surbl.org
      // lesdealsdeouf.com.dbl.spamhaus.org
      // /multi.uribl.com




      $data = \DB::table('blacklist_lookup')->whereNotNull('id')->get();
      // var_dump($data);

      echo 'Check des IP ' . "\n";

      foreach ($data as $d) {
       // continue; // debug
        if(isset($d->ip)){
        if (!filter_var($d->ip, FILTER_VALIDATE_IP) === false) {

        // refaire la fonction avec un tableau
          // $r = dns_get_record($d->ip.".sbl.spamhaus.org");

          foreach ($ipbltab as $ipbl) {
            $r = dns_get_record($d->ip.'.'.$ipbl);

            if($r !== null){
              if(isset($r[0])){
              echo '[insert bl] : ' . $d->ip . "\n" ;
                \DB::table('blacklist_lookup_records')->insert(['id_blacklist_lookup' => $d->id, 'bl_record' => json_encode($r[0]),'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);
                \DB::table('blacklist_lookup')->where('id', $d->id)->update(['is_bl'=> 1,'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);

              }
            }
          }


        // echo 'MAJ du reverse ' . "\n";
          $iphost = gethostbyaddr ( $d->ip );
          // faire tableau ndd parsé
          $ndd = explode('.',$iphost);
          if(!in_array($ndd[1] . '.' . $ndd[2],$nddtab)){
            $nddtab[] = $ndd[1] . '.' . $ndd[2];
          }

          // je fais un tableau host et je supprime ce qui n'existe plus
          // echo 'Reverse DNS IP (gethostbyaddr) : ' . gethostbyaddr ( $d->ip ) . "\n";

          if($iphost !== $d->ip){

            $checkhostip = \DB::table('blacklist_lookup')->where('domain_reverse', $iphost)->first();
            // var_dump($checkhostip);

            if($checkhostip == null){
              echo 'MAJ Reverse DNS IP (gethostbyaddr) : ' . gethostbyaddr ( $d->ip ) . "\n";
              \DB::table('blacklist_lookup')->insert(['domain' => null,'domain_reverse' => $iphost, 'ip' => null, 'is_bl' => 0,'start_bl_date' => null,'end_bl_date' => null,'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);
            } else {
              // update
            }

            }
          }

        }
      }

      // var_dump($nddtab);
      echo 'Ajout BDD domaine simple ' . "\n";
      foreach ($nddtab as $ndd) {
        $checkndd = \DB::table('blacklist_lookup')->where('domain', $ndd)->first();
        if($checkndd == null){
          \DB::table('blacklist_lookup')->insert(['domain' => $ndd,'domain_reverse' => null, 'ip' => null, 'is_bl' => 0,'start_bl_date' => null,'end_bl_date' => null,'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);
        }
      }

      // je check si j'ai déjà le reverse de set dans la liste bdd
      // si oui maj
      // si non create + add status



      // enfin je prends les ndd de ma liste

      echo 'Check des Reverse ' . "\n";

      $datard = \DB::table('blacklist_lookup')->whereNotNull('domain_reverse')->get();
      // var_dump($datard);

      foreach ($datard as $rd) {
        //  continue;
        // var_dump($rd->domain_reverse);
        // $rreverse = dns_get_record($rd->domain_reverse.".dbl.spamhaus.org");

        foreach ($nddtab as $nddbl) {
          // var_dump($rd->domain_reverse.'.'.$nddbl);
          $rreverse = dns_get_record($rd->domain_reverse.'.'.$nddbl);
          // $rreverse = null;
          if($rreverse !== null){
            if(isset($rreverse[0])){
              // insert and update
              // echo '[insert bl] : ' . $d->domain_reverse . "\n" ;
              echo '[insert bl reverse] : ' . $rd->domain_reverse . "\n" ;
              \DB::table('blacklist_lookup_records')->insert(['id_blacklist_lookup' => $rd->id, 'bl_record' => json_encode($rreverse[0]),'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);
              \DB::table('blacklist_lookup')->where('id', $rd->id)->update(['is_bl'=> 1,'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);


            }
          }
        }
        // var_dump($rreverse);

      }



      echo 'Check des ndd ' . "\n";

      $datad = \DB::table('blacklist_lookup')->whereNotNull('domain')->get();
      foreach ($datad as $d) {
        var_dump($d->domain);
        // $rdomain = dns_get_record($d->domain.".dbl.spamhaus.org");

        foreach ($nddtab as $nddbl) {
          $rdomain = dns_get_record($d->domain.".".$nddbl);

          var_dump($rdomain);

          if(isset($rdomain[0])){

            echo '[insert bl domaine] : ' . $d->domain . "\n" ;
            // recup le bl type et l'insert avec le lookup record

            \DB::table('blacklist_lookup_records')->insert(['id_blacklist_lookup' => $d->id, 'bl_record' => json_encode($rdomain[0]),'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);
            \DB::table('blacklist_lookup')->where('id', $d->id)->update(['is_bl'=> 1,'created_at' => new \DateTime(),'updated_at' => new \DateTime()]);

          }
        }
      }


      die();


      $handle = fopen(storage_path('bl/ndd_ovh_1.txt'), 'r');

      if ($handle)
      {
      	while (!feof($handle))
      	{
      		$buffer = fgets($handle);
            $buffer = trim($buffer);
            $bl = null;
            $bl = dns_get_record($buffer.".dbl.spamhaus.org");

            if($bl == null){
              echo $buffer . " CLEAN \n";

            } else {
              echo $buffer . " LIST dbl.spamhaus \n";

            }
        }

      }

      // $bl = dns_get_record("lesoffresinmanquables.com.dbl.spamhaus.org");
      // var_dump($bl);


      // check IP

      // check ip maison

      // recup reverse via ip (dans la boucle ip ?)
      // string gethostbyaddr ( string $ip_address )



      die();

      // $this->checkipmaison('lesoffresinmanquables.com');
      // die();

      $chaineresultat = null;
      //
      $email = '';

      $handle = fopen(storage_path('ip/ip_m4y.txt'), 'r');

      if ($handle)
      {
      	while (!feof($handle))
      	{
      		$buffer = fgets($handle);
            $buffer = trim($buffer);

            if (!filter_var($buffer, FILTER_VALIDATE_IP) === false) {
                // echo("$buffer is a valid IP address \n");
                echo $this->checkipmaison($buffer);
                $email .= $this->checkipmaison($buffer);
            } else {
                // echo("$buffer is not a valid IP address \n");
                echo $buffer . "\n";
                $email .= $buffer . "\n";;
            }

            $buffer = null;
      	}

      	fclose($handle);
      }

      // var_dump($email);
      /*
      Mail::raw($email, function ($message) {

          $message->from('dev@lead-factory.net', 'DevTor');
          $message->to('fabien@lead-factory.net')->subject('Rapport Blacklist IP MD ' . date('d-m-Y'));
          $message->cc('hernoux.fabien@gmail.com');
          $message->cc('adeline.sc2@gmail.com');
          $message->cc('yveschaponic@gmail.com');


          return "true";
      });
        */
    }


    public function checkipmaison($ip){
      $listed = null;
      $dnsbl_lookup = array(
        "zen.spamhaus.org",
        "dnsbl.sorbs.net",
        "all.spamrats.com",
        "b.barracudacentral.org",
        "dbl.spamhaus.org"
      );

      $versionlongue = array(
        "spam.fusionzero.com",
        "access.redhawk.org",
        "all.rbl.jp",
        "all.s5h.net",
        "all.spamrats.com",
        "b.barracudacentral.org",
        "bl.blocklist.de",
        "bl.emailbasura.org",
        "bl.mailspike.org",
        "bl.score.senderscore.com",
        "bl.spamcannibal.org",
        "bl.spamcop.net",
        "bl.spameatingmonkey.net",
        "bogons.cymru.com",
        "cidr.bl.mcafee.com",
        "combined.njabl.org",
        "db.wpbl.info",
        "dnsbl-1.uceprotect.net",
        "dnsbl-2.uceprotect.net",
        "dnsbl-3.uceprotect.net",
        "dnsbl.dronebl.org",
        "dnsbl.inps.de",
        "dnsbl.justspam.org",
        "dnsbl.kempt.net",
        "dnsbl.rv-soft.info",
        "dnsbl.sorbs.net",
        "dnsbl.tornevall.org",
        "dnsbl.webequipped.com",
        "dnsrbl.swinog.ch",
        "fnrbl.fast.net",
        "ip.v4bl.org",
        "ips.backscatterer.org",
        "ix.dnsbl.manitu.net",
        "korea.services.net",
        "l2.apews.org",
        "l2.bbfh.ext.sorbs.net",
        "list.blogspambl.com",
        "mail-abuse.blacklist.jippg.org",
        "psbl.surriel.com",
        "rbl2.triumf.ca",
        "rbl.choon.net",
        "rbl.dns-servicios.com",
        "rbl.efnetrbl.org",
        "rbl.orbitrbl.com",
        "rbl.polarcomm.net",
        "singlebl.spamgrouper.com",
        "spam.abuse.ch",
        "spam.dnsbl.sorbs.net",
        "spam.pedantic.org",
        "spamguard.leadmon.net",
        "spamrbl.imp.ch",
        "spamsources.fabel.dk",
        "st.technovision.dk",
        "tor.dan.me.uk",
        "tor.dnsbl.sectoor.de",
        "truncate.gbudb.net",
        "ubl.unsubscore.com",
        "virbl.dnsbl.bit.nl",
        "zen.spamhaus.org"
      );

      if ($ip){
      echo 'Check IP : ' . $ip . "\n";
      $reverse_ip = implode(".", array_reverse(explode(".", $ip)));
      foreach($dnsbl_lookup as $host){
        if (checkdnsrr($reverse_ip . "." . $host . ".", "A")) {

          $listed.= $ip . ' - ' . $reverse_ip . ' - ' . $host . "\n";
          $lebl = $ip . ' - ' . $reverse_ip . ' - ' . $host . "\n";
          }
        }
      }

    if ($lebl){
      return $lebl;
      } else {
      // echo '"A" not found';
      return null;
      }
    }
}
