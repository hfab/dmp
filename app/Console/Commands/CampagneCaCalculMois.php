<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CampagneCaCalculMois extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ca:calcul';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fait les calculs pour les bases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $today = date("Y-m-d H:i:s");
      $moisdelacompta = date_parse(date("Y-m-d H:i:s"));

      $get_base_id = \DB::table('bases')
      ->select('id')
      ->get();

      foreach ($get_base_id as $v) {

      $get_campagne_ca = \DB::table('campagnes_ca')
      ->join('campagnes','campagnes_ca.campagne_id','=','campagnes.id')
      ->where('mois_compta', $moisdelacompta['month'])
      ->where('base_id',$v->id)
      ->get();

      $campagne_ca_brut_cumul_base = 0;
      $campagne_ca_net_cumul_base = 0;
      foreach ($get_campagne_ca as $cadecampagne) {

        $campagne_ca_brut_cumul_base = $campagne_ca_brut_cumul_base + $cadecampagne->ca_brut;
        $campagne_ca_net_cumul_base = $campagne_ca_net_cumul_base + $cadecampagne->ca_net;

      }

        $get_campagne_ca = \DB::table('campagnes_ca_base')
        ->where('periode_compta', $moisdelacompta['month'])
        ->where('base_id', $v->id)
        ->first();

        if(is_null($get_campagne_ca)){

          \DB::table('campagnes_ca_base')->insert(
              ['base_id' => $v->id,
              'ca_brut' => $campagne_ca_brut_cumul_base,
              'ca_net'=> $campagne_ca_net_cumul_base,
              'created_at' => $today,
              'updated_at' => $today,
              'periode_compta' => $moisdelacompta['month']]
          );

        } else {

          \DB::table('campagnes_ca_base')
            ->where('base_id', $v->id)
            ->where('periode_compta', $moisdelacompta['month'])
            ->update(['updated_at' => $today,
            'ca_brut' => $campagne_ca_brut_cumul_base,
            'ca_net'=> $campagne_ca_net_cumul_base]);

        }

      }


      die();


      // calcul du ca

      $date_limit = date("Y-m-d H:i:s",strtotime('-15 day'));

      // suite

      $get_campagne_name_range = \DB::table('campagnes')
      ->select('nom')
      ->where('created_at','>',$date_limit)
      ->groupBy('nom')
      ->get();

      foreach ($get_campagne_name_range as $bloc_name) {

        $get_id_calcul = \DB::table('campagnes')
        ->select('id')
        ->where('created_at','>',$date_limit)
        ->where('nom',$bloc_name->nom)
        // ->groupBy('nom')
        ->get();

        // var_dump($get_id_calcul);

        $campagne_ca_brut_cumul = 0;
        $campagne_ca_net_cumul = 0;

        foreach ($get_id_calcul as $gic) {

          // var_dump($gic->id);

          // je vais chercher les infos dans campages_ca et je sum 30 day
          $get_value_ca = \DB::table('campagnes_ca')
          ->where('campagne_id',$gic->id)
          ->first();



          $campagne_ca_brut_cumul = $campagne_ca_brut_cumul + $get_value_ca->ca_brut;
          $campagne_ca_net_cumul = $campagne_ca_net_cumul + $get_value_ca->ca_net;

          // column
          // type_record = last 30 days / mois en cours

        }

        // je rqt bloc maj
        // $bloc_maj = \DB::table('campagnes_ca_total')
        // ->max('bloc_maj');

        // si existe pas j'insert sinon update

        \DB::table('campagnes_ca_total')->insert(
            ['nom_campagne' => $bloc_name->nom,
            'base_id' => null,
            'ca_brut' => $campagne_ca_brut_cumul,
            'ca_net'=> $campagne_ca_net_cumul,
            'commentaire'=> null,
            'created_at' => $today,
            'updated_at' => $today]
        );




      }



      // je get le nom
      // je supprime les chiffres


      // je recupére chaque bloc sur la ref

      // j'aditionne les ca pour pour chaque




    }
}
