<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Partner;
use App\Models\Editor;
use App\Models\Destinataire;
class SimulateDmpMatched extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simulate:dmp_matched';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill dmp_matched table with ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         *TODO: Faire une commande simulant les actions des destinataires
         * remplissant la dmp. On remplit la table dmp_cookies et dmp_matched avec les hash
         * des destinataires,l'id de l'editeur et de l'id du site partenaire ainsi que de son 
         * theme_id.
         *
         */
        $destinatairesHash = DB::select('SELECT hash FROM dmp_dicomail ORDER BY id');
        $arrayDmpMatched = $arrayDmpCookies = [];
        $index = 0;
        foreach($destinatairesHash as $hash){
            $index++;
            $randomPartner = Partner::where('theme_id','!=',0)->orderByRaw("RAND()")->first();
            $randomEditor = Editor::orderByRaw("RAND()")->first();
            $randValue = rand(1,30);
            array_push($arrayDmpMatched,[
                "created_at"=>date('Y-m-d H:i:s',strtotime("-$randValue days")),
                    "updated_at"=>date('Y-m-d H:i:s',strtotime("-$randValue days")),
                    "base_site_id"=>$randomPartner->id,
                    "theme_id"=>$randomPartner->theme_id,
                    "editor_id"=>$randomEditor->id,
                    "destinataire_hash"=>$hash->hash,
                    "nb_has_not_ordered"=>rand(0,5),
                    "nb_has_ordered"=>rand(0,5)
                ]);

            array_push($arrayDmpCookies,[
                "token"=>json_encode(["hsh"=>$hash->hash,"edt"=>$randomEditor->id]),
                    "editor_id"=>$randomEditor->id,
                    "hash"=>$hash->hash,
                    "created_at"=>date('Y-m-d H:i:s'),
                    "updated_at"=>date('Y-m-d H:i:s')
                ]);
            if($index>5000){
                DB::table('dmp_matched')->insert($arrayDmpMatched);
                //DB::table('dmp_cookies')->insert($arrayDmpCookies);
                $index = 0;
                $arrayDmpMatched = $arrayDmpCookies = [];
            }
        }
        DB::table('dmp_matched')->insert($arrayDmpMatched);

    }
}
