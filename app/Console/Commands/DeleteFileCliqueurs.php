<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeleteFileCliqueurs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:file_cliqueurs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Supprime les fichiers cliqueurs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // exec("rm -rf * $filepath ".storage_path()."/csv/");
         exec("rm -rf " .storage_path()."/cliqueurs/*");
        // echo exec("pwd");
    }
}
