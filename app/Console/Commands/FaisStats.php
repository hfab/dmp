<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class FaisStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fais:stats {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Met à jour les stats des fai par base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $idplanning = $this->argument('planning_id');

        $monplanning = \DB::table('plannings')->where('id',$idplanning)->first();
        $macampagne = \DB::table('campagnes')->where('id',$monplanning->campagne_id)->first();
        $labase = \DB::table('bases')->where('id',$macampagne->base_id)->first();

        $lesfais = \DB::table('fais')->get();

          foreach ($lesfais as $lefai) {

            $tokenscount = \DB::table('tokens')->where('base_id',$labase->id)->where('date_active', date('Y-m-d'))->where('fai_id',$lefai->id)->count();
            $tokenscountused = \DB::table('tokens')->where('base_id',$labase->id)->where('date_active', date('Y-m-d'))->where('fai_id',$lefai->id)->where('planning_id',$idplanning)->count();

            // echo 'Le nombre de tokens pour la base : ' . $labase->nom . ' et le fai : ' . $lefai->nom . "\n" ;

            \DB::statement("INSERT INTO fais_stats (base_id,fai_id,volume,created_at,updated_at,volume_used,planning_id) VALUES ('". $labase->id . "','". $lefai->id ."','". $tokenscount ."','" . date("Y-m-d H:i:s") ."','". date("Y-m-d H:i:s") ."','". $tokenscountused ."','". $idplanning ."')");

          }

          \Log::info("Calcul des statistiques FAI OK pour la plannif n : " . $idplanning);
    }

    protected function getArguments()
    {
      return [
        ['planning_id', InputArgument::OPTIONAL, 'Planning id.'],
      ];
    }


}
