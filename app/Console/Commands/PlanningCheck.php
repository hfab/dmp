<?php namespace App\Console\Commands;

use App\Models\Planning;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class PlanningCheck extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'planning:check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check si les plannifs du jour > 1h tjs pas parties.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        \DB::disableQueryLog();

        \App\Helpers\Profiler::start('planning:check');

        $today = date('Y-m-d');
        $now = date('H:i:s');

        $plannings = Planning::where('date_campagne', $today)
            ->where('sent_at', NULL)
            ->where('time_campagne', '<', $now)
            ->get();

        $not_sent_plannings = array();

        foreach($plannings as $p)
        {
            $now = \Carbon\Carbon::now();
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p->date_campagne.' '.$p->time_campagne);
            $diff = $now->diffInMinutes($from);

            if($diff > 90){
                $not_sent_plannings[] = $p;
            }
        }

        //On envoie un mail d'alerte pour lister les campagnes qui ne sont pas parties
        if(count($not_sent_plannings) > 0){
            echo "\nOn envoie un mail";
            \Mail::send('mail.mailplanningsprob',
                [
                    'not_sent_plannings' => $not_sent_plannings,
                    'url' => getenv('CLIENT_TITLE')
                ], function ($message) {

                    $groups = \DB::table('user_group')
                        ->select('id')
                        ->whereIn('name', ['Admin', 'Super Admin'])
                        ->get();

                    $destinataires = \App\Models\User::where('is_valid', 1)
                        ->whereIn('user_group_id', array_pluck($groups, 'id'))
                        ->where('email', '!=', "")
                        ->get();

                    $message->from('rapport@lead-factory.net', 'Tor')->subject("Alerte campagne(s) non-envoyée(s) " . date('d-m-Y H:i').' ['.getenv('CLIENT_TITLE').']');
                    foreach($destinataires as $d){
                        $message->to($d->email);
                    }
                });
        }

       \App\Helpers\Profiler::report('planning:check');

	}
}
