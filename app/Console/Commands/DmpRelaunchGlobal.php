<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DmpRelaunchGlobal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relaunch:global';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lance les deux commandes relaunch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        \Log::info("[CampagneRelaunch - relaunch:check] - DEBUT");
        \Artisan::call('relaunch:check');
        \Log::info("[CampagneRelaunch - relaunch:check] - FIN");
        sleep(30);
        \Log::info("[CampagneRelaunch - relaunch:go] - DEBUT");
        \Artisan::call('relaunch:go');
        \Log::info("[CampagneRelaunch - relaunch:go] - FIN");
    }
}
