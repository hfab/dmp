<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Sender;

class BaseImportMindbazChoixSender extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'base:import_mindbaz_dmp {sender_id} {source}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->file = "";
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $sender_id = $this->argument('sender_id');
        $file = $this->argument('source');

        $this->file = storage_path().'/listes/'.$file;
        \Log::info("Import Destinataires dans le dico (QUEUE COMMAND) du fichier -- '.$file");
        $ts = date('Y-m-d H:i:s');

        $extension = \File::extension($file);
        $name = \File::name($file);

        $liste = str_replace(".$extension",'',$name);
        echo 'WHERE Liste = ' . $name . "\n";
        $mindbaz = new RouteurMindbaz();

        $routeur = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->where('is_active', 1)
            ->first();

        $sender = Sender::where('routeur_id', $routeur->id)
            ->where('id',$sender_id)
            ->first();

        $imported = false;
        $filename = $file;

            $import_file = storage_path()."/mindbaz/s$sender->id"."_".$name."_import_mindbaz_total.csv";

            $this->current_max_id = $mindbaz->count_subscribers($sender) + 11111111;
            $this->current_max_id++;

            $this->handle = fopen( $import_file , "w+");

            \DB::table('dmp_dicomail')
                ->select('id', 'mail')
                ->where('statut', 0)
                // ->where('source', $file)
                ->chunk(10000, function ($destinataires) {
                    $content = "";
                    foreach ($destinataires as $desti) {
                        $content .= "dmp_$desti->id;$desti->mail;$this->current_max_id;".getenv('CLIENT_TITLE')."\r\n";
                        $this->current_max_id++;
                    }
                    fwrite($this->handle, $content);
                });
            fclose($this->handle);

            $mindbaz->import_list($sender, $import_file);
            $imported = true;


        \Log::info("End of Import Mindbaz base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");
        // \App\Helpers\Profiler::report();

    }

    protected function getArguments()
    {
        return [
            ['sender_id', InputArgument::REQUIRED, 'Sender'],
            ['source', InputArgument::REQUIRED, 'Fichier source'],
        ];
    }
}
