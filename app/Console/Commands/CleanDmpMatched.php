<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CleanDmpMatched extends Command 
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
	    parent::__construct();
    }
	
    protected $name = 'clean:dmpMatched';
	
    protected $description = 'Clean dmp_matched table when there more than one same entry';
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        //
	$destinataires = \DB::table('dmp_matched')
            ->select('destinataire_id', \DB::raw('COUNT(*) as occ'))
            ->groupBy('destinataire_id')
            ->havingRaw('occ > 1')
            ->get();

        foreach($destinataires as $dest){

            //On garde la premiere occurence
            $doublon_tags = \DB::table('dmp_matched')
                ->where('destinataire_id', $dest->destinataire_id)
                ->get();

            $keep_tag = $doublon_tags[0];
	    var_dump($keep_tag);

            //On mets à jour les medias concernes avec le bon tag id
            $delete_others = \DB::table('dmp_matched')
                ->where('destinataire_id', $dest->destinataire_id)
                ->where('id', '!=', $keep_tag->id)
                ->delete();
        }
		
    }
}
