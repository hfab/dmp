<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;
use App\Models\Campagne;
use App\Models\Destinataire;
use App\Models\Fai;
use App\Models\Planning;
use App\Models\Token;

use Mail;

class CampagneDestinataires extends Command {

    protected $max_tries = 5;

    protected $name = 'campagne:destinataires';
    protected $description = 'Prepares a campaign (target email addresses...)';

    public function __construct()
    {
        parent::__construct();
        $this->passe = 0;
        $this->selected = 0;
        $this->fai_selected = 0;
        $this->how_many_to_select = 0;
        $this->remaining = 0;
        $this->total = 0;
        $this->chunk_size = 25000;
        $this->total_tested = 0;
        $this->total_excluded = 0;
        $this->more_criterion = false;

        $this->desti_target = array();

        $this->to_exclude = [];
        $this->doublons = [];
        $this->uniques_doublons = [];

        $this->today = date('Y-m-d');
    }

    /**
     *
     */
    public function fire()
    {
        \App\Helpers\Profiler::start('campagne_destinataires');

        \DB::disableQueryLog();

        $planning = Planning::find($this->argument('planning_id'));
        $planning->nb_trials++;
        $planning->save();
        // var_dump($planning);
        \Log::info("[CampagneDestinataires][P$planning->id] : Début (Planning $planning->id / Campagne $planning->campagne_id) {Trial n°$planning->nb_trials}");

        $count = \DB::table('tokens')
            ->where('planning_id',$planning->id)
            ->count();

        if($planning->tokens_at != null or $count > 0){
            \DB::statement("UPDATE tokens SET planning_id = 0, campagne_id = NULL WHERE planning_id = $planning->id");
            // au cas où le script se relance on ré-initialise les tokens déjà sélectionnés
        }
        $campagne = Campagne::find($planning->campagne_id);

        $today = $planning->date_campagne;

        $this->remaining = $planning->volume;
        $done_fais = [];
        $geoloc_query = "";
        $age_query = "";
        $civil_query = "";
        $base_id_query = "";

        $get_bases = \DB::table("campagne_base")
            ->select('base_id')
            ->where('campagne_id', $campagne->id)
            ->get();

        $selected_bases = array_pluck($get_bases, 'base_id');
        $bases_query = \DB::table('bases')
            ->select('id')
            ->where('is_active', 1);

        if(count($selected_bases) > 0) {
            $bases_query
                ->whereIn('id', $selected_bases);
        }

        $bases = $bases_query->get();

        if(empty($campagne->base_id)){
            $base_id_query = "base_id IN (";
            foreach ($bases as $bid) {
                $base_id_query .= "$bid->id,";
            }
            $base_id_query = substr($base_id_query, 0, -1) . ")";

        } else {
            $base_id_query = "base_id = $campagne->base_id";
        }

        \DB::table('repoussoirs')
            ->where('campagne_id', $campagne->id)
            ->chunk($this->chunk_size, function ($repoussoirs) {
                foreach ($repoussoirs as $r) {
                    $this->to_exclude[$r->destinataire_id] = 1;
                }
            });

//        echo 'Repoussoirs : '.count($this->to_exclude)."\n";

        $planning_segment = \DB::table('plannings_segments')
            ->select('segment_id')
            ->where('planning_id', $planning->id)
            ->first();

        //AUTRES CRITERES RAJOUTES : ouvreurs / cliqueurs / thématique / geoloc / age
        if(!empty($planning_segment)) {
            $critQuery = \DB::table('segments_conditions')
                ->where('segment_id', $planning_segment->segment_id);

            $critQuery2 = clone $critQuery;
            $critQuery3 = clone $critQuery;
            $critQuery4 = clone $critQuery;
            $critQuery5 = clone $critQuery;
            $critQuery6 = clone $critQuery;

            //Si segment avec ouvreurs
            $conditions_ouv = $critQuery->where('condition_type','ouvreurs')->get();
            //Si segment avec cliqueurs
            $conditions_clic = $critQuery2->where('condition_type','cliqueurs')->get();
            //Si segment avec themes
            $conditions_theme = $critQuery3->where('condition_type', 'theme')->get();
            //Si segment avec geoloc
            $conditions_geoloc = $critQuery4->where('condition_type','geoloc')->get();
            //Si segment avec sexe
            $conditions_civilite = $critQuery5->where('condition_type','sexe')->get();
            //Si segment avec age
            $conditions_age = $critQuery6->where('condition_type','age')->get();

        }

        if(isset($conditions_theme) && !empty($conditions_theme)) {
            $campagne_ids_query = \DB::table('campagnes')
                ->select('id');
            if(!empty($campagne->base_id)) {
                $campagne_ids_query->where('base_id', $campagne->base_id);
            } else {
                $campagne_ids_query->whereIn('base_id', array_pluck($bases, 'id'));
            }
            $campagne_ids_query->whereIn('theme_id', array_pluck($conditions_theme,'condition_value'));
            $campagne_ids_query2 = clone $campagne_ids_query;
        }

        if(isset($conditions_ouv) && !empty($conditions_ouv)){
//            $ouvreursQuery = \DB::table('ouvertures')
//                ->select('destinataire_id');

            $ouvreursQuery = "SELECT distinct destinataire_id FROM ouvertures WHERE";

            foreach($conditions_ouv as $k => $co)
            {
//                echo '\n'.$co->condition_column.' - '. $co->condition_operator.' - '. $co->condition_value;
//                $ouvreursQuery->where($co->condition_column, $co->condition_operator, $co->condition_value) ;
                if($k != 0){
                    $ouvreursQuery .= " AND";
                }

                $ouvreursQuery .= " $co->condition_column $co->condition_operator '$co->condition_value'";
                if(empty($conditions_theme)) {
                    continue;
                }
                $campagne_ids_query->where('created_at', $co->condition_operator, $co->condition_value);
            }

            if(!empty($campagne_ids_query)){
                $campagne_ids = $campagne_ids_query->get();
//                $ouvreursQuery->whereIn('campagne_id', array_pluck($campagne_ids, 'id'));
                if(count($campagne_ids) > 0) {
                    $ouvreursQuery .= " AND campagne_id IN (" . implode(',', array_pluck($campagne_ids, 'id')) . ")";
                }
            }

            \Log::info("[CampagneDestinataires][P$planning->id] : Before DROP TABLE IF EXISTS ouvreurs_p$planning->id");
            \DB::statement("DROP TABLE IF EXISTS ouvreurs_p$planning->id");
//            echo "\nAfter DROP TABLE IF EXISTS ouvreurs_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : After DROP TABLE IF EXISTS ouvreurs_p$planning->id");
            \DB::statement("CREATE TABLE ouvreurs_p$planning->id (id INT AUTO_INCREMENT PRIMARY KEY, destinataire_id INT UNIQUE);");
//            echo "\nAfter CREATE TABLE ouvreurs_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : After CREATE TABLE ouvreurs_p$planning->id $ouvreursQuery" );
            \DB::statement("INSERT INTO ouvreurs_p$planning->id (destinataire_id) $ouvreursQuery");
//            echo "\nAfter INSERT INTO ouvreurs_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : After INSERT INTO ouvreurs_p$planning->id $ouvreursQuery");

            \DB::table("ouvreurs_p$planning->id")
                ->chunk($this->chunk_size, function ($ouvreurs) {
//                    echo "\nCHUNK 25K";
                    $this->desti_target = $this->desti_target + array_fill_keys(array_pluck($ouvreurs, 'destinataire_id'), '');
            });

            \DB::statement("DROP TABLE IF EXISTS ouvreurs_p$planning->id");
            \Log::info("[CampagneDestinataires][P$planning->id] : After DROP TABLE IF EXISTS ouvreurs_p$planning->id");

        }

        echo "\n COUNT destinataires OUV -- ".count($this->desti_target);

        if(isset($conditions_clic) && !empty($conditions_clic)){
            $cliqueursQuery = \DB::table('clics')->select('destinataire_id');

            foreach($conditions_clic as $cc)
            {
                $cliqueursQuery->where($cc->condition_column, $cc->condition_operator, $cc->condition_value);
                if(empty($conditions_theme)) {
                    continue;
                }
                $campagne_ids_query2->where('created_at', $cc->condition_operator, $cc->condition_value);
            }
            if(!empty($campagne_ids_query2)){
                $campagne_ids = $campagne_ids_query2->get();
                $cliqueursQuery->whereIn('campagne_id', array_pluck($campagne_ids, 'id'));
            }

            $cliqueursQuery->chunk($this->chunk_size, function ($cliqueurs) {
//                echo "\n -- cliqueurs -- ".count($cliqueurs);
                $this->desti_target = $this->desti_target + array_fill_keys(array_pluck($cliqueurs, 'destinataire_id'), '');
                //interset_key pour ne garder que les cliqueurs en commun par rapport aux
            });
        }

        echo "\n COUNT destinataires OUV+CLIC -- ".count($this->desti_target);
        if(count($this->desti_target) > 0){
            $this->more_criterion = true;
        }

        // ajout fabien
        if(isset($conditions_civilite) && !empty($conditions_civilite)) {
            foreach ($conditions_civilite as $civil) {
              $civil_query .= " AND (civilite = " . $civil->condition_value . " ) ";
            }
            $lesexe = array_pluck($conditions_civilite, 'condition_value');
        }

        if(isset($conditions_age) && !empty($conditions_age)) {
            // a voir plus propre
//            echo 'ligne 212';
            $conditiondusegmentmin = \DB::table('segments_conditions')
                ->where('segment_id', $planning_segment->segment_id)
                ->where('condition_operator','<')
                ->first();

            $conditiondusegmentmax = \DB::table('segments_conditions')
                ->where('segment_id', $planning_segment->segment_id)
                ->where('condition_operator','>')
                ->first();
            //
            $yearnow = intval(date('Y'));
            $conditiondusegmentminc = $yearnow - $conditiondusegmentmin->condition_value;
            $conditiondusegmentmaxc = $yearnow - $conditiondusegmentmax->condition_value;
            var_dump('Min : ' . $conditiondusegmentminc);
            var_dump('Max : ' . $conditiondusegmentmaxc);
            $age_query .= " AND (datenaissance < " . $conditiondusegmentminc . " AND datenaissance > " . $conditiondusegmentmaxc . " ) ";
        }
        // fin ajout fabien

        if(isset($conditions_geoloc) and count($conditions_geoloc) > 0) {
            $all_geoloc = array_pluck($conditions_geoloc, 'condition_value');

            $geoloc_query .= 'and (';
            foreach($all_geoloc as $ag)
            {
                $geoloc_query .= 'departement like "'.$ag.'%" or ';
            }
            $geoloc_query = substr($geoloc_query, 0, -4).")";
        }

        if(empty($campagne->base_id)) {
            //On créé la table provisoire des doublons pour la plannif donnée
//            echo "\nBefore DROP TABLE IF EXISTS doublons_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : Before DROP TABLE IF EXISTS doublons_p$planning->id");
            \DB::statement("DROP TABLE IF EXISTS doublons_p$planning->id");
//            echo "\nAfter DROP TABLE IF EXISTS doublons_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : After DROP TABLE IF EXISTS doublons_p$planning->id");
            \DB::statement("CREATE TABLE doublons_p$planning->id (id INT AUTO_INCREMENT PRIMARY KEY, destinataire_id INT UNIQUE, mail VARCHAR(255));");
//            echo "\nAfter CREATE TABLE doublons_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : After CREATE TABLE doublons_p$planning->id");
            \DB::statement("INSERT INTO doublons_p$planning->id (destinataire_id, mail) select d.id, d.mail from destinataires d , (select mail from destinataires where statut = 0 group by mail having count(mail) > 1) sr WHERE $base_id_query and d.mail = sr.mail order by rand();");
//            echo "\nAfter INSERT INTO doublons_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : After INSERT INTO doublons_p$planning->id");

            //Récupération des doublons -> et on les dédoublonne
            \DB::table("doublons_p$planning->id")->chunk($this->chunk_size, function ($doublons) {
                foreach ($doublons as $d) {
                    if ($this->more_criterion === true && !isset($this->desti_target[$d->destinataire_id])) {
                        continue;
                    }
                    $this->uniques_doublons[$d->mail] = $d->destinataire_id;
                    $this->doublons[$d->destinataire_id] = "";
                }
            });

            \DB::statement("DROP TABLE IF EXISTS doublons_p$planning->id");
//            echo "\nAfter DROP TABLE IF EXISTS doublons_p$planning->id";
            \Log::info("[CampagneDestinataires][P$planning->id] : After DROP TABLE IF EXISTS doublons_p$planning->id");

            $uniques_doublons = array_flip($this->uniques_doublons);
            unset($this->uniques_doublons);
//            echo "\n\n UNIQUE DOUBLONS";
//            var_dump($doubles);
//            var_dump(count($uniques_doublons));

            $doubles =  array_diff_key($this->doublons, $uniques_doublons);
            unset($this->doublons);
//            echo "\n\n DOUBLONS";
//            var_dump($doubles);
//            var_dump(count($doubles));

            $this->to_exclude = $this->to_exclude + $doubles ;
//            echo "\n\n TO EXCLUDE";
//            var_dump($this->to_exclude);
//            var_dump(count($this->to_exclude));
            \Log::info("[CampagneDestinataires][P$planning->id] : After unset uniques_doublons / doublons $planning->id");
        }


        $this->pdo = \DB::connection()->getPdo();
        $sql = "INSERT INTO tokens (base_id, destinataire_id, campagne_id, fai_id, planning_id, date_active, created_at, updated_at) VALUES (:base_id, :destinataire_id, :campagne_id, :fai_id, :planning_id, :date_active, :ts1, :ts2)";
        $stmt = $this->pdo->prepare($sql);
        $this->pdo->beginTransaction();
        $bulk_count = 0;

        //Volume par FAI pour la campagne
        $planvols = \DB::table('plannings_fais_volumes')
            ->select('volume','fai_id')
            ->where('planning_id',$planning->id)
            ->get();

        foreach($planvols as $planvol)
        {
            echo "// FAI ID // $planvol->fai_id ";

            $getMin = array();

            $this->fai_selected = 0;
            $done_fais[] = $planvol->fai_id;
            $lefai = Fai::find($planvol->fai_id);

            \Log::info("[CampagneDestinataires][P$planning->id] : // FAI ID // $lefai->nom ( $planvol->fai_id )");

            foreach($bases as $bse) {
                if ($this->remaining <= 0) {
                    break;
                }

                $total_fai_per_day = \DB::table('fai_sender')
                    ->selectRaw('sum(quota_left) as total')
                    ->where('fai_id', $lefai->id)
                    ->where('quota_left', '>', 0)
                    ->first();

                $getMin[] = $planvol->volume;

                if (!empty($total_fai_per_day)) {
                    $getMin[] = $total_fai_per_day->total;
                }

                if ($lefai->quota_campagne > 0) {
                    $getMin[] = $lefai->quota_campagne;
                }

                $how_many = min($getMin);
                \Log::info("// HOW MANY // $how_many");
                echo "// HOW MANY // $how_many";
                $this->how_many_to_select = $how_many;
                $chunk = min($how_many, $this->chunk_size);

                if ($chunk == 0) {
                    continue;
                }
                echo "Trying to find $how_many tokens \n";
                $targetDestinataire = \DB::table('destinataires')
                    ->select('id', 'fai_id')
                    ->where('statut', 0)
                    ->where('base_id', $bse->id)
                    ->where('fai_id', $planvol->fai_id);

		//CAS DMP
		if($bse->id == \DB::table("bases")->where('code','dmp')->first()->id){
		    \Log::info("[CampagnesDestinataires] DMP CAMPAIGN");
		    //On récupère le nom du thème
		    $theme = \DB::select('Select nom from themes where id = '.$campagne->theme_id.';');
		//On récupère le nom du thème en minuscule
		//Si nom composé,on ajoute des _
		$themeName = str_replace(" ","_",$theme[0]->nom); 
		if(strpos($themeName,"/") !== false){
			$dataSplitted = explode("/",$themeName);
			$themeName = $dataSplitted[0]."_".$dataSplitted[1];
		}
		
                $targetTokens = \DB::table('destinataires')
		    ->join('dmp_'.strtolower($themeName),'destinataires.id','=','dmp_'.strtolower($themeName))
                    ->select('id', 'fai_id')
                    ->where('statut', 0)
                    ->where('base_id', $bse->id)
                    ->where('fai_id', $fai->id);
	    }


//                    ->whereRaw('campagne_id IS null')
//                    ->where('date_active', $today);

                if (isset($all_geoloc)) {
                    $targetDestinataire->where(function ($targetDestinataire) use ($all_geoloc) {
                        foreach ($all_geoloc as $k => $ag) {
                            $targetDestinataire->Orwhere('departement', 'LIKE', $ag . '%');
                        }
                    });
                }

                if (isset($lesexe)) {
                    $targetDestinataire->where(function ($targetDestinataire) use ($lesexe) {
                        foreach ($lesexe as $k => $s) {
                            $targetDestinataire->Orwhere('civilite', $s);
                        }
                    });
                }

                if (isset($conditions_age) && !empty($conditions_age)) {
                    $targetDestinataire->where('datenaissance', '<', $conditiondusegmentminc)
                        ->where('datenaissance', '>', $conditiondusegmentmaxc);
                }

                $targetDestinataire->take($how_many)
//                    ->orderBy('priority')
                    ->orderByRaw('rand()')
                    ->chunk($chunk, function ($destinataires) use ($planning, $planvol) {
                        echo "\n -- in CHUNK --";
                        if ($this->fai_selected < $this->how_many_to_select) {
                            \Log::info("[CampagneDestinataires][P$planning->id] : // IF // Fai selected $this->fai_selected -- HowManyToSelect $this->how_many_to_select");
                            echo "// IF // Fai selected $this->fai_selected -- HowManyToSelect $this->how_many_to_select";
                            $this->fai_selected += $this->checkDestis($planning, $destinataires, ($planvol->volume - $this->fai_selected));
                        } else {
                            \Log::info("[CampagneDestinataires][P$planning->id] : // ELSE // Fai selected $this->fai_selected -- HowManyToSelect $this->how_many_to_select");
                            echo "// ELSE // Fai selected $this->fai_selected -- HowManyToSelect $this->how_many_to_select";
                            return false;
                        }
                    });
            }
        }

        // les FAI à quota
        $fais = Fai::where('quota_campagne','>',0)
            ->whereNotIn('id',$done_fais)
            ->orderBy('quota_campagne')
            ->get();

        foreach($fais as $fai)
        {
            echo "\n\nFAI (quota) : $fai->nom ( $fai->quota_campagne ) \n";
            \Log::info("[CampagneDestinataires][P$planning->id] : $fai->nom // FAI ID // $fai->id ( $fai->quota_campagne )");

            $getMin = array();
            $this->fai_selected = 0;
            $tries = 0;
            $done_fais[] = $fai->id;

            foreach($bases as $bse) {
                if ($this->remaining <= 0) {
                    break;
                }

                $getMin[] = $this->remaining;
                $getMin[] = $fai->quota_campagne;

                $planvol = \DB::table('plannings_fais_volumes')
                    ->select('volume')
                    ->where('planning_id', $planning->id)
                    ->where('fai_id', $fai->id)
                    ->first();

                $total_fai_per_day = \DB::table('fai_sender')
                    ->selectRaw('sum(quota_left) as total')
                    ->where('fai_id', $fai->id)
                    ->where('quota_left', '>', 0)
                    ->first();

                if (!empty($total_fai_per_day)) {
                    $getMin[] = $total_fai_per_day->total;
                }
                if (!empty($planvol)) {
                    $getMin[] = $planvol->volume;
                }
//            echo "Trying to find $how_many tokens \n";
                $how_many = min($getMin);
                $chunk = min($how_many, $this->chunk_size);
                $this->how_many_to_select = $how_many;

                if ($chunk == 0) {
                    continue;
                }

                $targetTokens = \DB::table('destinataires')
                    ->select('id', 'fai_id')
                    ->where('statut', 0)
                    ->where('base_id', $bse->id)
		    ->where('fai_id', $fai->id);
		//CAS DMP
		if($bse->id == \DB::table("bases")->where('code','dmp')->first()->id){
		    \Log::info("[CampagnesDestinataires] DMP CAMPAIGN");
		    //On récupère le nom du thème
		    $theme = \DB::select('Select nom from themes where id = '.$campagne->theme_id.';');
		//On récupère le nom du thème en minuscule
		//Si nom composé,on ajoute des _
		$themeName = str_replace(" ","_",$theme[0]->nom); 
		if(strpos($themeName,"/") !== false){
			$dataSplitted = explode("/",$themeName);
			$themeName = $dataSplitted[0]."_".$dataSplitted[1];
		}
		
                $targetTokens = \DB::table('destinataires')
		    ->join('dmp_'.strtolower($themeName),'destinataires.id','=','dmp_'.strtolower($themeName))
                    ->select('id', 'fai_id')
                    ->where('statut', 0)
                    ->where('base_id', $bse->id)
                    ->where('fai_id', $fai->id);
	    }
//                    ->whereRaw('campagne_id IS null')
//                    ->where('date_active', $today);

                if (isset($all_geoloc)) {
                    $targetTokens->where(function ($targetTokens) use ($all_geoloc) {
                        foreach ($all_geoloc as $k => $ag) {
                            $targetTokens->Orwhere('departement', 'LIKE', $ag . '%');
                        }
                    });
                }

                if (isset($lesexe)) {
                    $targetTokens->where(function ($targetTokens) use ($lesexe) {
                        foreach ($lesexe as $k => $s) {
                            $targetTokens->Orwhere('civilite', $s);
                        }
                    });
                }

                if (isset($conditions_age) && !empty($conditions_age)) {
                    $targetTokens->where('datenaissance', '<', $conditiondusegmentminc)
                        ->where('datenaissance', '>', $conditiondusegmentmaxc);
                }

                $targetTokens
                    ->orderByRaw('rand()')
                    ->take($how_many)
                    ->chunk($chunk, function ($tokens) use ($planning, $planvol) {
                        \Log::info("[CampagneDestinataires][P$planning->id] : // Fai selected $this->fai_selected -- HowManyToSelect $this->how_many_to_select");
//                    $this->checkTokens($planning, $tokens);
                        if (!empty($this->how_many_to_select)) {
                            if ($this->fai_selected < $this->how_many_to_select) {
                                $this->fai_selected += $this->checkDestis($planning, $tokens, ($this->how_many_to_select - $this->fai_selected));
                            }
                        } else {
                            // Ca veut dire qu'on n'a pas précisé de volume pour ce FAI
                            // -> donc pas de limite mis à part la limite du volume global
                            $this->checkDestis($planning, $tokens);
                        }
                    });
//            $this->remaining -= $this->selected;
                echo "REMAINING : " . $this->remaining . "\n";
            }
//            die();
        }
//        die();
//        echo "Le reste : Trying to find $this->remaining tokens\n";
        $cache_tokens = array();
        $pdo = \DB::connection()->getPdo();

        $fai_query = "";

        if(count($done_fais)>0) {
            echo "\n\n DONE_FAI -- ";
            $fai_query = "and fai_id not in (" . implode(',', $done_fais) . ")";
        }

        foreach($bases as $bse) {
           $query = "SELECT id, fai_id FROM destinataires WHERE statut = 0 and base_id = $bse->id $fai_query $geoloc_query $age_query $civil_query order by rand()";
	    //Cas DMP
	    if($bse->id == \DB::table("bases")->where('code','dmp')->first()->id){
		    \Log::info("[CampagnesDestinataires] DMP CAMPAIGN");
		    //On récupère le nom du thème
		    $theme = \DB::select('Select nom from themes where id = '.$campagne->theme_id.';');
		//On récupère le nom du thème en minuscule
		//Si nom composé,on ajoute des _
		$themeName = str_replace(" ","_",$theme[0]->nom); 
		if(strpos($themeName,"/") !== false){
			$dataSplitted = explode("/",$themeName);
			$themeName = $dataSplitted[0]."_".$dataSplitted[1];
		}
            	$query = "SELECT destis.id, destis.fai_id FROM destinataires destis,dmp_".strtolower($themeName)." dmp WHERE destis.statut = 0 AND dmp.destinataire_id = destis.id $fai_query $geoloc_query $age_query $civil_query order by rand() limit 100";
	    }
            \Log::info("CampagneDestinataires : $query");
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $now = \Carbon\Carbon::now();

            $base_id = $bse->id;
            $campagne_id = $planning->campagne_id;
            $planning_id = $planning->id;

            while ($destinataire = $stmt->fetchObject()) {

                if ($this->remaining > 0) {
                    $cache_tokens[] = $destinataire;
                }

                if (count($cache_tokens) >= 5000) {
                    $this->checkDestis($planning, $cache_tokens);
                    $cache_tokens = array();
                }
            }
        }

        if (count($cache_tokens) >= 0) {
            $this->checkDestis($planning, $cache_tokens);
            $cache_tokens = array();
        }

        $pdo = null;

        \Log::info("[CampagneDestinataires][P$planning->id] : After last checkTokens (Planning $planning->id / Campagne $planning->campagne_id)");

        echo "Total : $this->total\n";
        echo "Passes : $this->passe \n";
        echo "Total Sélectionnés : $this->total \n";
        echo "Remaining : $this->remaining \n";
        echo "Total tokens testés : ".$this->total_tested."\n";
        echo "Total exclus : ".$this->total_excluded."\n";

        \App\Helpers\Profiler::report('campagne_destinataires');

        $planning->tokens_at = date('Y-m-d H:i:s');
        $planning->nb_trials = 0;
        $planning->save();
        \Log::info("[CampagneDestinataires][P$planning->id] : C$planning->campagne_id -- terminé (Planning $planning->id / Campagne $planning->campagne_id)");

        $resultat_selected = \DB::table('tokens')
                                ->where('date_active', date('Y-m-d'))
                                ->where('planning_id',$planning->id)
                                ->count();
        \DB::statement("UPDATE plannings SET volume_selected ='" . $resultat_selected . "' WHERE id = '" . $planning->id ."'");
        \Log::info("[CampagneDestinataires][P$planning->id] :  Calcul volume réel selected {".$resultat_selected."} (Planning $planning->id / Campagne $planning->campagne_id)");

        \Log::info("Lancement du calcul des statistiques pour la plannif n : " . $planning->id);
        \Artisan::call('tokens:stats', ['planning_id' => $planning->id ]);

        // alert info
        $controlev = round(0.15 * $planning->volume);
        if($resultat_selected < $controlev or $resultat_selected == 0){
            $email = 'Un volume de ' . $planning->volume . ' a été demandé pour la campagne ' . $campagne->nom . ' mais le nombre réel de destinataires selectionnés est de : ' . $resultat_selected ;

            Mail::raw($email, function ($message, $campagne) {
                $destinataires = \App\Models\User::where('is_valid', 1)
                    ->where('user_group_id', 1)
                    ->where('email', '!=', "")
                    ->get();

                $message->from('dev@lead-factory.net', 'Tor')->subject('Avertissement : pb sur la sélection de tokens pour '.$campagne->ref.' [' . $campagne->id .'] le ' . date('d-m-Y H:i'));
                foreach($destinataires as $d){
                    $message->to($d->email);
                }
	   });
        }
    }

    function checkDestis($planning, $destinataires, $how_many = null) {
        $now = \Carbon\Carbon::now();
        $this->selected = 0;
        $this->passe++;

        $base_id = $planning->campagne->base_id;
        $campagne_id = $planning->campagne_id;
        $planning_id = $planning->id;

        if(empty($how_many)){
            $how_many = $this->remaining;
        }

        if ($this->remaining <= 0 or $how_many <= 0) {
            return;
        }

        $cache_ids = [];

        echo "Found ".count($destinataires)." destinataires\t remaining avant : $this->remaining \t";

        foreach($destinataires as $idx => $destinataire) {
            $this->total_tested++;

            if (isset($this->to_exclude[$destinataire->id])) {
                $this->total_excluded++;
                continue;
            }

            if($this->more_criterion === true && !isset($this->desti_target[$destinataire->id])){
                continue;
            }

            $this->to_exclude[$destinataire->id] = 1;

            if ($this->remaining > 0 && $how_many > 0) {
//                echo "Remain : ".$this->remaining."\n";
                $this->selected++;
                $this->remaining--;
                $how_many--;
                $this->total++;

                $cache_ids[] = ['base_id' => $base_id, 'destinataire_id' => $destinataire->id, 'campagne_id' => $campagne_id, 'fai_id' => $destinataire->fai_id, 'planning_id' => $planning_id, 'date_active' => $this->today, 'created_at' => $now, 'updated_at' => $now ];
//                echo "Passe $this->passe \t Planning ".$planning->id." \t idx ".str_pad($idx,3, ' ',  STR_PAD_LEFT) ." \t Reste $this->remaining \t Sélectionnés $this->selected \n";
            }
//            echo "\t Fai ".$token->fai_id." \t Token $token->id \t Reste ".($this->remaining - $this->selected)." \n";
            if (count($cache_ids) > 500) {
                $this->writeInserts($cache_ids);
                $cache_ids = array();
        //            $num = count($cache_ids);
        //            echo "Exclus $this->total_excluded \t Writing $num - Remaining après : $this->remaining";
            }
        }

        if (count($cache_ids) > 0) {
            $this->writeInserts($cache_ids);
//            $num = count($cache_ids);
//            echo "Exclus $this->total_excluded \t Writing $num - Remaining après : $this->remaining";
        }
        return $this->selected;
//        echo "\n";
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }

    private function writeInserts($ids) {
        \DB::table('tokens')->insert($ids);
//        \DB::table('tokens')->whereIn('id', $ids)->update(['campagne_id' => $campagne_id, 'planning_id' => $planning_id]);
    }

}
