<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DmpUnsubscribeDico extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dmp:import_unsubscribe {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Désabonne les adress du dico';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $file = storage_path().'/desinscrits/'.$this->argument('file');
      $filename = $this->argument('file');
      if (!is_file($file)) {
          $this->error('File not found : '.$file);
      }

      \Log::info("[BaseImportDesinscrits] : Import desinscrits à partir du fichier -- $file");

      $ts = date('Y-m-d H:i:s');

      $bulk_count = 0;

      set_time_limit(0);
      ini_set('max_execution_time', 0);

      $extension = \File::extension($file);
      $name = \File::name($file);

      $updateData = array();

      $fh = fopen($file, 'r');

      $total = 0;
      $inserted = 0;

      while (!feof($fh)) {

          $cells = [];
          $row = fgets($fh, 1024);
          $row = trim($row, "\r\n\t ");

          if (strpos($row, ';') !== false) {
              $cells = explode(';', $row);
          } elseif (strpos($row, '|') !== false) {
              $cells = explode('|', $row);
          } else {
              $cells[] = trim($row, " \r\n\t\"");
          }

          foreach($cells as $cell) {

              if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                  $total++;

                  $updateData[] = $cell;

                  $bulk_count++;
                  if (count($updateData) >= 5000) {
                      $this->writeUpdates($updateData, $filename);
                      $updateData = array();
                      $bulk_count = 0;
                  }

                  $inserted++;
                  continue;
              }
          }
      }

      if (count($updateData) > 0) {
          $this->writeUpdates($updateData, $filename);
          $updateData = array();
          $bulk_count = 0;
      }

      if (isset(\Auth::User()->id)) {
          $user_id = \Auth::User()->id;
      } else {
          $user_id = 1;
      }

      \Log::info("[BaseImportDesinscritsDMP] : End of Import desinscrits a partir fichier -- $file");
    }

    protected function getArguments()
    {
        return [
            ['file', InputArgument::REQUIRED, 'Path to the file']
        ];
    }

    private function writeUpdates($mails, $filename) {
        $now = date('Y-m-d H:i:s');
        \DB::table('dmp_dicomail')
        ->whereIn('mail', $mails)
        ->update(['statut' => 3, 'optout_at'=> $now, 'optout_file' => $filename]);
    }
}
