<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Classes\Routeurs\RouteurMailDrop;
use App\Models\Destinataire;
use App\Models\Sender;
use App\Models\Planning;
use App\Models\Campagne;

class DmpSendCampaignMaildrop extends Command {
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMaildrop();
    }

    protected $signature = 'campagne:dmp_send_maildrop {planning_id}';
    protected $description = 'Send a DMP campaign with MailDrop as router';

    /**
     * Execute the command.
     *
     * @return void
     */

    public function handle()
    {
        $planning = Planning::find($this->argument('planning_id'));
        $campaign = Campagne::find($planning->campagne_id);
        //Récupération du sender associé au planning
        $senderId = \DB::table('planning_senders')
            ->where('planning_id',$this->argument('planning_id'))
            ->first();
        $sender = null;
        $errorMessage = "[Command:DmpSendCampaignMaildrop] No sender found.(planning_id: $planning->id)";
        if(!empty($senderId)){
            $sender = Sender::find($senderId->sender_id);
        }
        if(empty($senderId)){
            \Log::info($errorMessage);
            exit();
        }

        if(!empty($sender)){
            $errorMessage = "[Command:DmpSendCampaignMaildrop] Error: No campaign found.";
            if(!empty($campaign)){
                $list = DB::table('dmp_planning_listid')
                    ->where('planning_id',$planning->id)->first();
                $listId = null;
                $errorMessage = "[Command:DmpSendCampaignMaildrop] Error: (Planning_id :$planning->id) No listid found in database.";
                if(empty($list)){
                    \Log::info($errorMessage);
                    exit();
                }

                if(!empty($list)){
                    $listId = $list->listid;
                    $cid = $this->routeur->createCampagne($campaign,$sender,[$listId]);
                    \Log::info("[Command::DmpSendCampaignMaildrop] campaign : $campaign->id");
                    \Log::info("[Command::DmpSendCampaignMaildrop] cid : $cid");
                    $send = $this->routeur->send_campagne($sender,$cid);
                    $message = "[Command:DmpSendCampaignMaildrop] Planning $planning->id finished";
                    $errorMessage = "[Command:DmpSendCampaignMaildrop] Error: Failed to send campaign (Campagne_id_id: $planning->campagne_id, Planning_id: $planning->id)";
                    if($send){
                        //On date l'envoi de la campagne
                        $planning->sent_at = date("Y-m-d H:i:s");
                        $volume_selected = DB::table('dmp_history')
                            ->where('planning_id',$planning->id)
                            ->where("relaunch_at",date("Y-m-d"))->count();
                        //Dans le cas où il n'y a pas de tokens disponible et l'envoi est tout de même lancé
                        if($planning->volume_selected == 0){
                            \Log::info("[Command:DmpSendCampaignMaildrop] Volume selected for this campaign: ".$volume_selected);
                            $planning->volume_selected = $volume_selected;
                        }
                        $planning->save();
                        //On retire au sender une campagne et le volume qui a été choisi
                        $sender->quota_left -= $volume_selected; 
                        $sender->nb_campagnes_left--;
                        $sender->save();
                        //Office d'historique
                        DB::table('campagnes_routeurs')->insert([
                            "sender_id"=>$sender->id,
                            "listid"=>$listId,
                            "planning_id"=>$planning->id,
                            "campagne_id"=>$campaign->id,
                            "created_at"=>date('Y-m-d  H:i:s'),
                            "updated_at"=>date('Y-m-d H:i:s')
                        ]);
                        \Log::info($message);
                    }

                    if(!$send){
                        \Log::info($errorMessage);
                    }
                }
            }

            if(empty($campaign)){
                \Log::info($errorMessage);
            }
        }
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
