<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class geolocmajvolume extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geoloc:majvolume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mise à jour des volumes restant [lancer apres geolocmajdestinataire]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $timestart=microtime(true);
        $nbrtotaldesti = null;
        $nbruniquedesti = null;
        $nbruniqueip = null;
        $nbruniqueville = null;
        $nbruniquepays = null;
        $nbruniquecodepostal = null;
        $pdo = \DB::connection()->getPdo();

        //   _____           _   _         _____  ____  _        _____  _     _   _            _
        //  |  __ \         | | (_)       / ____|/ __ \| |      |  __ \(_)   | | (_)          | |
        //  | |__) |_ _ _ __| |_ _  ___  | (___ | |  | | |      | |  | |_ ___| |_ _ _ __   ___| |_
        //  |  ___/ _` | '__| __| |/ _ \  \___ \| |  | | |      | |  | | / __| __| | '_ \ / __| __|
        //  | |  | (_| | |  | |_| |  __/  ____) | |__| | |____  | |__| | \__ \ |_| | | | | (__| |_
        //  |_|   \__,_|_|   \__|_|\___| |_____/ \___\_\______| |_____/|_|___/\__|_|_| |_|\___|\__|
        //
        //

        // --------------------------------------------------------- //



        $pdo = \DB::connection()->getPdo();

        echo 'Destinataire distinct : ' . "\n";

        $query = "SELECT distinct(destinataire_id) from ouvertures";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        while ($db = $stmt->fetchObject()) {
            $nbruniquedesti = $nbruniquedesti + 1;
            echo 'Destinataire_id distinct ' . $db->destinataire_id . "\n";

        }

        // --------------------------------------------------------- //

        echo 'IP distinct : ' . "\n";

        $pdo = \DB::connection()->getPdo();

        $query = "SELECT distinct(ip) from ouvertures";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        while ($db = $stmt->fetchObject()) {

            $nbruniqueip = $nbruniqueip + 1;
            echo 'IP distinct ' . $db->ip . "\n";

        }

        // --------------------------------------------------------- //


        echo 'Departement different trouvees : ' . "\n";

        $pdo = \DB::connection()->getPdo();
        $query = "SELECT distinct(departement) from destinataires";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        while ($db = $stmt->fetchObject()) {

            echo 'Code Postal distinct ' . $db->departement . "\n";
            $nbruniquecodepostal = $nbruniquecodepostal + 1;

        }


        // --------------------------------------------------------- //

        echo 'Villes differentes trouvees : ' . "\n";

        $pdo = \DB::connection()->getPdo();
        $query = "SELECT distinct(ville) from destinataires";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        while ($db = $stmt->fetchObject()) {

            echo 'Ville distinct ' . $db->ville . "\n";
            $nbruniqueville = $nbruniqueville + 1;

        }


        //
        //   _______    _     _                              _
        //  |__   __|  | |   | |                            | |
        //     | | __ _| |__ | | ___  ___    __ _  ___  ___ | | ___   ___
        //     | |/ _` | '_ \| |/ _ \/ __|  / _` |/ _ \/ _ \| |/ _ \ / __|
        //     | | (_| | |_) | |  __/\__ \ | (_| |  __/ (_) | | (_) | (__
        //     |_|\__,_|_.__/|_|\___||___/  \__, |\___|\___/|_|\___/ \___|
        //                                   __/ |
        //                                  |___/

        \DB::table('geoloc_departement')->truncate();
        echo "Nettoyage de la table geoloc_departement"."\n";
        $query = "SELECT distinct(base_id) from destinataires";
        $lapdo = $pdo->prepare($query);
        $lapdo->execute();

        foreach ($lapdo as $value) {

            echo "\r" . 'base_id ' . $value["base_id"] . "\n";

            $arraydepartement = array();
            // a voir
            $pdo = \DB::connection()->getPdo();

            $query = "SELECT departement from destinataires where base_id ='" . $value["base_id"] . "'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();

            while ($db = $stmt->fetchObject()) {

                $departementcourant = $db->departement;

                if(isset($arraydepartement[$departementcourant])){

                    $arraydepartement[$departementcourant] = $arraydepartement[$departementcourant] + 1;

                } else {

                    $arraydepartement[$departementcourant] = '1';

                }

            }

            while (list($key, $val) = each($arraydepartement)) {
                echo "Insertion $key => $val\n";

                \DB::statement("INSERT INTO geoloc_departement (code_departement, nombre_ip, code, base_id) VALUES ('" . $key . "','" . $val . "','". substr($key,0,2) ."','" . $value["base_id"] . "')");
            }


            // --------------------------------------------------------- //

            // \DB::table('geoloc_ville')->truncate();
            //
            // echo "Nettoyage de la table geoloc_ville"."\n";
            //
            // echo 'Villes differentes trouvees : ' . "\n";
            //
            // $arrayville = array();
            //
            // $pdo = \DB::connection()->getPdo();
            // // $query = "SELECT distinct(ville) from geoloc LIMIT 50000";
            // $query = "SELECT ville from destinataires LIMIT 50000";
            //
            // $stmt = $pdo->prepare($query);
            // $stmt->execute();
            //
            // while ($db = $stmt->fetchObject()) {
            //   $counterville = 0;
            //
            //   echo 'Ville : ' . $db->ville . "\n";
            //
            //   $villecourante = $db->ville;
            //
            // if(isset($arrayville[$villecourante])){
            //
            //   $arrayville[$villecourante] = $arrayville[$villecourante] + 1;
            //
            // } else {
            //
            // $arrayville[$villecourante] = '1';
            //
            // }
            // }
            //
            // while (list($key, $val) = each($arrayville)) {
            //     echo "Insertion $key => $val\n";
            //
            //     \DB::statement("INSERT INTO geoloc_ville (nom_ville, nombre_ip) VALUES ('" . addslashes($key) . "','" . $val . "')");
            // }

            // --------------------------------------------------------- //


            // suite avec la selection des themes et comptage
//            $arraytheme = array();
//            // a voir
//            $pdo = \DB::connection()->getPdo();
//
//            $query = "SELECT dernier_theme_id from destinataires where base_id ='" . $value["base_id"] . "'";
//            $stmt = $pdo->prepare($query);
//            $stmt->execute();
//
//            while ($db = $stmt->fetchObject()) {
//
//                $arraythemecourant = $db->dernier_theme_id;
//
//                if(isset($arraytheme[$arraythemecourant])){
//
//                    $arraytheme[$arraythemecourant] = $arraytheme[$arraythemecourant]  + 1;
//
//                } else {
//
//                    $arraytheme[$arraythemecourant]  = '1';
//
//                }
//
//            }
//
//            while (list($key, $val) = each($arraytheme)) {
//                echo "Les themes :  $key => $val\n";
//
//                // inserer theme id, base id, nombre $val
//                \DB::statement("INSERT INTO volume_theme (theme_id, nombre_ip, base_id) VALUES ('" . $key . "','" . $val . "','" . $value["base_id"] . "')");
//            }
        }







        //           __  __ _      _
        //    /\    / _|/ _(_)    | |
        //   /  \  | |_| |_ _  ___| |__   __ _  __ _  ___
        //  / /\ \ |  _|  _| |/ __| '_ \ / _` |/ _` |/ _ \
        // / ____ \| | | | | | (__| | | | (_| | (_| |  __/
        // /_/    \_\_| |_| |_|\___|_| |_|\__,_|\__, |\___|
        //                                      __/ |
        //                                     |___/

        // custom report
        $timeend=microtime(true);
        $time=$timeend-$timestart;
        $page_load_time = number_format($time, 3);

        echo "Debut du script: ".date("H:i:s", $timestart) ."\n";
        echo "Fin du script: ".date("H:i:s", $timeend) ."\n";
        echo "Script execute en " . $page_load_time . " sec " ."\n";

        echo 'Nombre total de destinataires : ' . $nbrtotaldesti ."\n";
        echo 'Nombre total de destinataires unique : ' . $nbruniquedesti ."\n";

        echo 'Nombre total de ip unique : ' . $nbruniqueip ."\n";

        echo 'Nombre total de pays unique : ' . $nbruniquepays ."\n";
        echo 'Nombre total de departement unique : ' . $nbruniquecodepostal ."\n";
        echo 'Nombre total de villes unique : ' . $nbruniqueville ."\n";

        // $nbrboucle = substr($nbrtotaldesti, 0,2);
        // echo 'Nombre de tour probable de boucle : ' . $nbrboucle ."\n";

        // echo 'ID dernier destinataire : ' . $nbrboucle ."\n";

        //
        //   _____                     _   _                _____ _        _
        //  |_   _|                   | | (_)              / ____| |      | |
        //    | |  _ __  ___  ___ _ __| |_ _  ___  _ __   | (___ | |_ __ _| |_ ___
        //    | | | '_ \/ __|/ _ \ '__| __| |/ _ \| '_ \   \___ \| __/ _` | __/ __|
        //   _| |_| | | \__ \  __/ |  | |_| | (_) | | | |  ____) | || (_| | |_\__ \
        //  |_____|_| |_|___/\___|_|   \__|_|\___/|_| |_| |_____/ \__\__,_|\__|___/
        //
        //

        $datemaj = date('Y-m-d H:i:s');

        \DB::table('geoloc_stats')->insert(
            [
                'total_destinataire_geoloc' => $nbrtotaldesti,
                'unique_destinataire_geoloc' => $nbruniquedesti,
                'nombre_ville_unique' => $nbruniqueville,
                'nombre_ip_unique' => $nbruniqueip,
                'nombre_pays_unique' => $nbruniquepays,
                'nombre_departement_unique' => $nbruniquecodepostal,
                'id_last_destinataire' => null,
                'created_at' => $datemaj,
                'updated_at' => $datemaj

            ]
        );
    }


}
