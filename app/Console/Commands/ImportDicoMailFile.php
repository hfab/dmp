<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportDicoMailFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:dico {le_fichier}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importe fichier cliqueur';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      set_time_limit(0);
      ini_set('max_execution_time', 0);

      // $file = $this->argument('le_fichier');

      $ts = date('Y-m-d H:i:s');

      $pdo = \DB::connection()->getPdo();
      $sql = "INSERT IGNORE INTO dmp_dicomail (mail, hash, source, created_at, updated_at) VALUES (:mail, :hash, :source, :ts1, :ts2)";
      $stmt = $pdo->prepare($sql);
      $pdo->beginTransaction();

      $bulk_count = 0;

      $file = storage_path().'/listes/'.$this->argument('le_fichier');
      \Log::info("Import Dico du fichier -- '.$file");
      $extension = \File::extension($file);
      $name = \File::name($file);
      $fh = fopen($file, 'r');
      $total = 0;
      $inserted = 0;
      $rejected = 0;
      $already = 0;

      $source = str_replace(".$extension",'',$name);

      $bulk = [];
      while (!feof($fh)) {
          $cells = [];

          $row = fgets($fh, 1024);
          $row = trim($row, "\r\n\t ");

          if (strpos($row, ';') !== false) {
              $cells = explode(';', $row);
          } elseif (strpos($row, '|') !== false) {
              $cells = explode('|', $row);
          } else {
              $cells[] = trim($row, " \r\n\t\"");
          }

          foreach($cells as $cell) {

              if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                  $total++;
                  $insertData = [$cell, md5($cell), $source, $ts, $ts];
                  $stmt->execute($insertData);

                  $bulk_count++;
                  if ($bulk_count >= 250) {
                      $pdo->commit();
                      $pdo->beginTransaction();
                      $bulk_count = 0;
                  }

                  $inserted++;
                  continue;
              } else {
                  $rejected++;
              }
          }
      }

      $pdo->commit();


      \Log::info("FIN Import Destinataires dans la base (QUEUE COMMAND) du fichier -- '.$file");



    }

    protected function getArguments()
    {
        return [
            ['le_fichier', InputArgument::REQUIRED, 'Nom du fichier']
        ];
    }
}
