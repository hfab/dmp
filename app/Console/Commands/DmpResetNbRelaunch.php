<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Theme;

class DmpResetNbRelaunch extends Command 
{
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$themes = Themes::all();
		$database = "Tables_in_".env('DB_DATABASE');
		foreach($tables as $table){
			array_push($tablesNames,$table->$database);
		}

		foreach($themes as $theme){
			if($this->checkTable("dmp_".$theme->getThemeName(),$database)){
				DB::statement("UPDATE dmp_".$theme->getThemeName()." SET nbRelaunch = 0");
			}
		}
	}

	function checkTable($str,$arr){
		for($index = 0 ;$index<count($arr);$index++){
			if($str == $arr[$index]){
				return true;
			}
		}
		return false;
	}

}
