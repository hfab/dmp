<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class RegroupMail extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $signature = "regroup:mail";
    protected $description = "Regroup dmp_matched mail by themes";
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */

    function getValue($str,$arr){
        for($index = 0 ;$index<count($arr);$index++){
            if($str == $arr[$index]){
                return true;
            }
        }
        return false;
    }

    function removeAccents($str){
        $str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
        $str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
        $str = preg_replace('#&[^;]+;#', '', $str);
        return $str;
    }

    public function handle()
    {
        //TODO:Lancer ceci Mensuellement
        $themes = DB::select("SELECT nom,id from themes;");
        $tables = DB::select('SHOW TABLES');
        $tablesNames = [];
        $database = "Tables_in_".env('DB_DATABASE');

        foreach($tables as $table){
            array_push($tablesNames,$table->$database);
        }

        foreach($themes as $theme){
            $mails =  [];
            //on retire les espaces et les / par des _
            $themeName = str_replace(" ","_",$theme->nom);
            if(strpos($themeName,"/") !== false){
                $dataSplitted = explode("/",$themeName);
                $themeName = $dataSplitted[0]."_".$dataSplitted[1];
            }

            $themeName = $this->removeAccents($themeName);
            $arrayMails= DB::select("SELECT distinct dmp_dicomail.id,dmp_dicomail.hash
                FROM dmp_matched,dmp_dicomail,themes
                WHERE dmp_matched.theme_id = $theme->id
                AND dmp_dicomail.hash = dmp_matched.destinataire_hash
                AND dmp_matched.nb_has_ordered < dmp_matched.nb_has_not_ordered
                AND dmp_matched.theme_id = themes.id");

            $message = "[Command::RegroupMail] Empty destinataires theme_id = $theme->id";

            if(!empty($arrayMails)){
                $message = "[Command::RegroupMail] Found destinataires (number:".count($arrayMails).") when has_ordered = 0 with theme_id = $theme->id ($themeName)\n";
            }

            \Log::info($message);

            foreach($arrayMails as $elem){
                array_push($mails, array('destinataire_id'=>$elem->id,'destinataire_hash'=>$elem->hash));
            }

            if($this->getValue("dmp_".strtolower($themeName),$tablesNames)) {
                echo "Table existe déjà\n";
                if(!empty($mails)){
                    //DB::table('dmp_'.strtolower($themeName))->insert($mails);
                    foreach($mails as $mail){
                        $dest = DB::table('dmp_'.strtolower($themeName))->where('destinataire_id',$mail["destinataire_id"])->first();
                        if(empty($dest)){
                            DB::table('dmp_'.strtolower($themeName))->insert($mail);                        }
                    }
                }
            }
            else{
                if(!empty($mails)){
                    echo "On crée la table\n";
                    DB::statement("CREATE TABLE dmp_".strtolower($themeName)." (
                        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                        destinataire_id INTEGER NOT NULL,
                        destinataire_hash VARCHAR(255) NOT NULL,
                        nbRelaunch INTEGER DEFAULT 0);");
                    DB::table('dmp_'.strtolower($themeName))->insert($mails);
                }
            }
        }
    }
}
