<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OuverturesStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ouvertures:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mise a jour des ouvertures';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("DEBUT - Calcul des ouvertures OK - " .  date('Y-m-d'));
        // $ouv_24 = \App\Models\Ouverture::where('created_at','>',date('Y-m-d H:i:s',time()-86400))->count();
        $ouv_24 = \App\Models\Ouverture::where('updated_at','LIKE', date('Y-m-d') . '%')->count();
        // SELECT id, created_at, max(count) FROM `ouvertures_stats` where count > 0 group by created_at
        // bien laisser seulement la date
        \DB::statement("INSERT INTO ouvertures_stats (count,created_at,updated_at) VALUES ('". $ouv_24 . "','". date("Y-m-d") ."','". date("Y-m-d")."')");
        \DB::table('ouvertures_stats_bases')->truncate();
        // par base
        $bases = \DB::table('bases')->get();
        foreach ($bases as $v) {
          $arrayid = array();
          $campagnesouv =  \DB::table('campagnes')->select('id')->where('base_id', $v->id)->get();
          echo $v->nom . " \n";
          foreach ($campagnesouv as $c) {
            $arrayid[] = $c->id;
          }

          $ouvbase = \App\Models\Ouverture::where('updated_at','LIKE', date('Y-m-d') . '%')->whereIn('campagne_id', $arrayid)->count();
          \DB::statement("INSERT INTO ouvertures_stats_bases (base_id,count,created_at,updated_at) VALUES ('". $v->id . "','". $ouvbase . "','". date("Y-m-d") ."','". date("Y-m-d")."')");
          \Log::info("FIN - Calcul des ouvertures par base - ". $v->nom ." Nombre : " . $ouvbase);
        }


        \Log::info("FIN - Calcul des ouvertures OK - " .  date('Y-m-d'));
    }
}
