<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Profiler;

class CheckEmailBl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'base:checkbl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cette commande met à jour le status 99 des destinataires en BL via liste';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        \DB::disableQueryLog();
        Profiler::start("Check BL Email");
        $nombremail99 = 0;
        $nbrligne = 0;
        $lemail = null;
        $counter = 0;
        $tabin = array();
        $fichierliste = fopen(storage_path('listes/bl/pdm_bl_prod.txt'), "r");
        echo "Nombre d'email dans le fichier : " . $this->compter_ligne_fichier(storage_path('listes/bl/pdm_bl_prod.txt')) . "\n";
        $nbrligne = $this->compter_ligne_fichier(storage_path('listes/bl/pdm_bl_prod.txt'));
        while(!feof($fichierliste)) {
        $lemail = fgets($fichierliste);
        $lemail = $this->supprimer_caract($lemail);
        $counter = $counter + 1;

          $tabin[] = "'" . $lemail . "'";
          if(count($tabin) > 2500){
            \DB::statement("UPDATE IGNORE destinataires SET statut ='99' WHERE mail IN (" . implode(',',$tabin) . ")");
            $tabin = array();
          }

        echo 'Email ' . $counter . '/' . $nbrligne . "\r";
        }

        echo 'Dernier chunck' . "\r";
        \DB::statement("UPDATE IGNORE destinataires SET statut ='99' WHERE mail IN (" . implode(',',$tabin) . ")");
        fclose($fichierliste);
        Profiler::report("Check BL Email");

    }

    function supprimer_caract($string){
    	$string = str_replace(' ', '', $string);
    	$string = str_replace("\n", '', $string);
    	$string = str_replace(chr(10), '', $string);
    	$string = str_replace(chr(13), '', $string);
  	  return $string;
  }

    function compter_ligne_fichier($fichier){
      $tabFich = file($fichier);
      $nbLignes = count($tabFich);
      return $nbLignes;
    }
}
