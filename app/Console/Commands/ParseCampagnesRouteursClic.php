<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurSmessage;

class ParseCampagnesRouteursClic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:clic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse la table campagnes_routeurs et recupère les cliqueurs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      \App\Helpers\Profiler::start('Debut recup cliqueurs');
      $today = date("Y-m-d");
      $searchdate = date("Y-m-d", strtotime("-5 day"));

      // var_dump($searchdate);
      // die();

      // $campagnes_routeurs = \DB::table('campagnes_routeurs')->where('created_at','LIKE','2016-07-19%')->get();
      // $campagnes_routeurs = \DB::table('campagnes_routeurs')->where('created_at','LIKE', $searchdate . '%')->get();
      $campagnes_routeurs = \DB::table('campagnes_routeurs')->whereBetween('created_at', [$searchdate, $today])->get();
      // die();
      foreach ($campagnes_routeurs as $v) {

        if($v->planning_id !== 0){

        $lesender = \DB::table('senders')->where('id',$v->sender_id)->first();
        $lerouteur = \DB::table('routeurs')->where('id',$lesender->routeur_id)->first();
        $leplanning = \DB::table('plannings')->where('id',$v->planning_id)->first();
        $lacampagne = \DB::table('campagnes')->where('id',$leplanning->campagne_id)->first();
        // var_dump($lacampagne);
        var_dump($lerouteur->nom);

        if($lerouteur->nom == 'Maildrop'){

          // on met en pause pour debug sm
           $routeurmaildrop = new RouteurMaildrop();
           $routeurmaildrop->getClicks($lesender->password, $v->cid_routeur);


        // a debug car invalid argument ligne 674 class
        } else if ($lerouteur->nom == 'Smessage') {

          $routeursmessage = new RouteurSmessage();
          $retour = $routeursmessage->recup_cliqueurs_fichier($lesender->nom, $lesender->password, $v->cid_routeur);

        } else if ($lerouteur->nom == 'Phoenix'){

          // on met en pause pour debug sm
           $routeur = new RouteurPhoenix();
           $routeur->getClicks($lesender->password, $v->cid_routeur);

        }
        // une seule fois le code
        $fichier = 'clic_cid_' . $v->cid_routeur . '_date_' . $today . '.csv';
        // pour debug sm
        $this->call('import:fcliqueur',array('le_fichier' => $fichier, 'info_envoi' => $lacampagne));
        }
      }
      \App\Helpers\Profiler::report();
    }
}
