<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class QualifBase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qualif:base';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Determine le sexe via prenom et un fichier';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lefichier = "prenoms.csv";
        // j'ouvre mon fichier
        $fichiercsv = fopen(storage_path("listes/")  . $lefichier , "r");
        $csv = array_map('str_getcsv', file(storage_path("listes/")  . $lefichier));
        $tableautravail = array();
        $c = null;
        $civilite = null;
        $desti_3 = null;

         foreach ($csv as $v){
           $c = explode(";",$v[0]);

           if($c[2] === 'F'){
             $civilite = 0;
           }

           if($c[2] === 'M'){
             $civilite = 1;
           }

           $tableautravail[strtolower($c[0])] = $civilite;
         }

         $bases = \DB::table('bases')->select('id')->get();

         foreach ($bases as $b) {
           $desti_3 = \DB::table('destinataires')
           ->where('base_id', $b->id)
           ->where('civilite', 3)
           ->get();

           if(count($desti_3) > 0){
             $chunckrqt = array_chunk($desti_3, 100);

             foreach ($chunckrqt as $lechunck) {
               foreach ($lechunck as $i) {
                  if(in_array($i->prenom,$tableautravail)){
                    if(isset($tableautravail[$i->prenom])){

                      echo "MAJ adresse " .  $i->mail . "\n";
                      \DB::table('destinataires')->where('id', $i->id)->update(['civilite' => $tableautravail[$i->prenom]]);

                  }
                 }
               }
             }
           }
         }
    }
}
