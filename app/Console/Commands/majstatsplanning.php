<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class majstatsplanning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geoloc:majstatsplanning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Met a jour les statistiques de la page planning en fonction de la table tokens et geoloc_departement';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // remplissage de la table pour faire les add
        //
        // $lesthemes = \DB::table('themes')->get();
        //
        // foreach ($lesthemes as $letheme) {
        //   echo 'Le theme : ' . $letheme->nom . ' id : '. $letheme->id;
        //
        //   // \DB::statement("UPDATE destinataires SET ville = ");
        //
        //   }
        //
        //   // init var
        //
        //   $tableauip = array();
        //   $tableauinfo = array();
        //
        //
        //
        //   $nbrtotaldesti = \DB::table('ouvertures')->whereNotNull('ip')->distinct()->count();
        //
        //   $offset = 10000;
        //
        //   $long = strlen($nbrtotaldesti) - substr_count($offset,'0');
        //
        //   echo $nbrtotaldesti;
        //   $nbretape = substr($nbrtotaldesti,0,$long);
        //   $skip = 0;
        //
        //   for ($i = 1; $i <= $nbretape; $i++) {
        //
        //   $lignedb = \DB::table('ouvertures')->whereNotNull('ip')->skip($skip)->take($offset)->get();
        //
        //     foreach ($lignedb as $db) {
        //
        //       // echo $db->ip;
        //       echo "i = " . $i . "\n";
        //
        //       if(in_array($db->ip,$tableauip)){
        //
        //         echo "\n" . 'Deja dans le tableau passage au suivant'. "\n";
        //
        //       } else {
        //
        //         echo  "\n" . 'On ajoute en + et rqt sql update'. "\n";
        //         $tableauip[] = $db->ip;
        //
        //         echo $db->campagne_id .  "\n";
        //
        //         // on va chercher le theme id de la campagne
        //
        //         $theme = \DB::table('campagnes')->where('id', $db->campagne_id)->first();
        //         echo 'Le theme id est : ';
        //         echo $theme->theme_id;
        //
        //
        //         // $tableauinfo[$idtheme] =
        //
        //         // tableau multi dimension
        //
        //         $idtheme = $theme->theme_id;
        //         if(isset($tableauinfo[$idtheme])){
        //
        //           $tableauinfo[$idtheme] = $tableauinfo[$idtheme] + 1;
        //
        //         } else {
        //
        //           $tableauinfo[$idtheme] = '1';
        //
        //           }
        //
        //           if($i == 2){
        //           // on affiche les valeurs pour le debug
        //           while (list($key, $val) = each($tableauinfo)) {
        //               echo "Insertion pour le themeid $key => avec commme valeur $val\n";
        //
        //             //  \DB::statement("INSERT INTO geoloc_departement (code_departement, nombre_ip, code, base_id) VALUES ('" . $key . "','" . $val . "','". substr($key,0,2) ."','" . $value["base_id"] . "')");
        //           }
        //           exit;
        //         }
        //
        //
        //         }
        //       }
        //
        //    $skip = $skip + $offset;
        //     }


        // \DB::table('planning_theme')->truncate();

        // selection des différentes ip
        // $pdotheme = \DB::connection()->getPdo();
        // $query = "SELECT distinct(ip) from ouvertures";
        // $lapdo = $pdotheme->prepare($query);
        // $lapdo->execute();

        //   foreach ($lapdo as $value) {

        // echo 'lecture ' . $value;

        //     var_dump($value->ip);

        // echo "\r" . $value->ip;

        //     }


        // pour chaque ip
        // recup destinataire id
        // campagne id
// exit;

        \DB::table('planning_geoloc')->truncate();
        // recuperation des differentes bases
        $pdo = \DB::connection()->getPdo();
        $query = "SELECT id from bases";
        $lapdo = $pdo->prepare($query);
        $lapdo->execute();

        foreach ($lapdo as $value) {

            echo "\r" . 'base_id ' . $value["id"] . "\n";


            // recuperation des diffents departements
            $lesdepartements = \DB::table('geoloc_departement')
                ->where('base_id',$value["id"])
                ->where('nombre_ip','>','50')
                ->groupBy('code')
                ->orderBy('nombre_ip','desc')
                ->get();

            foreach ($lesdepartements as $departement) {

                echo 'Code : ' . $departement->code . "\n";

                // $query = "SELECT id,fai_id,base_id,sender_id,origine, ville, departement FROM destinataires WHERE statut < 3 and tokenized_at < '$date_active' and sender_id > 0";
                // $stmt = $pdo->prepare($query);
                // $stmt->execute();

                $nbrtotal = \DB::table('tokens')
                    ->where('base_id', $value["id"])
                    ->where('geoloc',$departement->code)
                    ->where('priority','1')
                    ->count();

                $nbrnonnull =\DB::table('tokens')
                    ->where('base_id', $value["id"])
                    ->whereNotNull('campagne_id')
                    ->where('geoloc',$departement->code)
                    // ->where('priority','1')
                    // ->distinct('destinataire_id')
                    ->count();

                $planningid = null;

                $nombre_restant = $nbrtotal - $nbrnonnull;

                // netoyage de la table
                // \DB::table('planning_geoloc')->truncate();
                \DB::statement("INSERT INTO planning_geoloc (base_id, departement, nombre_total_mail, nombre_select_mail, nombre_restant) VALUES ('" . $value["id"] . "','" . $departement->code . "','" . $nbrtotal ."','" . $nbrnonnull . "','" . $nombre_restant . "')");
                echo "\r" . 'nombre total ' . $nbrtotal . "\n";
                echo "\r" . 'nombre deja select ' . $nbrnonnull . "\n";
                // echo "INSERT INTO planning_geoloc (planning_id, departement, nombre_total_mail, nombre_select_mail, nombre_restant) VALUES ('". $planningid . "','" . $departement->code . "','" . $nbrtotal ."','" . $nbrnonnull . "','" . $nombre_restant . "')";

            }

        }



        //   // ajout 08/01/16
        //
        //   // fct pour les themes
        //   \DB::table('planning_theme')->truncate();
        //   foreach ($lapdo as $value) {
        //
        //     echo "\r" . 'base_id ' . $value["base_id"] . "\n";
        //
        //
        // // recuperation des diffents departements
        //     $lesthemes = \DB::table('planning_volume')->where('base_id',$value["base_id"])->where('nombre_ip','>','50')->orderBy('nombre_ip','desc')->get();
        //
        //     foreach ($lesthemes as $theme) {
        //
        //     echo 'Code : ' . $theme->theme_id . "\n";
        //
        //     // $query = "SELECT id,fai_id,base_id,sender_id,origine, ville, departement FROM destinataires WHERE statut < 3 and tokenized_at < '$date_active' and sender_id > 0";
        //     // $stmt = $pdo->prepare($query);
        //     // $stmt->execute();
        //
        //     $nbrtotal = \DB::table('tokens')->where('base_id', $value["base_id"])->where('geoloc','LIKE',$departement->code . '%')->where('priority','1')->count();
        //     $nbrnonnull =\DB::table('tokens')->whereNotNull('campagne_id')->where('base_id', $value["base_id"])->where('geoloc','LIKE',$departement->code . '%')->count();
        //     // whereNotNull
        //     $planningid = null;
        //
        //     $nombre_restant = $nbrtotal - $nbrnonnull;
        //
        //     // netoyage de la table
        //     // \DB::table('planning_geoloc')->truncate();
        //     \DB::statement("INSERT INTO planning_geoloc (base_id, departement, nombre_total_mail, nombre_select_mail, nombre_restant) VALUES ('" . $value["base_id"] . "','" . $departement->code . "','" . $nbrtotal ."','" . $nbrnonnull . "','" . $nombre_restant . "')");
        //     echo "\r" . 'nombre total ' . $nbrtotal . "\n";
        //     echo "\r" . 'nombre deja select ' . $nbrnonnull . "\n";
        //     // echo "INSERT INTO planning_geoloc (planning_id, departement, nombre_total_mail, nombre_select_mail, nombre_restant) VALUES ('". $planningid . "','" . $departement->code . "','" . $nbrtotal ."','" . $nbrnonnull . "','" . $nombre_restant . "')";
        //
        //     }
        //
        //   }


        // pour chaque base_id

        // pour chaque departement

        // compter le campagne id NULL
        // compter le campagne id non NULL

        // recuperation planning id via campagne_id
        // insert

    }
}
