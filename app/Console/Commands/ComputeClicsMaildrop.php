<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMaildrop;
use App\Models\Routeur;

class ComputeClicsMaildrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clics:compute_maildrop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les cliqueurs de Maildrop et lance job : insertion cliqueurs dans la DB > table > clics.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("[ClicsMaildrop] : Début");
        $today = date('Ymd');
        $routeur = new RouteurMaildrop();
        // je recupere dans campagne routeur les created ad du jour
        $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-1 week'));

        $maildrop = Routeur::where('nom','Maildrop')->first();

        // on récupère que les senders de MD
        $senders = \DB::table('senders')->select('id')->where('routeur_id', $maildrop->id)->get();

        $informations = \DB::table('campagnes_routeurs')
            ->where('created_at','>',$two_weeks_ago)
            ->whereIn('sender_id', array_pluck($senders, 'id'))
            ->get();

        $cliqueurs = array();

        foreach ($informations as $lignecampagnerouteur)
        {
            \Log::info("ClicsMaildrop : Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id");
            if(empty($lignecampagnerouteur->cid_routeur)){
                \Log::info("ClicsMaildrop : empty cid_routeur (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id)");
                continue;
            }

            $lesender = \DB::table('senders')->where('id', $lignecampagnerouteur->sender_id)->first();
            $lacampagne = \DB::table('campagnes')->where('id', $lignecampagnerouteur->campagne_id)->first();

            $clickers = $routeur->getClicksDesti($lesender->password, $lacampagne, $lignecampagnerouteur->cid_routeur);

            if(!isset($cliqueurs[$lignecampagnerouteur->campagne_id])){
                $cliqueurs[$lignecampagnerouteur->campagne_id] = array();
            }

            $cliqueurs[$lignecampagnerouteur->campagne_id] = $cliqueurs[$lignecampagnerouteur->campagne_id] + $clickers;

        }

        // $filename = "/cliqueurs/clics_md_$today.csv";
        $file = "clics_md_$today.csv";
        $filename = storage_path() . '/cliqueurs/clics_md_' . $today . '.csv';
        $handle = fopen($filename, 'w');
        $content = "";

        $compteur = 0;
        foreach($cliqueurs as $lacampagneid => $clk){
            $lacampagne = \DB::table('campagnes')->where('id', $lacampagneid)->first();

            foreach($clk as $c) {
                $content .= "$lacampagne->id;$c;$lacampagne->theme_id\n";
                $compteur++;
            }
        }
        fwrite($handle, $content);
        fclose($handle);
        \Log::info("[ClicsMaildrop] : finished (Count : $compteur)");
        \Log::info("[ClicsMaildrop] : launch of BaseImportDesinscrits command");
        \Artisan::call('base:import_clics', ['file' => $file]);
        \Log::info("[ClicsMaildrop] : launch of BaseImportDesinscrits command OK");

    }
}
