<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMaildrop;
use App\Models\Routeur;

class CampagneUnsubscribeMaildrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:unsubscribe_maildrop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les désabonnés de Maildrop et lance le job de désinscription sur toutes les bases.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("[CampagneUnsubscribeMaildrop] : Début");
        $today = date('Ymd');
        $routeur = new RouteurMaildrop();
        // je recupere dans campagne routeur les created ad du jour
        $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-2 weeks'));

        $maildrop = Routeur::where('nom','Maildrop')->first();

        // on récupère que les senders de MD
        $senders = \DB::table('senders')->select('id')->where('routeur_id', $maildrop->id)->get();

        $informations = \DB::table('campagnes_routeurs')
            ->where('created_at','>',$two_weeks_ago)
            ->whereIn('sender_id', array_pluck($senders, 'id'))
            ->get();

        $compteur = 0;
        $handle = fopen(storage_path()."/desinscrits/unsubscribe_md_$today.csv", 'w');

        foreach ($informations as $lignecampagnerouteur)
        {
            \Log::info("CampagneUnsubscribeMaildrop : Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id");
            if(empty($lignecampagnerouteur->cid_routeur)){
                \Log::info("CampagneUnsubscribeMaildrop : empty cid_routeur (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id)");
                continue;
            }

            $lesender = \DB::table('senders')->where('id', $lignecampagnerouteur->sender_id)->first();
            $lacampagne = \DB::table('campagnes')->where('id', $lignecampagnerouteur->campagne_id)->first();

            $from_since = null;

            $last_stats = \DB::table('last_stats')
                ->where('planning_id', $lignecampagnerouteur->planning_id)
                ->where('sender_id', $lignecampagnerouteur->sender_id)
                ->where('type', 'desinscrits')
                ->first();

           if(!empty($last_stats)){
               $from_since = $last_stats->since_id;
           }

           $desinscrits = $routeur->getDesinscritsNew($lesender->password, $lacampagne, $lignecampagnerouteur->cid_routeur, $from_since);
            if(empty($desinscrits['desinscrits'])){
                continue;
            }

            if(empty($last_stats) && !empty($desinscrits['last_id'])){
                \DB::table('last_stats')->insert([
                    'planning_id' => $lignecampagnerouteur->planning_id,
                    'sender_id' => $lignecampagnerouteur->sender_id,
                    'type' => 'desinscrits',
                    'since_id' => $desinscrits['last_id']
                ]);
            } else {
                \DB::table('last_stats')->where('planning_id', $lignecampagnerouteur->planning_id)
                ->where('sender_id', $lignecampagnerouteur->sender_id)
                ->where('type', 'desinscrits')
                ->where('since_id', $desinscrits['last_id'])
                ->update(['since_id' => $desinscrits['last_id']]);
            }

            $content = "";
            foreach($desinscrits['desinscrits'] as $unsub => $date_unsub){
                $content .= "$unsub;$date_unsub;$lacampagne->id;$lignecampagnerouteur->planning_id\n";
                $compteur ++;
            }
            fwrite($handle, $content);
        }

        fclose($handle);

        \Log::info("[CampagneUnsubscribeMaildrop] : finished (Count : $compteur)");
        \Log::info("[CampagneUnsubscribeMaildrop] : launch of BaseImportDesinscrits command");
        \Artisan::call('dmp:import_unsubscribe', ['file' => "unsubscribe_md_$today.csv"]);
        // \Artisan::call('dmp:import_unsubscribe', ['file' => "unsubscribe_mb_api_$today.csv"]);
        \Log::info("[CampagneUnsubscribeMaildrop] : launch of BaseImportDesinscrits command OK");

        // \Log::info("[CampagneUnsubscribeMaildrop] : launch of BaseInsertDesinscrits command");
        // \Artisan::call('base:insert_desinscrits', ['file' => "unsubscribe_md_$today.csv"]);
        // \Log::info("[CampagneUnsubscribeMaildrop] : launch of BaseInsertDesinscrits command OK");

    }
}
