<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Campagne;
use App\Models\Sender;
use App\Models\Routeur;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurMindbaz;

class relaunchGo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relaunch:go';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les relaunch et les lance sur le compte';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMaildrop();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


      \Log::info("[CampagneRelaunchMD]");
      $maildrop = Routeur::where('nom','Maildrop')->first();
      $daydate = date("Y-m-d");
      $H = date("H");
      $partneri = \DB::table('dmp_partnersites')
      ->get();

      foreach($partneri as $partner){

        $check_relaunch = \DB::table('dmp_relaunch')
        ->where('state','=',0)
        ->where('site_id',$partner->id)
        ->get();

        // var_dump('Check_relaunch');
        // var_dump($check_relaunch);

        $content = "email;first_name;last_name" . "\n";;
        $lecampagneid=0;
        foreach ($check_relaunch as $relaunch) {

          // var_dump('relaunch');
          // var_dump($relaunch);

          $infodesti = \DB::table('dmp_dicomail')
          ->where('hash','=',$relaunch->destinataire_hash)
          ->first();

          if($infodesti !== NULL){
          // var_dump('infodesti');
          // var_dump($infodesti);

            // $chainedestirelaunch .= "dmp_".$infodesti->id . "\r\n";
            $content = $content . $infodesti->mail . ";;" . "\n";
            \DB::table('dmp_relaunch')
                ->where('destinataire_hash',$relaunch->destinataire_hash)
                ->where('campagne_id',$relaunch->campagne_id)
                ->whereNull('relaunch_at')
                ->update(['relaunch_at' => date("Y-m-d H:i:s"),
                  'state'=> 1]);

          }
          // var falloir trouver + sexy
          $lecampagneid = $relaunch->campagne_id;

        }

        if($lecampagneid == 0){
          echo 'On continue ' . "\n";
          continue;
        }

        $fichier = storage_path()."/maildrop/$partner->id-$daydate-$H-c$lecampagneid.csv";
        $fp = fopen($fichier,"a+");
        fwrite($fp,$content);
        fclose($fp);

        $lacampagne = Campagne::where('id',$lecampagneid)->first();

        if($lacampagne == NULL){
          continue;
        }

        $html = $lacampagne->generateHtml($maildrop);
        // var_dump($chainedestirelaunch);
        $sender = \DB::table('senders')
        ->where('routeur_id',$maildrop->id)
        ->first();
        $sender = Sender::find($sender->id);


        // PENSER à garder le LIST ID pour les désabo

        $date_plus = date("Y-m-d H:i:s", strtotime('now +5 Minutes'));

        $html = $lacampagne->generateHtml($maildrop);
        $listid = $this->routeur->create_list($sender,"$partner->id-$daydate-$H-c$lecampagneid");

        $url = 'https://' . getenv('CLIENT_URL') . "/mailexport/maildrop/$partner->id-$daydate-$H-c$lecampagneid.csv";
        $taskid = $this->routeur->import_by_url($sender->password, $listid, $url);

        echo 'sleeep' . "\n";
        sleep(10);
        $laliste = array($listid);
        $cid = $this->routeur->createCampagneL($lacampagne, $html, $sender, $laliste, '' );

        // on fait un insert dans campagne routeur
        \DB::statement("UPDATE campagnes_routeurs SET listid ='$listid', taskid ='$taskid' WHERE cid_routeur = '$cid'");

        for ($i=0; $i < 20 ; $i++) {
          if($this->routeur->checkStatutImport($sender,$taskid)){
            echo "On lance l'envoi" . "\n";

            $this->routeur->send_campagne($sender, $cid);
            break;

          } else {
            echo "Pas encore OK" . "\n";

          }
          sleep(60);
        }

      }






        die();
        /* OLD on garde */

        \Log::info("[CampagneRelaunchMB]");
        $mindbaz = Routeur::where('nom','Mindbaz')->first();
        $daydate = date("Y-m-d");
        $H = date("H");
        $partneri = \DB::table('dmp_partnersites')
        ->get();

        foreach($partneri as $partner){

          $check_relaunch = \DB::table('dmp_relaunch')
          ->where('state','=',0)
          ->where('site_id',$partner->id)
          ->get();

          // var_dump('Check_relaunch');
          // var_dump($check_relaunch);

          $chainedestirelaunch = '';
          $lecampagneid=0;
          foreach ($check_relaunch as $relaunch) {

            // var_dump('relaunch');
            // var_dump($relaunch);

            $infodesti = \DB::table('dmp_dicomail')
            ->where('hash','=',$relaunch->destinataire_hash)
            ->first();

            if($infodesti !== NULL){
            // var_dump('infodesti');
            // var_dump($infodesti);

              $chainedestirelaunch .= "dmp_".$infodesti->id . "\r\n";

              \DB::table('dmp_relaunch')
                  ->where('destinataire_hash',$relaunch->destinataire_hash)
                  ->where('campagne_id',$relaunch->campagne_id)
                  ->whereNull('relaunch_at')
                  ->update(['relaunch_at' => date("Y-m-d H:i:s"),
                    'state'=> 1]);

            }
            // var falloir trouver + sexy
            $lecampagneid = $relaunch->campagne_id;

          }

          $fichier = storage_path()."/dmp/$partner->id-$daydate-$H-c$lecampagneid.csv";
          $fp = fopen($fichier,"a+");
          fwrite($fp,$chainedestirelaunch);
          fclose($fp);

          $lacampagne = Campagne::where('id',$lecampagneid)->first();

          if($lacampagne == NULL){
            continue;
          }

          $html = $lacampagne->generateHtml($mindbaz);
          // var_dump($chainedestirelaunch);
          $sender = \DB::table('senders')
          ->where('routeur_id',$mindbaz->id)
          ->first();
          $sender = Sender::find($sender->id);

          $targetid = $this->routeur->create_list($sender, $fichier);
          $targetids[] = $targetid;
          $cid = $this->routeur->create_campagne_light($lacampagne, $sender, null, $targetids);
          $this->routeur->calculate_spamscore($sender, $cid);

          $date_plus = date("Y-m-d H:i:s", strtotime('now +5 Minutes'));
          $this->routeur->schedule_campaign($sender, $cid, "$date_plus");


        }






        // je check le site id et le kit rataché

        // je fais la liste de mail par site id (kit)

        // pour chaque site id je renvoie


    }
}
