<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Notification;
use Symfony\Component\Console\Input\InputArgument;

class BaseImportClics extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'base:import_clics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajoute les clics depuis un fichier situé dans le dossier cliqueurs sous le format (destinataire_id;campagne_id;theme_id).';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $file = storage_path().'/cliqueurs/'.$this->argument('file');

        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        \Log::info("Import cliqueurs a partir fichier -- $file");

        $ts = date('Y-m-d H:i:s');
        $today = date('Y-m-d');

        \App\Helpers\Profiler::start('import_clics');

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO clics (campagne_id, destinataire_id, created_at, updated_at, theme_id, clic_file, date_active) VALUES (:campagne_id, :destinataire_id, :ts1, :ts2, :theme_id, :clic_file, :date_active)";
        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();
        $bulk_count = 0;

        $bulk_count = 0;

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $name = \File::name($file);

        $insertData = array();

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;

        while (!feof($fh))
        {
            $cells = [];
            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            }

            if(count($cells) != 3) {
                continue;
            }

            $total++;

            $insertData = [$cells[0], $cells[1], $ts, $ts, $cells[2], $name, $today];
            $stmt->execute($insertData);

            $bulk_count++;
            if ($bulk_count >= 500) {
                $pdo->commit();
                $pdo->beginTransaction();
                $bulk_count = 0;
            }

            $inserted++;
        }

        $pdo->commit();
        // if (count($insertData) > 0) {
        //     $this->writeUpdates($insertData);
        //     $updateData = array();
        //     $bulk_count = 0;
        // }

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => 1,
            'message' => "Fichier cliqueurs $name importé."
        ]);

        \Log::info("End of Import cliqueurs a partir fichier -- $file");
        \App\Helpers\Profiler::report("import_clics");

        \Log::info("[Inserted] ++ : " . $inserted );
        \Log::info("[Total++] : " . $total );
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['file', InputArgument::REQUIRED, 'Path to the file']
        ];
    }

}
