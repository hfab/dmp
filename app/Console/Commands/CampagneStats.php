<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;
use App\Models\Campagne;
use App\Models\CampagneStat;
use App\Models\Ouverture;
use App\Models\Clic;
use App\Models\Bounce;
use App\Models\Planning;

class CampagneStats extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'campagne:stats';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Compute stats for a campaign';

	/**
	 * Create a new command instance.ha voilà
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        \DB::disableQueryLog();

        $planning = Planning::find($this->argument('planning_id'));
        $campagne = Campagne::find($planning->campagne_id);

        $date = Carbon::today();
        $stats = CampagneStat::firstOrCreate(['campagne_id' => $campagne->id, 'date' => $date]);
        $stats->ouvertures = Ouverture::where('campagne_id', $campagne->id)->where('created_at', '>', $date)->count();
        $stats->clicks = Clic::where('campagne_id', $campagne->id)->where('created_at', '>', $date)->count();
        $stats->bounces = Bounce::where('campagne_id', $campagne->id)->where('created_at', '>', $date)->count();
        $stats->save();

		\App\Helpers\Profiler::report();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['planning_id', InputArgument::REQUIRED, 'Planning id.'],
		];
	}

}
