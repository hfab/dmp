<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Sender;
use App\Models\Destinataire;
use App\Models\Base;
use App\Models\Theme;
use Illuminate\Support\Facades\DB;
use App\Classes\Routeurs\RouteurMailDrop;
use App\Classes\Routeurs\RouteurMindbaz;


class DmpPrepareCampaign extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected $name = 'campagne:dmp_prepare';
    protected $description = 'Set dmp campaign for each themes';
    /**
     * Execute the command.
     *
     * @return void
     */

    public function handle()
    {
        $date = date('Y-m-d');
        $dateWithHours = date('Y-m-d H:i:s');
        $fais = \DB::table('fais')->get();
        $baseIdDmp = Base::ByCode('dmp')->first();
        $dmpBranch = \DB::table('branches')->where('name','DMP')->first();

        //Récupération des tables de la base
        $tables = DB::select('SHOW TABLES');
        $tablesNames = [];
        $database = "Tables_in_".env('DB_DATABASE');
        foreach($tables as $table){
            array_push($tablesNames,$table->$database);
        }

        //Relance: lancement de campagnes déjà Shootées?
        $campaignsToRelaunchAvailables = \DB::select("select distinct id,theme_id
            from campagnes
            where expires_at >= $date order by expires_at");

        if(!empty($campaignsToRelaunchAvailables)){
            foreach($campaignsToRelaunchAvailables as $campaign){
                $theme = Theme::find($campaign->theme_id);
                $errorMessage = "[Command::DmpPrepareCampaign] Error: Theme not found for this campaign (campagne_id : $campaign->id).";
                if(!empty($theme)){
                    $errorMessage = "[Command::DmpPrepareCampaign] Error: Dmp Table not found for this theme (campagne_id : $campaign->id,Nom theme:$theme->nom).";
                    $themeName = $this->getThemeName($theme->nom);
                    if($this->checkTable("dmp_".$themeName,$tablesNames)){
                        $errorMessage = "[Command:DmpPrepareCampaign] Error: No @ availables for this theme (campagne_id : $campaign->id,Nom theme:$theme->nom).";

                        //Choix des @ en fonction du theme et de leur présence ou non le jour de la relance dans la table dmp_history
                        //Une @ ne peut être relancée qu'une seule fois sur la même campagne par jour (actuellement)
                        //Pour pousser plus loin,faut-il régler le nombre de
                        $users = \DB::select("select distinct destinataire_id
                            from dmp_".$themeName." dmp,fais f
                            where destinataire_id not in (select destinataire_id
                            from dmp_history dmp
                            where dmp.relaunch_at like '$date %'
                            and dmp.campagne_id = $campaign->id)
                            and dmp.nbRelaunch < f.dmp_pression limit 100;");

                        if(count($users) > 0){
                            $errorMessage = "[Command::DmpPrepareCampaign] No senders availables for this theme (campagne_id : $campaign->id,Nom theme:$theme->nom).";
                            $sender = Sender::where('branch_id',$dmpBranch->id)
                                ->where('quota_left','>',0)
                                ->where('nb_campagnes_left','>',0)
                                ->orderBy('quota_left','desc')
                                ->first();
                            $message = "[Command::DmpPrepareCampaign] Nb @ taken : ".count($users);
                            \Log::info($message);

                            if(!empty($sender)){
                                //For Mindbaz
                                $currentMaxId = null;
                                $importFile = storage_path()."/mindbaz/dmp/s$sender->id"."_dmp_c".$campaign->id."_".date("Ymd")."_import_mindbaz.csv";
                                //To import to Mindbaz before creating list
                                $importContent = "";
                                //To use to make the list for the campaign (Used for mindbaz case)
                                $listContent = "";

                                $arrayCountFai = [];
                                $faiPlanning = [];
                                $planning  = new Planning();
                                $destinataires = array();
                                $history = array();
                                $message = "[Command::DmpPrepareCampaign] Routeur name : ".$sender->routeur->nom;
                                \Log::info($message);

                                if($sender->routeur->nom == "Mindbaz"){
                                    $message = "[Command::DmpPrepareCampaign] Preparing list @ for Mindbaz (campagne_id: $campaign->id,Nom theme:$theme->nom).";
                                    @mkdir(storage_path().'/mindbaz/dmp',0755,true);
                                    $routeur = new RouteurMindbaz();
                                    $currentMaxId = $routeur->count_subscribers($sender) + 6000000; //Pour éviter des problèmes d'import
                                    $currentMaxId++;
                                }

                                if($sender->routeur->nom == "Maildrop"){
                                    $message = "[Command::DmpPrepareCampaign] Preparing list @ for Maildrop(campagne_id: $campaign->id,Nom theme:$theme->nom).";
                                    $routeur = new RouteurMaildrop();
                                }

                                foreach($fais as $fai){
                                    $arrayCountFai[$fai->id] = 0;
                                }

                                \Log::info($message);
                                $planning->campagne_id = $campaign->id;
                                $planning->routeur_id = $sender->routeur_id;
                                $planning->date_campagne = $date;
                                $planning->time_campagne = date('H:i:s',strtotime('+4 minutes'));
                                $planning->branch_id = $dmpBranch->id;
                                $planning->save();

                                foreach($users as $user){
                                    $destinataire = Destinataire::find($user->destinataire_id);
                                    $destinataire = \DB::table('dmp_dicomail')->where('id',$user->destinataire_id)->first();
                                    if(!empty($destinataire)){
                                        //$userFaiId = $destinataire->fai_id;
                                        //$arrayCountFai[$userFaiId] = $arrayCountFai[$userFaiId] + 1;
                                        array_push($destinataires,$destinataire);
                                        array_push($history,[
                                            "campagne_id"=>$campaign->id,
                                            "planning_id"=>$planning->id,
                                            "destinataire_id"=>$destinataire->id,
                                            "relaunch_at"=>$date
                                        ]);
                                        if($sender->routeur->nom == "Mindbaz"){
                                            $importContent .= "$destinataire->id;$destinataire->mail;$currentMaxId;".getenv('CLIENT_TITLE')."\r\n";
                                            $listContent .= "$destinataire->id\r\n";
                                            $currentMaxId++;
                                        }
                                    }
                                }

                                foreach($fais as $fai){
                                    array_push($faiPlanning, array(
                                        "planning_id"=>$planning->id,
                                        "fai_id"=>$fai->id,
                                        "volume"=>$arrayCountFai[$fai->id],
                                        "created_at"=>date('Y-m-d H:i:s'),
                                        "updated_at"=>date('Y-m-d H:i:s')));
                                }

                                //Préparation de la campagne avec Maildrop
                                //Il est nécessaire d'importer les @ que l'on souhaite shooter
                                //puis créer une liste à partir de cet import pour préparer une campagne avec Mindbaz
                                if($sender->routeur->nom == "Mindbaz"){
                                    //Importation des @
                                    file_put_contents($importFile,$importContent);
                                    $handle = fopen($importFile,"w");
                                    fwrite($handle,$importContent);
                                    fclose($handle);
                                    $routeur->import_list($sender,$importFile);

                                    //Création de la liste et remplissage de celle-ci
                                    $anonyme = "";
                                    if($planning->type == 'anonyme'){
                                        $anonyme = "_a";
                                    }
                                    $mindbazPath = storage_path().'/mindbaz/dmp/s'.$sender->id.'_dmp_c'.$campaign->id.'_p'.$planning->id.$anonyme.'.csv';
                                    file_put_contents($mindbazPath,$listContent);
                                    $listId = $routeur->create_list($sender,$mindbazPath);
                                    $message = "[Command::DmpPrepareCampaign] List created on Mindbaz.";
                                }

                                //Préparation de la campagne avec Maildrop
                                if($sender->routeur->nom == "Maildrop"){
                                    $listId = $routeur->create_list($sender);
                                    //On remplit la liste nouvellement créée
                                    foreach($destinataires as $dest){
                                        $routeur->add_destinataire($dest,$sender,$listId);
                                    }
                                    $message = "[Command::DmpPrepareCampaign] List created on Maildrop.";
                                }
                                \Log::info($message);

                                $planning->volume = count($destinataires);
                                $planning->save();
                                //Remplit les bonnes tables
                                if(!empty($planning->id) && !empty($listId)){
                                    $this->completeTables($planning->id,$sender->id,$campaign->id,$listId,$history,$faiPlanning);
                                    $message = "[Command::DmpPrepareCampaign] Insertions réussies.";
                                }
                                \Log::info($message);
                            }

                            if(empty($sender)){
                                \Log::info($errorMessage);
                            }
                        }

                        if(count($users) == 0){
                            \Log::info($errorMessage);
                        }
                    }

                    if(!$this->checkTable("dmp_".$themeName,$tablesNames)){
                        \Log::info($errorMessage);
                    }
                }

                if(empty($theme)){
                    \Log::info($errorMessage);
                }
            }
        }
    }


    function checkTable($str,$arr){
        for($index = 0 ;$index<count($arr);$index++){
            if($str == $arr[$index]){
                return true;
            }
        }
        return false;
    }

    function getThemeName($name){
        $themeName = str_replace(" ","_",$name);
        if(strpos($themeName,"/") !== false){
            $dataSplitted = explode("/",$themeName);
            $themeName = $dataSplitted[0]."_".$dataSplitted[1];
        }
        return strtolower($this->removeAccents($themeName));
    }

    function removeAccents($str){
        $str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
        $str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
        $str = preg_replace('#&[^;]+;#', '', $str);
        return $str;
    }

    function completeTables($planningId,$senderId,$campaignId,$listId,$history,$faiPlanning){
        \DB::table("planning_senders")->insert([
            'planning_id'=>$planningId,
            'sender_id'=>$senderId,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        \DB::table('campagnes_routeurs')->insert([
            'campagne_id'=>$campaignId,
            'sender_id'=>$senderId,
            'listid'=>$listId,
            'planning_id'=>$planningId,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('dmp_planning_listid')->insert([
            "listid"=>$listId,
            "planning_id"=>$planningId
        ]);
        \DB::table('plannings_fais_volumes')->insert($faiPlanning);
        \DB::table('dmp_history')->insert($history);
    }

}
