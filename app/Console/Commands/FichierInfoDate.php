<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FichierInfoDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fichier_import:date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'met à jour datenaissance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $lefichier = "ve_date.txt";
      $handle = fopen(storage_path("listes/")  . $lefichier , "r");

      if ($handle)
      {

      	while (!feof($handle))
      	{

      		$buffer = fgets($handle);

      		// echo $buffer;

          $c = explode(";",$buffer);
          echo 'MAJ ' . $c[0] . ' par ' . $c[1] . "\n";
          \DB::table('destinataires')->where('base_id', 2)->where('mail', $c[0])->update(['datenaissance' => $c[1]]);

      	}

      	fclose($handle);
      }



    }
}
