<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extract Info CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = storage_path() . '/listes/listmanager/BFAST.csv';
        $fh = fopen($file,"r");
        $chainefichier = '';


        while (!feof($fh)) {

            $cells = [];
            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ',') !== false) {
                $cells = explode(',', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }


            // var_dump(trim($cells[4]));
            // var_dump(trim($cells[5]));
            if(isset($cells[5]) && isset($cells[4])){



              if(trim($cells[5]) !== ''
              && trim($cells[4]) !== ''){

                if(substr(trim($cells[4]),0,2) == '06' || substr(trim($cells[4]),0,2) == '07'){
                // $tel = str_replace(' ','',trim($cells[4]));
                // var_dump($tel);
                $chainefichier .=  '"' .str_replace(' ','',trim($cells[4])) .'"'. ';' . '"' . trim($cells[5]) . '"' . ';' . '"' . trim($cells[0]) . '"'. "\n";

                }
              }
            }

            // var_dump($cells);

            // foreach($cells as $cell) {
            // }
        }

        var_dump($chainefichier);
        $file = storage_path() . '/listes/listmanager/BFAST_tel_codepostal_civil.csv';
        $fp = fopen($file,"a+");
        fwrite($fp, $chainefichier);
        fclose($fp);
    }
}
