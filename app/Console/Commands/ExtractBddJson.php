<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractBddJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:bdd {base_id} {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoi de destinataires vers un autre tor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = 1000;
        // lire la table au besoin

        $histo_export = \DB::table('export_api')
        ->where('base_id', $this->argument('base_id'))
        ->where('link', $this->argument('url'))
        ->orderBy('id','desc')
        ->first();

        var_dump($histo_export);

        if($histo_export == NULL){
          $nb_desti = \DB::table('destinataires')
          ->where('base_id', $this->argument('base_id'))
          ->count();
        } else {
          $nb_desti = \DB::table('destinataires')
          ->where('base_id', $this->argument('base_id'))
          ->where('id','>', $histo_export->last_destinataire_id)
          ->count();
        }


        // ->count() - $offset; histo de bdd

        var_dump($nb_desti);

        if($offset < $nb_desti){

          foreach (range(0, $nb_desti, $offset) as $number) {

            if($histo_export == NULL){
              $desti_export = \DB::table('destinataires')
              ->where('base_id', $this->argument('base_id'))
              ->orderBy('id')
              ->skip($number)
              ->take($offset)
              ->get();
            } else {
              $desti_export = \DB::table('destinataires')
              ->where('base_id', $this->argument('base_id'))
              ->where('id','>', $histo_export->last_destinataire_id)
              ->orderBy('id')
              ->skip($number)
              ->take($offset)
              ->get();
            }

              // http://tor-ve.lead-factory.net/webservice-import-lead?webservice_key=webrivage-rAySXtzRK2LWTTVEQczJM6nmTxXg9zbCnOIJLG0rU757mEC1CyCldJ5yFW6UeoWN&last_name=test&first_name=toto&city=Paris&zipcode=75016&civilite=0&birth_date=2017-02-20&email=superalex3@orange.fr
              // http://tor-agv.lead-factory.net/webservice-import-lead?webservice_key=T4Qnzg17r2XptRYvYTD9qD36A5uo93gOnNUKb23J9p03h5SMm1J5K51kl6U3aIxn
              foreach ($desti_export as $value) {
                // var_dump($this->argument('url') . '&last_name='.$value->nom.'&first_name='.$value->prenom.'&city='.$value->ville.'&zipcode='.$value->departement.'&civilite='.$value->civilite.'&birth_date='.$value->datenaissance.'&email=' . $value->mail);
                // $r = file_get_contents($this->argument('url') . '&last_name='.$value->nom.'&first_name='.$value->prenom.'&city='.$value->ville.'&zipcode='.$value->departement.'&civilite='.$value->civilite.'&birth_date='.$value->datenaissance.'&email=' . $value->mail);
                // var_dump($value->mail .' '. $r);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->argument('url') . '&last_name='.$value->nom.'&first_name='.$value->prenom.'&city='.$value->ville.'&zipcode='.$value->departement.'&civilite='.$value->civilite.'&birth_date='.$value->datenaissance.'&email=' . $value->mail);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);

                // convert response
                $output = json_decode($output);

                // handle error; error output
                if(curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {

                  var_dump($output);
                }

                curl_close($ch);


                $last_id = $value->id;
              }
          }

        } else {

          if($histo_export == NULL){
            $desti_export = \DB::table('destinataires')
            ->where('base_id', $this->argument('base_id'))
            ->orderBy('id')
            ->get();
          } else {
            $desti_export = \DB::table('destinataires')
            ->where('base_id', $this->argument('base_id'))
            ->where('id','>', $histo_export->last_destinataire_id)
            ->orderBy('id')
            ->get();
          }

          foreach ($desti_export as $value) {

            var_dump($this->argument('url') . '&last_name='.$value->nom.'&first_name='.$value->prenom.'&city='.$value->ville.'&zipcode='.$value->departement.'&civilite='.$value->civilite.'&birth_date='.$value->datenaissance.'&email=' . $value->mail);

            $r = file_get_contents($this->argument('url') . '&last_name='.$value->nom.'&first_name='.$value->prenom.'&city='.$value->ville.'&zipcode='.$value->departement.'&civilite='.$value->civilite.'&birth_date='.$value->datenaissance.'&email=' . $value->mail);
            var_dump($value->mail .' '. $r);

            $last_id = $value->id;
          }

        }

        if(isset($last_id)){

        \DB::table('export_api')->insert(
            ['base_id' => $this->argument('base_id'),
            'last_destinataire_id' => $last_id,
            'link'=> $this->argument('url')]
        );
        }
        // var_dump($nb_desti);
    }

    protected function getArguments()
    {
        return [
            ['base_id', InputArgument::REQUIRED, 'Base'],
            ['url', InputArgument::REQUIRED, 'Link'],
        ];
    }
}
