<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class relaunchCampagneMatchDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relaunch:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recupère les matchs pour remplir la table relance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // rajouter une colone state
        $daydate = date("Y-m-d");
        // parcours des matchs du jour sans commande
        $matched_day = \DB::table('dmp_matched')
        ->where('nb_has_ordered','=',0)
        ->where('updated_at','LIKE',$daydate.'%')
        ->get();

        $dateSecure = date('Y-m-d H:i:s',strtotime('-1 days'));

        foreach ($matched_day as $mD) {

          $checkDate = \DB::table('dmp_cookies')
          ->where('hash',$mD->destinataire_hash)
          // le cookie doit avoir fait son dernier update il y a + 24h pour pas relancer un frais
          // voir après la stratégie de relance de plus près
          // ->where('updated_at','<',$dateSecure)
          ->get();

          if(count($checkDate) !== 0){
            $check_relaunch = \DB::table('dmp_relaunch')
            ->where('destinataire_hash','=',$mD->destinataire_hash)
            ->where('site_id',$mD->base_site_id)
            ->get();

              if(count($check_relaunch) === 0){

                // à opty car on peut rqt qu'une fois entre les deux scripts
                $aboOrNot = \DB::table('dmp_dicomail')
                ->where('hash','=',$mD->destinataire_hash)
                ->where('statut','=',0)
                ->first();

                if(count($aboOrNot) !== 0){

                  // je set la campagne id (pour le moment on prend la premiere dispo)
                  $get_campagne = \DB::table('campagnes')
                  ->where('dmp_partnersite_id','=',$mD->base_site_id)
                  ->where('expires_at','>',date('Y-m-d'))
                  ->first();

                  // si il n'y a pas de campagne dispo
                  if($get_campagne === NULL){
                    continue;
                  }

                  $arrayInsert=[
                    "destinataire_hash"=>$mD->destinataire_hash,
                    "sender_id"=>0,
                    "campagne_id"=>$get_campagne->id,
                    "editor_id"=>$mD->editor_id,
                    "site_id"=>$mD->base_site_id,
                    "theme_id"=>$mD->theme_id,
                    "state"=>0,
                    "relaunch_at"=>NULL];
                    \DB::table('dmp_relaunch')->insert($arrayInsert);

                  }
              }
          }
        }
    }
}
