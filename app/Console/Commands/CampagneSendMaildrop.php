<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMaildrop;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Sender;

use Mail;

class CampagneSendMaildrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:send_maildrop {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lance les envois maildrop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->routeur = new RouteurMaildrop();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_maildrop')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : MAILDROP DISABLE');
            return 0;
        }

        $today = \Carbon\Carbon::today();
        $now = \Carbon\Carbon::now();
        $p = Planning::find($this->argument('planning_id'));
        $campagne = Campagne::find($p->campagne_id);
        \Log::info("[CampagneSendMaildrop][P$p->id] : Début du lancement des envois (Planning $p->id/Campagne $campagne->id)");
        if ($p->sent_at != null) {
            echo "Already sent\n";
            \Log::info("Planning $p->id (Campagne $campagne->id) : already sent");
            die();
        }

        $cidasend = \DB::table('campagnes_routeurs')
            ->where('campagne_id',$campagne->id)
            ->where('planning_id',$p->id)
            ->get();

        if(count($cidasend) == 0){
            \Log::error("[CampagneSendMaildrop][P$p->id] : Il n'y ø de segments créés pour cette campagne (Planning $p->id/Campagne $campagne->id)");
            exit;
        }

        foreach ($cidasend as $key => $envoi) {
            $sender = \DB::table('senders')->where('id',$envoi->sender_id)->first();
            $this->routeur->send_campagne($sender, $envoi->cid_routeur);
        }

        $anonyme = "";
        // if ici et on creé la var
        if($p->type == 'anonyme'){
            $anonyme = "_a";
        }

        exec("rm ~/".getenv('CLIENT_URL')."/storage/maildrop/*p$p->id$anonyme*");

        $p->sent_at = date('Y-m-d H:i:s');
        $p->next_campaign = 0;
        $p->save();

        \App\Models\Notification::create([
            'user_id' => 1, // /!\ User System
            'level' => 'info',
            'message' => "Campagne $campagne->ref (planning $p->id) envoyée.",
            'url' => '/campagne/'.$campagne->id.'/stats'
        ]);

        $email_volume_demande = 'Volume demandé : ' . $p->volume;
        $email_volume_orange = 'Volume orange demandé : ' . $p->volume_orange;
        $email_volume_selected = 'Volume réel utilisé : ' . $p->volume_selected;
        $email_date = 'Campagne programmée pour le ' . $p->date_campagne . ' ' . $p->time_campagne . ' / Envoi de la campagne terminé à ' . $p->sent_at;

        $senders = \DB::select("select nom from senders where id in (select sender_id from campagnes_routeurs where planning_id = :planning_id)", ['planning_id' => $p->id]);

        $used_senders = 'Compte(s) utilisé(s) : <br/>';

        foreach($senders as $s) {
            $used_senders .= "$s->nom <br/>";
        }

        \Mail::send('mail.mailinfocampagne',
            [
                'email_volume_demande' => $email_volume_demande,
                'email_volume_orange' => $email_volume_orange,
                'email_volume_selected' => $email_volume_selected,
                'email_date' => $email_date,
                'used_senders' => $used_senders,
                'url' => getenv('CLIENT_TITLE')
            ], function ($message) use ($campagne) {
                $destinataires = \App\Models\User::where('is_valid', 1)
                    ->where('user_group_id', 1)
                    ->where('email', '!=', "")
                    ->get();
                $message->from('rapport@lead-factory.net', 'Tor')->subject("Rapport MD : envoi de $campagne->ref [$campagne->id] terminé au " . date('d-m-Y H:i'));
                foreach($destinataires as $d){
                    $message->to($d->email);
                }
                return "true";
                // $message->to('fabien@lead-factory.net');
                // $message->to('adeline.sc2@gmail.com');
            });
        \Log::info("[CampagneSendMaildrop][P$p->id] : Fin du lancement des envois (Planning $p->id/Campagne $campagne->id)");
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
