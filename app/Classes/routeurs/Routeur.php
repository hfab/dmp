<?php namespace  App\Classes\Routeurs;

class Routeur
{
    protected $repertoire_serveur;
    protected $name = null;
    protected $campagne = null;

    public function __construct()
    {
        $this->repertoire_serveur = storage_path().'/campagnes/';
        $this->setConfig();
    }

    function setConfig() {
        $this->account = (object) config("bat.$this->name");
    }

    function addCampagne($campagne, $account = null) {
        $this->campagne = $campagne;

        $this->html = $this->formatHtml($campagne->html);
    }

    function makeBat($campagne = null) {
        if ($this->campagne == null && $campagne == null) {
            die('Envoi BAT impossible: pas de campagne');
        }
    }

    function addList($query) {
        $this->list = $query;
        $this->count_destinataires = $query->count();
    }

    function sendCampagne(){

    }

    function formatHtml($html) {
        $plateforme = \App\Models\Plateforme::find($this->plateforme_id);

        foreach($plateforme->variables as $variable) {
            $html = str_ireplace($variable->search, $variable->replace, $html);
        }

        $this->html = $html;
        return $html;
    }
}
