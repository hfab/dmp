<?php namespace App\Classes\Routeurs;

use \App\Models\Destinataire;
use \App\Models\Clic;
use \App\Models\Ouverture;
use \App\Models\Campagne;
use \App\Models\Sender;
use \App\Models\Bounce;
use \App\Models\Routeur;
use \App\Models\CampagneRouteur;

class RouteurSmessage
{

    //  _
    // | |
    // | |__   __ _ ___  ___
    // | '_ \ / _` / __|/ _ \
    // | |_) | (_| \__ \  __/
    // |_.__/ \__,_|___/\___|
    //
    //

    private $routeur;

    public function __construct()
    {
        $this->routeur = Routeur::where('nom','Smessage')->first();
    }

    function creation_campagne($login, $mdp)
    {
        // http://www.wiki-emailing.fr/SOAP:Cr%C3%A9er_une_campagne
        // Ajout_Campagne
        // int Ajout_Campagne (id_campagne, login)
        // Retour - L'identifiant de la campagne 'id' ou un message d'erreur

        try {
            $client = new \SoapClient(
                null,
                array(
                    'location' => "http://production.smessage.net/_soap/control.php",
                    'uri'      => "http://production.smessage.net",
                    'encoding'=>'ISO-8859-1'
                )
            );

            $variable = $client->Ajout_Campagne($login,$mdp);
            // pour renvoyer seulement l'id
            return $variable[0]['id'];
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }

    function recup_campagne($login,$mdp,$idcampagne)
    {
        // http://www.wiki-emailing.fr/SOAP:R%C3%A9cup%C3%A9rer_les_param%C3%A8tres_d%27une_campagne
        // Recup_Parametres_Campagne
        // int Recup_Parametres_Campagne (id_campagne, login,password)

        // Retour - Un tableau contenant les paramètres de la campagne
        // [id] => identifiant de la campagne
        // [reference] => Référence de la campagne (pour votre usage)
        // [date_creation] => timespam unix
        // [date_envoi] => timespam unix ou 'Non envoyée'
        // [return_path] => Return path et from
        // [gestion_npai] => 0
        // [nom_expediteur] => Ediware
        // [reply] => Adresse de réponse
        // [objet] => Objet du message
        // [total_mail] => Nombre de mail dans la base de la campagne
        // [images_jointes] => 0 si les images sont téléchargées sur un serveur, 1 si les images sont incrustées dans le mail
        // [lien_desinscription] => 0 si vous gérez le lien de désinscription, 1 si le lien de désinscription est géré par MDWorks
        // [passphrase] => Ce mot de passe associé à la campagne est nécessaire pour afficher la page de statistiques de la campagne

        try {
            $client = new \SoapClient(
                null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Recup_Parametres_Campagne($idcampagne, $login,$mdp);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }

    function modif_campagne_general($idcampagne,$login,$mdp,$champ,$valeur)
    {

    //    http://www.wiki-emailing.fr/SOAP:Modifier_les_param%C3%A8tres_d'une_campagne
    //   Modif_Parametres_Campagne
    //   int Modif_Parametres_Campagne (id_campagne, login,password,champ,valeur)
    //   Seules les campagnes qui n'ont pas encore été envoyées peuvent être modifiées.
    //
    //   Champ 	Valeur
    //   reference 	Référence de la campagne (référence interne)
    //   nom_expediteur 	Nom de l'expéditeur
    //   reply 	Adresse email de réponse
    //   objet 	Objet du message
    //   images_jointes 	0 pour désactiver l'activation des images, 1 pour l'activer
    //   lien_desinscription 	0 pour désactiver le lien de désinscription, 1 pour l'activer

        try { $client = new \SoapClient(null, array(
            'location' => "http://production.smessage.net/_soap/control.php",
            'uri'      => "http://production.smessage.net",
            'encoding'=>'ISO-8859-1'
        ));

        $variable = $client->Modif_Parametres_Campagne($idcampagne,$login,$mdp,$champ,$valeur);

        } catch (SoapFault $fault) {

            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function createCampagne(\App\Models\Sender $sender, \App\Models\Campagne $campagne, \App\Models\Planning $planning = null)
    {
        $planningid = 0;
        if(!empty($planning)){
            $planningid = $planning->id;
        }
        //Multi-objets aléatoire
        $objets = explode('|',$campagne->objet);
        $objet = $objets[rand(0,count($objets)-1)];

        $objet = str_replace("’","'", $objet);
        $objet = str_replace("–","-", $objet);
        $objet = str_replace("’","'", $objet);

        $expediteur = str_replace("–","-", $campagne->expediteur);
        $expediteur = str_replace("–","-", $expediteur);
        $expediteur = str_replace("’","'", $expediteur);

        $url = 'http://' . getenv('CLIENT_URL') . '/campagne/' . $campagne->id . "/routeur_{$this->routeur->id}";

        \Log::info("[RouteurSmessage] createCampagne : before appel $campagne->id / Sender $sender->id");
        $idcampagne = $this->creation_campagne($sender->nom, $sender->password);
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'objet',utf8_decode($objet));
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'reference',$campagne->ref);
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'nom_expediteur',utf8_decode($expediteur));
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'lien_desinscription',0);
        $this->ajouter_message($sender->nom, $sender->password,$idcampagne,$url);
        \Log::info("[RouteurSmessage] createCampagne : after appel $campagne->id / Sender $sender->id");

        $campagnerouteur = CampagneRouteur::create(['campagne_id'=>$campagne->id, 'planning_id' => $planningid, 'sender_id' => $sender->id, 'cid_routeur' => $idcampagne]);
        return $idcampagne;
    }

    // a refaire pour plus propre
    function createCampagneAnonyme(\App\Models\Sender $sender, \App\Models\Campagne $campagne, \App\Models\Planning $planning = null)
    {
        $planningid = 0;
        if(!empty($planning)){
            $planningid = $planning->id;
        }
        //Multi-objets aléatoire
        $objets = explode('|',$campagne->objet);
        $objet = $objets[rand(0,count($objets)-1)];

        $objet = str_replace("’","'", $objet);
        $objet = str_replace("–","-", $objet);
        $objet = str_replace("’","'", $objet);

        $expediteur = str_replace("–","-", $campagne->expediteur_anonyme);
        $expediteur = str_replace("–","-", $expediteur);
        $expediteur = str_replace("’","'", $expediteur);

        $url = 'http://' . getenv('CLIENT_URL') . '/campagne/' . $campagne->id . "/routeura_{$this->routeur->id}";

        $idcampagne = $this->creation_campagne($sender->nom, $sender->password);
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'objet',utf8_decode($objet));
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'reference',$campagne->ref);
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'nom_expediteur',utf8_decode($expediteur));
        $this->modif_campagne_general($idcampagne,$sender->nom, $sender->password,'lien_desinscription',0);
        $this->ajouter_message($sender->nom, $sender->password,$idcampagne,$url);

        $campagnerouteur = CampagneRouteur::create(['campagne_id'=>$campagne->id, 'planning_id' => $planningid, 'sender_id' => $sender->id, 'cid_routeur' => $idcampagne]);
        return $idcampagne;
    }

    function ajouter_message($login,$mdp,$idcampagne,$url)
    {
        // http://www.wiki-emailing.fr/SOAP:Ajouter_le_message
        // Ajouter / Modifier le message d'une campagne (non envoyée)
        // array Modif_Message_Campagne( id_campagne, login, motdepasse, url)

        // Besoin de ca : http://wiki.routage-email.com/SOAP:Lien_vers_le_message_h%C3%A9berg%C3%A9_d'une_campagne

        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Modif_Message_Campagne($idcampagne,$login,$mdp,$url);
            print_r($variable);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function supprimer_campagne($login,$mdp,$idcampagne)
    {
        // http://www.wiki-emailing.fr/SOAP:Supprimer_une_campagne
        // Supprimer une campagne (qui n'a pas encore été envoyée)
        // array Supprimer_Campagne( login, motdepasse, id_campagne)

        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Supprimer_Campagne($idcampagne,$login,$mdp);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function ajouter_adresse($login,$mdp,$idcampagne,$mail)
    {
        // http://www.wiki-emailing.fr/SOAP:Ajouter_une_adresse
        // Ajouter une adresse à la liste des destinataires
        // array Ajout_Adresse_Campagne( login, motdepasse, id_campagne, ligne)

        // $ligne=array('email@domaine.com','La société','L\'adresse','Le code postal','La ville','Le Téléphone','Le Fax','Le site Web');
        $ligne=array($mail);

        try {
            $client = new \SoapClient(null,
                array(
                    'location' => "http://production.smessage.net/_soap/control.php",
                    'uri'      => "http://production.smessage.net",
                    'encoding'=>'ISO-8859-1'
                )
            );
            $variable = $client->Ajout_Adresse_Campagne($login,$mdp,$idcampagne,$ligne);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function nettoyage_campagne($login,$mdp,$idcampagne){
        // http://www.wiki-emailing.fr/SOAP:D%C3%A9doublonner_et_nettoyer_le_fichier_de_destinataires
        // Nettoyer le fichier de destinataires d'une campagne (qui n'a pas encore été envoyée)
        // array Nettoyer_Campagne( login, motdepasse, id_campagne)

        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Nettoyer_Campagne($login,$mdp,$idcampagne);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }

    function send_campagne(\App\Models\Sender $sender,$idcampagne)
    {
        // http://www.wiki-emailing.fr/SOAP:Envoyer_une_campagne
        // Envoi_Campagne
        // int Envoi_Campagne (id_campagne, login, motdepasse )
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Envoi_Campagne($idcampagne,$sender->nom,$sender->password);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }


    function send_bat($idcampagne)
    {
        // http://www.wiki-emailing.fr/SOAP:Envoyer_un_BAT
        // Envoi_BAT
        // int Envoi_BAT (id_campagne, login,password,email)
        //on envoie un BAT qu'aux admins
        $campagne = Campagne::find($idcampagne);

        $smessage = $this->routeur;
        $senders = Sender::where('routeur_id',$smessage->id)
            ->where('is_bat', true)
            ->get();

        foreach($senders as $sender) {
            $utilisateurs = \App\Models\User::where('is_bat', 1)
                ->where('is_valid', 1)
                ->get();

            $cid = $this->createCampagne($sender,$campagne);
            $bat = "";
            foreach ($utilisateurs as $user) {
                $bat .= $user->email."\n";
            }

            try {
                $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                    'uri' => "http://production.smessage.net",
                    'encoding' => 'ISO-8859-1'
                ));

                $variable = $client->Envoi_BAT($cid, $sender->nom, $sender->password, $bat); //ou //$variable = $client->__soapCall(Envoi_BAT,$parametres);
                //print_r($variable);
		\Log::info("Envoi de BAT : ".json_encode($variable));
            } catch (SoapFault $fault) {
                return false;
                trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
            }
        }
        return 'ok';
    }

    function email_expediteur($idcampagne,$login,$mdp,$email,$gestion_npai)
    {
        // http://wiki.routage-email.com/SOAP:Email_de_l'exp%C3%A9diteur_/_Gestion_automatique_des_retours
        // Modif_Gestion_Retours_Campagne
        // int Modif_Gestion_Retours_Campagne (id_campagne, login,password,return_path,gestion_npai)
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Modif_Gestion_Retours_Campagne($idcampagne,$login,$mdp,$email,$gestion_npai);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }}

    function transfert_email($login, $mdp, $idcampagne, $url)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            \Log::info("[RouteurSmessage] transfert_email : before appel cid $idcampagne / Sender $login");
            $variable = $client->Ajout_Destinataires_URL_Campagne($login, $mdp, $idcampagne, $url);
            \Log::info("[RouteurSmessage] transfert_email : after appel cid $idcampagne / Sender $login");
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }


    //
    //  _        __                           _   _
    // (_)      / _|                         | | (_)
    //  _ _ __ | |_ ___  _ __ _ __ ___   __ _| |_ _  ___  _ __
    // | | '_ \|  _/ _ \| '__| '_ ` _ \ / _` | __| |/ _ \| '_ \
    // | | | | | || (_) | |  | | | | | | (_| | |_| | (_) | | | |
    // |_|_| |_|_| \___/|_|  |_| |_| |_|\__,_|\__|_|\___/|_| |_|
    //
    //
    //
    //
    function verifier_npai($login, $mdp, $mail)
    {
        // http://www.wiki-emailing.fr/SOAP:V%C3%A9rifier_la_pr%C3%A9sence_d'un_email_dans_votre_liste_de_NPAI
        // Verifier_NPAI
        // int Verifier_NPAI (login,password,email)
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

        $variable = $client->Verifier_NPAI($login,$mdp,$mail);
        } catch (SoapFault $fault) {

            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function verifier_blackliste($login, $mdp, $mail)
    {
        // http://www.wiki-emailing.fr/SOAP:V%C3%A9rifier_la_pr%C3%A9sence_d'un_email_dans_votre_blackliste
        // Verifier_Blackliste
        // int Verifier_Blackliste (login,password,email)
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Verifier_Blackliste($login,$mdp,$mail);
        } catch (SoapFault $fault) {

            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function recup_credit($login,$mdp)
    {
        try {

            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Recup_Credit($login,$mdp);
            return $variable;
        } catch (SoapFault $fault) {

            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function lister_campagne_compte($login,$mdp)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Liste_Campagnes($login,$mdp, time()-72*3600);
            return $variable;

        } catch (SoapFault $fault) {

            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }



    //
    //      _        _   _     _   _
    //     | |      | | (_)   | | (_)
    //  ___| |_ __ _| |_ _ ___| |_ _  __ _ _   _  ___  ___
    // / __| __/ _` | __| / __| __| |/ _` | | | |/ _ \/ __|
    // \__ \ || (_| | |_| \__ \ |_| | (_| | |_| |  __/\__ \
    // |___/\__\__,_|\__|_|___/\__|_|\__, |\__,_|\___||___/
    //                                  | |
    //                                  |_|
    //

    function recup_npai($login, $mdp, $idcampagne)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Extrait_Campagne($login, $mdp, $idcampagne,'NPAI');
            return $variable;
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }}

    function recup_npai_soft($login, $mdp, $idcampagne)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Extrait_Campagne($login, $mdp, $idcampagne,'SOFT');
            return $variable;
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }}

    function recup_overquota($login, $mdp, $idcampagne)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Extrait_Campagne($login, $mdp, $idcampagne,'OVERQUOTA');
            return $variable;
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }}

    function getBounces(\App\Models\Sender $sender,\App\Models\Campagne $campagne, $idcampagne)
    {
        $bounces = array();
        $bulk = array();
        $npai = $this->recup_npai($sender->nom,$sender->password,$idcampagne);
        if(is_array($npai)) {
            foreach ($npai as $res)
            {
                $destinataire = \DB::table('destinataires')
                    ->select('id')
                    ->where('base_id', $campagne->base_id)
                    ->where('mail', $res)
                    ->first();

                if(empty($destinataire)) {
                    continue;
                }

                $bounce = \DB::table('bounces')
                    ->select('destinataire_id')
                    ->where('destinataire_id', $destinataire->id)
                    ->where('campagne_id', $campagne->id)
                    ->first();

                if (!empty($bounce)) {
                    continue;
                }

                $bounce = new Bounce();
                $bounce->destinataire_id = $destinataire->id;
                $bounce->campagne_id = $campagne->id;
                $bounce->message = "NPAI_HARD";
                $bounce->save();

                $bulk[] = $destinataire->id;
                if (count($bulk) > 500) {
                    \DB::table('destinataires')
                        ->whereIn('id', $bulk)
                        ->update(['statut' => DESTINATAIRE_BOUNCE]);
                    $bulk = [];
                }
            }
        }

        if (count($bulk) > 0 ) {
            \DB::table('destinataires')
                ->whereIn('id', $bulk)
                ->update(['statut' => DESTINATAIRE_BOUNCE]);
            $bulk = [];
        }

        $npai_soft = $this->recup_npai_soft($sender->nom,$sender->password,$idcampagne);

        if(is_array($npai_soft)) {
            foreach ($npai_soft as $res)
            {
                $destinataire = \DB::table('destinataires')
                    ->select('id')
                    ->where('base_id', $campagne->base_id)
                    ->where('mail', $res)
                    ->first();

                if(empty($destinataire)){
                    continue;
                }
                $bounce = \DB::table('bounces')
                    ->where('destinataire_id', $destinataire->id)
                    ->where('campagne_id', $campagne->id)
                    ->first();

                if (!empty($bounce)) {
                    continue;
                }

                $bounce = new Bounce();
                $bounce->destinataire_id = $destinataire->id;
                $bounce->campagne_id = $campagne->id;
                $bounce->message = "NPAI_SOFT";
                $bounce->save();
            }
        }
        return true;
    }

    function recup_ouvreurs($login, $mdp, $idcampagne)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Extrait_Campagne($login, $mdp, $idcampagne,'ouvreurs');
		return $variable;
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }}

    function recup_desinscrits($login, $mdp, $idcampagne)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Extrait_Campagne($login, $mdp, $idcampagne,'desinscrits'); //ou //$variable = $client->__soapCall(Extrait_Campagne,$parametres);
//            print_r($variable);
            return $variable;
        } catch (SoapFault $fault) {
            return false;
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }}

    function addDesinscrits(\App\Models\Sender $sender,\App\Models\Campagne $campagne,$idcampagne)
    {
        \Log::info("[RouteurSmessage] : Sender $sender->id / Campagne $campagne->id -- CID ($idcampagne)");
        $desinscrits = $this->recup_desinscrits($sender->nom,$sender->password,$idcampagne);
        if(!$desinscrits){
            return false;
        }
        foreach($desinscrits as $d)
        {
            $destinataire = \DB::table('destinataires')
                            ->where('base_id',$campagne->base_id)
                            ->where('mail',$d)
                            ->where('statut','!=',DESTINATAIRE_OPTOUT)
                            ->first();

            if(empty($destinataire)) {
                continue;
            }

            \DB::table('destinataires')
                ->where('base_id',$campagne->base_id)
                ->where('mail',$d)->update([
                    'statut' => DESTINATAIRE_OPTOUT,
                    'optout_at' => \Carbon\Carbon::today()
                ]);
        }
        return true;
    }

    function recup_cliqueurs($login, $mdp, $idcampagne)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Extrait_Campagne($login, $mdp, $idcampagne,'cliqueurs'); //ou //$variable = $client->__soapCall(Extrait_Campagne,$parametres);
            return $variable;
        } catch (SoapFault $fault) {
            return false;
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function recup_cliqueurs_fichier($login, $mdp, $idcampagne)
    {

        $chainef = '';
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $var = $client->Extrait_Campagne($login, $mdp, $idcampagne,'cliqueurs'); //ou //$variable = $client->__soapCall(Extrait_Campagne,$parametres);
            // return $variable;

            var_dump($var);

            if(!empty($var) || $var !== NULL ){

            foreach ($var as $v) {
              if(!empty($v) || $v !== NULL){

              $chainef .= $v . "\n";

              }
            }

            }

            var_dump($chainef);

            echo $chainef;
            $today = date('Y-m-d');
            $file = storage_path() . '/cliqueurs/clic_cid_' . $idcampagne . '_date_' . $today . '.csv';
            /* $file = storage_path() . '/cliqueurs/clic_cid_smessagedbug' . time() . '.csv'; */
            $fp = fopen($file,"w+");
            fwrite($fp,$chainef);
            fclose($fp);

        } catch (SoapFault $fault) {
            return false;
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }
    }

    function getClicks(\App\Models\Sender $sender, \App\Models\Campagne $campagne, $idcampagne)
    {
        $cliqueurs = array();
        $cliqueurs = $this->recup_cliqueurs($sender->nom,$sender->password,$idcampagne);

        if(!$cliqueurs)
        {
            return false;
        }

        foreach($cliqueurs as $cliqueur)
        {
            $destinataire = \DB::table('destinataires')
                                ->select('id')
                                ->where('base_id',$campagne->base_id)
                                ->where('mail', $cliqueur)
                                ->first();

            if(empty($destinataire)){
                $destinataire = new Destinataire();
                $destinataire->mail = $cliqueur;
                $destinataire->base_id = $campagne->base_id;
                $destinataire->save();
            }

            $clic = \DB::table('clics')
                        ->select('id')
                        ->where('destinataire_id',$destinataire->id)
                        ->where('campagne_id',$campagne->id)
                        ->first();
            if(!empty($clic)) {
                continue; //pour ne pas inserer plusieurs fois le meme clic...
            }
            $clic = new Clic();
            $clic->destinataire_id = $destinataire->id;
            $clic->campagne_id = $campagne->id;

            \DB::table('destinataires')
                ->where('base_id',$campagne->base_id)
                ->where('mail', $cliqueur)
                ->update(['clicked_at' => \DB::raw('now()')] );

        }
        return true;
    }

    function recup_inactifs($login, $mdp, $idcampagne){
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Extrait_Campagne($login, $mdp, $idcampagne,'inactifs');
            return $variable;

        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }}


    function stats_general_smessage($login, $mdp, $idcampagne){

        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Recup_Statistiques_Campagne($idcampagne, $login, $mdp);

            return $variable[0];
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }


    function checkStatutImport(\App\Models\Sender $sender, $idcampagne)
    {
        try {
            $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control.php",
                'uri'      => "http://production.smessage.net",
                'encoding'=>'ISO-8859-1'
            ));

            $variable = $client->Check_Statut_Campagne($sender->nom, $sender->password,$idcampagne);
            return $variable;
        } catch (SoapFault $fault) {
            return false;
        }
    }

    function recup_blacklistmail($login, $mdp){
      try {
        $client = new \SoapClient(null, array('location' => "http://production.smessage.net/_soap/control_v2.php",
          'uri'      => "http://production.smessage.net",
          'encoding'=>'ISO-8859-1'
        ));

        $variable = $client->Extrait_Blackliste($login,$mdp);
        return $variable;
      } catch (SoapFault $fault) {
          trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
      }
    }

    //  ______               _   _
    // |  ____|             | | (_)
    // | |__ ___  _ __   ___| |_ _  ___  _ __  ___
    // |  __/ _ \| '_ \ / __| __| |/ _ \| '_ \/ __|
    // | | | (_) | | | | (__| |_| | (_) | | | \__ \
    // |_|  \___/|_| |_|\___|\__|_|\___/|_| |_|___/
    //
    //

    function decoupe_compte($nombreCompte)
    {
        $tableauSenders = array();
        $mesSenders = \DB::table('senders')
            ->where('routeur_id',$this->routeur->id)
            ->get();
        foreach ($mesSenders as $valeur) {
            echo $valeur->nom . ' - ' . $valeur->password . '<br />';
            $tableauSenders[$valeur->nom] = $valeur->password;
        }
        $tableauSenders = array_rand($tableauSenders, 2);
        return $tableauSenders;
    }
}

?>
