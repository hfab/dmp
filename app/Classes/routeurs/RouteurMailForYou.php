<?php namespace App\Classes\Routeurs;

use \App\Models\Campagne;
use App\Models\Routeur;

class RouteurMailForYou
{
    private $routeur;
    // private $client;
    private $header;

    public function __construct()
    {

      $this->url = 'https://api.mailforyou.pro';

      // SERVEUR A
      // $this->login = 'A9SJWHV3X-R';
      //  $this->key = "Cc7QkM&_QHo6''4";

      // SERVEUR B
      // $this->login = 'X7B4S2MBB-R';
      // $this->key = "\byaL[G@I`16BU$";

      // $this->login = 'VB9QJVQDM-R';
      // $this->key = "tL3bI+`kPWe^n&j";


      // SERVEUR 1
      // $this->login = 'DVW29HB8V-R';
      // $this->key = "*S!]a@}nK]TeMYh";

      // PROD
      $this->login = env('M4Y_LOGIN');
      $this->key = env('M4Y_PASS');
    }

    // PARTIE DOSSIER

    function CreationDossier($nomdossier){

      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array('title' => $nomdossier);
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/folder");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;
    }

    function ModificationDossier($dossierid,$nomdossiernew,$urltrackingnew){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array('title' => $nomdossiernew,'urlTrackingServer' => $urltrackingnew);
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/folder/".$dossierid);

      curl_setopt($this->client,CURLOPT_CUSTOMREQUEST,'PUT');
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode);
      return $arrayretour;
    }

    function SuppressionDossier($dossierid){
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array('folderid' => $dossierid);
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/folder/" . $dossierid);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "DELETE");

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      return $reponsecode;

    }

    // PARTIE CAMPAGNE

    function CreationCampagne($nomCampagne, $objetCampagne, $folderId){

      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array(
        'title' => $nomCampagne,
        'subject' => $objetCampagne,
        'folderId' => $folderId,
        'msgType' => '0',
        // 'msgSMS' => ' Le message sms',
        // 'urlUnsubscribe' => null,
        'txtOnlineViewTag' => 'Voir en ligne',
        'txtHtmlUnsubscribeTag' => 'Me désabonner',
        'txtTextUnsubscribeTag' => 'Me désabonner',
        'txtSendToAFriendTag' => 'Envoyer a un ami'

      );

      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/campaign");
      curl_setopt($this->client, CURLOPT_POST, 1);

      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;

    }

    function ModificationCampagne(){
      // After non prio

    }

    function SuppressionCampagne($campagneid){
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array('campaignId' => $campagneid);
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/campaign/".$campagneid);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "DELETE");

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      return $reponsecode;

    }

    function SaveHtmlZip($campagneid,$cheminzip){

      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-type: application/octet-stream';

      curl_setopt($this->client,CURLOPT_CUSTOMREQUEST,'PUT');
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/campaignZipFile/".$campagneid."/0");
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      $file = $cheminzip;

      $fp = fopen($file,'r');
      var_dump($fp);
      curl_setopt($this->client, CURLOPT_UPLOAD, 1);
      curl_setopt($this->client, CURLOPT_INFILE, $fp);
      // curl_setopt($this->client, CURLOPT_INFILESIZE, filesize($file));
      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode);
      return $arrayretour;


    }

    function LectureHtml($campagneid){

      /*

      // marche pas
      // ecrit l'ensemble du stream dans le fichier

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      // $this->header[] = 'Content-Type: application/json';
      // $this->header[] = 'Accept: text/plain';
      $this->header[] = 'Content-type: application/octet-stream';

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/campaignZipFile/".$campagneid."/0");
      // curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_HEADER, 1);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_BINARYTRANSFER, 1);
      // curl_setopt($this->client, CURLOPT_FOLLOWLOCATION, 0);
      $raw_file_data = curl_exec($this->client);


      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      var_dump($reponseApi);
      var_dump($reponsecode);
      $filepath = storage_path()."/mailforyou/zip/dl.zip";
      // $filepath = storage_path()."/mailforyou/zip/dl.txt";
      file_put_contents($filepath, $raw_file_data);

      */

    }

    // LISTES

    function CreationListeDestinataire($dossierid,$listenom){
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array(
        'folderId' => $dossierid,
        'title' => $listenom,
      );
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/contactList");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      // $content = curl_multi_getcontent($this->client);
      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;

    }

    function CreationListeDestinataireFields($dossierid,$listenom){
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array(
        'folderId' => $dossierid,
        'title' => $listenom,
        'fields' => array(['name' => 'tor_id', 'type' =>'TXT','len' => 25],['name' => 'md5', 'type' =>'TXT','len' => 32])
      );
      $json = json_encode($arrayinfo);
      var_dump($json);
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/contactList");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      // $content = curl_multi_getcontent($this->client);
      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;

    }

    function ModificationListeDestinataire(){


    }

    function LectureListeDestinataire($listid){
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/contactList/".$listid."/recipients");

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      var_dump($reponseApi);
      var_dump($reponsecode);

    }

    function SuppressionListeDestinataire(){


    }

    function AjoutListeDestinataire($listid, $arraydestinataire){
      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      // bien penser pour le passage des param
      $arrayinfo = array(
        'columns' => array('Email'),
        // 'records' => array(array('hernoux.fabien@gmail.com'),array('adeline.sc2@gmail.com'),array('fabien@lead-factory.net')),
        'records' => $arraydestinataire,
      );
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/contactList/".$listid."/recipients");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode, 'json' => $json);
      return $arrayretour;

    }

    function AjoutListeDestinataireFields($listid, $arraydestinataire){
      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      // bien penser pour le passage des param
      $arrayinfo = array(
        'columns' => array('Email','tor_id','md5'),
        // 'records' => array(array('hernoux.fabien@gmail.com'),array('adeline.sc2@gmail.com'),array('fabien@lead-factory.net')),
        'records' => $arraydestinataire,
      );
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/contactList/".$listid."/recipients");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode, 'json' => $json);
      return $arrayretour;

    }



    // Liste rouge

    function AjoutListeRougeDestinataire(){


    }

    function LectureListeRougeDestinataire($folderid){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/redList/".$folderid."/recipients");

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      // var_dump($reponseApi);
      // var_dump($reponsecode);

      preg_match("/\{(.+)\}/",$reponseApi,$matches);
      if(isset($matches[0])){
      $t = json_decode($matches[0]);
      } else {
        $t = null;

      }


      return $t;

    }

    function SuppressionListeRougeDestinataire(){


    }

    // PLANIFICATION

    function SetPlanificationRoutage($campagneid /* ,$datetime */,$listeid, $exp, $domaine){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      // bien penser pour le passage des param
      $arrayinfo = array(
        'campaignId' => $campagneid,
        'dateTimeUTC' => date("Y-m-d H:i:s"),
        'schedule' => '0',
        'sendingRate' => env('M4Y_FREQ_SEND'),
        'filter' => null,
        'contactListId' => $listeid,
        'senderName' => $exp,
        'senderEmail' => 'news@' . $domaine,
        'returnPathEmail' => 'reply@' . $domaine,
        'transactional' => '0'
      );

      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;

    }

    function ValidationPlanificationRoutage($batchid){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array('batchId' => $batchid);
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch/".$batchid."/validate");

      curl_setopt($this->client,CURLOPT_CUSTOMREQUEST,'PUT');
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode);
      return $arrayretour;

    }

    function ChangementStatutPlanificationRoutage($batchid){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array('state' => '8');
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch/".$batchid);

      curl_setopt($this->client,CURLOPT_CUSTOMREQUEST,'PUT');
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      var_dump($reponseApi);
      var_dump($reponsecode);

    }

    function LecturePlanificationRoutage($debut, $fin){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch/".$debut."/".$fin);

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      // var_dump($reponseApi);
      // var_dump($reponsecode);
      // dd($reponseApi);

      preg_match("/\{(.+)\}/",$reponseApi,$matches);
      if(isset($matches[0])){
      $t = json_decode($matches[0]);
      } else {
        $t = null;
      }

      return $t;
    }

    /*
    function LectureStatutPlanificationRoutage(){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch/4/6");

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      var_dump($reponseApi);
      var_dump($reponsecode);
      dd($reponseApi);

    }
    */

    function m4u_bat($idcampagne){

      // compte BAT 102
       $m4u = Routeur::where('nom','MailForYou')->first();
      $lacampagne = Campagne::where('id',$idcampagne)->first();
      $utilisateurs = \App\Models\User::where('is_bat', 1)
          ->where('is_valid', 1)
          ->get();
      $tabat = array();
      foreach ($utilisateurs as $user) {
          $tabat[] = array($user->email);
          // var_dump($user->email);
      }

    // creation campagne
    $retour = $this->CreationCampagne('BAT-' . time() . $lacampagne->objet,'M4Y - ' . $lacampagne->objet,102);
    // var_dump($retour);

    // \Log::info(json_encode($retour));

    if(file_exists(storage_path() . '/mailforyou/zip/' . $lacampagne->ref)){
      \Log::info("Le fichier zip de la campagne existe deja");
    } else {
      // prevoir new modif ici
      mkdir(storage_path() . '/mailforyou/zip/' . $lacampagne->ref);
    }

    // zip
    $zip = new \ZipArchive;
    $filename = storage_path() . '/mailforyou/zip/'. $lacampagne->ref .'/fichiercampagne.zip';
    // $html = $lacampagne->generateHtml($m);
    if($zip->open($filename, \ZipArchive::CREATE) === true)
    {
        $zip->addFile('index.html');
        $zip->addFromString('index.html',$lacampagne->generateHtmlM4u($m4u));
        $zip->close();
    } else {
      echo 'Echec creation ZIP' . "\n";
    }

    $retour2 = $this->SaveHtmlZip($retour['id'], $filename);
    // \Log::info(json_encode($retour2));
    $retour3 = $this->CreationListeDestinataire(102,'Liste_BAT_'.time());
    // \Log::info(json_encode($retour3));
    // var_dump($retour3);
    $retour4 = $this->AjoutListeDestinataire($retour3['id'], $tabat);
    // \Log::info(json_encode($retour4));
    // var_dump()
    $retour5 = $this->SetPlanificationRoutage($retour['id'],$retour3['id'], $lacampagne->expediteur, 'lesoffresinmanquables.com');
    // \Log::info(json_encode($retour5));
    /// var_dump($retour5);
    $retour6 = $this->ValidationPlanificationRoutage($retour5['id']);
    // var_dump($retour6);
    // \Log::info(json_encode($retour6));
    }

    function m4u_bat_choix($idcampagne,$idsender){

      // MAJ on a le choix maintenant
      $m4u = Routeur::where('nom','MailForYou')->first();
      $lacampagne = Campagne::where('id',$idcampagne)->first();
      $utilisateurs = \App\Models\User::where('is_bat', 1)
          ->where('is_valid', 1)
          ->get();

      $lesenderbat = \DB::table('senders')->where('id',$idsender)->first();
      // var_dump($lesenderbat);
      // die();

      $tabat = array();
      foreach ($utilisateurs as $user) {
          $tabat[] = array($user->email);
      }

    // creation campagne
    $retour = $this->CreationCampagne('BAT-' . time() . $lacampagne->objet,'M4Y - ' . $lacampagne->objet,$lesenderbat->password);
    // \Log::info(json_encode($retour));


    if(file_exists(storage_path() . '/mailforyou/zip/' . $lacampagne->ref)){
      \Log::info("Le fichier zip de la campagne existe deja");
    } else {
      // prevoir new modif ici
      mkdir(storage_path() . '/mailforyou/zip/' . $lacampagne->ref);
    }

    // zip
    $zip = new \ZipArchive;
    $filename = storage_path() . '/mailforyou/zip/'. $lacampagne->ref .'/fichiercampagne.zip';
    // $html = $lacampagne->generateHtml($m);
    if($zip->open($filename, \ZipArchive::CREATE) === true)
    {
        $zip->addFile('index.html');
        $zip->addFromString('index.html',$lacampagne->generateHtmlM4u($m4u));
        $zip->close();
    } else {
      echo 'Echec creation ZIP' . "\n";
    }

    $retour2 = $this->SaveHtmlZip($retour['id'], $filename);
     \Log::info('2');
     \Log::info(json_encode($retour2));

    $retour3 = $this->CreationListeDestinataire(102,'Liste_BAT_'.time());
     \Log::info('3');
     \Log::info(json_encode($retour3));

    // var_dump($retour3);
    $retour4 = $this->AjoutListeDestinataire($retour3['id'], $tabat);
     \Log::info('4');
     \Log::info(json_encode($retour4));

    // var_dump()
    $retour5 = $this->SetPlanificationRoutage($retour['id'],$retour3['id'], $lacampagne->expediteur, $lesenderbat->domaine);
     \Log::info('5');
     \Log::info(json_encode($retour5));

    $retour6 = $this->ValidationPlanificationRoutage($retour5['id']);
     \Log::info('6');
     \Log::info(json_encode($retour6));

    }

    function LectureStatsRoutage($batchid){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $this->login;
      $this->header[] = 'X-Key:'. $this->key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/statsRecipients/" . $batchid);

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      // var_dump($reponseApi);
      // var_dump($reponsecode);

      // dd($reponseApi);

      preg_match("/\{(.+)\}/",$reponseApi,$matches);

      // var_dump($matches[0]);
      // dd($matches[0]);

      // var_dump(json_decode($matches[0]));
      if(isset($matches[0])){
      $t = json_decode($matches[0]);
      } else {
        $t = null;

      }
      // var_dump(json_decode($jsonretour));
      // $decode = json_decode($reponseApi);
      // var_dump($decode);
      return $t;
    }




    // new serveur manière de faire
    // CreationCampagne_new

    function CreationCampagne_new($nomCampagne, $objetCampagne, $folderId, $senderApiLogin, $senderApiKey){

      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $senderApiLogin;
      $this->header[] = 'X-Key:'. $senderApiKey;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array(
        'title' => $nomCampagne,
        'subject' => $objetCampagne,
        'folderId' => $folderId,
        'msgType' => '0',
        // 'msgSMS' => ' Le message sms',
        // 'urlUnsubscribe' => null,
        'txtOnlineViewTag' => 'Voir en ligne',
        'txtHtmlUnsubscribeTag' => 'Me désabonner',
        'txtTextUnsubscribeTag' => 'Me désabonner',
        'txtSendToAFriendTag' => 'Envoyer a un ami'

      );

      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/campaign");
      curl_setopt($this->client, CURLOPT_POST, 1);

      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;

    }

    // SaveHtmlZip_new

  function SaveHtmlZip_new($campagneid,$cheminzip,$senderApiLogin, $senderApiKey ){

      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $senderApiLogin;
      $this->header[] = 'X-Key:'. $senderApiKey;

      $this->header[] = 'Content-type: application/octet-stream';

      curl_setopt($this->client,CURLOPT_CUSTOMREQUEST,'PUT');
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/campaignZipFile/".$campagneid."/0");
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      $file = $cheminzip;

      $fp = fopen($file,'r');
      var_dump($fp);
      curl_setopt($this->client, CURLOPT_UPLOAD, 1);
      curl_setopt($this->client, CURLOPT_INFILE, $fp);
      // curl_setopt($this->client, CURLOPT_INFILESIZE, filesize($file));
      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode);
      return $arrayretour;


    }

    // CreationListeDestinataire_new

    function CreationListeDestinataireFields_new($dossierid,$listenom,$senderApiLogin,$senderApiKey){
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $senderApiLogin;
      $this->header[] = 'X-Key:'. $senderApiKey;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array(
        'folderId' => $dossierid,
        'title' => $listenom,
        'fields' => array(['name' => 'tor_id', 'type' =>'TXT','len' => 25],['name' => 'md5', 'type' =>'TXT','len' => 32])
      );
      $json = json_encode($arrayinfo);
      var_dump($json);
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/contactList");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      // $content = curl_multi_getcontent($this->client);
      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;

    }

    // AjoutListeDestinataire_new

    function AjoutListeDestinataireFields_new($listid, $arraydestinataire,$senderApiLogin,$senderApiKey){
      $arrayretour = null;
      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $senderApiLogin;
      $this->header[] = 'X-Key:'. $senderApiKey;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      // bien penser pour le passage des param
      $arrayinfo = array(
        'columns' => array('Email','tor_id','md5'),
        // 'records' => array(array('hernoux.fabien@gmail.com'),array('adeline.sc2@gmail.com'),array('fabien@lead-factory.net')),
        'records' => $arraydestinataire,
      );
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/contactList/".$listid."/recipients");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode, 'json' => $json);
      return $arrayretour;

    }


    // SetPlanificationRoutage_new

    function SetPlanificationRoutage_new($campagneid ,$listeid, $exp, $domaine,$senderApiLogin,$senderApiKey,$freq){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $senderApiLogin;
      $this->header[] = 'X-Key:'. $senderApiKey;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      // bien penser pour le passage des param
      $arrayinfo = array(
        'campaignId' => $campagneid,
        'dateTimeUTC' => date("Y-m-d H:i:s"),
        'schedule' => '0',
        'sendingRate' => $freq,
        'filter' => null,
        'contactListId' => $listeid,
        'senderName' => $exp,
        'senderEmail' => 'news@' . $domaine,
        'returnPathEmail' => 'reply@' . $domaine,
        'transactional' => '0'
      );

      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch");
      curl_setopt($this->client, CURLOPT_POST, 1);
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      \Log::info($reponseApi);
      $arrayretour = array('code' => $reponsecode, 'id' => $this->getInfoRetour($reponseApi));
      return $arrayretour;

    }

    // ValidationPlanificationRoutage_new

    function ValidationPlanificationRoutage_new($batchid,$senderApiLogin,$senderApiKey){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $senderApiLogin;
      $this->header[] = 'X-Key:'. $senderApiKey;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      $arrayinfo = array('batchId' => $batchid);
      $json = json_encode($arrayinfo);

      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch/".$batchid."/validate");

      curl_setopt($this->client,CURLOPT_CUSTOMREQUEST,'PUT');
      curl_setopt($this->client, CURLOPT_POSTFIELDS, $json);
      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      $arrayretour = array('code' => $reponsecode);
      return $arrayretour;

    }

    // m4u_bat_choix

    function m4u_bat_choix_new($idcampagne,$idsender){

      // MAJ on a le choix maintenant
      $m4u = Routeur::where('nom','MailForYou')->first();
      $lacampagne = Campagne::where('id',$idcampagne)->first();
      $utilisateurs = \App\Models\User::where('is_bat', 1)
          ->where('is_valid', 1)
          ->get();

      $lesenderbat = \DB::table('senders')->where('id',$idsender)->first();
      // var_dump($lesenderbat);
      // die();

      $tabat = array();
      foreach ($utilisateurs as $user) {
          $tabat[] = array($user->email);
      }

    // creation campagne
    $retour = $this->CreationCampagne_new('BAT-' . time() . $lacampagne->objet,'M4Y - ' . $lacampagne->objet,$lesenderbat->password,$lesenderbat->api_login,$lesenderbat->api_key);
    // \Log::info(json_encode($retour));
    // echo 'Create campagne';
    \Log::info('1');
    \Log::info(json_encode($retour));

    if(file_exists(storage_path() . '/mailforyou/zip/' . $lacampagne->ref)){
      \Log::info("Le fichier zip de la campagne existe deja");
    } else {
      // prevoir new modif ici
      mkdir(storage_path() . '/mailforyou/zip/' . $lacampagne->ref);
    }

    // zip
    $zip = new \ZipArchive;
    $filename = storage_path() . '/mailforyou/zip/'. $lacampagne->ref .'/fichiercampagne.zip';
    // $html = $lacampagne->generateHtml($m);
    if($zip->open($filename, \ZipArchive::CREATE) === true)
    {
        $zip->addFile('index.html');
        $zip->addFromString('index.html',$lacampagne->generateHtmlM4u($m4u));
        $zip->close();
    } else {
      echo 'Echec creation ZIP' . "\n";
    }

    $retour2 = $this->SaveHtmlZip_new($retour['id'], $filename,$lesenderbat->api_login,$lesenderbat->api_key);
     \Log::info('2');
     \Log::info(json_encode($retour2));

    $retour3 = $this->CreationListeDestinataireFields_new($lesenderbat->password,'Liste_BAT_'.time(),$lesenderbat->api_login,$lesenderbat->api_key);
     \Log::info('3');
     \Log::info(json_encode($retour3));

    // var_dump($retour3);
    $retour4 = $this->AjoutListeDestinataireFields_new($retour3['id'], $tabat,$lesenderbat->api_login,$lesenderbat->api_key);
     \Log::info('4');
     \Log::info(json_encode($retour4));

    // var_dump()
    $retour5 = $this->SetPlanificationRoutage_new($retour['id'],$retour3['id'], $lacampagne->expediteur, $lesenderbat->domaine,$lesenderbat->api_login,$lesenderbat->api_key,1000);
     \Log::info('5');
     \Log::info(json_encode($retour5));

    $retour6 = $this->ValidationPlanificationRoutage_new($retour5['id'],$lesenderbat->api_login,$lesenderbat->api_key);
     \Log::info('6');
     \Log::info(json_encode($retour6));

    }

    // maj les bdd des serveurs en prod

    function LectureStatsRoutage_new($batchid,$senderid){

      $routagesender = \DB::table('senders')->where('id',$senderid)->first();

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $routagesender->api_login;
      $this->header[] = 'X-Key:'. $routagesender->api_key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/statsRecipients/" . $batchid);

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      // var_dump($reponseApi);
      // var_dump($reponsecode);

      // dd($reponseApi);

      preg_match("/\{(.+)\}/",$reponseApi,$matches);

      // var_dump($matches[0]);
      // dd($matches[0]);

      // var_dump(json_decode($matches[0]));
      if(isset($matches[0])){
      $t = json_decode($matches[0]);
      } else {
        $t = null;

      }
      // var_dump(json_decode($jsonretour));
      // $decode = json_decode($reponseApi);
      // var_dump($decode);
      return $t;
    }


    function LecturePlanificationRoutage_new($debut, $fin, $senderid){

      $routagesender = \DB::table('senders')->where('id',$senderid)->first();

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $routagesender->api_login;
      $this->header[] = 'X-Key:'. $routagesender->api_key;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/batch/".$debut."/".$fin);

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);

      preg_match("/\{(.+)\}/",$reponseApi,$matches);
      if(isset($matches[0])){
      $t = json_decode($matches[0]);
      } else {
        $t = null;
      }

      return $t;
    }

    function LectureListeRougeDestinataire_new($folderid,$senderApiLogin,$senderApiKey){

      $this->client = curl_init();
      $this->header = array();

      $this->header[] = 'X-Client:'. $senderApiLogin;
      $this->header[] = 'X-Key:'. $senderApiKey;

      $this->header[] = 'Content-Type: application/json';
      $this->header[] = 'Accept: text/plain';

      curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($this->client, CURLOPT_URL, $this->url . "/v1/redList/".$folderid."/recipients");

      curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->client, CURLOPT_HTTPHEADER,$this->header);
      curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);

      curl_setopt($this->client, CURLOPT_HEADER, true);

      $reponseApi = curl_exec($this->client);
      $reponsecode = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

      curl_close($this->client);
      // var_dump($reponseApi);
      // var_dump($reponsecode);

      preg_match("/\{(.+)\}/",$reponseApi,$matches);
      if(isset($matches[0])){
      $t = json_decode($matches[0]);
      } else {
        $t = null;

      }


      return $t;

    }



    function getInfoRetour($chaine){

      // ok for
        // create dossier

      $chaine = trim($chaine);
      $a = strpos($chaine,'/v1');
      $b = strpos($chaine,'Content-Type:');
      $scope = $b - $a;
      $r = substr($chaine,$a,$scope);
      $r = trim($r);
      // var_dump($r);
      $explode = explode("/",$r);
      $max = count($explode);
      for ($i=0; $i < $max; $i++) {
        if(is_numeric($explode[$i])){
          // var_dump($explode[$i]);
          $valretour = $explode[$i];
          break;
        }
      }

      if(isset($valretour)){

        return $valretour;
      } else {
        return false;

      }

    }

}
