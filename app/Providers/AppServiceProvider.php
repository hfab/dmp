<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        $app_version = "";

        if (\Schema::hasTable('settings')) {
            $app_major_version = \DB::table('settings')->where('parameter', 'app_major_version')->first();
            $app_minor_version = \DB::table('settings')->where('parameter', 'app_minor_version')->first();
            $app_patch_version = \DB::table('settings')->where('parameter', 'app_patch_version')->first();


            if ($app_major_version && $app_minor_version && $app_patch_version) {
                $app_version = "$app_major_version->value.$app_minor_version->value.$app_patch_version->value";
            }
        }
        define('APP_VERSION', $app_version);
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
