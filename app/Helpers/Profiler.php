<?php namespace App\Helpers;

class Profiler {
    public static $session_name;
    public static $start_time;

    static function start($session_name = null) {
        if ($session_name != null) {
            self::$session_name = $session_name;
        }

        self::$start_time = microtime(true);
    }

    static function report($name = null) {
        $report = "";
        $report_time = microtime(true);

        $execution_time = round($report_time - self::$start_time, 3);

        $ram = memory_get_peak_usage();
        $ram_clean = self::formatBytes($ram);

        if ($name != null) {
            $report .= "\n*** PROFILER REPORT : $name *** ";
            echo "\n*** PROFILER REPORT : $name *** ";
        } else {
            $report .= "\n*** PROFILER REPORT ***";
            echo "\n*** PROFILER REPORT ***";
        }

        if (self::$session_name != null) {
            echo "for ".self::$session_name."\n";
        }

        echo "\tExecution Time : $execution_time s\n";
        echo "\tRAM Usage: $ram_clean\n\n";
        
        $report .= "\tExecution Time : $execution_time s\n";
        $report .= "\tRAM Usage: $ram_clean\n\n";
        return $report;
    }

    static function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}