<?php

namespace App\Commands;

use App\Commands\Command;
use App\Models\Base;
use App\Models\Destinataire;
use App\Models\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class BaseImportDestinataires extends Command implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $base_id;
    public $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($base_id, $file)
    {
        $this->base_id = $base_id;
        $this->file = storage_path().'/listes/'.$file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {

        $base_id = $this->base_id;
        $file = $this->file;

        if (!is_file($file)) {
            die('File not found : '.$file);
        }

        \Log::info('BASE ID: '.$base_id);

        \App\Helpers\Profiler::start('import');

        \DB::disableQueryLog();

        set_time_limit(0);
        ini_set('max_execution_time', 0);

//        $nochecks = $this->option('no-checks');

        $nochecks = null;

        $base = Base::find($base_id);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;
        $rejected = 0;
        $already = 0;
        $comptageFAI = array();


        $base_id = $base->id;
        $liste = str_replace(".$extension",'',$name);

        $bulk = [];
        while (!feof($fh)) {
            $cells = [];

            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell) {
                // if cell is an email, insert it
                if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                    $total++;

                    $fai = Destinataire::getFai($cell);

                    if (!isset($fai->nom)) {
                        var_dump($fai);
                        var_dump($cell);
                        die();
                    }

                    if(!isset($comptageFAI[$fai->nom]))
                    {
                        $comptageFAI[$fai->nom]=1;
                    }
                    else
                    {
                        $comptageFAI[$fai->nom]++;
                    }

/*                    if ($nochecks == null) {
                        $dest_check = \DB::table('destinataires')->where('base_id', $base_id)->where('mail', $cell)->first();
                        if ($dest_check != null) {
                            $already++;
                            continue;
                        }
                    }
*/

//                    echo "$cell - ";

                    $bulk[] = array(
                        'base_id' => $base_id,
                        'fai_id' => $fai->id,
                        'mail' => $cell,
                        'origine' => $liste,
                    );
//                    echo count($bulk);

                    echo '.';

                    if (count($bulk) >= 25) {
                        \DB::table('destinataires')->insert($bulk);



                        $bulk = [];
                        echo "\n";
                    }

                    $inserted++;
                    continue;
                } else {
                    $rejected++; // aucun mail sur la ligne
                }
            }
        }

        $infos = [
            "infos_fais" => $comptageFAI,
            "total" => $total,
            "already" => $already,
            "inserted" => $inserted,
            "rejected" => $rejected,
            'file' => $name
        ];

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => true,
            'message' => "Fichier $name importé. $inserted insérés, $already déjà présents, $rejected rejetés."
        ]);

        \App\Helpers\Profiler::report();
    }
}
