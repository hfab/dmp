<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\Models\Campagne;
use App\Models\Repoussoir;

class CampagneImportRepoussoirs extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $campagne_id;
    public $file;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campagne_id, $file)
    {
        $this->campagne_id = $campagne_id;
        $this->file = storage_path().'/repoussoirs/'.$file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $campagne_id = $this->campagne_id;
        $file = $this->file;
        $ts = date('Y-m-d H:i:s');
        $bulk_count = 0;
        if (!is_file($file)) {
            \Log::error('Repoussoirs : File not found : '.$file);
            $this->error('File not found : '.$file);
            exit;
        }

        \Log::info('Insert (QUEUE COMMAND) in repoussoir pour la campagne '.$campagne_id.' -- Fichier '.$file);

        \App\Helpers\Profiler::start('import');

        \DB::disableQueryLog();

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO repoussoirs (destinataire_id, campagne_id, created_at, updated_at) VALUES (:destinataire, :campagne, :ts1, :ts2)";
        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $today = Carbon::today();

        $campagne = Campagne::find($campagne_id);

        $old = Repoussoir::where('campagne_id', $campagne_id)->delete();

        $extension = \File::extension($file);
        $name = \File::name($file);

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;
        $rejected = 0;
        $not_in_base = 0;

        $baseid = $campagne->base_id;
        $bulk = [];
        while ($row = fgets($fh, 1024)) {
            $cells = [];
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell) {
                // if cell is an md5, insert it
                if (strlen($cell)!=32) {
                    $rejected++;
                    continue;
                }
                $total++;

                $dest_check = \DB::table('destinataires')
                    ->select('id')
                    ->where('base_id', $baseid)
                    ->where('hash', $cell)
                    ->first();
//                        var_dump($dest_check);
                if ($dest_check == null) {
                    $not_in_base++;
                    continue;
                }

                $insertData = [$dest_check->id,$campagne->id, $ts, $ts];
                $stmt->execute($insertData);

                $bulk_count++;
                if ($bulk_count >= 250) {
                    $pdo->commit();
                    $pdo->beginTransaction();
                    $bulk_count = 0;
                }

                $inserted++;
                continue;
            }
        }

        $pdo->commit();

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        \App\Models\Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => true,
            'message' => "Repoussoir $name associé à $campagne->ref / C$campagne->id-R
             [$inserted communs/ $not_in_base absents / $rejected rejetés sur $total]"
        ]);

        \Log::info("Fichier repoussoir $name associé à $campagne->ref / C$campagne->id-R
        [$inserted communs/ $not_in_base absents / $rejected rejetés sur $total]");
//        \Event::fire(new \App\Events\DestinatairesImported($infos));

        \App\Helpers\Profiler::report();

    }
}
