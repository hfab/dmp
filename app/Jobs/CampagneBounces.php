<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Campagne;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampagneBounces extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Campagne $campagne)
    {
        $this->campagne = $campagne;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $campagne = $this->campagne;

        $camp_rout = CampagneListe::where('campagne_id', $campagne->id)
            ->whereRaw('routeur_cid is not null')
            ->whereRaw('bounces_at is null')
            ->get();

        foreach($camp_rout as $row)
        {
            $sender = Sender::find($row->sender_id);
            if ($sender->routeur->id != 1) {
                continue;
            }

            \Log::info('Job-Bounces : campagne '.$campagne->id.', sender '.$sender->id.', campagne_routeur '.$row->routeur_cid);

            $routeurmaildrop = new RouteurMaildrop();
            $routeurmaildrop->getBounces($sender->password, $campagne, $row->routeur_cid);
        }
    }
}
