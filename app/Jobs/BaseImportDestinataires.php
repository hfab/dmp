<?php

namespace App\Jobs;

use App\Models\Base;
use App\Models\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\SendersPopulate;

class BaseImportDestinataires extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $base_id;
    public $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($base_id, $file)
    {
        $this->base_id = $base_id;
        $this->file = storage_path().'/listes/'.$file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {

        $base_id = $this->base_id;
        $file = $this->file;

        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        \Log::info("Import Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");

        $ts = date('Y-m-d H:i:s');

        \App\Helpers\Profiler::start('import');

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO destinataires (mail, base_id, fai_id, hash, liste, created_at, updated_at) VALUES (:mail, :base_id, :fai_id, :hash, :liste, :ts1, :ts2)";
        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();
        $bulk_count = 0;

        $fais = \App\Models\Fai::all();
        $default_fai = \App\Models\Fai::find(6);

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $base = Base::find($base_id);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;
        $rejected = 0;
        $already = 0;


        $base_id = $base->id;
        $liste = str_replace(".$extension",'',$name);

        $bulk = [];
        while (!feof($fh)) {
            $cells = [];

            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell) {
                $fai = $default_fai;

                if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                    $total++;

                    foreach($fais as $k_fai) {
                        foreach($k_fai->get_domains() as $dom) {
                            if (strpos($cell,$dom) !== false) {
                                $fai = $k_fai;
                                break 2;
                            }
                        }
                    }

                    if (!isset($fai->nom)) {
                        var_dump($fai);
                        var_dump($cell);
                        die();
                    }

                    $insertData = [$cell, $base_id, $fai->id, md5($cell), $liste, $ts, $ts];
                    $stmt->execute($insertData);

                    $bulk_count++;
                    if ($bulk_count >= 250) {
                        $pdo->commit();
                        $pdo->beginTransaction();
                        $bulk_count = 0;
                    }

                    $inserted++;
                    continue;
                } else {
                    $rejected++;
                }
            }
        }

        $pdo->commit();

        //On blackliste ensuite les @ contenues en blacklistes
        \DB::query("UPDATE destinataires SET statut = 6666 WHERE base_id = $base_id and mail in (SELECT email FROM blacklistes) and liste = '$liste'");

        //On desinscrits toutes les @ qui se sont déjà désinscrites
        \DB::query("UPDATE destinataires SET statut = 3 WHERE base_id = $base_id and mail in (SELECT mail FROM desinscrits) and liste = '$liste'");

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => true,
            'message' => "Fichier $name importé. $inserted insérés, $already déjà présents, $rejected rejetés."
        ]);

        \Log::info("End of Import Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");
        \App\Helpers\Profiler::report();

        $list_or_segment = \DB::table('settings')
            ->where('parameter', 'mindbaz_mode')
            ->first();
        
//        if($list_or_segment->value == 'segment') {
//            \Artisan::call('base:import_mindbaz', ["base_id" => $base_id, 'filename' => "$name.$extension"]);
//        }
    }
}
