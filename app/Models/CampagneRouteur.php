<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampagneRouteur extends Model {

    protected $table = 'campagnes_routeurs';

    protected $fillable = ['campagne_id', 'planning_id', 'sender_id', 'cid_routeur'];

    function scopeByDate($query, $date)
    {
        return $query->where('created_at', $date);
    }

}
