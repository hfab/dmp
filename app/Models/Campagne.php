<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Campagne extends Model
{
    protected $table = 'campagnes';

    public function base()
    {
        return $this->hasOne('\App\Models\Base', 'id', 'base_id');
    }

    public function bases()
    {
        return $this->hasMany('\App\Models\CampagneBase', 'base_id', 'id');
    }

    public function plannings()
    {
        return $this->hasMany('\App\Models\Planning', 'campagne_id', 'id');
    }

    public function theme()
    {
        return $this->hasOne('\App\Models\Theme', 'id', 'theme_id');
    }

    public function partner()
    {
        return $this->hasOne('\App\Models\Partner', 'id', 'dmp_partnersite_id');
    }
    //Genère HTML en fonction du routeur
    private function generateHeader(\App\Models\Routeur $routeur)
    {
        $mentions = "";
        $mentions2 = "";

        if($this->withtags == 1 && $this->base_id != 0) {
            $mentions =
                "<TR>
                    <TD width='WWLONGWW' style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:grey; text-align:center;'> En poursuivant votre navigation, vous acceptez l'utilisation de cookies pour vous proposer des services et offres adapt&eacute;s <br/> &agrave; vos centres d'int&eacute;r&ecirc;ts et mesurer la fr&eacute;quentation de nos services. Pour en savoir plus suivez <a href='" . trim($this->base->mentions_header, " \t\n\r\0\x0B") . "' target='_blank'> ce lien </a></TD>
                </TR>";
            $mentions2  = "<p width='WWLONGWW' style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:grey; text-align:center;'> En poursuivant votre navigation, vous acceptez l'utilisation de cookies pour vous proposer des services et offres adapt&eacute;s <br/> &agrave; vos centres d'int&eacute;r&ecirc;ts et mesurer la fr&eacute;quentation de nos services. Pour en savoir plus suivez <a href='" . trim($this->base->mentions_header, " \t\n\r\0\x0B") . "' target='_blank'> ce lien </a></p>";
        }

        $headerrouteur =
            "<div> <TABLE width='WWLONGWW' border='0' cellspacing='0' cellpadding='0' align='center' >
                <TR>
                  <TD width='WWLONGWW' valign='top' align='center'>
                  <table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' >
                      <tr>
                        <td width='49%'  style='text-align:left;'><a href='$routeur->variable_mirror' style='color:#000; text-decoration:underline; font-family:Arial,Helvetica,sans-serif; font-size:11px; text-align:left; font-weight:bold;'>Afficher la version web.</a></td>
                        <td width='6'>&#124;</td>
                        <td width='49%'  style='text-align:center;'><a href='$routeur->variable_unsubscribe' style='color:#000; text-decoration:underline; font-family:Arial,Helvetica,sans-serif; font-size:11px; text-align:center; font-weight:bold;'>Annuler votre abonnement.</a></td>
                      </tr>
                    </table></TD>
                </TR>
                    $mentions
                <TR>
                  <TD  width='WWLONGWW' align='center'>WWTOPWW</TD>
                </TR>
            </TABLE>
            </div>";

        if( $this->base_id != 0 && strlen($this->base->header_html) > 10){
            $headerrouteur = "<div align='center' style='color:#000; font-family:Arial,Helvetica,sans-serif; font-size:11px;'> ".utf8_decode($this->base->header_html)." $mentions2 </div>";
        }

        return $headerrouteur;
    }

    private function generateFooter(\App\Models\Routeur $routeur)
    {
        if($this->base_id == 0) {
            return
                "<div>
            <TABLE width='WWLONGWW' border='0' cellspacing='0' cellpadding='0' align='center'style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:black; text-align:center;'>
                <tr>
                  <td style='font-size:10px; text-align:center;' ><br />
                    WWBOTTOMWW
                    </td>
                </tr>
                <tr><td>WWBASBASWW</td></tr>
                <tr><td>Vous souhaitez vous d&eacute;sinscrire de cette liste ? <a href='$routeur->variable_unsubscribe' style='color: #000; text-decoration:underline; font-size: 11px;'>Cliquez ici.</a></td></tr>
            </TABLE>
           </div>";
        }

        $footerrouteur =
            "<div>
            <TABLE width='WWLONGWW' border='0' cellspacing='0' cellpadding='0' align='center'style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:black; text-align:center;'>
                <tr>
                  <td style='font-size:10px; text-align:center;' ><br />
                    WWBOTTOMWW
                    ".str_ireplace("\r","<br/>",utf8_decode($this->base->mentions_legales))."
                    </td>
                </tr>
                <tr><td>WWBASBASWW</td></tr>
                <tr><td>Vous souhaitez vous d&eacute;sinscrire de cette liste ? <a href='$routeur->variable_unsubscribe' style='color: #000; text-decoration:underline; font-size: 11px;'>Cliquez ici.</a></td></tr>
            </TABLE>
           </div>";

        if(!empty($this->base->footer_html)){
            $footerrouteur = "<div align='center' style='color:#000; font-family:Arial,Helvetica,sans-serif; font-size:11px;'> <br/>".str_ireplace("\r","<br/>",utf8_decode($this->base->mentions_legales))."</br> ".str_ireplace("\r","<br/>",utf8_decode($this->base->footer_html))."</div>";
        }

        return $footerrouteur;
    }

    // pas propre à refaire
    private function generateFooterAnonyme(\App\Models\Routeur $routeur)
    {
        $footerrouteur =
            "<div>
                <TABLE width='WWLONGWW' border='0' cellspacing='0' cellpadding='0' align='center'style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:black; text-align:center;'>
                        <tr><td></td></tr>
                        <tr><td>Vous souhaitez vous d&eacute;sinscrire de cette liste ? <a href='$routeur->variable_unsubscribe' style='color: #000; text-decoration:underline; font-size: 11px;'>Cliquez ici.</a></td></tr>
                </TABLE>
      		</div>";
        if($this->base_id != 0){
            $footerrouteur = "<div align='center' style='color:#000; font-family:Arial,Helvetica,sans-serif; font-size:11px;'> ".str_ireplace("\r","<br/>",utf8_decode($this->base->footer_html))." </div>";
        }
        return $footerrouteur;
    }

    //Genère HTML en fonction du routeur
    private function getMaxWidth($domdocument,$tagname)
    {
        //On recupère la largeur du body si il y a
        $tags   =    $domdocument->getElementsByTagName($tagname);
        $tagWidth = -1 ;

        foreach($tags as $tag)
        {
            if(isset($tag->attributes->getNamedItem('width')->nodeValue)) {
                if ($tag->attributes->getNamedItem('width')->nodeValue > $tagWidth) {
                    $tagWidth = $tag->attributes->getNamedItem('width')->nodeValue;
                }
            }
            elseif(isset($tag->attributes->getNamedItem('style')->nodeValue))
            {
                $positionWidthStart = stripos($tag->attributes->getNamedItem('style')->nodeValue,"width:");
                $blocWidth = substr($tag->attributes->getNamedItem('style')->nodeValue,$positionWidthStart,strlen($tag->attributes->getNamedItem('style')->nodeValue));
                $positionWidthEnd = stripos($blocWidth,";");
                $blocWidth = substr($blocWidth,0,$positionWidthEnd);
                $explodeBloc = explode(':',$blocWidth);

                if(count($explodeBloc)!=2)
                {
                    continue;
                }

                $explodeBloc[1] = str_ireplace('px','',$explodeBloc[1]);

                if($explodeBloc[1]> $tagWidth)
                {
                    $tagWidth = $explodeBloc[1] ;
                }
            }
        }
        if($tagWidth == -1)
        {
            $tagWidth = '100%';
        }
        return $tagWidth;
    }

    public function generateHtml(\App\Models\Routeur $routeur, $planning_id = null)
    {
        $this->planning_id = $planning_id;
        $cache_key = 'c' . $this->id . '-r' . $routeur->id;
        if (\Cache::has($cache_key)) {
            $this->generated_html = \Cache::get($cache_key);
        } else {
            $campagne = $this;

            //Generation du Header et du Footer en fonction du routeur et de la base
            $header = $this->generateHeader($routeur);

            $htmlcampagneWidth = $this->getWidthHtml($campagne->html);

            $header = str_ireplace("WWLONGWW", $htmlcampagneWidth, $header);
            $header = str_ireplace("WWTOPWW", $campagne->toptext, $header);
            $header = str_ireplace("[mirrorlink]", $routeur->variable_mirror, $header);
            $header = str_ireplace("[unsubscribelink]", $routeur->variable_unsubscribe, $header);

            $dom_header = new \DOMDocument();
            @$dom_header->loadHTML($header);

            // on récupère le DIV dans le footer
            $header_table = $dom_header->getElementsByTagName("div")->item(0);

            $footer = $this->generateFooter($routeur);
            $footer = str_ireplace("WWLONGWW", $htmlcampagneWidth, $footer);
            $footer = str_ireplace("WWBOTTOMWW", $campagne->bottomtext, $footer);
            $footer = str_ireplace("WWBASBASWW", $campagne->belowtext, $footer);
            $footer = str_ireplace("[mirrorlink]", $routeur->variable_mirror, $footer);
            $footer = str_ireplace("[unsubscribelink]", $routeur->variable_unsubscribe, $footer);

            $dom_footer = new \DOMDocument();
            @$dom_footer->loadHTML($footer);

            // on récupère le DIV dans le footer
            $footer_table = $dom_footer->getElementsByTagName("div")->item(0);

            $dom_pixel = new \DOMDocument();
            @$dom_pixel->loadHTML("<div style='height:0px;width:0px;'>".$this->getPixel($routeur)."</div>");
            // on récupère le DIV du pixel
            $img_pixel = $dom_pixel->getElementsByTagName("div")->item(0);

            //on remplace toutes les variables personnalisables par les variables routeurs ou des references
            $this->html = str_ireplace("[email]", $routeur->variable_email, $this->html);
            $this->html = str_ireplace("[emailsha1]", $routeur->variable_email_sha1, $this->html);
            $this->html = str_ireplace("[emailmd5]", $routeur->variable_email_md5, $this->html);
            $this->html = str_ireplace("[subcode]", 'tor-'.getenv('CLIENT_NAME').'-c'.$this->id, $this->html); //reference de tracking campagne pour un tag
            $this->html = str_ireplace("[nom]", $routeur->variable_nom, $this->html);
            $this->html = str_ireplace("[prenom]", $routeur->variable_prenom, $this->html);
            $this->html = str_ireplace("[base]", $this->base_id, $this->html);

            // on créé un Dom pour la Campagne
            $dom_campagne = new \DOMDocument();
            //urlencode pour les pbs avec les "doubles" urls avec le deuxieme lien deja encodé.. permet de garder l'encodage
            @$dom_campagne->loadHTML(urlencode($this->html));

            // on transforme le header et le footer en node
            $node_footer = $dom_campagne->importNode($footer_table, true);
            $node_header = $dom_campagne->importNode($header_table, true);

            //on injecte le pixel dans le body
            $node_pixel = $dom_campagne->importNode($img_pixel, true);

            // on injecte le footer dans le body
            $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_pixel);
            $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_footer);

            //on injecte le header dans le body
            $firstItem = $dom_campagne->getElementsByTagName('body')->item(0)->childNodes->item(0);

            //parentNode necessaire pour faire le insertBefore, sans le parentNode -> ne fonctionne pas BON A SAVOIR
            @$firstItem->parentNode->insertBefore($node_header,$firstItem);

            $this->generated_html = $dom_campagne->saveHTML();
            if($this->hostimage) {
                $this->generated_html = $this->store_images('', getenv('CDN_URL'));
            }

            $this->generated_html = htmlspecialchars_decode($this->generated_html);
            $this->generated_html = html_entity_decode($this->generated_html);

//            $this->generated_html = str_replace($link_replaces['out'], $link_replaces['in'], $this->generated_html);

            \Cache::put($cache_key, urldecode($this->generated_html), 60);
        }

        return urldecode($this->generated_html);
    }

    // modif ajout anonyme
    public function generateHtmlAnonyme(\App\Models\Routeur $routeur, $planning_id = null)
    {
        $this->planning_id = $planning_id;
        $cache_key = 'c' . $this->id . '-r' . $routeur->id .'-anonyme';
        if (\Cache::has($cache_key)) {
            $this->generated_html = \Cache::get($cache_key);
        } else {
            $campagne = $this;

            //Generation du Header et du Footer en fonction du routeur et de la base
            $header = $this->generateHeader($routeur);

            $htmlcampagneWidth = $this->getWidthHtml($campagne->html);

            $header = str_ireplace("WWLONGWW", $htmlcampagneWidth, $header);
            $header = str_ireplace("WWTOPWW", $campagne->toptext, $header);
            $header = str_ireplace("[mirrorlink]", $routeur->variable_mirror, $header);
            $header = str_ireplace("[unsubscribelink]", $routeur->variable_unsubscribe, $header);

            $dom_header = new \DOMDocument();
            @$dom_header->loadHTML($header);

            // on récupère le DIV dans le footer
            $header_table = $dom_header->getElementsByTagName("div")->item(0);

            $footer = $this->generateFooterAnonyme($routeur);
            $footer = str_ireplace("WWLONGWW", $htmlcampagneWidth, $footer);
            $footer = str_ireplace("WWBOTTOMWW", $campagne->bottomtext, $footer);
            $footer = str_ireplace("WWBASBASWW", $campagne->belowtext, $footer);
            $footer = str_ireplace("[mirrorlink]", $routeur->variable_mirror, $footer);
            $footer = str_ireplace("[unsubscribelink]", $routeur->variable_unsubscribe, $footer);

            $dom_footer = new \DOMDocument();
            @$dom_footer->loadHTML($footer);

            // on récupère le DIV dans le footer
            $footer_table = $dom_footer->getElementsByTagName("div")->item(0);

            $dom_pixel = new \DOMDocument();
            @$dom_pixel->loadHTML("<div style='height:0px;width:0px;'>".$this->getPixel($routeur)."</div>");
            // on récupère le DIV du pixel
            $img_pixel = $dom_pixel->getElementsByTagName("div")->item(0);

            //on remplace toutes les variables personnalisables par les variables routeurs ou des references
            $this->html = str_ireplace("[email]", $routeur->variable_email, $this->html);
            $this->html = str_ireplace("[emailsha1]", $routeur->variable_email_sha1, $this->html);
            $this->html = str_ireplace("[emailmd5]", $routeur->variable_email_md5, $this->html);
            $this->html = str_ireplace("[subcode]", 'tor-'.getenv('CLIENT_NAME').'-c'.$this->id, $this->html); //reference de tracking campagne pour un tag
            $this->html = str_ireplace("[nom]", $routeur->variable_nom, $this->html);
            $this->html = str_ireplace("[prenom]", $routeur->variable_prenom, $this->html);
            $this->html = str_ireplace("[base]", $this->base_id, $this->html);

            // on créé un Dom pour la Campagne
            $dom_campagne = new \DOMDocument();
            //urlencode pour les pbs avec les "doubles" urls avec le deuxieme lien deja encodé.. permet de garder l'encodage

            @$dom_campagne->loadHTML(urlencode($this->html));

            // on transforme le header et le footer en node
            $node_footer = $dom_campagne->importNode($footer_table, true);
            $node_header = $dom_campagne->importNode($header_table, true);

            //on injecte le pixel dans le body
            $node_pixel = $dom_campagne->importNode($img_pixel, true);

            // on injecte le footer dans le body
            $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_pixel);
            $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_footer);

            //on injecte le header dans le body
            $firstItem = $dom_campagne->getElementsByTagName('body')->item(0)->childNodes->item(0);

            //parentNode necessaire pour faire le insertBefore, sans le parentNode -> ne fonctionne pas BON A SAVOIR
            @$firstItem->parentNode->insertBefore($node_header,$firstItem); //TODO : a mettre en commentaire si ca ne marche pas en PROD

            $this->generated_html = $dom_campagne->saveHTML();
            if($this->hostimage) {
                $this->generated_html = $this->store_images('', getenv('CDN_URL'));
            }

            $this->generated_html = htmlspecialchars_decode($this->generated_html);
            $this->generated_html = html_entity_decode($this->generated_html);

            \Cache::put($cache_key, urldecode($this->generated_html), 60);
        }

        return urldecode($this->generated_html);
    }



    /**
     * @param $html
     * @return string
     */
    private function getWidthHtml($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);

        $getMax = array(0);

        $bodyMaxWidth = $this->getMaxWidth($dom,'body');
        $divMaxWidth = $this->getMaxWidth($dom,'div');
        $tableMaxWidth = $this->getMaxWidth($dom,'table');
        $imgMaxWidth = $this->getMaxWidth($dom,'img');

        if(is_numeric($getMax) or strpos($bodyMaxWidth, 'px')){
            $getMax[] = $bodyMaxWidth;
        }
        if(is_numeric($getMax) or strpos($divMaxWidth, 'px')){
            $getMax[] = $divMaxWidth;
        }
        if(is_numeric($getMax) or strpos($tableMaxWidth, 'px')){
            $getMax[] = $tableMaxWidth;
        }
        if(is_numeric($getMax) or strpos($imgMaxWidth, 'px')){
            $getMax[] = $imgMaxWidth;
        }

        return max($getMax);
    }


    /**
     * @param $path Directory where to store the images
     */
    public function store_images($path, $url, $minify = true)
    {
        $url = getenv('CDN_URL');
        @mkdir($path, 0777, true);

        //TODO change URL according to $this->base_id
        //TODO table base: ajouter champ "images_url"

        $this->set_xpath();

        //Si jamais il y a une balise <base href='..'>
        $balbase = $this->xpath->query('//base')->item(0);
        $baseurl = "";
        if($balbase != null && strlen($balbase->getAttribute('href')) > 0) {
            $baseurl = $balbase->getAttribute('href');
        }
        $this->create_images_list();

        if (isset($this->generated_html)) {
            $this->html_out = $this->generated_html;
        } else {
            $this->html_out = $this->html;
        }

        $path = public_path().'/cdn/'.$this->ref.'/';
        @mkdir($path);

        foreach($this->images as $image) {
            $src = $baseurl.$image->getAttribute('src');
            $data = @file_get_contents($src);

            if (!$data) {
                continue;
            }

            $extension = \File::extension($src);
            if (!in_array($extension, ['jpg','gif','png'])) {
                continue;
            }

            $hash = md5($src).".$extension";

            if (!is_file($path.$hash)) {
                file_put_contents($path.$hash, $data);
            }
            $src = $image->getAttribute('src');
            $imgPath = $url.$this->ref.'/'.$hash;
            $img_replaces['in'][] = urlencode($src);
            $img_replaces['out'][] = urlencode($imgPath);

//            $this->html_out = str_replace($src, $imgPath, $this->html_out );

        }

        if(!empty($img_replaces['in']) || !empty($img_replaces['ou'])){
            $this->html_out = str_replace($img_replaces['in'], $img_replaces['out'], $this->html_out);
        }

        if ($minify)
        {
            $replace = array(
                '/<!--[^\[](.*?)[^\]]-->/s' => '',
                "/<\?php/"                  => '<?php ',
                "/\r/"                      => '',
                "/>\n</"                    => '><',
                "/>\s+\n</"    				=> '><',
                "/>\n\s+</"					=> '><',
            );

            $this->html_out = preg_replace(array_keys($replace), array_values($replace), $this->html_out);
        }

//        echo $this->html_out;
//        die();

        return $this->html_out;
    }

    public function store_links($planning)
    {
        $this->set_xpath();

        //Si jamais il y a une balise <base href='..'>
        $balbase = $this->xpath->query('//base')->item(0);
        $baseurl = "";
        if($balbase != null && strlen($balbase->getAttribute('href')) > 0) {
            $baseurl = $balbase->getAttribute('href');
        }
        //je crée ma liste de liens
        $this->create_links_list();

        if (isset($this->generated_html)) {
            $this->html_out = $this->generated_html;
        } else {
            $this->html_out = $this->html;
        }

        foreach($this->links as $link){
            $href = json_encode($baseurl.$link->getAttribute('href'));

            $dmp_link = "";

//            $dmp_domain_partner = \DB::table('dmp_domain_partner')
            /*$dmp_domain_partner = \DB::table('dmp_bases_sites')
//                ->where('partner_id', $this->partner_id)
                ->where('base_id', $this->base_id)
                ->orderByRaw("RAND()")
		->first();*/

	    $dmp_domain_partner = DB::table('dmp_partnersites')->first();
            if(!empty($dmp_domain_partner)){
                $dmp_link = $dmp_domain_partner->url;

                $links_replaces['in'][] = $href;
    //            $links_replaces['out'][] = $dmp_link."?prd=$this->partner_id&vsd=".$planning->routeur->variable_tor_id."&pld=$planning->id&to=".$href;
                $links_replaces['out'][] = $dmp_link."?bsd=$this->base_id&vsd=".$planning->routeur->variable_tor_id."&pld=$planning->id&to=".$href;
//            urlencode($href);
            }

        }

        if(!empty($links_replaces['in']) || !empty($links_replaces['ou'])){
            $this->html_out = str_replace($links_replaces['in'], $links_replaces['out'], $this->html_out);
        }
        return $this->html_out;
    }

    // TODO remplacer par un getXpathAttribute
    private function set_xpath()
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML($this->html);
        $this->xpath = new \DOMXPath($dom);
    }

    private function create_images_list()
    {
        $this->images = $this->xpath->query('//img');
    }

    private function create_links_list()
    {
        $this->links = $this->xpath->query('//a');
    }

    public function getPixel(\App\Models\Routeur $routeur)
    {
        $pixel = "";
        $pixels = array();

        //Intern pixel from .env file or database
        $pixelsndd = explode(',',getenv('PIXELS_NDD'));

        foreach($pixelsndd as $pn) {
            if (!empty($pn)) {
                $pixels[] = '<img src="' . $pn . '/pixel/' . $routeur->variable_tor_id . '/' . $this->id . '" style="width:1px; height:1px; display:none;" />';
            }
        }

        $rand_pixel = \DB::table('pixel_domain_base_routeur')
            ->where('routeur_id', $routeur->id)
            ->where('base_id', $this->base_id)
            ->orderBy(\DB::raw('RAND()'))
            ->first();


        if(!empty($rand_pixel)){
            $pixel = '<img src="http://ouv.' . $rand_pixel->domain . '/pixel/' . $routeur->variable_tor_id . '/' . $this->id . '" style="width:1px; height:1px; display:none;" />';
        }

        //Priority to domain names declared in databases than in .env file
        if( count($pixels) > 0 ) {
            $pixel = $pixels[array_rand($pixels)];
        }

	\Log::info("[CampagneModel] dmpCookiable ? $this->dmpCookiable");
	if($this->dmpCookiable == 1){

            // $dmp_link = 'https://'.getenv('CLIENT_URL').'/rdrct/'.$editor_id.'/[emailmd5]';
            $dmp_link = 'https://'.getenv('CLIENT_URL').'/rdrct/1/[emailmd5]';

           	// $dmp_link = 'http://'.getenv('CLIENT_URL')."/rdrct/?bsd=$this->base_id&vsd=" . $routeur->variable_tor_id ."&pld=$this->planning_id&to=";
		$pixel .= '<img src="'.$dmp_link.'" style="width:1px; height:1px; display:none;" />';

    	}
        //\Log::info($pixel);
        //Pour rajouter les tags partenaires
        if($this->withtags != 1) {
            return $pixel;
        }

//        $dmp_link = "";

//        \DB::table('dmp_partner_sites')
//        $dmp_site = \DB::table('dmp_bases_sites')
//            ->where('base_id',$this->base_id)
//            ->orderBy(\DB::raw('RAND()'))
//            ->first();

        /* Ajout/Transfo des tags pixel partenaires*/
        $tags = Tag::orderByRaw("RAND()")->get();
        $tagspartenaires = "";

        $get_trigram = \DB::table('base_wizweb')->where('base_id', $this->base->id)->first();


        foreach($tags as $t){
            //on remplace toutes les variables personnalisables par les variables routeurs ou des references
            $t->url = str_ireplace("[email]", $routeur->variable_email, $t->url);
            $t->url = str_ireplace("[emailsha1]", $routeur->variable_email_sha1, $t->url);
            $t->url = str_ireplace("[emailmd5]", $routeur->variable_email_md5, $t->url);
            $t->url = str_ireplace("[subcode]", 'tor-'.getenv('CLIENT_NAME').'-c'.$this->id, $t->url); //reference de tracking campagne pour un tag
            $t->url = str_ireplace("[nom]", $routeur->variable_nom, $t->url);
            $t->url = str_ireplace("[prenom]", $routeur->variable_prenom, $t->url);
            $t->url = str_ireplace("[base]", $this->base->id, $t->url);

            if(!empty($get_trigram)){
                $t->url = str_ireplace("[trigram]", $get_trigram->trigram, $t->url);
            }

            //On ne mets pas le tag wizweb avec le routeur MailForYou pour le moment car passe en SPAM
            if(stripos($t->url, '[trigram]') !== false || $routeur->nom != 'MailForYou'){
                continue;
            }

            $tagspartenaires.='<img src="'.$t->url.'" style="width:1px; height:1px; display:none;" />';
        }

//        return sprintf($pixel, $app_url, $this->id)
        return $pixel
        .$tagspartenaires;

    }

    public function generateHtmlLocal(\App\Models\Routeur $routeur, $planning_id = null)
    {
        $this->planning_id = $planning_id;
        //  $cache_key = 'c' . $this->id . '-r' . $routeur->id;
        //  if (\Cache::has($cache_key)) {
        //      $this->generated_html = \Cache::get($cache_key);
        //  } else {
        $campagne = $this;

        //Generation du Header et du Footer en fonction du routeur et de la base
        $header = $this->generateHeader($routeur);

        $htmlcampagneWidth = $this->getWidthHtml($campagne->html);

        $header = str_ireplace("WWLONGWW", $htmlcampagneWidth, $header);
        $header = str_ireplace("WWTOPWW", $campagne->toptext, $header);
        $header = str_ireplace("[mirrorlink]", $routeur->variable_mirror, $header);
        $header = str_ireplace("[unsubscribelink]", $routeur->variable_unsubscribe, $header);

        $dom_header = new \DOMDocument();
        @$dom_header->loadHTML($header);

        // on récupère le DIV dans le footer
        $header_table = $dom_header->getElementsByTagName("div")->item(0);

        $footer = $this->generateFooter($routeur);
        $footer = str_ireplace("WWLONGWW", $htmlcampagneWidth, $footer);
        $footer = str_ireplace("WWBOTTOMWW", $campagne->bottomtext, $footer);
        $footer = str_ireplace("WWBASBASWW", $campagne->belowtext, $footer);
        $footer = str_ireplace("[mirrorlink]", $routeur->variable_unsubscribe, $footer);
        $footer = str_ireplace("[unsubscribelink]", $routeur->variable_mirror, $footer);

        $dom_footer = new \DOMDocument();
        @$dom_footer->loadHTML($footer);

        // on récupère le DIV dans le footer
        $footer_table = $dom_footer->getElementsByTagName("div")->item(0);

        $dom_pixel = new \DOMDocument();
        @$dom_pixel->loadHTML("<div style='height:0px;width:0px;'>".$this->getPixel($routeur)."</div>");
        // on récupère le DIV du pixel
        $img_pixel = $dom_pixel->getElementsByTagName("div")->item(0);

        //on remplace toutes les variables personnalisables par les variables routeurs ou des references
        $this->html = str_ireplace("[email]", $routeur->variable_email, $this->html);
        $this->html = str_ireplace("[emailsha1]", $routeur->variable_email_sha1, $this->html);
        $this->html = str_ireplace("[emailmd5]", $routeur->variable_email_md5, $this->html);
        $this->html = str_ireplace("[subcode]", 'tor-'.getenv('CLIENT_NAME').'-c'.$this->id, $this->html); //reference de tracking campagne pour un tag
        $this->html = str_ireplace("[nom]", $routeur->variable_nom, $this->html);
        $this->html = str_ireplace("[prenom]", $routeur->variable_prenom, $this->html);
        $this->html = str_ireplace("[base]", $this->base_id, $this->html);
        $this->html = str_ireplace("[mirrorlink]", $routeur->variable_unsubscribe, $this->html);
        $this->html = str_ireplace("[unsubscribelink]", $routeur->variable_mirror, $this->html);

        // on créé un Dom pour la Campagne
        $dom_campagne = new \DOMDocument();
        //urlencode pour les pbs avec les "doubles" urls avec le deuxieme lien deja encodé.. permet de garder l'encodage

        @$dom_campagne->loadHTML(urlencode($this->html));

        // on transforme le header et le footer en node
        $node_footer = $dom_campagne->importNode($footer_table, true);
        $node_header = $dom_campagne->importNode($header_table, true);

        //on injecte le pixel dans le body
        $node_pixel = $dom_campagne->importNode($img_pixel, true);

        // on injecte le footer dans le body
        $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_pixel);
        $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_footer);

        //on injecte le header dans le body
        $firstItem = $dom_campagne->getElementsByTagName('body')->item(0)->childNodes->item(0);

        //parentNode necessaire pour faire le insertBefore, sans le parentNode -> ne fonctionne pas BON A SAVOIR
        @$firstItem->parentNode->insertBefore($node_header,$firstItem); //TODO : a mettre en commentaire si ca ne marche pas en PROD

        $this->generated_html = $dom_campagne->saveHTML();
        // $this->generated_html = $this->store_images('', getenv('CDN_URL'));

        $this->generated_html = htmlspecialchars_decode($this->generated_html);
        $this->generated_html = html_entity_decode($this->generated_html);

        //    \Cache::put($cache_key, urldecode($this->generated_html), 60);
        // }

        return urldecode($this->generated_html);
    }

    private function generateHeaderM4u(\App\Models\Routeur $routeur)
    {
        $mentions = "";
        $mentions2 = "";

        if($this->withtags == 1 && $this->base_id != 0) {
            $mentions =
                "<TR>
                    <TD width='WWLONGWW' style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:grey; text-align:center;'> En poursuivant votre navigation, vous acceptez l'utilisation de cookies pour vous proposer des services et offres adapt&eacute;s <br/> &agrave; vos centres d'int&eacute;r&ecirc;ts et mesurer la fr&eacute;quentation de nos services. Pour en savoir plus suivez <a href='" . trim($this->base->mentions_header, " \t\n\r\0\x0B") . "' target='_blank'> ce lien </a></TD>
                </TR>";
            $mentions2  = "<p width='WWLONGWW' style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:grey; text-align:center;'> En poursuivant votre navigation, vous acceptez l'utilisation de cookies pour vous proposer des services et offres adapt&eacute;s <br/> &agrave; vos centres d'int&eacute;r&ecirc;ts et mesurer la fr&eacute;quentation de nos services. Pour en savoir plus suivez <a href='" . trim($this->base->mentions_header, " \t\n\r\0\x0B") . "' target='_blank'> ce lien </a></p>";
        }

        $headerrouteur =
            "<div style='text-align: center'> <TABLE width='WWLONGWW' border='0' cellspacing='0' cellpadding='0' align='center' >
                <TR>
                  <TD width='WWLONGWW' valign='top' align='center'>
                  <table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' >
                      <tr>
                      <td>
                        <center> $routeur->variable_mirror &#124; $routeur->variable_unsubscribe </center>
                      </td>
                      </tr>
                    </table>
                    </TD>
                </TR>
                    $mentions
                <TR>
                  <TD  width='WWLONGWW' align='center'>WWTOPWW</TD>
                </TR>
            </TABLE>
            </div>";

        // if( strlen($this->base->header_html) > 10){
        //     $headerrouteur = "<div align='center' style='color:#000; font-family:Arial,Helvetica,sans-serif; font-size:11px;'> ".utf8_decode($this->base->header_html)." $mentions2 </div>";
        // }

        return $headerrouteur;
    }


    public function generateHtmlM4u(\App\Models\Routeur $routeur, $planning_id = null)
    {
        $this->planning_id =$planning_id;
        // $cache_key = 'c' . $this->id . '-r' . $routeur->id;
        // if (\Cache::has($cache_key)) {
        //     $this->generated_html = \Cache::get($cache_key);
        // } else {
        \Log::info("Call generateHtmlM4u");
        $campagne = $this;

        //Generation du Header et du Footer en fonction du routeur et de la base
        $header = $this->generateHeaderM4u($routeur);

        $htmlcampagneWidth = $this->getWidthHtml($campagne->html);

        $header = str_ireplace("WWLONGWW", $htmlcampagneWidth, $header);
        $header = str_ireplace("WWTOPWW", $campagne->toptext, $header);
        $header = str_ireplace("[mirrorlink]", $routeur->variable_mirror, $header);
        $header = str_ireplace("[unsubscribelink]", $routeur->variable_unsubscribe, $header);

        $dom_header = new \DOMDocument();
        @$dom_header->loadHTML($header);

        // on récupère le DIV dans le footer
        $header_table = $dom_header->getElementsByTagName("div")->item(0);

        $footer = $this->generateFooterM4u($routeur);
        $footer = str_ireplace("WWLONGWW", $htmlcampagneWidth, $footer);
        $footer = str_ireplace("WWBOTTOMWW", $campagne->bottomtext, $footer);
        $footer = str_ireplace("WWBASBASWW", $campagne->belowtext, $footer);
        $footer = str_ireplace("[mirrorlink]", $routeur->variable_mirror, $footer);
        $footer = str_ireplace("[unsubscribelink]", $routeur->variable_unsubscribe, $footer);

        $dom_footer = new \DOMDocument();
        @$dom_footer->loadHTML($footer);

        // on récupère le DIV dans le footer
        $footer_table = $dom_footer->getElementsByTagName("div")->item(0);

        $dom_pixel = new \DOMDocument();
        @$dom_pixel->loadHTML("<div style='height:0px;width:0px;'>".$this->getPixel($routeur)."</div>");
        // on récupère le DIV du pixel
        $img_pixel = $dom_pixel->getElementsByTagName("div")->item(0);

        //on remplace toutes les variables personnalisables par les variables routeurs ou des references
        $this->html = str_ireplace("[email]", $routeur->variable_email, $this->html);
        $this->html = str_ireplace("[emailsha1]", $routeur->variable_email_sha1, $this->html);
        $this->html = str_ireplace("[emailmd5]", $routeur->variable_email_md5, $this->html);
        $this->html = str_ireplace("[subcode]", 'tor-'.getenv('CLIENT_NAME').'-c'.$this->id, $this->html); //reference de tracking campagne pour un tag
        $this->html = str_ireplace("[nom]", $routeur->variable_nom, $this->html);
        $this->html = str_ireplace("[prenom]", $routeur->variable_prenom, $this->html);
        $this->html = str_ireplace("[base]", $this->base->id, $this->html);

        // on créé un Dom pour la Campagne
        $dom_campagne = new \DOMDocument();
        //urlencode pour les pbs avec les "doubles" urls avec le deuxieme lien deja encodé.. permet de garder l'encodage
        @$dom_campagne->loadHTML(urlencode($this->html));

        // on transforme le header et le footer en node
        $node_footer = $dom_campagne->importNode($footer_table, true);
        $node_header = $dom_campagne->importNode($header_table, true);

        //on injecte le pixel dans le body
        $node_pixel = $dom_campagne->importNode($img_pixel, true);

        // on injecte le footer dans le body
        $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_pixel);
        $dom_campagne->getElementsByTagName('body')->item(0)->appendChild($node_footer);

        //on injecte le header dans le body
        $firstItem = $dom_campagne->getElementsByTagName('body')->item(0)->childNodes->item(0);

        //parentNode necessaire pour faire le insertBefore, sans le parentNode -> ne fonctionne pas BON A SAVOIR
        @$firstItem->parentNode->insertBefore($node_header,$firstItem); //TODO : a mettre en commentaire si ca ne marche pas en PROD

        $this->generated_html = $dom_campagne->saveHTML();
        if($this->hostimage) {
            $this->generated_html = $this->store_images('', getenv('CDN_URL'));
        }

        $this->generated_html = htmlspecialchars_decode($this->generated_html);
        $this->generated_html = html_entity_decode($this->generated_html);

//            $this->generated_html = str_replace($link_replaces['out'], $link_replaces['in'], $this->generated_html);

        //  \Cache::put($cache_key, urldecode($this->generated_html), 60);
        // }

        return urldecode($this->generated_html);
    }

    private function generateFooterM4u(\App\Models\Routeur $routeur)
    {
        if($this->base_id == 0){
            return "<div>
            <TABLE width='WWLONGWW' border='0' cellspacing='0' cellpadding='0' align='center'style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:black; text-align:center;'>
                <tr>
                  <td style='font-size:10px; text-align:center;' ><br />
                    WWBOTTOMWW
                    </td>
                </tr>
                <tr><td>WWBASBASWW</td></tr>
                <tr><td>Vous souhaitez vous d&eacute;sinscrire de cette liste ? $routeur->variable_unsubscribe </td></tr>
            </TABLE>
            </div>";
        }

        $footerrouteur =
            "<div style='text-align: center'>
            <TABLE width='WWLONGWW' border='0' cellspacing='0' cellpadding='0' align='center'style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:black; text-align:center;'>
                <tr>
                  <td style='font-size:10px; text-align:center;' ><br />
                    WWBOTTOMWW
                    ".str_ireplace("\r","<br/>",utf8_decode($this->base->mentions_legales))."
                    </td>
                </tr>
                <tr><td>WWBASBASWW</td></tr>
                <tr><td>Vous souhaitez vous d&eacute;sinscrire de cette liste ? $routeur->variable_unsubscribe </td></tr>
            </TABLE>
           </div>";
        // if(!empty($this->base->footer_html)){
        //     $footerrouteur = "<div align='center' style='color:#000; font-family:Arial,Helvetica,sans-serif; font-size:11px;'> <br/>".str_ireplace("\r","<br/>",utf8_decode($this->base->mentions_legales))."</br> ".str_ireplace("\r","<br/>",utf8_decode($this->base->footer_html))."</div>";
        // }

        return $footerrouteur;
    }
}
