<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampagneSegments extends Model
{
    protected $fillable = [
        'campagne_id',
        'planning_id',
        'md_segment_id',
        'sender_id',
        'md_list_id'
    ];

    function Sender() {
        return $this->belongsTo('\App\Models\Sender', 'sender_id', 'id');
    }
}
