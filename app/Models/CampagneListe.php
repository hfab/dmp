<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampagneListe extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campagne_liste';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['campagne_id', 'sender_id', 'md_list_id', 'routeur_cid'];

    function getNomAttribute() {
        return 'TOR-TEST-'
            .\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d')
            .'-c'.$this->campagne_id.
            '-s'.$this->sender_id;
    }

    function Sender() {
        return $this->belongsTo('\App\Models\Sender', 'sender_id', 'id');
    }

}
