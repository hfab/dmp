<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Routeur extends Model {
	protected $table = 'routeurs';
	protected $fillable = ['nom'];
}
