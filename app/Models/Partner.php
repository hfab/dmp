<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Theme;
class Partner extends Model
{
    //
    protected $table = 'dmp_partnersites';

	public function theme()
	{
		return $this->hasOne('\App\Models\Theme','id','theme_id');
	}
}
