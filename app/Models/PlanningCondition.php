<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PlanningCondition extends Model {

    protected $table = 'plannings_conditions';
    protected $fillable = ['planning_id', 'condition_type', 'condition_table', 'condition_column', 'condition_value'];

    public function planning() {
        return $this->hasOne('\App\Models\Planning', 'id', 'planning_id');
    }
}
