<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model {

	protected $table = 'themes';
    protected $fillable = ['nom'];
    protected $primaryKey = 'id';
    public $timestamps = false;
   

	function getThemeName(){
		$themeName = str_replace(" ","_",$this->nom);
		if(strpos($themeName,"/") !== false){
			$dataSplitted = explode("/",$themeName);
			$themeName = $dataSplitted[0]."_".$dataSplitted[1];
		}
		return strtolower($this->removeAccents($themeName));
	}

	function removeAccents($str){
		$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
		$str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
		$str = preg_replace('#&[^;]+;#', '', $str);
		return $str;
	}

}
