<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SenderFai extends Model {
	protected $table = 'fai_sender';
    protected $fillable = ['sender_id', 'fai_id', 'quota', 'quota_left'];

    function sender() {
        return $this->belongsTo('\App\Models\Sender');
    }

    function fai() {
        return $this->belongsTo('\App\Models\Fai');
    }

}
