<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Planning extends Model {

    protected $table = 'plannings';
    protected $fillable = ['campagne_id', 'date_campagne', 'volume', 'volume_orange', 'sent_at', 'tokens_at', 'segments_at', 'branch_id', 'freq_send', 'nbr_sender'];

    public function campagne() {
        return $this->belongsTo('\App\Models\Campagne', 'campagne_id', 'id');
    }

    public function routeur() {
        return $this->hasOne('\App\Models\Routeur', 'id', 'routeur_id');
    }

    public function volumesfais() {
        return $this->hasMany('\App\Models\PlanningFaiVolume', 'planning_id', 'id');
    }

    public function getSelectedSenderAttribute() {
        $selected_sender = null;

        $ps = \DB::table('planning_senders')
            ->select('sender_id')
            ->where('planning_id', $this->id)
            ->first();

        if(!empty($ps)) {
            $selected_sender = \App\Models\Sender::find($ps->sender_id);
        }
        return $selected_sender;
    }

    public function getSelectedSendersAttribute() {
        $selected_senders = array();

        $ps = \DB::table('planning_senders')
            ->select('sender_id')
            ->where('planning_id', $this->id)
            ->get();

        if(!empty($ps)) {
            $selected_senders = \App\Models\Sender::whereIn('id', array_pluck($selected_senders,'sender_id'))->get();
        }
        return $selected_senders;
    }

    public function getSelectedListsAttributes(){
        $selected_lists = array();

        $cr = \DB::table('campagnes_routeurs')
            ->select('listid')
            ->where('planning_id', $this->id)
            ->get();

        if(!empty($cr)) {
            $selected_lists = array_pluck($cr, 'listid');
        }
        return $selected_lists;
    }
}
