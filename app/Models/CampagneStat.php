<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampagneStat extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campagne_stats';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['campagne_id', 'date', 'envoyes', 'clicks', 'ouvertures', 'optout', 'bounces'];

}
