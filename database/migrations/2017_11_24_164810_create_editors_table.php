<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('editors',function(Blueprint $table){
	    	$table->increments('id');
		    $table->string('name');
        $table->text('domain');
	    });

	    Schema::table('users',function(Blueprint $table){
	    	$table->integer('editor_id')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
