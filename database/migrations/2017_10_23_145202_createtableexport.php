<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Createtableexport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('export_api', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('base_id');
          $table->integer('last_destinataire_id');
          $table->text('link');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('export_api');
    }
}
