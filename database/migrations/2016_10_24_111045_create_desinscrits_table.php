<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesinscritsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desinscrits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mail');
            $table->string('hash');
            $table->integer('campagne_id');
//            $table->integer('planning_id');
            $table->date('date_out');
            $table->unique(['campagne_id', 'mail']);
            $table->index(['campagne_id']);
            $table->index(['campagne_id', 'date_out']);
//            $table->index(['planning_id', 'destinataire_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('desinscrits');
    }
}
