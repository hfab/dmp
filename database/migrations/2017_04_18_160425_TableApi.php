<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('api', function (Blueprint $table) {

          $table->increments('id');

          $table->string('api_login_info');
          $table->string('api_key');
          $table->integer('api_call');
          // tJ7o29KRrZI15nlE1zovGMKXnWN9JcMWhxLTsSN8prlLrLafBp3i3a5qCbhLIOypAGGHdmNKKz87oGoqBWoMhV
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api');
    }
}
