<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsMindbazTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats_mindbaz', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('planning_id');
            $table->string('cid_routeur');
            $table->string('reference');
            $table->integer('total_sent');
            $table->integer('openers');
            $table->integer('clickers');
            $table->integer('soft_bounces');
            $table->integer('hard_bounces');
            $table->integer('spam_bounces');
            $table->integer('spam_complaints');
            $table->integer('unsubscribers');
            $table->date('date_maj');
            $table->integer('bloc_maj');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_mindbaz');
    }
}
