<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFaiMailforyouStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('stats_m4y', function (Blueprint $table) {

          $table->integer('nombrebhotmail')->default(null);
          $table->integer('nombrebaol')->default(null);
          $table->integer('nombrebbouygues')->default(null);

          $table->integer('nombrethotmail')->default(null);
          $table->integer('nombretaol')->default(null);
          $table->integer('nombretbouygues')->default(null);

          $table->float('aboutis_hotmail',6,2)->default(null);
          $table->float('aboutis_aol',6,2)->default(null);
          $table->float('aboutis_bouygues',6,2)->default(null);

          $table->float('per_cent_hotmail_ouv',6,2)->default(null);
          $table->float('per_cent_aol_ouv',6,2)->default(null);
          $table->float('per_cent_bouygues_ouv',6,2)->default(null);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
