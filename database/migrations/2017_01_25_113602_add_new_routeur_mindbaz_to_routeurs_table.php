<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewRouteurMindbazToRouteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('routeurs')->insert(
            [
                'nom' => 'Mindbaz',
                'variable_email' => '[[FIELD.1]]',
                'variable_unsubscribe' => '[[URL.1]]',
                'variable_mirror' => '[[URL.2]]',
                'variable_email_sha1' => '',
                'variable_email_md5' => '',
                'variable_prenom' => '[[FIELD.15]]',
                'variable_nom' => '[[FIELD.16]]',
                'variable_tor_id' => '[[FIELD.43]]',
                'created_at' => '2017-01-25 14:56:00',
                'updated_at' => '2017-01-25 14:56:00'
            ]
        );

        $mindbaz = \DB::table('routeurs')->where('nom', 'Mindbaz')->first();

        \DB::table('settings')->insert(
            [
                'parameter' => 'is_mindbaz',
                'value' => '1',
                'context' => 'routeur',
                'info' => $mindbaz->id
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
