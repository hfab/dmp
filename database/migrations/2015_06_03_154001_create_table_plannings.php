<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlannings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plannings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('campagne_id');
            $table->date('date_campagne');
            $table->integer('volume');

            $table->boolean('is_prepared')->default(false);
            $table->boolean('is_sent')->default(false);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plannings');
	}

}
