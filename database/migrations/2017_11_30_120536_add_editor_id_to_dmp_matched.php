<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEditorIdToDmpMatched extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("dmp_matched",function(Blueprint $table){
            $table->integer('editor_id');
        });

        Schema::table('dmp_cookies',function(Blueprint $table){
            $table->integer('editor_id');
        });

        Schema::table('dmp_history',function(Blueprint $table){
            $table->integer('dmp_partnersite_id');
        });

        Schema::table('dmp_partnersites',function(Blueprint $table){
            $table->string('name');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
