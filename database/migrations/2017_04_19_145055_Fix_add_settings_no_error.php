<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixAddSettingsNoError extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      $check = \DB::table('settings')->where('parameter', 'mindbaz_mode')->first();

      if($check == null){

      \DB::table('settings')->insert([
          'parameter'=>'mindbaz_mode',
          'value' => 'segment', //or list
          'context' => 'type_segment',
          'info' => 'Segmentation à la Tor (segment) ou Tor light (list)'
      ]);

      }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
