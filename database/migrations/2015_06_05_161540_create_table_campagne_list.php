<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCampagneList extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('campagne_liste', function(Blueprint $table)
		{
            $table->integer('campagne_id');
            $table->string('md_list_id');

            $table->integer('sender_id');

			$table->timestamps();

            $table->index('campagne_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('campagne_liste');
	}

}
