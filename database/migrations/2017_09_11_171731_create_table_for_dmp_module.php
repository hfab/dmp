<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForDmpModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dmp_hash', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash');
            $table->integer('destinataire_id');
//            $table->integer('dmp_partner_id');
            $table->integer('base_id');
            $table->timestamps();
            $table->index(['base_id', 'hash']);
        });

//        Schema::create('dmp_partners', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('name');
//            $table->integer('theme_id');
//            $table->boolean('is_active');
//
//        });

//        Schema::create('dmp_partners_sites', function (Blueprint $table) {
        Schema::create('dmp_partnersites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->unique();
//            $table->integer('dmp_partner_id');
//            $table->integer('base_id');
            $table->timestamps();
//            $table->index('base_id');
//            $table->index('partner_id');
            $table->index('url');
        });

        Schema::create('dmp_matched', function (Blueprint $table) {
            $table->increments('id');
//            $table->mediumInteger('occurrence');
//            $table->integer('partner_site_id');
            $table->integer('planning_id');
            $table->integer('base_site_id');
            $table->integer('destinataire_id');
            $table->boolean('has_ordered');
//            $table->integer('theme_id');
            $table->timestamps();

        });

        Schema::create('dmp_js_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
//            $table->integer('partner_id');
            $table->integer('base_id');
        });

        Schema::create('dmp_cookies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token')->unique();
            $table->string('to');

//            $table->integer('partner_id'); //??
            $table->integer('base_id'); //??

            $table->integer('destinataire_id'); //??
            $table->integer('planning_id'); //
            $table->index(['token']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
