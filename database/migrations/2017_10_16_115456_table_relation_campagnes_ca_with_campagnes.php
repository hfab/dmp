<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableRelationCampagnesCaWithCampagnes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idcampca_idcamp', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_campagnes_ca');
            $table->integer('base_id_ca');
            $table->integer('id_campagnes');
            $table->integer('base_id_campagnes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('idcampca_idcamp');
    }
}
