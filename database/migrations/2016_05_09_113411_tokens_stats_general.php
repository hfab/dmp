<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TokensStatsGeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tokens_stats_general', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('volume');
          $table->integer('volume_used');
          $table->integer('volume_left');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tokens_stats_general');
    }
}
