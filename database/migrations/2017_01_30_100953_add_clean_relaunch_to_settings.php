<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCleanRelaunchToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $items [] = ['parameter' => 'clean_relaunch', 'value' => '1', 'context' => 'type_envoi'];

        foreach($items as $item) {
            \DB::table('settings')->insert($item);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
