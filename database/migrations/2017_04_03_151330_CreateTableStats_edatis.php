<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatsEdatis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stats_edatis', function (Blueprint $table) {

          $table->increments('id');

          $table->integer('bloc_maj');
          $table->string('reference');
          $table->string('sender_id');

          $table->integer('nb_sent');
          $table->integer('nb_received');
          $table->integer('nb_open');
          $table->integer('nb_clicks');

          $table->integer('nb_unsubscribe');
          $table->integer('nb_soft_bounce');
          $table->integer('nb_hard_bounce');

          


          // $table->integer('nb_invalid');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_edatis');
    }
}
