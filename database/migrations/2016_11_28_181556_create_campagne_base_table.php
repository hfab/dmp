<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampagneBaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campagne_base', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campagne_id');
            $table->integer('base_id');
            $table->unique(['campagne_id', 'base_id']);
            $table->index(['campagne_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagne_base');
    }
}
