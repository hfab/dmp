<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanCA extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('idcampca_idcamp');

        Schema::create('campagne_ca_relation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('id_campagnes_ca');
            $table->integer('base_id_ca');
            $table->integer('id_campagne');
            $table->timestamps();
        });


        Schema::table('campagnes_ca_concat', function (Blueprint $table) {
            $table->integer('plateforme_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagne_ca_relation');
    }
}
