<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampagneCaConcat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('campagnes_ca_concat', function (Blueprint $table) {

        $table->increments('id');
        $table->integer('base_id'); // j'ai mon baseid, plateformeid
        $table->float('ca_brut');
        $table->float('ca_net');
        $table->integer('aaf');
        $table->integer('envoi_facture');
        $table->integer('state');
        $table->integer('mois_compta');
        $table->text('commentaire');
        $table->integer('ca_volume_total')->nullable();
        $table->float('cout_routage')->nullable();
        $table->timestamps();
      });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagnes_ca_concat');
    }
}
