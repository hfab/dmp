<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUselessColumnsToDmpMatchedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dmp_matched',function(Blueprint $table){
            $table->dropColumn('planning_id');
            $table->dropColumn('destinataire_id');
            $table->dropColumn('has_ordered');
            $table->integer('nb_has_not_ordered');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
