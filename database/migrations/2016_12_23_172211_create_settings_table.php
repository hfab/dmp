<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('parameter');
            $table->string('value');
            $table->string('context')->nullable();
            $table->string('info')->nullable();

            $table->timestamps();
        });


        // PLANNINGS AUTO OR MANUAL
        $items [] = ['parameter' => 'is_auto', 'value' => '1', 'context' => 'type_envoi'];


        // ACTIVE DEFAULT ROUTEURS
        $items [] = ['parameter' => 'is_maildrop', 'value' => '1', 'context' => 'routeur'];
        $items [] = ['parameter' => 'is_mailforyou', 'value' => '1', 'context' => 'routeur'];
        $items [] = ['parameter' => 'is_smessage', 'value' => '1', 'context' => 'routeur'];
        $items [] = ['parameter' => 'is_phoenix', 'value' => '1', 'context' => 'routeur'];

        //ACTIVE WIZWEB API
        $items [] = ['parameter' => 'is_wizweb', 'value' => '1', 'context' => 'tag'];

        foreach($items as $item) {
            \DB::table('settings')->insert($item);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
