<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUselessColumnsToDmpCookiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dmp_cookies',function(Blueprint $table){
            $table->dropColumn('destinataire_id');
            $table->dropColumn('planning_id');
            $table->dropColumn('to');
            $table->dropColumn('base_id');
            $table->string('hash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
