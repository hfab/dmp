<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSender extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('senders', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('routeur_id');

            $table->string('nom');
            $table->string('password');

            $table->string('domaine');

            $table->integer('quota')->default(100);
            $table->integer('quota_left');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('senders');
	}

}
