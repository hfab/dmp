<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCampagneEdiware extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('campagnes_ediware', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('campagne_id')->nullable();
          $table->string('id_by_ediware')->nullable();
          $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagnes_ediware');
    }
}
