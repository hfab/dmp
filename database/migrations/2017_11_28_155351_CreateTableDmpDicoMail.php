<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDmpDicoMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dmp_dicomail',function(Blueprint $table){
        $table->bigIncrements('id');
        $table->char('mail', 50);
        $table->char('hash', 32);
        $table->char('source', 32);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dmp_dicomail');
    }
}
