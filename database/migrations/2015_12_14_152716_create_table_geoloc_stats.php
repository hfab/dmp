<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGeolocStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('geoloc_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('total_destinataire_geoloc')->nullable();
            $table->integer('unique_destinataire_geoloc')->nullable();
            $table->integer('nombre_ip_unique')->nullable();
            $table->integer('nombre_ville_unique')->nullable();
            $table->integer('nombre_departement_unique')->nullable();
            $table->integer('nombre_pays_unique')->nullable();
            $table->integer('id_last_destinataire')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('geoloc_stats');
    }
}
