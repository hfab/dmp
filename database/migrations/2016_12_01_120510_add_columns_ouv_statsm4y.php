<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOuvStatsm4y extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('stats_m4y', function (Blueprint $table) {
            $table->float('per_cent_sfr_ouv',6,2)->default(null);
            $table->float('per_cent_free_ouv',6,2)->default(null);
            $table->float('per_cent_laposte_ouv',6,2)->default(null);
            $table->float('per_cent_autre_ouv',6,2)->default(null);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
