<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DmpRelaunch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dmp_relaunch', function (Blueprint $table) {
          $table->increments('id');
          $table->string('destinataire_hash');
          $table->integer('sender_id')->nullable();
          $table->integer('campagne_id')->nullable();
          $table->integer('editor_id');
          $table->integer('site_id');
          $table->integer('theme_id');
          $table->integer('state');
          $table->dateTime('relaunch_at')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dmp_relaunch');
    }
}
