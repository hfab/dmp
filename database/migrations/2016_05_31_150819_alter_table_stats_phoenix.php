<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStatsPhoenix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE stats_phoenix MODIFY id_sender_phoenix varchar(255) NULL");
         \DB::statement("ALTER TABLE stats_phoenix MODIFY reference varchar(255) NULL");
        \DB::statement("ALTER TABLE stats_phoenix MODIFY routeur_created varchar(255) NULL");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
