<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePlanningDefaut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('planning_defaut', function (Blueprint $table) {
          $table->increments('id');

          $table->integer('config_id');
          $table->integer('fai_id')->nullable();
          $table->integer('volume')->nullable();
          $table->integer('volume_total')->nullable();

          $table->timestamps();
      });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planning_defaut');

    }
}
