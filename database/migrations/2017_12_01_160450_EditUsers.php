<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('user_group')->truncate();
          \DB::table('user_group')->insert([
              ['name' => 'Admin'],
              ['name' => 'Annonceur'],
              ['name' => 'Editeur']
          ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
