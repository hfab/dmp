<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addcolstatsedatis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::table('stats_edatis', function (Blueprint $table) {

          $table->integer('total_orange')->default(null);
          $table->integer('total_free')->default(null);
          $table->integer('total_sfr')->default(null);
          $table->integer('total_laposte')->default(null);
          $table->integer('total_autre')->default(null);
          $table->integer('total_club_internet')->default(null);

          $table->integer('receveid_orange')->default(null);
          $table->integer('receveid_free')->default(null);
          $table->integer('receveid_sfr')->default(null);
          $table->integer('receveid_laposte')->default(null);
          $table->integer('receveid_autre')->default(null);
          $table->integer('receveid_club_internet')->default(null);

          $table->float('p_receveid_orange',6,2)->default(null);
          $table->float('p_receveid_free',6,2)->default(null);
          $table->float('p_receveid_sfr',6,2)->default(null);
          $table->float('p_receveid_laposte',6,2)->default(null);
          $table->float('p_receveid_autre',6,2)->default(null);
          $table->float('p_receveid_club_internet',6,2)->default(null);

          $table->integer('ouv_orange')->default(null);
          $table->integer('ouv_free')->default(null);
          $table->integer('ouv_sfr')->default(null);
          $table->integer('ouv_laposte')->default(null);
          $table->integer('ouv_autre')->default(null);
          $table->integer('ouv_club_internet')->default(null);

          $table->float('p_ouv_orange',6,2)->default(null);
          $table->float('p_ouv_free',6,2)->default(null);
          $table->float('p_ouv_sfr',6,2)->default(null);
          $table->float('p_ouv_laposte',6,2)->default(null);
          $table->float('p_ouv_autre',6,2)->default(null);
          $table->float('p_ouv_club_internet',6,2)->default(null);

          $table->integer('planning_id');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
