<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDmpHistoryAndDmpPlanningListid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('dmp_history',function(Blueprint $table){
		    $table->bigIncrements('id');
		    $table->integer('destinataire_id');
		    $table->integer('campagne_id');
		    $table->integer('planning_id');
		    $table->timestamp('relaunch_at');
	    });

	    Schema::create('dmp_planning_listid',function(Blueprint $table){
		    $table->string('listid');
		    $table->integer('planning_id');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
