<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDmpNddTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dmp_ndd',function(Blueprint $table){
                $table->increments('id');
                $table->integer('editor_id');
                $table->string('ndd');
                $table->integer('hit')->default(0);
                $table->integer('max_hit')->default(100000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
