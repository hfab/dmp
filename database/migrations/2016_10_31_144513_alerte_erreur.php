<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlerteErreur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('alerte_erreur', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('campagnes_routeurs_id');
          $table->integer('statut_alert');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alerte_erreur');
    }
}
