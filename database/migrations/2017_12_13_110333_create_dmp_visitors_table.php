<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDmpVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dmp_visitors',function(Blueprint $table){
            $table->string('destinataire_hash');
            $table->timestamp('visited_at');
            $table->integer('base_site_id');
            $table->integer('theme_id');
            $table->integer('editor_id');
            $table->boolean('has_ordered');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
