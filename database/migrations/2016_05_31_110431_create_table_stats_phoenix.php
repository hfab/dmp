<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatsPhoenix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stats_phoenix', function (Blueprint $table) {
          $table->increments('id');

          $table->integer('id_sender_phoenix');
          $table->integer('volume');
          $table->integer('ouvreurs');
          $table->integer('cliqueurs');
          $table->integer('erreurs');
          $table->integer('desabo');
          $table->string('routeur_created');
          $table->integer('date_maj');
          $table->integer('bloc_maj');
          // $table->timestamps();
          $table->string('reference');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_phoenix');
    }
}
