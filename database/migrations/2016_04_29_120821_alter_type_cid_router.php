<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTypeCidRouter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // Schema::table('campagnes_routeurs', function (Blueprint $table) {
      //     $table->string('cid_routeur');
      // });

      \DB::statement("ALTER TABLE campagnes_routeurs MODIFY cid_routeur varchar(255) NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
