<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTokensv2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \DB::statement("
        CREATE TABLE IF NOT EXISTS `tokensv2` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `base_id` int(11) NOT NULL,
        `destinataire_id` int(11) NOT NULL,
        `campagne_id` int(11) DEFAULT NULL,
        `fai_id` int(11) NOT NULL,
        `sender_id` int(11) DEFAULT NULL,
        `date_active` date NOT NULL,
        `priority` int(11) NOT NULL,
        `uploaded_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
        `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
        `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
        `md_list_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `geoloc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `planning_id` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        KEY `tokens_base_id_fai_id_campagne_id_index` (`base_id`,`fai_id`,`campagne_id`),
        KEY `tokens_priority_index` (`priority`),
        KEY `tokens_campagne_id_fai_id_date_active_sender_id_index` (`campagne_id`,`fai_id`,`date_active`,`sender_id`)
      );");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
