<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePixelDomainBaseRouteurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pixel_domain_base_routeur', function (Blueprint $table) {
            $table->increments('id');

            $table->string('domain')->unique();
            $table->integer('base_id');
            $table->integer('routeur_id');
            $table->timestamps();

            $table->index(['base_id', 'routeur_id']);
        });

        $domains = array();

        if( getenv('CLIENT_URL') == 'tor.sc2consulting.fr' ){
            $domains = array();

            $domains[] = ['domain' => '320aqf.fr'               ,   'base_id' => '23',      'routeur_id' => '1'];
            $domains[] = ['domain' => 'aa987.fr'                ,   'base_id' => '23',      'routeur_id' => '1'];
            $domains[] = ['domain' => '8652g.fr'                ,   'base_id' => '14',      'routeur_id' => '1'];
            $domains[] = ['domain' => '43da.fr'                 ,   'base_id' => '14',      'routeur_id' => '1'];
            $domains[] = ['domain' => '422f.fr'                 ,   'base_id' => '31',      'routeur_id' => '1'];
            $domains[] = ['domain' => '505h.fr'                 ,   'base_id' => '31',      'routeur_id' => '1'];
            $domains[] = ['domain' => '7il.fr'                  ,   'base_id' => '24',      'routeur_id' => '1'];
            $domains[] = ['domain' => 'bgd987.fr'               ,   'base_id' => '24',      'routeur_id' => '1'];
            $domains[] = ['domain' => 'tr4ck000.fr'             ,   'base_id' => '23',      'routeur_id' => '6'];
            $domains[] = ['domain' => 'monpix3l.fr'             ,   'base_id' => '23',      'routeur_id' => '6'];
            $domains[] = ['domain' => 'orange78910.fr'          ,   'base_id' => '14',      'routeur_id' => '6'];
            $domains[] = ['domain' => '123ouvdiy.fr'            ,   'base_id' => '14',      'routeur_id' => '6'];
            $domains[] = ['domain' => '2569wtpix.fr'            ,   'base_id' => '31',      'routeur_id' => '6'];
            $domains[] = ['domain' => '777awesome.fr'           ,   'base_id' => '31',      'routeur_id' => '6'];
            $domains[] = ['domain' => 'classyn0.fr'             ,   'base_id' => '24',      'routeur_id' => '6'];
            $domains[] = ['domain' => 'track1ng.fr'             ,   'base_id' => '24',      'routeur_id' => '6'];
            $domains[] = ['domain' => 'sttsk200.fr'             ,   'base_id' => '23',      'routeur_id' => '7'];
            $domains[] = ['domain' => 'iilwtsoy.fr'             ,   'base_id' => '14',      'routeur_id' => '7'];
            $domains[] = ['domain' => 'jjyp985.fr'              ,   'base_id' => '31',      'routeur_id' => '7'];
            $domains[] = ['domain' => 'open8420.fr'             ,   'base_id' => '24',      'routeur_id' => '7'];
            $domains[] = ['domain' => 'tennis159.fr'            ,   'base_id' => '23',      'routeur_id' => '8'];
            $domains[] = ['domain' => 'live456.fr'              ,   'base_id' => '23',      'routeur_id' => '8'];
            $domains[] = ['domain' => '2139sng.fr'              ,   'base_id' => '14',      'routeur_id' => '8'];
            $domains[] = ['domain' => '83msc.fr'                ,   'base_id' => '14',      'routeur_id' => '8'];
            $domains[] = ['domain' => '637prty.fr'              ,   'base_id' => '31',      'routeur_id' => '8'];
            $domains[] = ['domain' => '5462fine.fr'             ,   'base_id' => '31',      'routeur_id' => '8'];
            $domains[] = ['domain' => '763tata.fr'              ,   'base_id' => '24',      'routeur_id' => '8'];
            $domains[] = ['domain' => '128tutu.fr'              ,   'base_id' => '24',      'routeur_id' => '8'];

        } elseif (getenv('CLIENT_URL') == 'tor-sc2.lead-factory.net' ){
            $domains = array();

            $domains[] = ['domain' => '63690ojaf.fr'            ,    'base_id' => '1',       'routeur_id' => '2'];
            $domains[] = ['domain' => '41520poj.fr'             ,    'base_id' => '1',       'routeur_id' => '2'];
            $domains[] = ['domain' => '98451wth.fr'             ,    'base_id' => '3',       'routeur_id' => '2'];
            $domains[] = ['domain' => '874a.fr'                 ,    'base_id' => '3',       'routeur_id' => '2'];
            $domains[] = ['domain' => '3674c.fr'                ,    'base_id' => '4',       'routeur_id' => '2'];
            $domains[] = ['domain' => '41d.fr'                  ,    'base_id' => '4',       'routeur_id' => '2'];
            $domains[] = ['domain' => '9841b.fr'                ,    'base_id' => '7',       'routeur_id' => '2'];
            $domains[] = ['domain' => '7912o.fr'                ,    'base_id' => '7',       'routeur_id' => '2'];


            $domains[] = ['domain' => '9634v.fr'                ,    'base_id' => '1',       'routeur_id' => '4'];
            $domains[] = ['domain' => 'm0npix3l.fr'             ,    'base_id' => '1',       'routeur_id' => '4'];
            $domains[] = ['domain' => 'offrir10001.fr'          ,    'base_id' => '3',       'routeur_id' => '4'];
            $domains[] = ['domain' => 'mespixels852.fr'         ,    'base_id' => '3',       'routeur_id' => '4'];
            $domains[] = ['domain' => 'customercare357.fr'      ,    'base_id' => '4',       'routeur_id' => '4'];
            $domains[] = ['domain' => 'ctnssvrldd143.fr'        ,    'base_id' => '4',       'routeur_id' => '4'];
            $domains[] = ['domain' => '526lwysbhppy.fr'         ,    'base_id' => '7',       'routeur_id' => '4'];
            $domains[] = ['domain' => 'wlcmthbstnws.fr'         ,    'base_id' => '7',       'routeur_id' => '4'];

            $domains[] = ['domain' => 'rghtnws33.fr'            ,    'base_id' => '1',       'routeur_id' => '5'];
            $domains[] = ['domain' => 'sflnwsfthdy22000.fr'     ,    'base_id' => '1',       'routeur_id' => '5'];
            $domains[] = ['domain' => 'lvblbtflntllgnt684.fr'   ,    'base_id' => '3',       'routeur_id' => '5'];
            $domains[] = ['domain' => 'smllthgs111.fr'          ,    'base_id' => '3',       'routeur_id' => '5'];
            $domains[] = ['domain' => 'lndxmlle2000.fr'         ,    'base_id' => '4',       'routeur_id' => '5'];
            $domains[] = ['domain' => 'wrthbst99000.fr'         ,    'base_id' => '4',       'routeur_id' => '5'];
            $domains[] = ['domain' => 'msstdncrncr1.fr'         ,    'base_id' => '7',       'routeur_id' => '5'];
            $domains[] = ['domain' => 'friendlink7.fr'          ,    'base_id' => '7',       'routeur_id' => '5'];

            $domains[] = ['domain' => 'alliswell8.fr'           ,    'base_id' => '1',       'routeur_id' => '6'];
            $domains[] = ['domain' => 'fstrbttr4.fr'            ,    'base_id' => '1',       'routeur_id' => '6'];
            $domains[] = ['domain' => 'cstttprlmmnt755.fr'      ,    'base_id' => '3',       'routeur_id' => '6'];
            $domains[] = ['domain' => '226tinybox.fr'           ,    'base_id' => '3',       'routeur_id' => '6'];
            $domains[] = ['domain' => 'hrdtfnd99.fr'            ,    'base_id' => '4',       'routeur_id' => '6'];
            $domains[] = ['domain' => '874st.fr'                ,    'base_id' => '4',       'routeur_id' => '6'];
            $domains[] = ['domain' => 'ras646.fr'               ,    'base_id' => '7',       'routeur_id' => '6'];
            $domains[] = ['domain' => 'soy357.fr'               ,    'base_id' => '7',       'routeur_id' => '6'];

        } elseif (getenv('CLIENT_URL') == 'tor-ve.lead-factory.net') {
            $domains = array();
            $domains[] = ['domain' => 'qg36921.fr'              ,   'base_id' => '2',        'routeur_id' => '6'];
            $domains[] = ['domain' => '218tutu.fr'              ,   'base_id' => '2',        'routeur_id' => '3'];

        } elseif (getenv('CLIENT_URL') == 'tor-agv.lead-factory.net') {
            $domains = array();
            $domains[] = ['domain' => 'rand0m1.fr'              ,   'base_id' => '1',        'routeur_id' => '3'];
            $domains[] = ['domain' => 'kndhrt679.fr'            ,   'base_id' => '1',        'routeur_id' => '3'];
            $domains[] = ['domain' => 'pomme340.fr'             ,   'base_id' => '1',        'routeur_id' => '3'];

        }

        foreach($domains as $d){
            \DB::table('pixel_domain_base_routeur')->insert($d);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pixel_domain_base_routeur');
    }
}
