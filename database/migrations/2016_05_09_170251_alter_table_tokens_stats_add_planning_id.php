<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTokensStatsAddPlanningId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     \DB::statement("ALTER TABLE tokens_stats ADD planning_id int");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
