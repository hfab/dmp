<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNextCampaignToPlannings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plannings', function (Blueprint $table) {
            $table->tinyInteger("next_campaign");
            //marqueur pour savoir a quelle custom field correspond chez MD dans le cas de campagne parallele
            //next_campaign1, next_campaign2, next_campaign3 ou next_campaign4
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
