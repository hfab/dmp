<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fais', function($table)
		{
			$table->increments('id');
            $table->string('nom');
            $table->integer('quota_pression');
            $table->integer('quota_sender');
            $table->string('domains', 500);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fais');
	}

}
