<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableThemesAddEtatActif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // ajout d'une colonne pour connaitre l'état du theme
      Schema::table('themes', function (Blueprint $table) {
          $table->tinyInteger("etat_actif")->default('1')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
