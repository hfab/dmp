<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNbOccurencesToDmpMatched extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('dmp_matched',function($table){
	    	$table->integer('nb_has_ordered')->default(0);
	    });

	    Schema::table('campagnes',function($table){
	    	$table->date('expires_at')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
