<?php
use Illuminate\Database\Seeder;
use App\Models\Routeur;
use Illuminate\Database\Eloquent\Model;

class RouteurSmessageTableSeeder extends Seeder {

    public function run()
    {
        Model::unguard();
        $r = Routeur::create(['nom' => 'Smessage', 'variable_email' => '%email%']);

        $r->variable_unsubscribe = "@@@";
        $r->variable_mirror = "&&&";
        $r->variable_email_sha1 = "";
        $r->variable_email_md5 = "";
        $r->variable_prenom = "%prenom%";
        $r->variable_nom = "%nom%";
        $r->variable_tor_id = "%p1%";
        $r->save();
        Model::reguard();
    }
}
