<?php
use Illuminate\Database\Seeder;
use App\Models\Ouverture;

class OuverturesTableSeeder extends Seeder {

    public function run()
    {
//        \DB::table('ouvertures')->delete();

        $items[] = ['id' => 1, 'destinataire_id' => 1, 'campagne_id'=> 1 ];
        $items[] = ['id' => 2, 'destinataire_id' => 2, 'campagne_id'=> 1 ];
        $items[] = ['id' => 3, 'destinataire_id' => 3, 'campagne_id'=> 1 ];
        $items[] = ['id' => 4, 'destinataire_id' => 5, 'campagne_id'=> 1 ];

        foreach($items as $item)
        {
            Ouverture::create($item);
        }
    }
}
