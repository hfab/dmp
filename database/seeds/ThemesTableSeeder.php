<?php
use Illuminate\Database\Seeder;
use App\Models\Theme;

class ThemesTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('themes')->delete();

        $themes[] = ['id' => 170, 'nom' => 'Voyage'];
        $themes[] = ['id' => 130, 'nom' => 'Loisirs'];
        $themes[] = ['id' =>  80, 'nom' => 'Défiscalisation immobilière'];
        $themes[] = ['id' =>  40, 'nom' => 'Bien être beauté et hygiène'];
        $themes[] = ['id' =>  10, 'nom' => 'Alimentaire'];
        $themes[] = ['id' =>  30, 'nom' => 'Automobile'];
        $themes[] = ['id' =>  50, 'nom' => 'Concours'];
        $themes[] = ['id' =>  20, 'nom' => 'Assurance/Mutuelle'];
        $themes[] = ['id' =>  60, 'nom' => 'Consommation'];
        $themes[] = ['id' =>  70, 'nom' => 'Crédit'];
        $themes[] = ['id' =>  90, 'nom' => 'Défiscalisation immobilier'];
        $themes[] = ['id' => 100, 'nom' => 'Enquêtes'];
        $themes[] = ['id' => 110, 'nom' => 'Formation'];
        $themes[] = ['id' => 120, 'nom' => 'Jeux'];
        $themes[] = ['id' => 140, 'nom' => 'Petites annonces'];
        $themes[] = ['id' => 150, 'nom' => 'Rachats de crédits'];
        $themes[] = ['id' => 160, 'nom' => 'Vêtements'];
        $themes[] = ['id' => 180, 'nom' => 'Maison et Déco'];
        $themes[] = ['id' => 190, 'nom' => 'Photos'];

        foreach($themes as $theme)
        {
            Theme::create($theme);
        }
    }
}
