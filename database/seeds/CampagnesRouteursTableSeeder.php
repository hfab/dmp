<?php
use Illuminate\Database\Seeder;
use App\Models\CampagneRouteur;

class CampagnesRouteursTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('campagnes_routeurs')->delete();

        $items[] =
            [
                'id' => 1,
                'campagne_id' => 1,
                'sender_id' => 3,
                'cid_routeur'=> 24645
            ];

        $items[] =
            [
                'id' => 2,
                'campagne_id' => 2,
                'sender_id' => 3,
                'cid_routeur'=> 24656
            ];

        $items[] =
            [
                'id' => 3,
                'campagne_id' => 1,
                'sender_id' => 4,
                'cid_routeur'=> 24444
            ];

        $items[] =
            [
                'id' => 4,
                'campagne_id' => 2,
                'sender_id' => 4,
                'cid_routeur'=> 21613
            ];

        $items[] =
            [
                'id' => 5,
                'campagne_id' => 2,
                'sender_id' => 5,
                'cid_routeur'=> 24221
            ];

        foreach($items as $item)
        {
            CampagneRouteur::create($item);
        }
    }
}
