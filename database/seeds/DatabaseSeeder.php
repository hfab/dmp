<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $this->call('UserGroupTableSeeder');
		$this->call('UsersTableSeeder');
        $this->call('ThemesTableSeeder');
        $this->call('BasesTableSeeder');
        $this->call('RouteursTableSeeder');
        $this->call('FaisTableSeeder');
        Artisan::call('tokens:manage');

        Model::reguard();
	}
}
