<?php

use Illuminate\Database\Seeder;
use App\Models\Editor;

class EditorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $array = array(array('name'=>'sc2consulting'),array('name'=>'capdecision'));   
	    DB::table('editors')->insert($array);
    }
}
