<?php
use Illuminate\Database\Seeder;
use App\Models\UserGroup;

class UserGroupTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('user_group')->truncate();

        $now = date('Y-m-d H:i:s');

        $groups[] = [
            'name'              => 'Utilisateur',
            'created_at'        => $now,
            'updated_at'        => $now,
        ];
        
        $groups[] = [
            'name'              => 'Admin',
            'created_at'        => $now,
            'updated_at'        => $now,
        ];

        $groups[] = [
            'name'              => 'Super Admin',
            'created_at'        => $now,
            'updated_at'        => $now,
        ];


        foreach($groups as $group)
        {
            UserGroup::create($group);
        }

        $super_admin = UserGroup::where('name', 'Super Admin')->first();
        \DB::table('users')
            ->where('is_valid', 1)
            ->where('email', 'LIKE', '%dev%')
            ->update(['user_group_id' => $super_admin->id]);
    }
}