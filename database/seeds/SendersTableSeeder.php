<?php
use Illuminate\Database\Seeder;
use App\Models\Sender;
use App\Models\Routeur;

class SendersTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('senders')->delete();

        //Tor PATH for more senders :
        // /home/web/tor.sc2consulting.fr/prod/inc/1_PARAMETRES_6.txt (EDIWARE)
        // /home/web/tor.sc2consulting.fr/prod/md/accounts.php (MAILDROP)
        // /home/web/tor.sc2consulting.fr/prod/a7/create_camp.php (A7)

//        $items[] = ['id' => 1, 'routeur_id' => 2, 'nom' => 'sc2_bat', 'password' => 'Dg5436pax', 'quota_left' => round(rand(0,100)) ];
//        $items[] = ['id' => 2, 'routeur_id' => 2, 'nom' => 'sc2_bat', 'password' => 'Dg5436pax', 'quota_left' => round(rand(0,100)) ];


        $md = [
            [   'nom' => 'Compte1', 'password' => '8c2b6b88418d401b87bed7f0419c31e9', 'domaine'=> 'news@zenewsoftheday.com',],
            [   'nom' => 'Compte2', 'password' => 'c87f56f3edf7793aaa0ff4e08d1207ef', 'domaine'=> 'news@lafolieduweb.com'],
            [   'nom' => 'Compte3', 'password' => '5a3b75e716ff616da2af79f1151166e8', 'domaine'=> 'news@lacampagneduweb.com'],
            [   'nom' => 'Compte4', 'password' => '26476d570801e18077bd0133c1e9fd36', 'domaine'=> 'news@lapromodunet.com'],
            [   'nom' => 'Compte5', 'password' => '66757ecf17ddd18458b05b5b85a3a1aa', 'domaine'=> 'news@offre-emailing.com'],
            [   'nom' => 'Compte6', 'password' => '453d4bee4c68f5aa7c6cbe5fb353b069', 'domaine'=> 'news@theoffredujour.com'],
            [   'nom' => 'Compte7', 'password' => '52ffaf288da1bb2151b75c9606bc6c46', 'domaine'=> 'news@thepromosdujour.com'],
            [   'nom' => 'Compte8', 'password' => '8d3efc3005238bdaa9cd61fcf7356819', 'domaine'=> 'news@zepromosdujour.com'],
            [   'nom' => 'Compte9', 'password' => '26199a761900b8056ba06ddfd1f6af44', 'domaine'=> 'news@zeoffreduweb.com'],
            [   'nom' => 'Compte10', 'password' => '3d4f302b84a149a1d937929f92e49b49', 'domaine'=> 'news@unjourunproduit.com'],
            [   'nom' => 'Compte11', 'password' => 'a3d0f6679a100e90025767580b73ab39', 'domaine'=> 'news@lesoccasesdunet.com'],
            [   'nom' => 'Compte12', 'password' => 'bfebdf31227fd0ae45659ddb2858a0e4', 'domaine'=> 'news@ledealduweb.com'],
            [   'nom' => 'Compte13', 'password' => '3aa90bf50d9605b888b7c8cf1adb4f49', 'domaine'=> 'news@lesoccasesduweb.com'],
            [   'nom' => 'Compte14', 'password' => '842fd2d6a937f6ad337f3c7db69f8b4a', 'domaine'=> 'news@zeannoncedunet.com'],
            [   'nom' => 'Compte15', 'password' => 'c198247605df76362aebc8c187450160', 'domaine'=> 'news@club-email.com'],
            [   'nom' => 'Compte16', 'password' => '895c9f871efc115be8d8c5b0d94315a7', 'domaine'=> 'news@club-newsletter.com'],
            [   'nom' => 'Compte17', 'password' => '8fe95b6fbfc554bbc837ad054a139eb9', 'domaine'=> 'news@uneaffaireenor.com'],
            [   'nom' => 'Compte18', 'password' => 'b7ec88b1da640d824fb5e555a07d46c3', 'domaine'=> 'news@lanewsenfolie.com'],
            [   'nom' => 'Compte19', 'password' => '9f866ff8bc0f94847355346c73fc9093', 'domaine'=> 'news@ledealmagique.com'],
            [   'nom' => 'Compte20', 'password' => 'bde52b9792f2282335cd8095a59c725b', 'domaine'=> 'news@lesoffresenfolie.com'],
            [   'nom' => 'Compte21', 'password' => '11c824e04fa8f5cf727eca27fc6d0112', 'domaine'=> 'news@letopdesaffaires.com'],
            [   'nom' => 'Compte22', 'password' => '4e3dc3601dac21578c5b179c4ca0300c', 'domaine'=> 'news@letopdesdeals.com'],
            [   'nom' => 'Compte23', 'password' => '57fca851cdc67ceea019dc0d35c6fa50', 'domaine'=> 'news@undealparnews.com'],
            [   'nom' => 'Compte24', 'password' => '3abd90fff1728fb6ddc25cd97be8a8f7', 'domaine'=> 'news@uneaffaireparnews.com'],
            [   'nom' => 'Compte25', 'password' => '940f6eb7f65934e09f16bd4ea755475a', 'domaine'=> 'news@unenewsunjour.com'],
            [   'nom' => 'Compte26', 'password' => 'cf6413712cc9d86fdab751e96e1789cc', 'domaine'=> 'news@mix.unepromoparnews'],
            [   'nom' => 'Compte27', 'password' => 'd601ea2533ec06bdcb3ccf4e5df28df3', 'domaine'=> 'news@zeaffairedusiecle.com'],
            [   'nom' => 'Compte28', 'password' => '99606d9cc2b0daba0170504f13545a33', 'domaine'=> 'news@lagaleriedesdeals.com'],
            [   'nom' => 'Compte29', 'password' => 'bcee52531afca1eb7b7125f58371e766', 'domaine'=> 'news@unepromoparnews.com'],
            [   'nom' => 'Compte30', 'password' => 'ab7d41174f5522f90639dbbbc466f331', 'domaine'=> 'news@lanewsdesbonnesaffaires.com'],
            [   'nom' => 'Compte31', 'password' => '402db01398b6fda20aab5810fc57ea95', 'domaine'=> 'news@lapassiondesdeals.com'],
            [   'nom' => 'Compte32', 'password' => '92dae988b60a3743f4dbb135f287ef4c', 'domaine'=> 'news@lapassiondesmarques.com'],
            [   'nom' => 'Compte33', 'password' => '4eb0e3ae33e03fabab504c49c8e97b6c', 'domaine'=> 'news@leclubdesnews.com'],
            [   'nom' => 'Compte34', 'password' => '9616509bf23530715185cb0e801d92b7', 'domaine'=> 'news@lecomptoirdesbonnesaffaires.com'],
            [   'nom' => 'Compte35', 'password' => 'e73fdf3e05eabd8ef8fab096eb7ef034', 'domaine'=> 'news@letopdelanews.com'],
            [   'nom' => "Compte36", 'password' => '44e3a87860459d44cea1835f0c1c5824', 'domaine'=> 'news@lexpertdesmarques.com'],
            [   'nom' => "Compte37", 'password' => '9c4ff475a59567388211f7e59ba03fc9', 'domaine'=> 'news@thecoindesmarques.com'],
            [   'nom' => "Compte38", 'password' => '138aacba7e90219ad160da306e53bc68', 'domaine'=> 'news@unemarqueparjour.com'],
            [   'nom' => "Compte39", 'password' => '6600763b99f141d8d868c27665f07aef', 'domaine'=> 'news@unenewsunemarque.com'],
            [   'nom' => "Compte40", 'password' => '350aa34cd4139dcd4255cecf488aa8ec', 'domaine'=> 'news@zecoindesbonnesaffaires.com'],
            [   'nom' => "Compte41", 'password' => '836c7046a8f4c5e0db16492fce24f1f3', 'domaine'=> 'news@unenewsparjour.com'],
            [   'nom' => "Compte42", 'password' => '6ff5df1e155892d25c91a2ad1185effb', 'domaine'=> ''],
            [   'nom' => "Compte43", 'password' => '3b1b594633b45ec1d1e91bd97cdb7cb1', 'domaine'=> ''],
            [   'nom' => "Compte44", 'password' => '42dbf69ba3133463f82d68cc44b9f803', 'domaine'=> ''],
            [   'nom' => "Compte45", 'password' => 'fc0ca28ac638984052a234ad61b78f14', 'domaine'=> ''],
            [   'nom' => "Compte46", 'password' => 'f7995182b4a63bfd14b271def32d20ba', 'domaine'=> ''],
            [   'nom' => "Compte47", 'password' => 'a2b9936aa2a9d723fdcfdf8abdfbbc63', 'domaine'=> ''],
            [   'nom' => "Compte48", 'password' => '0e5f79b5746210dc0d5d898dcb387c72', 'domaine'=> ''],
            [   'nom' => "Compte49", 'password' => 'bf163d176747211fe37e207f0e8f2965', 'domaine'=> ''],
            [   'nom' => "Compte50", 'password' => 'e37e70b6c62f720a281bc6dc2e21be1b', 'domaine'=> ''],
            [   'nom' => "Compte51", 'password' => '6c95af8f0062b7c20fa32d885ca27329', 'domaine'=> ''],
            [   'nom' => "Compte52", 'password' => '35e222f5e018c1f8a3748b8a6928637f', 'domaine'=> ''],
            [   'nom' => "Compte53", 'password' => '80c25e931331301864586735db678bec', 'domaine'=> ''],
            [   'nom' => "Compte54", 'password' => '34f89b0f4f46248ce71d225aae74f370', 'domaine'=> ''],
            [   'nom' => "Compte55", 'password' => '4032762724f438f2b9b2b610cc23f067', 'domaine'=> ''],
            [   'nom' => "Compte56", 'password' => 'cd49cfd8b31a1a37b52830da01da3c82', 'domaine'=> ''],
            [   'nom' => "Compte57", 'password' => '40701cc8cd523abea9bb1dfe86b60f92', 'domaine'=> ''],
            [   'nom' => "Compte58", 'password' => '59600d7097b97e2fb7e519eabdd7bc1f', 'domaine'=> ''],
            [   'nom' => "Compte59", 'password' => '4f0e6361e95932a967c8b25173741019', 'domaine'=> ''],
            [   'nom' => "Compte60", 'password' => '8f2b3e4e4afeed8c7b7df629607327b4', 'domaine'=> ''],
            [   'nom' => "Compte61", 'password' => 'a16198129380ccef2b0e86af0e72c377', 'domaine'=> ''],
            [   'nom' => "Compte62", 'password' => 'be6cc5be79890cfdc3082a6a928d0926', 'domaine'=> ''],
            [   'nom' => "Compte63", 'password' => '34729a16918f040a4601ba58b6713d22', 'domaine'=> ''],
            [   'nom' => "Compte64", 'password' => '0d7eb81e12038f02e86ec5f1f9d846fc', 'domaine'=> ''],
            [   'nom' => "Compte65", 'password' => 'c568cbd32b457f66ba174cbd2ae75977', 'domaine'=> ''],
            [   'nom' => "Compte66", 'password' => '8935e3fa0b3861cc937bfae6b08d3528', 'domaine'=> ''],
            [   'nom' => "Compte67", 'password' => '10658176e2abd81e48ef6483e7da6b16', 'domaine'=> ''],
            [   'nom' => "Compte68", 'password' => 'bbc9952290eda003c14a1264a25e0b8e', 'domaine'=> ''],
            [   'nom' => "Compte69", 'password' => 'cf46c00b716b44615269166a3fa51fd6', 'domaine'=> ''],
            [   'nom' => "Compte70", 'password' => 'df720c1281ec489aaf7c2f8396558ff6', 'domaine'=> ''],
            [   'nom' => "Compte71", 'password' => '8ea1652f4a0e963647e40bec8f2cc8a1', 'domaine'=> ''],
            [   'nom' => "Compte72", 'password' => '732dcedd303ffd805c74531d0587937e', 'domaine'=> ''],
            [   'nom' => "Compte73", 'password' => '0b6bcf2d58fa68b6401b850920391629', 'domaine'=> ''],
            [   'nom' => "Compte74", 'password' => 'ef7b130e74c26b7f5fbf612b30fac6dc', 'domaine'=> ''],
            [   'nom' => "Compte75", 'password' => '500ff8f50f06c044d12913e0c0000119', 'domaine'=> ''],
            [   'nom' => "Compte76", 'password' => 'b933b19ba004ffdc018a645c48157fc4', 'domaine'=> ''],
            [   'nom' => "Compte77", 'password' => '78dc9497e65469f60e9818d5a12fd7d4', 'domaine'=> ''],
            [   'nom' => "Compte78", 'password' => '47857672909845113b226a43fdb113bb', 'domaine'=> ''],
            [   'nom' => "Compte79", 'password' => '8988a2cb5b76dfdfd5de21829e650c72', 'domaine'=> ''],
            [   'nom' => "Compte80", 'password' => '82e72163dc2ed72e080f1668cc483305', 'domaine'=> ''],
            [   'nom' => "Compte81", 'password' => '9cad60f2c984b0c58372c8e06f68ede4', 'domaine'=> ''],
            [   'nom' => "Compte82", 'password' => '4752f75205e400ea8e10aeb23e5bab96', 'domaine'=> ''],
            [   'nom' => "Compte83", 'password' => '0b45e9b393d69e3081c8064ea03cdcc5', 'domaine'=> ''],
            [   'nom' => "Compte84", 'password' => 'bdf848007bb8ad426c32741f59a3f4ff', 'domaine'=> ''],
            [   'nom' => "Compte85", 'password' => '0e8d8530bd4161b39fec2b29d98f247a', 'domaine'=> ''],
            [   'nom' => "Compte86", 'password' => '6fa26581eeffbc64484564af53da31a1', 'domaine'=> ''],
            [   'nom' => "Compte87", 'password' => '417d3bc5cc76e33117c23b4947529864', 'domaine'=> ''],
            [   'nom' => "Compte88", 'password' => 'bcca3df1b4b5b7649b1a6518126e6a5c', 'domaine'=> ''],
            [   'nom' => "Compte89", 'password' => 'db7d44180fec2198415e22ccb4e9abd9', 'domaine'=> ''],
            [   'nom' => "Compte90", 'password' => '0cbf7950f1800e401df2543389ace435', 'domaine'=> ''],
            [   'nom' => "Compte91", 'password' => '84d38dede3d8b9e0358aa1970c8f2a66', 'domaine'=> ''],
            [   'nom' => "Compte92", 'password' => 'b40c012384dcb82bb0cf14f27cdcba05', 'domaine'=> ''],
            [   'nom' => "Compte93", 'password' => 'b5ebcb8f2ff008af87eec5b09ca7477b', 'domaine'=> ''],
            [   'nom' => "Compte94", 'password' => 'ab980f92e353fa359bbe424de9b4b1b1', 'domaine'=> ''],
            [   'nom' => "Compte95", 'password' => '7d6ebf5b57954009a16edab73a80f4b9', 'domaine'=> ''],
            [   'nom' => "Compte96", 'password' => '74ca19abbd3abf95631a8317d263cede', 'domaine'=> ''],
            [   'nom' => "Compte97", 'password' => '844b92878eb5762b256213012e0aceee', 'domaine'=> ''],
            [   'nom' => "Compte98", 'password' => '361a813bd9cc536569251407fc35c50b', 'domaine'=> ''],
            [   'nom' => "Compte99", 'password' => 'c2530e54a9601687aaa3822b48503725', 'domaine'=> ''],
            [   'nom' => "Compte100", 'password' => 'a5c1fdf580fb989e9dc3076b8c50a7da','domaine'=> 'news@mix.lanewsbonplan.com'],
            [   'nom' => "Compte101", 'password' => 'dcd7ac5c82f654d75d79ad699ba16ff2', 'domaine'=> 'news@mix.lashopdesdeals.com'],
            [   'nom' => "Compte102", 'password' => '003df13c86440133aa9889ab27bdee92', 'domaine'=> 'news@mix.lasuperoffredujour.com'],
            [   'nom' => "Compte103", 'password' => '2825682abc19c6f4f4d0602eea73468d', 'domaine'=> 'news@mix.lecoindubonplan.com'],
            [   'nom' => "Compte104", 'password' => 'a987e4b30148a870d187fbeeed2eed5a', 'domaine'=> 'news@mix.lemegadealdujour.com'],
            [   'nom' => "Compte105", 'password' => '389309dc58908d273f4cc5706a78c66e', 'domaine'=> 'news@mix.lesuperplandujour.com'],
            [   'nom' => "Compte106", 'password' => '4288c943241380e3ae38e40124384f62', 'domaine'=> 'news@mix.letopbonplan.com'],
            [   'nom' => "Compte107", 'password' => '406c188adef8ec256fc01fd7ce10a361', 'domaine'=> 'news@mix.letopdudeal.com'],
            [   'nom' => "Compte108", 'password' => 'ca3f941e6f1996137e728c1c06f1fd1c', 'domaine'=> 'news@mix.lextradeal.com'],
            [   'nom' => "Compte109", 'password' => '624a0fa2ab5c0788e989a92318f4e880', 'domaine'=> 'news@mix.loffrebonplan.com'],
            [   'nom' => "Compte110", 'password' => '1cacbfeb19219c24b0e5181c6935c59f', 'domaine'=> 'news@mix.loffreextraordinaire.com'],
            [   'nom' => "Compte111", 'password' => '3a13f2af5530531b8204623c2140f175', 'domaine'=> 'news@mix.loffremagique.com'],
            [   'nom' => "Compte112", 'password' => 'c8fd2962fb28b556984d7cad2db8d780', 'domaine'=> 'news@mix.loffrestar.com'],
            [   'nom' => "Compte113", 'password' => 'd97e4de970d93c549c917ba2f6616581', 'domaine'=> 'news@mix.luniversdesdeals.com'],
            [   'nom' => "Compte114", 'password' => 'ed73c9bc4a28ed1875da8885a7a17408', 'domaine'=> 'news@mix.luniversdesoffres.com']
        ];
        
        $routeur = Routeur::where('nom','Maildrop')->first();

        foreach($md as $item) {

            $item['quota']          = 12000;
            $item['quota_left']     = 12000;
            $item['routeur_id']     = $routeur->id;
            $sender = Sender::create($item);

            if (in_array($sender->id, range(5,25))
             || in_array($sender->id, range(27,46))
             || in_array($sender->id, range(51,66))
             || in_array($sender->id, range(119,120))
            ) {
                $fais = [1];
                $sender->quota = 10000;
                $sender->quota_left = 10000;
                $sender->save();

            } else {
                $fais = [2,3,4,5,6,7,8];
            }

            $sender->fais()->sync($fais);
        }
    }
}

