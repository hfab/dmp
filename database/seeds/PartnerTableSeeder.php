<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dmp_partnersites')->insert(["name"=>"sc2consulting","url"=>"www.sc2consulting.fr","created_at"=>date('Y-m-d'),'updated_at'=>date('Y-m-d'),"subDomain"=>"",'theme_id'=>0]);
        DB::table('dmp_partnersites')->insert(["name"=>"capdecision","url"=>"http://www.capdecision.fr","created_at"=>date('Y-m-d'),'updated_at'=>date('Y-m-d'),"subDomain"=>""]);
        DB::table('dmp_partnersites')->insert(["name"=>"voyaneo","url"=>"http://www.voyaneo.com","created_at"=>date('Y-m-d'),'updated_at'=>date('Y-m-d'),"subDomain"=>"http://resa.voyaneo.com",'theme_id'=>170]);
    }
}
