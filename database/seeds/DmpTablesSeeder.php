<?php
use Illuminate\Database\Seeder;
use App\Models\Base;

class DmpTablesSeeder extends Seeder {

    public function run()
    {
        $items[] =
            [
                'id' => 1,
                'url' => 'http://'.getenv('CLIENT_URL'),
//                'dmp_partner_id' => ,
//                'base_id' => 1,
                'created_at' => '2017-09-15 00:00:00',
                'updated_at' => '2017-09-15 00:00:00',
            ];


        $items[] =
            [
                'id' => 2,
                'url' => 'http://www.voyaneo.com',
//                'dmp_partner_id' => ,
//            'base_id' => 1,
            'created_at' => '2017-09-15 00:00:00',
            'updated_at' => '2017-09-15 00:00:00',
        ];


        foreach($items as $item)
        {
            \DB::table('dmp_partnersites')->insert($item);
//            \DB::table('dmp_partners_sites')->insert($item);
        }
    }
}
