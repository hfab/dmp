/*A mettre sur toutes les pages des sites partenaires ou sur leurs homePage
Via la création de la balise script avec pour url celle pointant vers la DMP,
On crée une copie identique du cookie créée au moment de l'ouverture du mail par le destinataire
Il est nécessaire de faire ceci pour pouvoir manipuler ensuite ces données au moment où on souhaite envoyer les infos à la DMP.*/

var ndd = "";
@listNdd

//Retourne la valeur du cookie donnée en paramètre
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    var i = 0;
    for(i; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function genCoo(){
    lnd.forEach(function(element){
        var s =document.createElement('script');
        var u = 'https://'+element+'/'+'ajcal_coo'; //Changer par l'url du DMP
        s.setAttribute("type","text/javascript");
        s.setAttribute("src", u);
        document.getElementsByTagName("head")[0].appendChild(s);
        if(getCookie('8a704337bac01056fdb5ffdef3d6c57e') != ""){
            hasOrdered('8a704337bac01056fdb5ffdef3d6c57e', 0);
            ndd = element;
            console.log(ndd);
        }
        else{
            document.getElementsByTagName("head")[0].removeChild(s);
        }
    });

}

function genCooDet(){
    document.addEventListener('DOMContentLoaded',genCoo,false);
    document.addEventListener('DOMContentLoaded',getExtl, false);
}
//Crée un objet permettant envoi de requête ajax

function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch(e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        console.log("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

//Envoie à la DMP si le destinataire à réaliser un achat ou non

function hasOrdered(token,hasOrdered){
    var cookieValue = getCookie(token);
    var here = window.location.href;


    if(cookieValue != ""){
        var xhr = getXMLHttpRequest();
        xhr.onreadystatechange = function(){
            var response = xhr.responseText;
            //console.log(response);
        }

        xhr.open("GET","https://"+ndd+"/ajcal_vis?token="+cookieValue+"&has_ordered="+hasOrdered+"&by_link="+here,true); //Changer par l'url du DMP
        xhr.send();
    }
    else{
        // console.log("Fail");
    }
}

function getExtl(){
    //Récupération de tous les liens externes de la page.
    var extLinks = document.querySelectorAll('a[href*="https://"]');
    for(var index = 0;index< extLinks.length;index++){
        //Si le destinataire sur un lien externe, on envoie à la DMP que le destinataire en question n'a pas réalisé d'achat
        extLinks[index].addEventListener('click',function(){
            hasOrdered('8a704337bac01056fdb5ffdef3d6c57e',0);
        });
    }
}

var s = window.location.hostname;
var call = getXMLHttpRequest();
call.open("GET", "https://scribouille.fr/ajcal_s/"+s, true);
call.send(null);


//A mettre sur toutes les pages pour détecter le départ de l'user
//genCooDet();
//A mettre sur la page de confirmation
//Informe que la DMP que l'utilisateur a réalisé un achat
//hasOrdered('8a704337bac01056fdb5ffdef3d6c57e', 1);
