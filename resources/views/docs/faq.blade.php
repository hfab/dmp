@extends('common.layout')

@section('content')
<div class="portlet-title">
  <div class="caption">
    <h3><i class="fa fa-book fa-5 font-green-sharp"></i>
      <span class="caption-subject font-green-sharp bold uppercase">Tor - FAQ</span>
    </h3>
  </div>
  <div class="portlet-body">
      <h4>Abonnés</h4>

      <h5>Peut-il y avoir des doublons dans une base ?</h5>
      <p>Chaque base possède des abonnés. Il ne peut exister d’adresses doublons dans une même base. <br/>
          Tor dédoublonne automatiquement lors de chaque import d’abonnés à la base.<br/>
          Toutefois, il peut exister des adresses communes entre les différentes bases.
      </p>

      <h5>Comment consulter les informations d'une base ?</h5>
      <p>Vous pouvez connaitre le nombre de destinataires par base en consultant cette page : <br/>
        > <a title="fai" href="/base">Liste des bases</a><br>
        Il est aussi possible de consulter les statistiques détaillées (tel que le nombre de désinscrits, répartition par FAI) de votre base en cliquant sur ce logo : <span class="btn yellow-casablanca btn-xs"><i class="fa fa-pie-chart" aria-hidden="true"></i></span>
      </p>

      <!-- Comment consulter le status d'une adresse -->
      <h5>Comment consulter le status d'une adresse ou d'un FAI</h5>
      <p>Il est intéréssant de savoir ce que votre base contient de manière plus détaillée. De plus vous voulez peut être connaitre le status d'une adresse en particulier. Pour ce faire vous pouvez utiliser une page qui vous permet de recherche une adresse complète ou seulement un FAI tel que Free par exemple en tappant : "@free.fr"<br/>
        > <a title="fai" href="/base/searchmailall">Faire une recherche dans les bases</a><br>
      </p>

      <h5>Comment désabonner une adresse manuellement ?</h5>
      <p>En cas de plainte, vous serez peut-être amené à désabonner une adresse email en particulier (si l'emetteur de la plainte n'a pas cliqué sur le lien de désabonnement)<br>
          > <a title="fai" href="/unsubscribe">Désabonnement manuel</a>
      </p>

      <h5>Tor gère t'il la désinscription automatique ?</h5>
      <p>Le logiciel Tor récupère automatiquement les désinscrits via les routeurs avec lequel il est compatible. Vous n'avez aucune action particulière à faire. Attention, lorsque Tor récupère les désinscriptions, l'abonné sera désinscrit de toutes les bases pour éviter toute nouvelle plainte. <br>
          > <a title="fai" href="/unsubscribe">Désabonnement manuel</a>
      </p>

      <h5> Comment puis-je régler la pression marketing excercée sur les adresses ?</h5>
      <p>
          Il est possible de la régler en fonction de chaque FAIs dans Réglages > Le FAI concerné<br>
          La modification sera prise en compte pour le lendemain. Si il y a de nouveaux imports, ils seront pris en compte pour ces nouvelles adresses importés.<br>
          > <a title="fai" href="/fai">Modifier la pression marketing</a>
      </p>

      <h5>Est-il possible de créer un segment ?</h5>
      <p>Il est possible de créer des segments générique type : « Segments ouvreurs assurance du 1er janvier au 15 janvier», « Segments ouvreurs des Hauts-de-Seine du 1er décembre au 1er janvier »<br>
          > <a title="fai" href="/segment">Ajouter un segment</a> <br>
          Une fois créé, le segment peut être indiqué lors de la planification de la campagne.
      </p>

      <h5> Comment importer des contacts ?</h5>
      <p>
          Vous pouvez importer des contacts sur votre base via un formulaire. Il faut se rendre sur la page qui liste vos bases à cette adressse, puis cliquer sur le bouton pour accéder au formulaire <span class="btn green btn-xs"><i class="fa fa-user-plus" aria-hidden="true"></i></span><br>

          > "Ajouter liste @" : vous permet d'importer une liste de contact simple avec un fichier qui comporte une adresse par ligne.<br>
          > "Ajouter liste @ + champs" : vous permet d'importer une liste de contact avec des champs en plus tel que le nom / prénom, le département, etc. Cela vous permet alors de créer un segment de géolocalisation en se basant sur le département par la suite et pouvoir envoyer sur les adresses d'une zone précise.<br><br>

          <b>Attention, lors de l'import de votre fichier avec des champs personnalisés, vous allez choisir l'ordre des champs, votre fichier CSV doit bien correspondre avec cet ordre. Les données seront séparées par un ";" ou un "|".</b>

      </p>

      <h4>Routeurs</h4>

      <h5>Quels sont les routeurs compatibles avec Tor ? </h5>
      <p>Tor est compatible avec les routeurs suivants :
          <ul>
              <li>Maildrop </li>
              <li>Mailforyou </li>
          </ul>
      </p>

      <h4>Senders</h4>
      <h5>Comment régler/gérer les quotas des senders ?</h5>
      <p>Un sender correspond au compte ou domaine unique expéditeur. <br/>
          Chaque sender peut avoir des spécificités et donc des capacités quotidiennes différentes (capacité globale, quota par FAI, nombre de campagnes par jour).
          Nous vous suggérons des quotas à ne pas dépasser pour éviter le blacklistage des senders :
      </p>

      <table class="table table-striped table-bordered">
          <tr>
              <th>

              </th>
              <th>
                  Capacité globale
              </th>
              <th>
                  Capacité par FAI français
              </th>
              {{--<th>--}}
                  {{--Capacité par FAI étranger--}}
              {{--</th>--}}
          </tr>
          <tr>
              <td>
                  Maildrop
              </td>
              <td>
                  150K
              </td>
              <td>
                  18K
              </td>
              {{--<td>--}}

              {{--</td>--}}
          </tr>
          <tr>
              <td>
                  Mailforyou
              </td>
              <td>
                  84K
              </td>
              <td>
                  lié au cadencement FAI du serveur
              </td>
              {{--<td>--}}

              {{--</td>--}}
          </tr>
      </table>

      <h4> Campagnes </h4>

      <h5>Comment héberger ultérieurement les images d'une campagne ?</h5>
      <p>
          Pour améliorer la délivrabilité de vos campagne, Tor peut copier les images (de liens externes) du kits vers un serveur Amazon pour en faire des liens propres pour chaque campagne. Pour cela, il suffit d’activer l’option ‘Hébergement images Amazon’ à la création de la campagne.
      </p>
      <p>
          Vous pouvez également héberger des images (de liens internes, images à fournir) à partir d’un fichier .zip contenant les images. Dans Campagnes > Actions > Importer images, il ne vous restera plus qu’à importer le fichier .zip et remplacer les liens du kit HTML par le nouveau lien de base.
      </p>

      <h5> Où puis-je consulter les statistiques de mes campagnes ? </h5>
      <p>
        Dans la rubrique Statistiques > Statistiques des campagnes > Le routeur concerné <br/>
          > <a title="fai" href="/stats/routeur">Consulter les statistiques par routeur</a>
      </p>

      <h5> A quelle fréquence sont mises à jour les statistiques des campagnes ? </h5>
      <p>
          Les statistiques des campagnes (ouvreurs, cliqueurs, désinscrits, bounces) sont mises à jour toutes les 4 heures.
      </p>

      <h5> Puis-je exclure des adresses sur l'envoi d'une campagne en particulier ? </h5>
      <p>
          Pour chaque campagne, on peut associer un fichier MD5 des adresses à mettre en repoussoir. <br/>
          Il faut d’abord importer le fichier repoussoir en MD5 : Campagnes > Actions > Importer repoussoir.<br>
          > <a title="fai" href="/upload">Importer un fichier repoussoir</a>
          Puis associer le fichier repoussoir à la campagne concernée via le bouton jaune ‘Repoussoir'.
      </p>

      <h5>Quels sont les tags de personnalisation disponible ?</h5>
      <p>
          Si vous souhaitez personnaliser votre kit avec l’adresse mail du destinataire : ajouter <b>[email]</b> dans le kit HTML.
      </p>
      <p>
        Il existe aussi des tags pour le header et le footer :
        <ul>
          <li> <b>[mirrorlink] </b> </li>
          <li> <b> [unsubscribelink] </b> </li>
        </ul>
      </p>

      <h5>Comment faire un BAT ?</h5>
      <p>
          Avant de procéder à l'envoi de votre campagne, il est préférable de s'assurer que le rendu correspont à vos attentes, pour cela vous pouvez envoyer un BAT « bon à tirer ». Pour envoyer votre BAT, sur votre campagne cliquez sur le bouton "Action" puis choisissez le routeur sur lequel vous allez envoyer votre BAT. N'oubliez pas de définir les utilisateurs qui vont recevoir les BAT.
      </p>

      <h4> Envois </h4>
      <h5> Quelles sont les étapes du processus d'envoi ? </h5>
      <p>
        Le processus d’envoi est composé de 3 étapes :
          <ul>
            <li><b>sélection des tokens </b> : sélection des adresses cibles ;</li>
            <li><b>segmentation </b> : dispatch des tokens, sélection de senders et imports de segments/listes ;</li>
            <li><b>déclenchement des envois </b> : lancement des envois sur chaque sender sélection de la campagne une fois tous les imports de listes terminés.</li>
          </ul>
      </p>
      <p>
          La date de la planifications correspond à la date à laquelle commence le processus d’envoi (sélections tokens + segmentation + déclenchement envois)
          Une même campagne ne peut être envoyée plusieurs fois à un même destinataire (d’une base) dans les 7 jours qui suivent le premier envoi.
      </p>

      <h5> Quand se génèrent les tokens ? </h5>
      <p>
          Les tokens du jour sont générés à partir de 4h.
      </p>

      <h5> Quelle est la durée totale du processus d'envoi ? </h5>

      <p>
          En règle général, si la planification est simple (premier envoi, sans ciblage, campagne mono-base) tout le processus dure au maximum 10 minutes. <br/>
          La durée du processus peut varier en fonction des plusieurs facteurs de planifications :
          <ul>
              <li> volume global +- 2min </li>
              <li> nombre de relances de la campagne = étalonnage sur plusieurs jours +-2min </li>
              <li> ciblage segment d’ouvreurs sur une grande période +-5min </li>
              <li> campagne multi-bases +-30min </li>
          </ul>
      </p>
      <p>
        Un cumul des paramètres cités plus-haut donne lieu à un cumul en durée d’exécution.
      </p>
      <p>
          <b> Pensez donc à bien espacer les planifications en fonction des durées estimées. </b>
      </p>

      <h5> Comment plannifier un envoi ? </h5>
      <p>
          Pour plannifier un envoi, allez sur votre campagne préalablement crée, plus cliquez sur le bouton "Planning". Ensuite vous pouvez cliquer sur "Ajouter un planning".<br>
          Il faut renseigner le volume pour chaque FAI avec le volume souhaité. Si vous ne renseignez pas le volume d'un FAI, Tor tentera de selectionner le volume totale en piochant dans les différents FAI.<br>
          Si vous ne souhaitez pas envoyer sur un FAI particulier de votre base, il faudra le selectionner et bien indiquer un volume à zero pour le FAI concerné.
      </p>

      <h5> Comment profiter du système de relance ? </h5>
      <p>
        Tor vous propose un système de relance. Cela signifie qu'il est possible de renvoyer une campagne sur votre base, mais sans envoyer sur les adresses qui ont déjà recu cette campagne.<br>
        Pour en profiter, il suffit de refaire une plannification sur la même campagne, vous verrez alors vos plannifications précédentes. Si vous faites plusieurs relance pour une même campagne, le volume restant pourra être en dessous de votre réglage car tout les contacts de votre base on recu la campagne.

        <br><br>
        Si ce critère n'est pas important pour vous, il est possible de dupliquer la campagne, et Tor ne se préocupera pas de savoir si les contacts ont déjà recu ou pas cette campagne.
        <br>

      </p>

      <h4> Outils </h4>
      <h5> Quelles outils sont disponibles dans Tor ?</h5>
      <p>Il existe plusieurs outils qui vous permettent de modifier, éditer, extraire ainsi que d'autres actions sur une liste. Il faudra au préalable l'importer. Cela est possible en passant par un formulaire prévu à cet effet.<br/>
      > <a title="fai" href="/outils/listemanager/upload">Importer une liste</a></p>

      <h5> Comment effectuer une action sur une liste ?</h5>
      <p>Vous pouvez effectuer les différentes actions sur une liste en passant par le menu listant les différentes actions possibles.
        > <a title="fai" href="/outils/listemanager">Action sur une liste</a></p>
      </p>

      <h4> Utilisateurs </h4>
      <h5> Comment gérer vos utilisateurs ? </h5>
      <p> Il est possible d'ajouter ou de supprimer des utilisateurs <a title="fai" href="/user">via cette page.</a><br>
      </p>
      <h5> Comment choisir les utilisateurs qui vont recevoir les BAT ? </h5>
      <p> Vous pouvez décider quel utilisateur recevra les BAT des campagnes en éditant ceux ci et en cochant ou non la case "BAT" du formulaire.<br>
      </p>
      <h5> Comment modifier le mot de passe d'un utilisateur ? </h5>
      <p> Si un utilisateur perd son mot de passe, il est possible d'en définir un nouveau en éditant cet utilisateur. Toutefois, si l'utilisateur du seul compte existant perd son mot de passe, il est possible d'en recevoir un nouveau sur l'adresse mail, en cliquant sur "Mot de passe oublié ?" dans la page de connection.<br>
      </p>

      <h4> Support </h4>
      <h5> Que faire en cas de problème ou question ? </h5>
      <p> Vous pouvez nous contacter à l'adresse : dev@lead-factory.net. <br>
          Nous vous répondrons dans les meilleurs délais.
      </p>



  </div>

    <script type="application/javascript">
        var headerElement = 'h4';
        var parentId = 'collapsible-section';

        $(headerElement)
                .parent()
                .wrapInner('<div id="' + parentId + '" class="panel-group"/>');

        $(headerElement).each(function(index) {
            var $header = $(this);
            var $children = $header.nextUntil(headerElement);
            var sectionId = 'section-' + index;
            var isOpen = function() {
                return (index === 0) ? 'in' : null;
            };

            // Wrap all elements in a panel
            $header
                    .add($children)
                    .wrapAll('<div class="panel panel-default"/>');

            // Make section header the panel header
            $header
                    .addClass('panel-title')
                    .wrap('<div class="panel-heading" data-toggle="collapse" href="#' + sectionId + '" data-parent="#' + parentId + '"/>')

            // Wrap header children in a collapsible container
            $children.wrapAll('<div id="' + sectionId + '" class="panel-collapse collapse ' + isOpen() + '"><div class="panel-body"></div></div>');
        });

    </script>
@endsection
