<div>
<input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Nom* (ex. : segment_ouvreurs)</label>
        <div class="col-sm-10">
            {!! Form::text('nom', \Input::old('nom'), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Statut</label>
        <div class="col-sm-10">
            <select name="statut" class='form-control' >
                <option value="0" @if($statut == '0') selected @endif> - </option>
                <option value="inactifs" @if($statut == 'inactifs') selected @endif> Inactifs </option>
                <option value="ouvreurs" @if($statut == 'ouvreurs') selected @endif> Ouvreurs </option>
                <option value="cliqueurs" @if($statut == 'cliqueurs') selected @endif> Cliqueurs </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Sexe</label>
        <div class="col-sm-10">
            <select name="sexe" class='form-control' >
                <option value="-"> - </option>
                <option value="0" @if($sexe == '0') selected @endif> Femme </option>
                <option value="1" @if($sexe == '1') selected @endif> Homme </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Age (min)</label>
        <div class="col-sm-10">
            <select name="agemin" class='form-control'>
                <option value="-"> - </option>
                <?php
                for ($i=18; $i < 100; $i++) {
                  if(intval($agemin) == $i){
                    echo '<option value="'.$i.'" selected> ' . $i . ' </option>';
                  } else {
                  echo '<option value="'.$i.'"> ' . $i . ' </option>';
                  }
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Age (max)</label>
        <div class="col-sm-10">
            <select name="agemax" class='form-control'>
                <option value="-"> - </option>
                <?php
                for ($i=18; $i < 100; $i++) {
                  if(intval($agemax) == $i){
                    echo '<option value="'.$i.'" selected> ' . $i . ' </option>';
                  } else {
                  echo '<option value="'.$i.'"> ' . $i . ' </option>';
                  }
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Période activité</label>
        <div class="col-sm-10">
            De <input type="date" id="from" name="from" value="{{$from}}" class='form-control' /> à <input type="date" id="to" name="to" value="{{$to}}" class='form-control' />
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Thématique</label>
        <div class="col-sm-10">
            {!! Form::select('themes[]', $themes, $selected_themes, array('multiple' => true, 'class' => 'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Géolocalisation</label>
        <div class="col-sm-10">
            {!! Form::select('departements[]', $departements, $selected_departements, array('multiple' => true, 'class' => 'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Campagnes spécifiques</label>
        <div class="col-sm-10">
{{--            {!! Form::select('campagnes[]', $campagnes, $selected_campagnes, array('multiple' => true, 'class' => 'form-control')) !!}--}}
            <select id="campagnes" name="campagnes[]" data-placeholder="Choisir" class="form-control" multiple>
                @foreach($campagnes as $c)
                    <option value="{{ $c->id }}" @if (in_array($c->id, $selected_campagnes)) selected @endif >{{ $c->ref }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Enregistrer</button>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.js"></script>
<script src="https://cdn.jsdelivr.net/chosen/1.1.0/chosen.proto.js"></script>
<script src="http://labo.tristan-jahier.fr/chosen_order/chosen.order.jquery.min.js"></script>

<script type="text/javascript">
var config = {

    allow_single_deselect:false,
    disable_search_threshold:10,
    no_results_text:'Oops, nothing found!',
    width:"60%"
};

var chosen = $('#campagnes').chosen(config);

$(function() {
    $("#from").datepicker({
    dateFormat: "yy-mm-dd"
    })
});

$(function() {
    $("#to").datepicker({
    dateFormat: "yy-mm-dd"
    })
});
</script>
