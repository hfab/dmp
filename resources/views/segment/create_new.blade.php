@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Créer un segment <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      @if(isset($message) && $message != '')
                      <div class="alert alert-dismissable alert-warning">
                      Problème ! {{$message}}
                      </div>
                      @endif

                      {!! Form::model(new \App\Models\Segment, array('route' => array('segment.store'), 'class'=>'form-horizontal') ) !!}
                      @include('segment.partial-fields')
                      {!! Form::close() !!}

                  </div>


      </div>
      </div>
      </div>

          @endsection
