<input type="hidden" name="_token" value="{{ csrf_token() }}" />

<div id="result"></div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Nom</label>
    <div class="col-sm-10">
        {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
        {!! Form::text('email', Input::old('email'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Mot de passe</label>
    <div class="col-sm-10">
        <input class="form-control" name="password" type="password" value="" id="mdp1">
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Confirmer mot de passe</label>
    <div class="col-sm-10">
          <input class="form-control" name="passwordconfirm" type="password" value="" id="mdp2" onkeyup="verif();">
    </div>
</div>

@if( \Auth::User()->user_group->name == 'Super Admin' )
    <div class="form-group">
        <label for="input_html" class="col-sm-2 control-label">Groupe</label>
        <div class="col-sm-10">
            {!! Form::select('user_group_id', $user_group, Input::old('user_group_id')) !!}
        </div>
    </div>
@endif

@if(\Auth::User()->user_group->name == 'Super Admin')
    <div class="form-group">
	<div class="col-sm-6">
            <label for="input_html" class="col-sm-2 control-label">Editeur</label>
            <select class="form-control" name="editorIdSelected">
                <option value="0">Aucun</option>
                @foreach($editors as $editor)
                    @if(!empty($user) && $user->editor_id == $editor->id)
                        <option value="{{$editor->id}}" selected="selected">{{$editor->name}}</option>
                    @else
                        <option value="{{$editor->id}}">{{$editor->name}}</option>
                    @endif
                @endforeach
            </select>
	</div>
    <div class="col-sm-6">
            <label for="input_html" class="col-sm-2 control-label">Annonceur</label>
            <select class="form-control" name="partnerIdSelected">
                <option value="0">Aucun</option>
                @foreach($partners as $partner)
                    @if(!empty($user) && $user->dmp_partnersite_id == $partner->id)
                        <option value="{{$partner->id}}" selected="selected">{{$partner->name}}</option>
                    @else
                        <option value="{{$partner->id}}">{{$partner->name}}</option>
                    @endif
                @endforeach
            </select>
    </div>
    </div>
@endif

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">BAT</label>
    <div class="col-sm-10">
        {!! Form::checkbox('is_bat', Input::old('is_bat'), true, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" id="envoi" class="btn blue-steel">Enregistrer</button>
    </div>
</div>

<script>

function verif()
{
var val1   = document.getElementById("mdp1").value,
    val2   = document.getElementById("mdp2").value,
    result = document.getElementById("result");


if(val1!=val2){

  $("#envoi").attr("disabled", true);
  result.innerHTML='<div class="alert alert-dismissable alert-danger">Attention ! Les deux mots de passe ne sont pas identiques</div>';

  } else {

    $("#envoi").attr("disabled", false);
    result.innerHTML='<div class="alert alert-dismissable alert-success">Les deux mots de passe sont identiques</div>';
  }

}





</script>
