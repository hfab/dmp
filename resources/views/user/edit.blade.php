@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Mettre à jour un utilisateur
                </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::model($user, array('method'=> 'PATCH', 'route' => array('user.update', $user->id), 'class'=>'form-horizontal')) !!}
            @include('user.form')
            {!! Form::close() !!}

        </div>
    </div>

@endsection