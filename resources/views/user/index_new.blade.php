@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Utilisateurs</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">

                      <div class="actions">
                      @if( \Auth::User()->user_group->name == 'Super Admin')
                          <div class="btn-group btn-group-devided">
                              <a title="Ajouter un utilisateur" class="btn btn-success" href="/user/create"><i class="fa fa-plus" aria-hidden="true"></i></a>
                          </div>
                      @endif
                      </div>

                    </div>

                    <br>
                    <div class="row">
                      <div style="overflow-x:auto;">
                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th>Nom</th>
                              <th>Email</th>
                              <th>Modifié le</th>
                              <th colspan="2">Actions</th>
                          </tr>
                      @foreach($users as $u)
                          <tr>
                              <td> {{$u->name}} </td>
                              <td> {{$u->email}} </td>
                              <td> {{$u->updated_at }} </td>
                              <td> <a title="Modifier" class="btn btn-primary" href="/user/{{$u->id}}/edit" !!}><i class="fa fa-pencil" aria-hidden="true"></i></a> </td>
                              <td>
                                  {!! Form::open(array('route' => array('user.destroy', $u->id), 'method' => 'delete')) !!}
                                  <button title="Supprimer" class="btn default red" type="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                                  {!! Form::close() !!}
                              </td>
                          </tr>
                      @endforeach
                      </table>

                    </div>

                  </div>


      </div>
      </div>
      </div>

          @endsection
