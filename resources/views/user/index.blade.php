@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
            Liste des Utilisateurs
            </span>
        </div>
        <div class="actions">
        @if( \Auth::User()->user_group->name == 'Super Admin')
            <div class="btn-group btn-group-devided">
                <a title="Ajouter un utilisateur" class="btn btn-success" href="/user/create"><i class="fa fa-plus" aria-hidden="true"></i></a>
            </div>
        @endif
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-hover table-bordered">
            <tr>
                <th>Nom</th>
                <th>Email</th>
                <th>Modifié le</th>
                <th colspan="2">Actions</th>
            </tr>
        @foreach($users as $u)
            <tr>
                <td> {{$u->name}} </td>
                <td> {{$u->email}} </td>
                <td> {{$u->updated_at }} </td>
                <td> <a title="Modifier" class="btn btn-primary" href="/user/{{$u->id}}/edit" !!}><i class="fa fa-pencil" aria-hidden="true"></i></a> </td>
                <td>
                    {!! Form::open(array('route' => array('user.destroy', $u->id), 'method' => 'delete')) !!}
                    <button title="Supprimer" class="btn default red" type="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </table>
    </div>
</div>
@endsection