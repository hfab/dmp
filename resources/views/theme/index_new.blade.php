@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Thème</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="/theme/create" class="btn btn-warning">Ajouter un thème</a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th>Identifiant</th>
                              <th>Nom</th>
                              <th>Actions</th>
                          </tr>

                          @foreach($themes as $t)
                              <tr>
                                  <td> {{$t->id}} </td>
                                  <td> {{$t->nom}} </td>

                                  @if($t->etat_actif == '1')
                                  <td> <a class="btn btn-primary" href="/theme/{{$t->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Désactiver" class="btn btn-success" href="/theme/{{$t->id}}/inactif"><i class="fa fa-toggle-off" aria-hidden="true"></i>
                                      </a> </td>
                                  @else
                                  <td> <a class="btn btn-primary" href="/theme/{{$t->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Activer" class="btn btn-danger" href="/theme/{{$t->id}}/activer"><i class="fa fa-toggle-on" aria-hidden="true"></i>
                                      </a></td>
                                  @endif

                              </tr>
                          @endforeach
                      </table>


                  </div>


      </div>
      </div>
      </div>

          @endsection
