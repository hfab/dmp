@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Editer un thème </h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      {!! Form::model($theme, array('route' => array('theme.update', $theme->id), 'method'=> 'put')) !!}
                      @include('theme.form')
                      {!! Form::close() !!}


                  </div>


      </div>
      </div>
      </div>

          @endsection
