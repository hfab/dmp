@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Liste des thèmes
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a title="Ajouter Thème" class="btn btn-success" href="/theme/create"><i class="fa fa-plus" aria-hidden="true"></i> </a>
                </div>
            </div>
        </div>
        <div class="portlet-body">


            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th>Identifiant</th>
                    <th>Nom</th>
                    <th>Actions</th>
                </tr>

                @foreach($themes as $t)
                    <tr>
                        <td> {{$t->id}} </td>
                        <td> {{$t->nom}} </td>

                        @if($t->etat_actif == '1')
                        <td> <a class="btn btn-primary" href="/theme/{{$t->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Désactiver" class="btn btn-success" href="/theme/{{$t->id}}/inactif"><i class="fa fa-toggle-off" aria-hidden="true"></i>
                            </a> </td>
                        @else
                        <td> <a class="btn btn-primary" href="/theme/{{$t->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Activer" class="btn btn-danger" href="/theme/{{$t->id}}/activer"><i class="fa fa-toggle-on" aria-hidden="true"></i>
                            </a></td>
                        @endif

                    </tr>
                @endforeach
            </table>


        </div>
    </div>


@endsection
