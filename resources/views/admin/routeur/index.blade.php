@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
            Routeurs
            </span>
        </div>
        <div class="actions">

        </div>
    </div>
    <div class="portlet-body">


        <table class="table table-striped table-hover table-bordered">
            <tr>
                <th>Identifiant</th>
                <th>Nom</th>
                <th>Actions</th>
            </tr>

            @foreach($routeurs as $r)
                <tr>
                    <td> {{$r->id}} </td>
                    <td> {{$r->nom}} </td>
                    <td>
                        <?php
                        // join en controller sinon
                        $status = \DB::table('settings')
                        ->where('context', 'routeur')
                        ->where('info', $r->id)
                        ->first();
                        // var_dump($status);
                        ?>

                        @if(isset($status->value))
                            @if($status->value == 1)
                            <a title="Désactiver" class="btn btn-success" href="/admin/routeur/{{$r->id}}/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                            @else
                            <a title="Activer" class="btn btn-danger" href="/admin/routeur/{{$r->id}}/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                            @endif

                            @if($r->nom == 'Mindbaz')
                                <?php
                                $status_mindbaz = \DB::table('settings')
                                ->where('parameter', 'mindbaz_mode')
                                ->first();
                                ?>
                                @if($status_mindbaz->value == 'list')
                                <a title="Activer mode liste" class="btn btn-success" href="/admin/routeur/{{$r->id}}/disable_segment"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                @else
                                <a title="Désactiver mode liste" class="btn btn-danger" href="/admin/routeur/{{$r->id}}/enable_segment"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                @endif
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
