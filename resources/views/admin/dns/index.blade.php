@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Configuration DNS
                </span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
          {!! Form::open(array('url' => '/admin/dns','files' => true, 'method' => 'post')) !!}

          <div class="form-group">
            <label for="ndd">Nom de domaine</label>
            <input type="text" class="form-control" id="ndd" name="ndd" placeholder="lesoffresinmanquables.com">
          </div>

          <div class="form-group">
            <label for="sndd">Sous domaine [sender]</label>
            <input type="text" class="form-control" id="sndd" name="sndd" placeholder="info">
          </div>
          <div class="form-group">
            <label for="dnsovh">Serveur DNS OVH [15|16]</label>
            <input type="text" class="form-control" id="dnsovh" name="dnsovh" placeholder="15 pour dns16|ns16.ovh.net.">
          </div>
          <div class="form-group">
            <label for="ipa">Adresse IP [A record]</label>
            <input type="text" class="form-control" id="ipa" name="ipa" placeholder="213.186.33.5">
          </div>

          <div class="form-group">
            <label for="plageip">Adresse IP [Plage]</label>
            <input type="text" class="form-control" id="plageip" name="plageip" placeholder="212.18.245.">
          </div>

          <div class="form-group">
            <label for="ipstart">Plage IP [bloc start]</label>
            <input type="text" class="form-control" id="ipstart" name="ipstart" placeholder="1">
          </div>

          <div class="form-group">
            <label for="ipend">Plage IP [bloc end]</label>
            <input type="text" class="form-control" id="ipend" name="ipend" placeholder="12">
          </div>

          <div class="form-group">
            <label for="spf">SPF [TXT record|0]</label>
            <input type="text" class="form-control" id="spf" name="spf" placeholder="212.18.245.0">
          </div>

          <div class="form-group">
            <label for="dkim">DKIM [TXT record]</label>
            <input type="text" class="form-control" id="dkim" name="dkim" placeholder="public key DKIM">
          </div>


          <hr>

            <button type="submit" class="btn btn-success">Valider</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
