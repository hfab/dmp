@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Réglages avancés
                </span>
            </div>
            <div class="actions">
              <a href="/outils/bl/add"> <button type="button" class="btn btn-success">Ajouter un nom de domaine</button> </a>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th>DATA</th>
                    <th>Status</th>
                    <th>Informations</th>
                    <th>Date début bl</th>
                </tr>

                @foreach($bl_data as $bl)

                <tr>
                    <td> {{$bl->ip}} {{$bl->domain_reverse}} {{$bl->domain}} </td>
                    <td> {{ $bl->is_bl }} </td>
                    <td>
                      <?php
                          if($bl->is_bl == 1){
                            echo '<center><a href="/outils/bl/'. $bl->id .'"> <button type="button" class="btn btn-danger">Voir</button> </a></center>';

                          } else {

                            echo '';
                          }
                      ?>

                    </td>
                    <td>
                      <?php
                        $blrstart = \DB::table('blacklist_lookup_records')->where('id_blacklist_lookup',$bl->id)->first();
                        if(isset($blrstart)){
                          echo '<center>' . $blrstart->created_at . '</center>';


                        }

                      ?>
                    </td>
                </tr>

                @endforeach

            </table>
        </div>
    </div>
@endsection
