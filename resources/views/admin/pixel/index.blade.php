@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Pixels de tracking
                </span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th>Nom de domaine</th>
                    <th>Base</th>
                    <th>Routeur</th>
                    <th> Actions </th>
                </tr>

                @foreach($pixels as $pixel)
                <tr>
                    <td> {{$pixel->domain}}  </td>
                    <td> {{$bases[$pixel->base_id]}}  </td>
                    <td> {{$routeurs[$pixel->routeur_id]}}  </td>
                    <td> <a href="/admin/pixel/{{$pixel->id}}"> <button type="button" class="btn btn-primary">Modifier</button> </a> </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
