@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Pixels de tracking
                </span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div style="width: 300px;">
                <form action="/admin/pixel/{{$pixel->id}}" method="post">
                    {{csrf_field()}}
                    <input class="form-control" name="domain" type="text" value="{{$pixel->domain}}" required />

                    <select class="form-control" name="base_id">
                        @foreach($bases as $bid => $bname)
                            <option value="{{$bid}}" @if($bid==$pixel->base_id) selected @endif>{{$bname}}</option>
                        @endforeach
                    </select>

                    <select class="form-control" name="routeur_id">
                        @foreach($routeurs as $rid => $rname)
                        <option value="{{$rid}}" @if($rid==$pixel->routeur_id) selected @endif>{{$rname}}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-default">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>
@endsection
