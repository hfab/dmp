@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
            Ajouter une branche
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
            </div>
        </div>
    </div>
    <div class="portlet-body">
        {!! Form::model(new \App\Models\Branch, array('route' => array('admin.branch.store'), 'method'=> 'post')) !!}
        @include('admin.branch.form')
        {!! Form::close() !!}
    </div>
</div>
@endsection