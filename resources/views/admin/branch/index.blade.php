@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Liste des branches
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a title="Ajouter Branche" class="btn btn-success" href="/admin/branch/create"><i class="fa fa-plus" aria-hidden="true"></i> </a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th>Identifiant</th>
                    <th>Nom</th>
                    <th>Actions</th>
                </tr>
                @foreach($branches as $b)
                    <tr>
                        <td> {{$b->id}} </td>
                        <td> {{$b->name}} </td>

                        @if($b->is_active == '1')
                        <td> <a class="btn btn-primary" href="/admin/branch/{{$b->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Désactiver" class="btn btn-success" href="/admin/branch/{{$b->id}}/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i>
                            </a> </td>
                        @else
                        <td> <a class="btn btn-primary" href="/admin/branch/{{$b->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a title="Activer" class="btn btn-danger" href="/admin/branch/{{$b->id}}/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i>
                            </a></td>
                        @endif

                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
