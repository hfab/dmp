@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Admin <small></small></h2>
                    <!--
					
					<ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
					-->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th>Type</th>
                              <th>Actions</th>
                          </tr>
                          <tr>
                              <td> Routeurs </td>
                              <td> <a href="/admin/routeur/"> <button type="button" class="btn btn-primary">Modifier</button> </a> </td>
                          </tr>
                          <tr>
                              <td> Envois automatiques </td>
                              <td>
                              @if(isset($autostatus->value))
                                  @if($autostatus->value == 1)
                                      <a title="Désactiver" class="btn btn-success" href="/admin/auto/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                  @else
                                      <a title="Activer" class="btn btn-danger" href="/admin/auto/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                  @endif
                              @endif
                              </td>
                          </tr>

                          <tr>
                              <td> Pression marketing </td>
                              <td>
                                  @if(isset($cleanrelaunchstatus->value))
                                      @if($cleanrelaunchstatus->value == 1)
                                          <a title="Désactiver" class="btn btn-success" href="/admin/cleanrelaunch/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                      @else
                                          <a title="Activer" class="btn btn-danger" href="/admin/cleanrelaunch/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                      @endif
                                  @endif
                              </td>
                          </tr>

                          <tr>
                              <td> Branches senders </td>
                              <td> <a href="/admin/branch/"> <button type="button" class="btn btn-primary">Modifier</button> </a> </td>
                          </tr>
                          <tr>
                              <td> Pixels de tracking </td>
                              <td> <a href="/admin/pixel/"> <button type="button" class="btn btn-primary">Modifier</button> </a> </td>
                          </tr>
                      </table>

                    </div>


      </div>
      </div>
      </div>

          @endsection
