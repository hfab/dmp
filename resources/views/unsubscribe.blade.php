@extends('template')

@section('content')

     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">

               <div class="x_title">
                    <h2>Désinscription manuelle</h2>
                    <ul class="nav navbar-right panel_toolbox"></ul>
                    <div class="clearfix"></div>
               </div>
               <div class="x_content">
                    <br>
                    <div class="row">
                         <p>{{$message}}</p>

                         <form action="/unsubscribe" method="get">
                             <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                             <label class="control-label"> Email
                                 <input type="email" name="email" value="" placeholder="a@desinscrire.fr" />
                                 <button class="btn btn-sm btn-danger" type="submit" name="unsubscribe" > Désinscrire </button>
                             </label>
                         </form>
                         <br>
                         <form action="/unsubscribeById" method="post">
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                              <label class="control-label"> Id
                                   <input type="number" name="Id" placeholder="Entrer un Id"/>
                                   <button class="btn btn-sm btn-danger" type="submit" name="unsubscribeById"> Désinscrire </button>
                              </label>
                         </form>
                    </div>
               </div>
          </div>
     </div>
@endsection
