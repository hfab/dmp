@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Extraire une base vers MD5
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                   
                    <a href="/base/" class="btn red">Retour</a>
                    

                </div>
            </div>
        </div>
        <div class="portlet-body">


            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>Identifiant</th>
                    <th>Code</th>
                    <th>Nom</th>
                    <th>Destinataires</th>
                    <th colspan="2">Action</th>
                </tr>

                @foreach($bases as $b)
                    <tr>
                        <td>{{ $b->id }}</td>
                        <td>{{ $b->code }}</td>
                        <td>{{ $b->nom }}</td>
                        <td>{{ $b->ViewCountDestinataires->num }}</td>
                        <td>
                            
                            <a class="btn green" href="/base/extract/{{$b->id}}/md5">Extraire vers MD5</a>
                            
                        </td>
                        
                    </tr>
                @endforeach
            </table>



        </div>
    </div>

@endsection

