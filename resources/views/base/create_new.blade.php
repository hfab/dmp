@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Ajouter une nouvelle base <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      @if(isset($error))
                          <div class="alert alert-dismissable alert-danger">
                              <strong> {{$error}} </strong>
                          </div>
                      @endif
                      {!! Form::model(new \App\Models\Base, array('route' => array('base.store'), 'method'=> 'post', 'class'=>'form-horizontal')) !!}
                      @include('base.form')
                      {!! Form::close() !!}


                    </div>


      </div>
      </div>
      </div>

          @endsection
