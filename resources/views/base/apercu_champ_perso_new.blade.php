@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Choix champs perso pour import <small>Apercu</small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      {!! Form::open(array('url' => 'base/' . $base->id . '/add_destinataires_perso/import','files' => true, 'method' => 'post')) !!}

                      <h3> Ordre des champs perso : </h3>

                      @foreach($listechamp as $l)

                      <!-- <span title="perso" class="btn btn-primary"> {{ $l }} </span> -->
                      <input type="hidden" name="champs[]" value="{{$l}}">
                      @endforeach
                      <br>
                      <table class="table table-bordered table-striped table-hover">

                        <tr>

                            @foreach($listechamp as $l)
                            <th>{{ $l }}</th>

                            @endforeach

                        </tr>

                        @foreach($content as $c)
                        <tr>

                          <?php
                          $cc = explode(';',$c);
                          ?>

                          @foreach($cc as $item)
                          <td> {{ $item }} </td>
                          @endforeach

                        </tr>

                        @endforeach

                      </table>

                      {!! Form::hidden('lefichier', $name) !!}
                    <button type="submit" class="btn btn-success">Importer dans la base</button>

                    {!! Form::close() !!}

                  <br /> <br />


              <script type="text/javascript">
                var config = {
                  '.chosen-select'           : {},
                  '.chosen-select-deselect'  : {allow_single_deselect:true},
                  '.chosen-select-no-single' : {disable_search_threshold:10},
                  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                  '.chosen-select-width'     : {width:"95%"}
                }

               $(document).ready(function() {
                // Chosenify every multiple select DOM elements with class 'chosen'
                $('#chzn').chosen();

                // Get a reference to the DOM element
                var MY_SELECT = $('select[multiple].chosen').get(0);

                $('#zclic , body').click(function() {
                    var selection = ChosenOrder.getSelectionOrder(MY_SELECT);

                    $('#order-list').empty();
                    $('#fabien').empty();
                    $(selection).each(function(i) {
                        $('#order-list').append("<li>" + selection[i] + "</li>");
                          $('#fabien').append('<input type="hidden" name="result[]" value="'  + selection[i] + '">');

                    });
                });

                // $('#set-order').click(function() {
                //     ChosenOrder.setSelectionOrder(MY_SELECT, $('#input-order').val().split(','), true);
                // });
            });


              </script>

        </div>


      </div>
      </div>
      </div>

@endsection
