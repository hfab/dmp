@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Bases</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="/base/create"><button class="btn btn-success" href="">Ajouter</button></a>

                      <a href="/unsubscribe"><button class="btn btn-primary" href="">Désinscription</button></a>
                      <a href="/segment"><button class="btn btn-primary" href="">Segments</button></a>
                      <a href="/base/add"><button class="btn btn-primary" href="">Imports</button></a>
                      <a href="/downl"><button class="btn btn-primary" href="">Exports</button></a>

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      {!! Form::open(array('url' => 'base/searchmailall')) !!}
                      {!! Form::label('searchall', 'Recherche all') !!}
                      {!! Form::text('searchall') !!}
                      {!! Form::submit('Rechercher',['class' => 'btn btn-sm btn-primary']) !!}
                      {!! Form::close() !!}


                    </div>
                  </div>

                  <div class="x_content">
                    <br>
                    <div class="row">
                      <div style="overflow-x:auto;">
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th>Identifiant</th>
                                <th>Code</th>
                                <th>Nom</th>
                                <th>Destinataires</th>
                                <th colspan="5">Actions</th>
                            </tr>

                            @foreach($bases as $b)
                                <tr>
                                    <td>{{ $b->id }}</td>
                                    <td>{{ $b->code }}</td>
                                    <td>{{ $b->nom }}</td>
                                    <td>{{ big_number($b->ViewCountDestinataires->num) }}</td>
                                    <td>
                                        <a title="Modifier" class="btn btn-primary btn-xs" href="/base/{{$b->id}}/edit" !!}><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                        <span style="position: relative;">
                                            <button title="Ajouter destinataires" class="btn btn-success btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a title="Ajouter contact" href="/base/{{$b->id}}/add_destinataires" !!} >Ajouter liste @</a></li>
                                                <li><a title="Ajouter contact perso" href="/base/{{$b->id}}/add_destinataires_perso" >Ajouter liste @ + champs</a></li>

                                            </ul>
                                        </span>

                                        <a title="Statistiques" class="btn btn-warning btn-xs" href="/base/{{$b->id}}/stats" !!}><i class="fa fa-pie-chart" aria-hidden="true"></i></a>
                                        <span style="position: relative;">
                                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Plus
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a title="Extraire contact" href="/base/{{$b->id}}/extractcontacts" !!} >Extraire @</a></li>
                                                <li><a title="Extraire MD5" href="/base/extract/{{$b->id}}/extractmd5" >Extraire MD5</a></li>
                                                <li><a title="Comparer" href="/base/{{$b->id}}/compare_upload/" >Comparer @</a></li>
                                                <li><a title="Comparer" href="/base/{{$b->id}}/destinataire/" >Consulter @</a></li>
                                            </ul>
                                        </span>
                                    </td>
                                    <td>
                                        {!! Form::open(array('route' => array('base.destroy', $b->id), 'method' => 'delete')) !!}
                                        <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                      </div>
                    </div>

                    </div>
                  </div>

      </div>
      </div>

      <script type="text/javascript">
          function filterByBase() {
              document.getElementById("baseTri").submit();
          }
      </script>

      @endsection
