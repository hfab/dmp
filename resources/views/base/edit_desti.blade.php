@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Modifier un destinataire de la base : {{$base->nom}}
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/base/{{$base->id}}/destinataire" class="btn btn-success">Retour</a>
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

          {!! Form::open(array('url' => 'base/destinataire/store','files' => true, 'method' => 'post')) !!}

          <div class="form-group">
              <label for="input_name" class="col-sm-2 control-label">Adresse Mail</label>
              <div class="col-sm-10">
                  {!! Form::text('mail', $desti->mail, ['class' => 'form-control']) !!}
              </div>
          </div>
          <br /><br />
          <div class="form-group">
              <label for="input_name" class="col-sm-2 control-label">Nom</label>
              <div class="col-sm-10">
                  {!! Form::text('nom', $desti->nom, ['class' => 'form-control']) !!}
              </div>
          </div>
          <br /><br />
          <div class="form-group">
              <label for="input_name" class="col-sm-2 control-label">Prenom</label>
              <div class="col-sm-10">
                  {!! Form::text('prenom', $desti->prenom, ['class' => 'form-control']) !!}
              </div>
          </div>
          <br /><br />
          <div class="form-group">
              <label for="input_name" class="col-sm-2 control-label">Date Naissance</label>
              <div class="col-sm-10">
                  {!! Form::text('datenaissance', $desti->datenaissance, ['class' => 'form-control']) !!}
              </div>
          </div>
          <br /><br />
          <div class="form-group">
              <label for="input_name" class="col-sm-2 control-label">Ville</label>
              <div class="col-sm-10">
                  {!! Form::text('ville', $desti->ville, ['class' => 'form-control']) !!}
              </div>
          </div>
          <br /><br />
          <div class="form-group">
              <label for="input_name" class="col-sm-2 control-label">Département</label>
              <div class="col-sm-10">
                  {!! Form::text('departement', $desti->departement, ['class' => 'form-control']) !!}
              </div>
          </div>
          <br /><br />
          <?php
          $sexe = '';
            if($desti->civilite == 0){
              $sexe = 0;
            }

            if($desti->civilite == 1){
              $sexe = 1;
            }
           ?>
          <div class="form-group">
              <label for="input_html" class="col-sm-2 control-label">Sexe</label>
              <div class="col-sm-10">
                  <select name="sexe" class='form-control' >
                      <option value="-"> - </option>
                      <option value="0" @if($sexe == '0') selected @endif> Femme </option>
                      <option value="1" @if($sexe == '1') selected @endif> Homme </option>
                  </select>
              </div>
          </div>
          {!! Form::hidden('desti_id', $desti->id) !!}
          {!! Form::hidden('base_id', $base->id) !!}
          <button type="submit" class="btn btn-success">Valider</button>
          {!! Form::close() !!}

        </div>
    </div>

@endsection
