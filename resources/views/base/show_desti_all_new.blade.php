@extends('template')

@section('content')
     <div class="container">
          <div class="row title">
               <div class="col-xs-12">
                    <i class="fa fa-2x fa-cogs text-success"></i>
                    <span class="lead text-success">
                         Destinataires toutes bases confondues
                    </span>
                    <br />
               </div>
          </div>
          <div class="col-xs-12">
               <h4>Rechercher :</h4>
               {!! Form::open(array('url' => 'base/searchmailall','files' => true, 'method' => 'post')) !!}
               {!! Form::label('recherche', ' ') !!}
               {!! Form::text('recherche', $recherche) !!}
               <button type="submit" class="btn btn-sm btn-success">Valider</button>
               @if($desti->count()>0)
               <input value="Exporter" name="export" type="submit" class="btn btn-sm btn-info">
               <a href="/base/searchmailall/desabo/{{$recherche}}" class="btn btn-sm btn-danger">Désabonner</a>
               @endif
               {!! Form::close() !!}
               <br />
               <br />
          </div>
          <div class="col-xs-12 table-responsive">
               <table class="table table-bordered table-striped table-hover table-condensed">
                    <tr>
                         <th>ID</th>
                         <th>Mail</th>
                         <th>Nom</th>
                         <th>Prénom</th>
                         <th>Date naissance</th>
                         <th>Ville</th>
                         <th>Département</th>
                         <th>Sexe</th>
                         <th>Statut</th>
                         <th>Base_id</th>
                    </tr>

                    @foreach($desti as $d)
                         <tr>
                              <td>{{ $d->id }}</td>
                              <td>{{ $d->mail }}</td>
                              <td>{{ $d->nom }}</td>
                              <td>{{ $d->prenom }}</td>
                              <td>{{ $d->datenaissance }}</td>
                              <td>{{ $d->ville }}</td>
                              <td>{{ $d->departement }}</td>
                              <td>
                                   <?php
                                   if($d->civilite === 0){
                                        echo 'Femme';
                                   }
                                   if($d->civilite == 1){
                                        echo 'Homme';
                                   }
                                   if($d->civilite === null || $d->civilite == 3){
                                        echo 'N/A';
                                   }
                                   ?>
                              </td>
                              <td>
                                   <?php
                                   if($d->statut > 0){
                                        echo '<span style="color:#c0392b"> Désabonné </span>';
                                   } else {
                                        echo '<span style="color:#27ae60"> Abonné </span>';
                                   }
                                   ?>
                              </td>
                              <td>
                                   {{ $d->base_id }}
                              </td>
                         </tr>
                    @endforeach
               </table>
               <div class="col-xs-12 text-center">
                    {!! $desti->appends(['recherche' => $recherche])->render() !!}
               </div>
          </div>
     </div>
@endsection
