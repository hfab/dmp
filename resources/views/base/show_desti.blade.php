@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Destinataires de la base : {{$base->nom}}
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <span>
            <a href="/base/{{$base->id}}/destinataire/abo/1" class="btn btn-success">Abonnés</a> <a href="/base/{{$base->id}}/destinataire/abo/0" class="btn btn-danger"> Désabonné </a>
          </span>

          <br />
          <br />

          <?php
            foreach ($lesbases as $b) {
              echo '<a href="/base/'. $b->id .'/destinataire/" class="btn btn-info btn-sm">'. $b->nom .'</a>  ';
            }

          ?>

         <br />
         <br />

          <?php
            echo '<a href="/base/'. $base->id .'/destinataire/" class="btn btn-primary btn-sm">All</a>  ';
            foreach ($fai as $f) {
              echo '<a href="/base/'. $base->id .'/destinataire/fai/' . $f->id . '" class="btn btn-primary btn-sm">'. $f->nom .'</a>  ';
            }

            ?>

            <br />
            <br />

            <span>Rechercher : </span>
           {!! Form::open(array('url' => 'base/' . $base->id . '/destinataire/search/','files' => true, 'method' => 'post')) !!}
           {!! Form::label('recherche', ' ') !!}
           {!! Form::text('recherche', $recherche) !!}
           <button type="submit" class="btn btn-success btn-xs">Valider</button>
           {!! Form::hidden('base_id', $base->id) !!}

           {!! Form::close() !!}
           <br />
           <br />

          <table class="table table-bordered table-striped table-hover">
              <tr>
                  <th>ID</th>
                  <th>Mail</th>
                  <th>Nom</th>
                  <th>Prénom</th>
                  <th>Date naissance</th>
                  <th>Ville</th>
                  <th>Département</th>
                  <th>Sexe</th>
                  <th>Statut</th>

                  <th colspan="5">Actions</th>
              </tr>

              @foreach($desti as $d)
                  <tr>
                      <td>{{ $d->id }}</td>
                      <td>{{ $d->mail }}</td>
                      <td>{{ $d->nom }}</td>
                      <td>{{ $d->prenom }}</td>
                      <td>{{ $d->datenaissance }}</td>
                      <td>{{ $d->ville }}</td>
                      <td>{{ $d->departement }}</td>
                      <td>
                       <?php
                        if($d->civilite === 0){
                          echo 'Femme';
                        }
                        if($d->civilite == 1){
                          echo 'Homme';
                        }
                        if($d->civilite === null || $d->civilite == 3){
                          echo 'N/A';
                        }
                       ?>

                      </td>
                      <td>
                        <?php
                          if($d->statut > 0){
                            echo '<span style="color:#c0392b"> Désabonné </span>';
                          } else {
                            echo '<span style="color:#27ae60"> Abonné </span>';
                          }
                        ?>
                      </td>
                      <td>
                        <a title="Modifier" class="btn btn-warning btn-sm" href="/base/{{$base->id}}/destinataire/{{$d->id}}/edit">Modifier</a>
                        <a title="Supprimer" class="btn btn-danger btn-sm" href="/base/{{$base->id}}/{{$d->id}}/delete">Supprimer</a>
                      </td>
                  </tr>
              @endforeach
          </table>
          {!! $desti->appends(['recherche' => $recherche])->render() !!}
        </div>
    </div>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<style>
.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
</style>

<script>
    $(document).ready(function(){


// script auto

        var jqxhr = {abort: function () {}};


        $('#recherche').on( "keypress", function() {

            jqxhr.abort();

            var recherche = $('#recherche').val();

            if(recherche.length < 3) {
                return;
            }

             $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            jqxhr = $.ajax({
                url: '/base/{{$base->id}}/destinataire/ajax',
                type: 'POST',
                data: {
                    base_id: {{$base->id}},
                    recherche: recherche
                },
                dataType: 'JSON',
                success: function (data) {
                    $('#recherche').autocomplete({minLength: 4, source: data});
                },
                error: function (e) {
                    console.log(e.responseText);
                }
            });
        });
    });
</script>

@endsection
