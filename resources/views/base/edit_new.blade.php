@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Mettre à jour une base <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      @if(isset($error))
                          <div class="alert alert-dismissable alert-danger">
                              <strong> {{$error}} </strong>
                          </div>
                      @endif
                      {!! Form::model($base, array('method'=> 'PATCH', 'route' => array('base.update', $base->id), 'class'=>'form-horizontal')) !!}
                      @include('base.form')
                      {!! Form::close() !!}

                  </div>


      </div>
      </div>
      </div>

          @endsection
