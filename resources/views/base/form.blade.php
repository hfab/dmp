<input type="hidden" name="_token" value="{{ csrf_token() }}" />

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Nom</label>
    <div class="col-sm-10">
        {!! Form::text('nom', Input::old('nom'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Code</label>
    <div class="col-sm-10">
        {!! Form::text('code', Input::old('code'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="mentions_header" class="col-sm-2 control-label">Lien vers les mentions cookies (Header)</label>
    <div class="col-sm-10">
        {!! Form::text('mentions_header', Input::old('mentions_header'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Mentions légales (Footer)</label>
    <div class="col-sm-10">
        {!! Form::textarea('mentions_legales', Input::old('mentions_legales'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="header_html" class="col-sm-2 control-label">Header (HTML)</label>
    <div class="col-sm-10">
        {!! Form::textarea('header_html', Input::old('header_html'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="footer_html" class="col-sm-2 control-label">Footer (HTML)</label>
    <div class="col-sm-10">
        {!! Form::textarea('footer_html', Input::old('footer_html'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="mentions_header" class="col-sm-2 control-label">Nom expediteur</label>
    <div class="col-sm-10">
        {!! Form::text('nom_reply', Input::old('nom_reply'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="mentions_header" class="col-sm-2 control-label">Email expediteur</label>
    <div class="col-sm-10">
        {!! Form::text('email_reply', Input::old('email_reply'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="withtags" class="col-sm-2 control-label">Active</label>
    <div class="col-sm-10">
        {!! Form::checkbox('is_active',Input::old('is_active'),1, array('id' => 'is_active', 'class' => 'form-control')) !!}
    </div>
</div>

@if( getenv('WIZWEB_API') == 'true' && (\Auth::User()->user_group->name == 'Admin' or \Auth::User()->user_group->name == 'Super Admin') )
<div class="form-group">
    <label for="trigram" class="col-sm-2 control-label">Trigramme Wizweb</label>
    <div class="col-sm-10">
        {!! Form::text('trigram', $trigram, array('id' => 'trigram', 'class' => 'form-control')) !!}
    </div>
</div>
@endif

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Enregistrer</button>
    </div>
</div>
