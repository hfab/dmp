@extends('common.layout')

@section('content')
    <div class="portlet light">

        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Résultat du check sur l'adresse email [{{$checkemail}}]
                </span>
                <span><a href='/base'><button type="submit" class="btn btn-warning"> Retour </button></a>
                </span>
            </div>
        </div>

        <div class="portlet-body">
          @if($message)

          @endif
        </div>


        <div class="portlet-body">
            <table id="tab_planning" class="table table-striped table-bordered">
                <tr>
                    <th>Email</th>
                    <th>Base</th>
                    <th>Etat</th>
                    <th>Informations Supp.</th>
                </tr>

                @foreach($tabcheck as $c)

                <tr>
                  <td>{{$c['email']}}</td>
                  <td>{{$c['base_nom']}}</td>
                  <td>{{$c['etat_fr']}}</td>
                  <td>
                  <?php
                  if($c['statut'] > 0){
                    echo 'Désabonné le : ' . $c['date_desabo'] . ' via le fichier : ' . $c['from'];
                  }
                  ?>


                  </td>
                </tr>

                @endforeach




            </table>


        </div>
    </div>
@endsection
