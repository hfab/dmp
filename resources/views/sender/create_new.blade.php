@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Créer sender <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <?php
                          $pattern = '/error/';
                          if(preg_match($pattern, $_SERVER['REQUEST_URI'])){
                              echo '<div class="alert alert-dismissable alert-danger">
                              <strong> Il y a une erreur dans la modification du routeur Maildrop </strong>
                              </div>';
                          }
                      ?>

                      {!! Form::model(new \App\Models\Sender, array('route' => array('sender.store'), 'class'=>'form-horizontal')) !!}

                      @include('sender.partial-fields')

                      <button class="col-md-offset-2 btn btn-primary">Sauvegarder</button>

                      {!! Form::close() !!}

                  </div>


      </div>
      </div>
      </div>

          @endsection
