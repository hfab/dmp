@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Noms de domaines (NDD) à éditer
                </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
            <div id="form_info">
                @if(isset($message) and $message === true)
                    <div class="alert alert-dismissable alert-info">
                        Votre modification a bien été prise en compte.
                    </div>
                @elseif(isset($message) and $message === false)
                    <div id="error_msg" class="alert alert-dismissable alert-danger">
                        Erreur : {{$message}}
                    </div>
                @endif
            </div>
            {!! Form::open(array('action' => 'SenderController@multi_edit_ovh', 'method' => 'post')) !!}
              <table class="table table-hover table-advance table-striped">
                <tr>
                    <th width="2%"> <input id="toutSelectionner" type="checkbox" /> </th>
                    <th>NDD</th>
                    <th>Routeur</th>
                    <th>Compte</th>
                </tr>

                @foreach($domains as $ndd => $sender)
                    <tr>
                        <td> <input type="checkbox" class="chkgroup" name="domains[]" value="{{$ndd}}" @if(in_array($ndd, $selected_domains)) checked="checked" @endif /> </td>
                        <td>{{ $ndd }}</td>

                        <td>{{ $sender->routeur->nom }}</td>
                        <td>{{ $sender->nom }}</td>
                    </tr>
                @endforeach
            </table>

            <select id='action' name="action" class="form-control input-medium">
                <option value="0"> - Choisir une action - </option>
                <option value="redirections"> Ajouter une redirection </option>
                {{--<option value="zones"> Modifier les serveurs DNS </option>--}}
            </select>

            <br/>
            <input type="button" class="btn btn-info" value="Sélectionner" id="selectionner" />

            <div id="parameters">
            </div>

            {!! Form::close() !!}
            </div>
    </div>
@endsection

@section('footer-scripts')

        <script type="text/javascript">
            $(document).ready(function(){
                $('#selectionner').on('click', function(e) {
                    e.preventDefault();
                    var action = $('#action').val() ;
                    if( action != 0 && $('input:checkbox').is(':checked')  ) {

                        if(action == 'zones'){
//                            $('#parameters').empty().append('<input name="server_name" />');
                        }
                        else if(action == 'redirections'){
                            $('#parameters').empty().append('<label> Site générique <input name="site" value="" /> </label> <input type="submit" class="btn btn-success" value="Valider" id="valider" />');
                        }
                        $('#valider').submit();
                    } else {
                        $('#form_info').empty().append('<div id="error_msg" class="alert alert-dismissable alert-danger"> Merci de préciser votre demande : <ul> <li>au moins un NDD coché</li> <li>une action sélectionnée.</li> </ul></div>');
                    }
                });

                $("#toutSelectionner").click(function(){
                    $('.chkgroup').click();
                });
            });
        </script>
@endsection