@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques Smessage</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-success">Retour</a>
                    <!-- <a href="/button2" class="btn btn-primary">Bouton2</a> -->
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <div class="caption">
              <i class="fa fa-cogs font-blue"></i>
              <span class="caption-subject font-blue bold uppercase">Liste des comptes :</span>
          </div>

            <table class="table table-hover table-bordered table-striped">

              @foreach($smessage as $valeur)

              <tr>
                  <td>{{ $valeur->nom }}</td>

                  <td>Nombre de crédits restants : {{ $routeur->recup_credit($valeur->nom, $valeur->password) }} </td>
              </tr>
              @endforeach

              </table>

              <div class="caption">
                  <i class="fa fa-cogs font-blue"></i>
                  <span class="caption-subject font-blue bold uppercase">Informations campagne Smessage :</span>
              </div>

              <table class="table table-hover table-bordered table-striped">
                <tr>

                    <th>Référence</th>
                    <th>Date de création</th>

                    <th>Total mail</th>
                    <th>NPAI</th>
                    <th>NPAI Soft</th>
                    <th>Ouvreurs</th>
                    <th>Cliqueurs</th>
                    <th>Désabonnement</th>
                    <th>Action</th>
                </tr>
              @foreach($statsrouteur as $info)
              <tr>

                  <td>{{$info->reference}}</td>
                  <td>{{-- $info->date_creation --}} {{date('d/m/Y', $info->date_creation)}}</td>

                  <td>{{$info->total_mail}}</td>
                  <td>{{$info->npai}}</td>
                  <td>{{$info->npai_soft}}</td>
                  <td>{{$info->ouvreurs}}</td>
                  <td>{{$info->cliqueurs}}</td>
                  <td>{{$info->desinscrits}}</td>
                  <td><a href="/stats/routeur/smessage/{{ $info->reference }}"/> <button type="button" class="btn btn-primary">+ de détails</button></a></td>
              </tr>
              @endforeach

            </table>

        </div>
    </div>

@endsection
