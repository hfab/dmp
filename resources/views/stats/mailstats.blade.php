<meta charset="UTF-8">

<!-- if smessage -->
@if($type === 'smessage')

<table class="table table-hover table-bordered table-striped">
  <tr>
      <th>Référence</th>
      <th>Volume réel envoyé</th>
      <th>NPAI</th>
      <th>Ouvreurs</th>
      <th>Cliqueurs</th>
      <th>Désabonnement</th>
      <th>Mise à jour</th>
  </tr>

@foreach($statscampagneold as $rowold)
<tr>
    <td>{{ $rowold->reference }}</td>
    <td>{{ big_number($rowold->total_mail) }}</td>
    <td>{{ big_number($rowold->npai) }}</td>
    <td>{{ big_number($rowold->ouvreurs) }}</td>
    <td>{{ big_number($rowold->cliqueurs) }}</td>
    <td>{{ big_number($rowold->desinscrits) }}</td>
    <td>{{ date('d/m/Y H:i', $rowold->date_maj) }}</td>

</tr>
@endforeach
</table>

<table class="table table-hover table-bordered table-striped">
  <tr>
      <th>Référence</th>
      <th>Volume réel envoyé</th>
      <th>NPAI</th>
      <th>Ouvreurs</th>
      <th>Cliqueurs</th>
      <th>Désabonnement</th>
      <th>Mise à jour</th>
  </tr>


@foreach($statscampagne as $row)
<tr>
    <td>{{ $row->reference }}</td>
    <td>{{ big_number($row->total_mail) }}</td>
    <td>{{ big_number($row->npai) }}</td>
    <td>{{ big_number($row->ouvreurs) }}</td>
    <td>{{ big_number($row->cliqueurs) }}</td>
    <td>{{ big_number($row->desinscrits) }}</td>
    <td>{{ date('d/m/Y H:i', $row->date_maj) }}</td>

</tr>
@endforeach
</table>
@endif


<!-- if maildrop -->
@if($type === 'maildrop')

<table class="table table-hover table-bordered table-striped">
<tr>

    <th>Référence</th>
    <th>Ouvreurs</th>
    <th>Cliqueurs</th>
    <th>Désinscription</th>
    <th>Mise à jour</th>
</tr>

@foreach($statscampagneold as $lastatold)

<tr>
    <td>{{ $lastatold->reference }}</td>
    <td>{{ big_number($lastatold->ouvreurs) }}</td>
    <td>{{ big_number($lastatold->cliqueurs) }}</td>
    <td>{{ big_number($lastatold->desinscriptions) }}</td>
    <td>{{ date('d/m/Y H:i', $lastatold->date_maj) }}

    </td>
 </tr>
@endforeach
</table>


<table class="table table-hover table-bordered table-striped">
<tr>

    <th>Référence</th>
    <th>Ouvreurs</th>
    <th>Cliqueurs</th>
    <th>Désinscription</th>
    <th>Mise à jour</th>
</tr>

@foreach($statscampagne as $lastat)

<tr>
    <td>{{ $lastat->reference}}</td>
    <td>{{ big_number($lastat->ouvreurs) }}</td>
    <td>{{ big_number($lastat->cliqueurs) }}</td>
    <td>{{ big_number($lastat->desinscriptions) }}</td>
    <td>{{ date('d/m/Y H:i', $lastat->date_maj) }}

    </td>
 </tr>
@endforeach
</table>


@endif
