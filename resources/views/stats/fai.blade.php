@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques par FAI</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                  <a href="/stats" class="btn btn-primary">Retour statistiques générales</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-bordered table-striped">

              <tr>
                  <th>FAI</th>
                  <th>Tokens totaux</th>
                  <th>Tokens utilisés</th>
                  <th>Tokens disponibles</th>

                  <th width="15%">%</th>
              </tr>

                    @foreach($statsfais as $stats)
                    <tr>
                      <td>
                        <?php
                           $fainom = \DB::table('fais')->where('id',$stats->fai_id)->first();
                           // var_dump();
                           echo $fainom->nom;
                        ?>

                      </td>
                      <td>
                        {{big_number($stats->volume)}}
                      </td>
                      <td>
                        {{big_number($stats->volume_used)}}
                      </td>
                      <td>
                          {{ big_number($stats->volume - $stats->volume_used) }}
                      </td>
                      <td>

                        <?php

                        $restant = $stats->volume - $stats->volume_used;
                        if ($stats->volume == 0) {
                            $percent = 0;
                        } else {
                            $percent = round($restant * 100 / $stats->volume);
                        }
                        if ($percent > 50) {
                            $class = "success";
                        } elseif ($percent <= 50 && $percent > 25) {
                            $class = 'warning';
                        } else {
                            $class = 'danger';
                        }
                        ?>

                        <div class="progress">
                            <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $restant }}"
                                 aria-valuemin="0" aria-valuemax="{{ $stats->volume }}" style="width:{{ $percent }}%">
                            </div>
                        </div>

                      </td>
                    </tr>
                    @endforeach


            </table>
        </div>
    </div>

@endsection
