@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Volume par routeur</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-primary">Retour - Accueil Statistiques</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
          <div class="list-group">
              <form action="/stats/routeurs" method="POST">
                  {{csrf_field()}}
                  <label>Routeur</label>
                  <select class="form-control" name="routeur">
                      <option value="0" @if(empty($routeur)) selected @endif>Tout</option>
                      @foreach($routeurs as $r)
                      <option value="{{$r->id}}" @if($r->id == $routeur) selected @endif>{{$r->nom}}</option>
                      @endforeach
                  </select>
                  <br/>
                  <label>De</label>
                  <input type="date" name="from" value="@if(!empty($from)){{$from}}@endif">
                  <label>à</label>
                  <input type="date" name="to" value="{{$to}}">
                  <input type="submit" class="btn btn-default" />
              </form>
              <hr/>
              <table class="table table-hover table-bordered table-striped">
                  <tr>
                    <th>Routeur</th>
                    <th>Volume total</th>
                  </tr>
                  @foreach($plannings as $pr)
                  <tr>
                      <td>{{$pr->routeur->nom}}</td>
                      <td>{{big_number($pr->total)}}</td>
                  </tr>
                  @endforeach
              </table>

          </div>
        </div>
    </div>
@endsection
