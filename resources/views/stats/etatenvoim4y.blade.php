@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques MailForYou API</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-success">Retour</a>
                    <!-- <a href="/button2" class="btn btn-primary">Bouton2</a> -->
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <div class="caption">
              <i class="fa fa-cogs font-blue"></i>
              <span class="caption-subject font-blue bold uppercase">Dernière mise à jour > {{$timemaj}}</span>
          </div>

          <table class="table table-hover table-bordered table-striped">
          <tr>

              <th>Informations</th>
              <th>Sender</th>
              <th>Campagne</th>
              <th>Support</th>
              <th>Nombre d'email total</th>
              <th>Nombre envoyé</th>
              <th>Etat de l'envoi</th>
          </tr>

          @foreach($ds as $r)

          <tr>
              <td>
                <p>Planning : {{$r->planning_id}}</p>
                <p>
                  <?php
                    $datesend = \DB::table('plannings')->where('id',$r->planning_id)->first();
                    echo($datesend->sent_at);
                  ?>
                </p>
                <td>
                <p>Sender : {{$r->sender_id}}</p>
                <p>
                  <?php
                    $infosender = \DB::table('senders')->where('id', $r->sender_id)->first();
                    echo($infosender->nom);
                  ?>
                </p>
                </td>
                <td>
                <p>Campagne : {{$r->campagne_id}}</p>
                <p>
                  <?php
                    $lacampagne = \DB::table('campagnes')->where('id',$datesend->campagne_id)->first();
                    echo($lacampagne->nom);
                  ?>
                </p>
                </td>
                <td>
                CID : {{$r->cid_routeur}}
              </td>
              <td>
                {{$r->tosend}}
              </td>
              <td>
                {{$r->send}}
              </td>
              <td>
                <?php
                  if($r->tosend == 0){
                    echo "<p class='text-danger'>Echec de l'envoi</p>";
                  }

                  if($r->tosend == $r->send){
                    if($r->tosend > 0){
                      echo "<p class='text-success'>Envoi terminé</p>";
                    }
                  }

                  if($r->tosend > $r->send){
                    if($r->tosend > 0){
                      echo "<p class='text-primary'>Envoi en cours</p>";
                    }
                  }

                ?>
              </td>

          </tr>
          @endforeach

        </table>
        </div>
    </div>

@endsection
