@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Bounces / NPAI</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-bordered table-striped">
                @foreach($bounces as $bounce)
                    @if($bounce->destinataire != null)
                        <tr>
                            <td>{{ $bounce->destinataire->fai->nom }}</td>
                            <td>{{ $bounce->campagne->nom }}</td>
                            <td>{{ $bounce->message }}</td>
                            <td>{{ $bounce->created_at }}</td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>

@endsection
