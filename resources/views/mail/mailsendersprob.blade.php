<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Rapport comptes problématiques</title>
<body style="margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: Trebuchet MS, Arial, Verdana, sans-serif;">

<!-- Start Main Table -->
<table width="100%" height="100%" cellpadding="0" style="padding: 20px 0px 20px 0px; background-color: #34495e;">
    <tr align="center">
        <td>

            <table>
                <tr>
                    <td>
                        <!-- <span style="font-size:10px; color:#000;">Si cet email ne s'affiche pas correctement, <a href="http://webdesignweb.fr/sources/email-html/index.html" target="_blank" style="font-size:10px; color:#22baba;">cliquez-ici</span> -->
                    </td>
                </tr>
            </table>
            <!-- Start Header -->
            <table style="width:580px; height:108px;" background="#2980b9" border="0">
            <tr>
                <td valign="top" style="width:456px;">
                    <h1 style="font-size:22px; color:#22baba;margin-left:20px; margin-top:26px;">Rapport comptes problématiques - {{ $url }}</h1>
                </td>
                <td valign="top">
                    <p style="color:#aeaeae; font-size:11px; margin-top:34px;"><?php echo date('d-m-Y H:i'); ?></p>
                </td>
            </tr>
            </table>

            <table cellpadding="0" cellspacing="0" width="650" style="padding:30px 25px 30px 25px; background-color:white; text-align:center;">
                 <tr>

                  <th style="color:red; padding:15px 5px 15px 5px;">FAI</th>
                  <th style="color:red; padding:15px 5px 15px 5px;">Campagnes</th>
                  <th style="color:red; padding:15px 5px 15px 5px;">Comptes</th>

                </tr>

                  @foreach ($campagnes_error as $fai => $campagnes_senders)

                        @foreach($campagnes_senders as $campagne_ref => $senders_noms)
                        <tr>
                            <td>{{$fai}}</td>
                            <td>
                                {{$campagne_ref}}
                            </td>
                            <td>
                                <?php $noms = ''; ?>
                                @foreach($senders_noms as $sender_nom)
                                        <?php $noms .= $sender_nom."<br/>"; ?>
                                @endforeach
                                <?php echo $noms; ?>
                            </td>
                        </tr>
                        @endforeach

                  @endforeach

                    </table>
                </td>
            </tr>

     </table>

</body>
</html>
