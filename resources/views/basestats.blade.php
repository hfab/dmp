@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques - Destinataires vs Repoussoirs</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats/email" class="btn btn-success">Retour</a>
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

        <table class="table table-striped table-hover">
		<tr>
                <th>Base</th>
				<th>Nombre email départ</th>
				<th>Nombre email dans repoussoir</th>
				<th>Résultat</th>
            </tr>
			@foreach ($bases as $base)
			<tr>
                <th>{{ $base->nom }}</th>
				<th>{{ $nbrorigine->total }}</th>
				<th>{{ $repounbr }}</th>
				<th>{{ $resultat }}</th>
            </tr>
			@endforeach
            
        </table>
		

   
    </div>
    </div>

    
@endsection
