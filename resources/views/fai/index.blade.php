@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Liste des FAIs
                </span>
            </div>
            <div class="actions">
            @if( \Auth::User()->user_group->name == 'Super Admin' || \Auth::User()->user_group->name == 'Admin' )
                <div class="btn-group btn-group-devided">
                    <a title="Ajouter un FAI" class="btn green" href="/fai/create"><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
            @endif
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>Nom</th>
                    <th>Pression marketing*</th>
                    <th>Maximum par envoi</th>
                    <th>Pourcentage par défaut par envoi</th>
                    <th>Actions</th>
                </tr>
            @foreach($fais as $f)
                <tr>
                    <td>{{ $f->nom }}</td>
                    <td>{{ $f->quota_pression }}</td>
                    <td>{{ $f->quota_campagne }}</td>
                    <td>{{ $f->default_planning_percent }}</td>
                    <td>
                        <a title="Modifier" class="btn btn-primary" href="/fai/{{$f->id}}/edit" !!}><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    </td>
                    {{--<td>--}}
                        {{--{!! Form::open(array('route' => array('fai.destroy', $f->id), 'method' => 'delete')) !!}--}}
                        {{--<button class="btn default red" type="submit">Supprimer</button>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </table>
        </div>
        <i>*Nombre maximum par jour de campagnes différentes que peut recevoir une adresse</i>
    </div>
@endsection

