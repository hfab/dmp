@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistique - Destinataires vs Repoussoirs</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-success">Retour</a>
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">
	{!! Form::open(array('url' => 'stats/email/base')) !!}
        <table class="table table-striped table-hover">
		
			
		@foreach ($bases as $base)
            <tr>
                <td> <label> {!! Form::radio('baseid',$base->id) !!} &nbsp;&nbsp; {{ $base->nom }} </label></td>
            </tr>
            @endforeach
            
			
            
        </table>
		
		
		{!! Form::submit('Valider',['class' => 'btn btn-large btn-primary']) !!}
        <input type="hidden" id="_token" value="{{ csrf_token() }}" />
        {!! Form::close() !!}
   
    </div>
    </div>

    
@endsection
