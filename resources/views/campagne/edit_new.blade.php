@extends('template')

@section('content')
      <!-- remettre bouton bat -->
      <div class="row">

        <div class="col-md-6 col-xs-12">
          {!! Form::model($campagne, array('method' => 'PATCH', 'route' => array('campagne.update', $campagne->id), 'class'=>'form-horizontal', 'id' => 'form_campagne')) !!}
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editer une campagne<small class="text-right"><button id="store" type="submit" class="btn btn-danger">Enregistrer</button></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                      {!! Form::model($campagne, array('method' => 'PATCH', 'route' => array('campagne.update', $campagne->id), 'class'=>'form-horizontal', 'id' => 'form_campagne')) !!}


                      @include('campagne.partial-fields')

                      <div class="col-md-12 text-right">
                        <button id="store" type="submit" class="btn btn-danger">Enregistrer</button>
                      </div>


                      {!! Form::close() !!}

                  </div>
                </div>
              </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Apercu de la campagne <small></small></h2>
            					<div class="nav navbar-right">

                            <?php

                            $date_m = date("m");
                            $moiscomptacampagne = date_parse($campagne->created_at);

                            if($moiscomptacampagne['month'] < $date_m){
                              // je redirect après le call
                              echo '<a href="/ca/add/'.$campagne->id.'" id="compta" class="btn btn-danger">Compta</a>';

                            }

                            ?>

                            <!-- <button id="store" type="submit" class="btn btn-danger">Enregistrer</button> -->
                            <a href="#" id="send_campagne" class="btn btn-warning">BAT MD</a>
                            <a href="/campagne/{{$campagne->id}}/choix_compte_bat_m4y" class="btn btn-warning">BAT M4Y</a>
                            <a href="#" id="send_campagne_mb" class="btn btn-warning">BAT MB</a>
                            <a href="/campagne/{{$campagne->id}}/planning" class="btn btn-info">Planning</a>

            					</div>
                    <!--
					<ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
					-->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    @if(!empty($count))
                        <div class="alert alert-dismissable alert-info">
                            La campagne a été dé-dupliquée {{$count}}
                        </div>
                    @endif


                    <br />
                    From : {{ $campagne->expediteur }} - Objet : {{ $campagne->objet }}<br />
                    <div style="border:solid 1px black">{!! $campagne->html !!}</div>
                </div>
                </div>
              </div>

      </div>



      <script>
          $().ready(function() {
              // toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
             console.log('ok');
             $("#send_campagne").click(function(e) {
                 console.log("BAT - MAILDROP");
                 e.preventDefault();
                 $(this).attr("disabled", true);

                 var campagne_dest = $('#campagne_dest').val();

                 $.post('/campagne/{{ $campagne->id }}/send_bat_md', { _token: '{{ csrf_token() }}'}, function(data) {
                     console.log(data);
                     $("#send_campagne").attr("disabled", false);

                     if (data == 'ok') {
                         toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                     } else {
                         toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                     }
                 });
             });

              $("#send_campagne_mb").click(function(e) {
                  console.log("BAT - MINDBAZ");
                  e.preventDefault();
                  $(this).attr("disabled", true);

                  var campagne_dest = $('#campagne_dest').val();

                  $.post('/campagne/{{ $campagne->id }}/send_bat_mb', { _token: '{{ csrf_token() }}'}, function(data) {
                      console.log(data);
                      $("#send_campagne").attr("disabled", false);

                      if (data == true) {
                          toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                      } else {
                          toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                      }
                  });
              });

              $("#send_campagne_edatis").click(function(e) {
                  console.log("BAT - Edatis");
                  e.preventDefault();
                  $(this).attr("disabled", true);

                  var campagne_dest = $('#campagne_dest').val();

                  $.post('/campagne/{{ $campagne->id }}/send_bat_edatis', { _token: '{{ csrf_token() }}'}, function(data) {
                      console.log(data);
                      $("#send_campagne").attr("disabled", false);

                      if (data == true) {
                          toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                      } else {
                          toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                      }
                  });
              });

             $("#send_campagne_sm").click(function(e) {
  	           console.log("BAT - SMESSAGE");
                 e.preventDefault();
                 $(this).attr("disabled", true);

                 var campagne_dest = $('#campagne_dest').val();

                 $.post('/campagne/{{ $campagne->id }}/send_bat_sm', { _token: '{{ csrf_token() }}'}, function(data) {
                     console.log(data);
                     $("#send_campagne").attr("disabled", false);

                     if (data == 'ok') {
                         toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                     } else {
                         toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                     }
                 });
             });


              $("#send_recap").click(function(e) {
                  e.preventDefault();

                  var campagne_dest = $('#campagne_dest').val();
                  console.log('Envoi du RECAP à '+campagne_dest);

                  $.post('/campagne/{{ $campagne->id }}/send_recap', { _token: '{{ csrf_token() }}'}, function(data) {
                      console.log('sent');
                      console.log(data);


                  });
              });

          });
      </script>

          @endsection
