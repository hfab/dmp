@extends('template')

@section('content')

<script type="text/javascript">
    function filterByBase() {
        document.getElementById("baseTri").submit();
    }
</script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<!-- Input -->
    <script>
        $(document).ready(function(){
            {!! FormAutocomplete::selector('#recherche')->db2() !!}
        });
    </script>



      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Campagne <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="/plateforme" class="btn btn-primary">Plateforme</a>
                      <a href="/ca" class="btn btn-success">Chiffre d'affaire</a>
                      <a href="/campagne/create" class="btn btn-warning">Ajouter une campagne</a>
                      <a href="/upload" class="btn btn-danger">Repoussoir</a>
                      <a href="/dezip" class="btn btn-danger">Importer images</a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">
                        <div class="span8">
                        {!! Form::open(array('action' => 'CampagneController@index', 'method' => 'get', 'id' => 'baseTri')) !!}
                        &nbsp;&nbsp;&nbsp;
                        {!! Form::label('recherche', ' ') !!}
                        {!! Form::text('recherche', $recherche) !!}
                        <button type="submit" class="btn btn-default">Rechercher</button>
                            <p>
                                <select id="base_id" name="base_id" class="form-control input-medium" onchange='filterByBase();'>
                                    <option>Filtrer par base </option>
                                    @foreach ($basesinfo as $labase)
                                        <option value="{{$labase->id}}" @if($labase->id == $base_id) selected="selected" @endif>{{$labase->nom}}</option>
                                    @endforeach
                                    <option value="0" @if( is_numeric($base_id) && $base_id == 0) selected="selected" @endif> Multi-bases </option>
                                </select>
                            </p>
                        {!! Form::close() !!}
                    </div>
                    </div>
                    <div class="row">
                      <br />
                    </div>

                    <div style="overflow-x:auto;">
                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th>Base</th>
                              <th>Nom</th>
                              <th>Référence</th>
                              <th>Planifications</th>
                              <th>Date de création</th>
                              <th>Actions</th>
                          </tr>
                          @foreach($campagnes as $campagne)
                          <tr>
                              <td class="col-xs-1">@if($campagne->base_id == 0) Multi-bases @else {{$campagne->base->nom}} @endif</td>
                              <td class="col-xs-1">{{$campagne->nom}}</td>
                              <td class="col-xs-1">{{$campagne->ref}}</td>
                              <td class="col-xs-1">{{$campagne->plannings->count()}}</td>

                              <td class="col-xs-1">{{$campagne->created_at}}</td>
                              <td class="col-xs-2">
                                  <a title="Editer" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                  <!-- <a title="Statistiques" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/stats"><i class="fa fa-area-chart" aria-hidden="true"></i></a> -->
                                  <a title="Plannifier" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/planning"><i class="fa fa-clock-o" aria-hidden="true"></i></a>
                                  @if($is_mindbaz_list)
                                      <a title="Plannifier Mindbaz" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/mindbaz_planning"><i>MB</i></a>
                                  @endif
                                  <a title="Dupliquer" id= 'c{{$campagne->id}}' class="btn btn-success btn-sm" href="/campagne/{{$campagne->id}}/dupliquercampagne"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                                  <a class="btn btn-warning btn-sm" href="/campagne/{{$campagne->id}}/md5">Repoussoir</a>
                              </td>

                          </tr>
                          @endforeach
                      </table>
                    </div>
                      {!! $campagnes->appends(['base_id' => $base_id, 'recherche' => $recherche])->render() !!}
                  </div>


      </div>
      </div>

          @endsection
