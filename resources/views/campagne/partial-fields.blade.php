<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Nom<strong>*</strong></label>
    <div class="col-sm-10">
        {!! Form::text('nom', Input::old('nom'), ['class' => 'form-control', 'onkeyup' => "var start = this.selectionStart, end = this.selectionEnd; $(this).val($(this).val().replace(/[^\_\-a-z0-9\s]/gi, '').replace(/[\s]/g, '_')); this.setSelectionRange(start, end);", 'required', 'id' => 'nom']) !!}
    </div>
</div>
<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Référence<strong>*</strong></label>
    <div class="col-sm-10">
        {!! Form::text('ref', Input::old('ref'), ['class' => 'form-control', 'onkeyup' => "var start = this.selectionStart, end = this.selectionEnd; $(this).val($(this).val().replace(/[^\_\-a-z0-9\s]/gi, '').replace(/[\s]/g, '_')); this.setSelectionRange(start, end);", 'required', 'id' => 'ref']) !!}
    </div>
</div>
<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Theme<strong>*</strong></label>
    <div class="col-sm-10">
        {!! Form::select('theme_id', $themes, Input::old('theme_id'), ['class' => 'chosen-select form-control', 'id' => 'theme']) !!}
    </div>
</div>
<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Expéditeur<strong>*</strong></label>
    <div class="col-sm-10">
        {!! Form::text('expediteur', Input::old('expediteur'), ['class' => 'form-control','required', 'id' => 'expediteur']) !!}
    </div>
</div>
<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Expéditeur Anonyme</label>
    <div class="col-sm-10">
        {!! Form::text('expediteur_anonyme', Input::old('expediteur_anonyme'), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Objet<strong>*</strong></label>
    <div class="col-sm-10">
        {!! Form::text('objet', Input::old('objet'), ['class' => 'form-control', 'required', 'id' => 'objet']) !!}
    </div>
</div>
<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Haut</label>
    <div class="col-sm-10">
        {!! Form::text('toptext', Input::old('toptext'), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Bas</label>
    <div class="col-sm-10">
        {!! Form::text('bottomtext', Input::old('bottomtext'), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label for="input_txt_suppr" class="col-sm-2 control-label">Texte à supprimer</label>
    <div class="col-sm-10">
        {!! Form::text('deletetext', Input::old('deletetext'), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label for="input_txt_suppr" class="col-sm-2 control-label">Volume souhaité</label>
    <div class="col-sm-10">
        {!! Form::text('volume_total', Input::old('volume_total'), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">HTML<strong>*</strong></label>
    <div class="col-sm-10">
        {!! Form::textarea('html',Input::old('html'),array('id' => 'html', 'class' => 'form-control')) !!}
    </div>
</div>
@if( (\Auth::User()->user_group->name == 'Admin' or \Auth::User()->user_group->name == 'Super Admin') and getenv('CLIENT_PLAN') == 'plus')
    <div class="form-group">
        <label for="withtags" class="col-sm-2 control-label">Avec tags partenaires</label>
        <div class="col-sm-10">
            {!! Form::checkbox('withtags',Input::old('withtags'),0, array('id' => 'withtags', 'class' => 'form-control')) !!}
        </div>
    </div>
@endif
<div class="form-group">
    <label for="withtags" class="col-sm-2 control-label">Hébergement images (Amazon)</label>
    <div class="col-sm-10">
        {!! Form::checkbox('hostimage',Input::old('hostimage'),1, array('id' => 'hostimage', 'class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Partenaire</label>
    <div class="col-sm-10">
            <select name="partenaireId" class="form-control">
                    <option value="0">Aucun</option>
                @foreach($partners as $partner)
                    @if(!empty($campagne) && $campagne->dmp_partnersite_id == $partner->id)
                        <option value="{{$partner->id}}" selected="selected">{{$partner->name}}</option>
                    @else
                        <option value="{{$partner->id}}">{{$partner->name}}</option>
                    @endif
                @endforeach
            </select>
    </div>
</div>
<div class="form-group">
        <label for="withtags" class="col-sm-2 control-label">Dépôt cookie DMP </label>
        <div class="col-sm-10">
            {!! Form::checkbox('dmpCookiable',Input::old('dmpCookiable'),0, array('id' => 'dmpCookiable', 'class' => 'form-control')) !!}
	</div>
</div>

<div class="form-group">
        <label for="withtags" class="col-sm-2 control-label">Relançable jusqu'au </label>
        <div class="col-sm-10">
	        <input  id="relaunchDateLimit" name="dateRelance" class="form-control" placeholder="Jour/Mois/Année"/>
	<script>
		 $( function() {
		       $( "#relaunchDateLimit" ).datepicker();
		       $( "#relaunchDateLimit" ).datepicker( "option", "dateFormat", "yy-mm-dd");
		       document.getElementById("relaunchDateLimit").value = "{{$campagne->expires_at or ''}}";
		  } );
	</script>
        </div>
</div>
@if( (\Auth::User()->user_group->name == 'Admin' or \Auth::User()->user_group->name == 'Super Admin') and getenv('CLIENT_PLAN') == 'plus')
    <div class="form-group">
        <label for="withtags" class="col-sm-2 control-label">Remarques</label>
        <div class="col-sm-10">
            {!! Form::textarea('info',Input::old('info'), ['id' => 'info', 'class' => 'form-control', 'rows' => '3', 'cols' => '50']) !!}
        </div>
    </div>
@endif

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Plateformes<strong>*</strong></label>
    <div class="col-sm-10">

      <select id="select" class="chosen-select form-control" name="plateforme_id">
        @if(isset($campagne))
          @foreach ($plateformes as $plateforme)
            @if($campagne->plateforme_id == $plateforme->id)
              <option value="{{$plateforme->id}}" selected>{{$plateforme->nom_plateforme}}</option>
            @else
              <option value="{{$plateforme->id}}">{{$plateforme->nom_plateforme}}</option>
            @endif
          @endforeach
        @else
        @foreach ($plateformes as $plateforme)
            <option value="{{$plateforme->id}}">{{$plateforme->nom_plateforme}}</option>
        @endforeach
        @endif
      </select>
      </div>
</div>

<p>
    <i style="text-align: right"><strong>*</strong>Obligatoire</i>
</p>

@section('footer-scripts-extra')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.js"></script>
<script src="https://cdn.jsdelivr.net/chosen/1.1.0/chosen.proto.js"></script>
<script src="http://labo.tristan-jahier.fr/chosen_order/chosen.order.jquery.min.js"></script>

<script type="text/javascript">
    var config = {

        allow_single_deselect:false,
        disable_search_threshold:10,
        no_results_text:'Oops, nothing found!',
        width:"60%"
    };

    var chosen = $('#base').chosen(config);
    var chosen2 = $('#theme').chosen(config);

    function warnBeforeRedirect($element) {
//            On construit le message d'affichage en cas de valeurs fausses lors de MAJ ou création de campagnes
        var required_parameters = ['nom', 'ref', 'base', 'theme', 'expediteur', 'objet', 'html'];

        var title = null;

        var text = '';

        $.each(required_parameters, function( index, champ ) {

            var new_camp_text = '';
            var valeur = $('#'+champ).val();

            if(valeur == undefined || valeur == ''){
                title = "Oops, il manque certaine(s) information(s)";
                type = "error";
                showCancelButton = false;
                new_camp_text = '<span style="color: red">' + champ + '</span> <br/>';
            }
            if(champ == 'html' && /<[a-z][\s\S]*>/i.test(valeur) == false){
                new_camp_text = '<span style="color: red">' + champ + ": n'est pas au bon format.</span> <br/>";
            }

            text = text + new_camp_text;
        });

        if(title != null) {
            swal
            (
                {
                    title: title,
                    text: text,
                    type: type,
                    showCancelButton: showCancelButton,
                    html: true
                }
                ,
                function(isConfirm) {
                    if (isConfirm) {
                        return false;
                    }
                }
            );
        } else {
            $element.closest("form").submit();
        }
    }

    $(document).ready(function() {
        // Chosenify every multiple select DOM elements with class 'chosen'

        $('#store').click(function(e){
            e.preventDefault();
            warnBeforeRedirect($(this));
	});
    });
</script>
@endsection
