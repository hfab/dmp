@extends('common.layout')

@section('content')
{!! Form::model($campagne, array('method' => 'PATCH', 'route' => array('campagne.update', $campagne->id), 'class'=>'form-horizontal', 'id' => 'form_campagne')) !!}
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
                Editer une Campagne
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <button id="store" type="submit" class="btn btn-danger">Enregistrer</button>
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Actions
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#" id="send_campagne">BAT MD</a></li>
                    <li><a href="/campagne/{{$campagne->id}}/choix_compte_bat_m4y">BAT M4Y</a></li>
                    <li><a href="#" id="send_campagne_mb" >BAT MB</a></li>
                    <li><a href="#" id="send_campagne_edatis" >BAT Edatis</a></li>
                    {{--<li><a href="#" id="send_campagne_sm" >BAT SM</a></li>--}}
                    <li role="separator" class="divider"></li>
                    <li><a href="/campagne/{{$campagne->id}}/planning">Planning</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/campagne/{{$campagne->id}}/html/check">Check Kit</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="portlet-body">
    @if(!empty($count))
        <div class="alert alert-dismissable alert-info">
            La campagne a été dé-dupliquée {{$count}}
        </div>
    @endif
    @if(isset($message))
        <div class="alert alert-dismissable alert-success">
          {{$message}}
        </div>
    @endif
        <div class="row">
            <div class="col-md-5">
                @include('campagne.partial-fields')
            </div>

            <?php
                $string = $campagne->html;
                if(stristr($string, 'base') === FALSE) {
              ?>
              <div class="col-md-7">
                  From : {{ $campagne->expediteur }} - Objet : {{ $campagne->objet }}<br />
                  <div style="border:solid 1px black">{!! $campagne->html !!}</div>
              </div>
              <?php
            } else {
              echo '<a href="voir" target="_blank"/><button type="button" class="btn btn-success">Prévisualiser</button></a>';
            }
            ?>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@section('footer-scripts')
    <script>
        $().ready(function() {
           console.log('ok');
           $("#send_campagne").click(function(e) {
               console.log("BAT - MAILDROP");
               e.preventDefault();
               $(this).attr("disabled", true);

               var campagne_dest = $('#campagne_dest').val();

               $.post('/campagne/{{ $campagne->id }}/send_bat_md', { _token: '{{ csrf_token() }}'}, function(data) {
                   console.log(data);
                   $("#send_campagne").attr("disabled", false);

                   if (data == 'ok') {
                       toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                   } else {
                       toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                   }
               });
           });

            $("#send_campagne_mb").click(function(e) {
                console.log("BAT - MINDBAZ");
                e.preventDefault();
                $(this).attr("disabled", true);

                var campagne_dest = $('#campagne_dest').val();

                $.post('/campagne/{{ $campagne->id }}/send_bat_mb', { _token: '{{ csrf_token() }}'}, function(data) {
                    console.log(data);
                    $("#send_campagne").attr("disabled", false);

                    if (data == true) {
                        toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                    } else {
                        toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                    }
                });
            });

            $("#send_campagne_edatis").click(function(e) {
                console.log("BAT - Edatis");
                e.preventDefault();
                $(this).attr("disabled", true);

                var campagne_dest = $('#campagne_dest').val();

                $.post('/campagne/{{ $campagne->id }}/send_bat_edatis', { _token: '{{ csrf_token() }}'}, function(data) {
                    console.log(data);
                    $("#send_campagne").attr("disabled", false);

                    if (data == true) {
                        toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                    } else {
                        toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                    }
                });
            });

           $("#send_campagne_sm").click(function(e) {
	           console.log("BAT - SMESSAGE");
               e.preventDefault();
               $(this).attr("disabled", true);

               var campagne_dest = $('#campagne_dest').val();

               $.post('/campagne/{{ $campagne->id }}/send_bat_sm', { _token: '{{ csrf_token() }}'}, function(data) {
                   console.log(data);
                   $("#send_campagne").attr("disabled", false);

                   if (data == 'ok') {
                       toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                   } else {
                       toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                   }
               });
           });


            $("#send_recap").click(function(e) {
                e.preventDefault();

                var campagne_dest = $('#campagne_dest').val();
                console.log('Envoi du RECAP à '+campagne_dest);

                $.post('/campagne/{{ $campagne->id }}/send_recap', { _token: '{{ csrf_token() }}'}, function(data) {
                    console.log('sent');
                    console.log(data);


                });
            });

        });
    </script>

    @yield('footer-scripts-extra')

@endsection
