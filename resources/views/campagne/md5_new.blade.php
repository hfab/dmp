@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Choisir un fichier repoussoir<b>*</b> pour <span style="color:#ffcc33">{{$campagne->ref}} - {{$campagne->nom}}</span> ({{$campagne->base->nom}}) <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                    <a href="/campagne" class="btn btn-danger">Retour</a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <form action="" id="repoussoirForm" method="post">
                          <table class="table table-striped table-hover">
                              <tr>
                                  <th>Fichier à utiliser</th>
                              </tr>
                              @foreach ($files as $file)
                              <tr>
                                  <td> <label> {!! Form::radio('file',$file) !!} &nbsp;&nbsp; {{\File::name($file)}} </label></td>
                              </tr>
                              @endforeach
                          </table>
                          {!! Form::hidden('campagneid', $campagne->id, ['id' => 'campagneid']) !!}
                          {!! Form::submit('Valider',['class' => 'btn btn-large btn-primary']) !!}
                          <input type="hidden" id="_token" value="{{ csrf_token() }}" />
                      </form>
                      <span><b>*</b>permet d'exclure sur 7 jours des destinataires lors de l'envoi de la campagne.</span>


                  </div>


      </div>
      </div>
      </div>


      <script>
          $( "#repoussoirForm" ).submit(function( event ) {
              event.preventDefault();
              console.log('go');
              $.post( "/campagne/md5/action", {
                  file: $('input[name=file]:checked').val(),
                  campagneid: $('#campagneid').val(),
                  _token: $('#_token').val()
              })
                      .done(function( data ) {
                      });
          });
      </script>


    @endsection
