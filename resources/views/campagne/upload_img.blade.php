@extends('common.layout')

@section('content')


<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
                Importer un zip d'images pour une campagne
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/campagne"><button class="btn btn-primary" href="">Retour</button></a>
            </div>
        </div>
    </div>

    <div class="portlet-body">
      <table class="table table-striped table-bordered">
        <tr>
            <th>Nom fichier</th>
            <!-- <th>Action</th> -->
        </tr>
      @foreach ($fichiers as $fichier)
          <tr>
            <td>{{\File::name($fichier)}}</td>
            <!-- <td> -->
              <!-- <a href="supprimer/{{basename($fichier)}}"><span class="label label-danger">Supprimer</span></a> - <a href="telecharger/{{basename($fichier)}}"><span class="label label-success">Télécharger</span></a> -->
            <!-- </td> -->
          </tr>
      @endforeach
      </table>
        <hr>

        {!! Form::open(array('url' => 'dezip','files' => true, 'method' => 'post')) !!}
            <div class="row">
                {!! Form::file('fichier') !!}
            </div>
            <hr>
            <div class="row">
                <button type="submit" class="btn btn-default">Enregistrer</button>
            </div>
        {!! Form::close() !!}

        <br>
        <br>
        <div>
          <span class="caption-subject font-green-sharp bold uppercase">
              Importer un zip d'images pour une campagne
          </span>
          <br />
          <span>Les liens vers les dossier d'images sur le serveur sont les suivants :</span>
          <br />
          <br />
        </div>
        <div>
        @foreach ($lesdirectoryp as $directory)

        http://{{ getenv('CLIENT_URL') }}/campagnes/{{ basename($directory) }}/[nomimage]<br />

        @endforeach
        </div>
    </div>

</div>
@endsection

@section('footer')


@endsection
