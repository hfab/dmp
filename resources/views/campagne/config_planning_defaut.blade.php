@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Configuration planning par defaut</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
              <a title="Retour" href="/planning/defaut/add" class="btn btn-primary">Ajouter Planning par defaut</a>
              <a title="Retour" href="/planning" class="btn btn-success">Retour Planning</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">

      <table id="tab_planning" class="table table-striped table-bordered">
          <tr>
              <th>Configuration ID</th>
              <th>Actions</th>
          </tr>
          @foreach($data as $d)
            <tr>
                <td>
                  Configuration par defaut : {{$d->config_id}} -
                  <?php
                  $config_name = \DB::table('planning_defaut')
                  ->where('config_id',$d->config_id)
                  ->where('config_key','name_config')->first();
                  echo $config_name->value;
                   ?>
                </td>
                <td>
                  <a title="Retour" href="/planning/defaut/{{$d->config_id}}/edit" class="btn btn-warning">Modifier</a>
                  <a title="Retour" href="/planning/defaut/{{$d->config_id}}/delete" class="btn btn-danger">Supprimer</a>
                </td>
            </tr>
          @endforeach
      </table>


    </div>
</div>
@endsection

@section('footer')


@endsection

@section('footer-scripts')
    <script type="text/javascript">
        function filterByBase() {
            document.getElementById("baseTri").submit();
        }
    </script>
@endsection
