@extends('common.layout')

@section('content')


    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Compte(s) utilisé(s) pour le planning {{$planning->id}} (Campagne {{$planning->campagne->ref}})</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    @if(empty($planning->sent_at))
                        <a href="/planning/{{$planning->id}}/send" class="btn btn-success">Envoyer la campagne manuellement</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <br />
            </div>

            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <th style="text-align:center;">Comptes sélectionnés</th>
                        <th style="text-align:center;">Statut import</th>
                    </tr>
                    @foreach($segments as $nomcompte => $statut)
                        <tr>
                            <td style="text-align:center;">{{ $nomcompte }}</td>
                            <td style="text-align:center;"> {{$statut}} </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection

@section('footer-scripts')
@endsection
