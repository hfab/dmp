<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="noindex,nofollow"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel="icon" type="image/png" href="http://tor.sc2consulting.fr/images/favicon.png" />

    <!-- jQuery -->
    <script src="{{url('/vendors/jquery/dist/jquery.min.js')}}"></script>

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" >
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <title>TOR
      <?php
        if(getenv('CLIENT_TITLE') !== null){
          echo getenv('CLIENT_TITLE');
        } else {
          echo 'TOR';
        }
      ?>
    </title>

    <!-- Bootstrap -->
    <link href="{{url('/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url('/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{url('/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{url('/vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{url('/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{url('/vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{url('/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{url('/vendors/custom.min.css')}}" rel="stylesheet">
    <!-- Jquery UI CSS -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


    <link href="{{url('/vendors/sweetalert.css')}}" rel="stylesheet">
    <script src="{{url('/vendors/sweetalert.min.js')}}"></script>

    <style>
      a#cRetour{
        border-radius:3px;
        padding:10px;
        font-size:15px;
        text-align:center;
        color:#fff;
        background:rgba(0, 0, 0, 0.25);
        position:fixed;
        right:20px;
        opacity:1;
        z-index:99999;
        transition:all ease-in 0.2s;
        backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
        text-decoration: none;
      }
      a#cRetour:before{ content: "\25b2"; }
      a#cRetour:hover{
        background:rgba(0, 0, 0, 1);
        transition:all ease-in 0.2s;
      }
      a#cRetour.cInvisible{
        bottom:-35px;
        opacity:0;
        transition:all ease-in 0.5s;
      }

      a#cRetour.cVisible{
        bottom:20px;
        opacity:1;
      }
      </style>


  </head>

  <body class="nav-md">
    <a name="haut" id="haut"></a>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; height: 90px;" >
              <a href="/"><center><img src="{{url('/images/logo1.png')}}" alt="logo Tor" class="img-responsive" width="50%"/></center></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">

              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>
                  <?php
                    $user = \Auth::User();
                    echo $user->name;
                  ?>
              </h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                @if(\Auth::User()->user_group_id == 3)
                  <li><a><i class="fa fa-dashboard"></i><span class="link_dmp">Dmp</span>
                      <span class="fa fa-chevron-down"></span>
                    </a>
                    <ul class="nav child_menu">

                      <li class="link_dmpManagement"><a>Gestion</a></li>
                      <li class="link_dmpPartners"><a>Liste Partenaires</a></li>
                      <li class="link_dmpAddresses"><a>Liste hash relancées</a></li>
                      <!-- <li class="link_dmpPlannings"><a>Liste Dmp plannings</a></li> -->
                    </ul>
		          </li>

                  <li><a><i class="fa fa-paper-plane"></i> <span class="link_sender">Senders</span> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/sender/create">Ajouter un sender</a></li>
                    </ul>
                  </li>
              @endif
              @if(\Auth::User()->user_group_id == 3 ||\Auth::User()->user_group->name == "Annonceur")
                  <li><a><span class="link_campagne"><i class="fa fa-envelope-o"></i> Campagnes</span> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                      <li><a href="/campagne/create">Ajouter une campagne</a></li>
                      <li><a href="/theme">Thèmes</a></li>
                    </ul>
                  </li>
              @endif
              @if(\Auth::User()->user_group_id == 3)
                  <!-- <li><a><span class="link_planning"><i class="fa fa-calendar-check-o"></i> Planning / Relance</span></a></li> -->
              @endif
              </ul>
              </div>
            </div>

            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->

            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $user->email ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>

                  <ul class="dropdown-menu dropdown-usermenu pull-right">

                    <li><a href="/deconnexion"><i class="fa fa-sign-out pull-right"></i> Déconnexion </a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <div class="right_col" role="main">

          @yield('content')

          <div><a id="cRetour" class="cInvisible" href="#haut"></a></div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">

            © TorDMP - 2017

            <script>
            document.addEventListener('DOMContentLoaded', function() {
              window.onscroll = function(ev) {
                document.getElementById("cRetour").className = (window.pageYOffset > 100) ? "cVisible" : "cInvisible";
              };
            });
            </script>


          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


    <!-- Bootstrap -->
    <script src="{{url('/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{url('/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{url('/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{url('/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{url('/vendors/gauge.js/dist/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{url('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{url('/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{url('/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{url('/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{url('/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{url('/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{url('/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{url('/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{url('/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{url('/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{url('/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{url('/vendors/DateJS/build/date.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{url('/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{url('/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{url('/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{url('/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{url('/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <!-- <script src="{{url('/vendors/custom.min.js')}}"></script> -->
    <script src="{{url('/vendors/custom2.js')}}"></script>
    <!--JQuery UI-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

    $('.link_base').click(function(){
     window.location.href='/base';
   });
   $('.link_sender').click(function(){
    window.location.href='/sender';
    });
    $('.link_campagne').click(function(){
     window.location.href='/campagne';
   });
   $('.link_planning').click(function(){
    window.location.href='/planning';
  });

  $('.link_user').click(function(){
     window.location.href='/user';
   });

   $('.link_tool').click(function(){
    window.location.href='/outils/listemanager';
  });

  $('.link_fai').click(function(){
   window.location.href='/fai';
  });

  $('.link_admin').click(function(){
   window.location.href='/admin';
  });

  $('.link_dmp').click(function(){
   window.location.href='/';
  });

  $('.link_dmpManagement').click(function(){
   window.location.href= '/admin/management';
  });

  $('.link_dmpPartners').click(function(){
   window.location.href= '/admin/annonceur';
  });

  $('.link_dmpAddresses').click(function(){
   window.location.href= '/admin/address';
  });

  $('.link_dmpPlannings').click(function(){
   window.location.href= '/admin/planning';
  });

    </script>

  </body>
</html>
