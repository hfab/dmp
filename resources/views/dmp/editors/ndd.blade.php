@extends('template')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">

      <h2>Statistiques de vos domaines </h2>


       <table class="table table-striped table-hover table-bordered">
           <tr>
               <th>Domaines</th>
               <th>Nombre de hit max (sécurité)</th>
               <th>Hits actuels</th>

           </tr>
           @foreach($ndd as $n)
           <tr>
               <td class="col-xs-1">{{$n->ndd}}</td>
               <td class="col-xs-1">{{$n->max_hit}}</td>
               <td class="col-xs-1">{{$n->hit}}</td>
           </tr>
           @endforeach
       </table>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

  <h2>Etat des demandes de nouveau domaines</h2>

   <table class="table table-striped table-hover table-bordered">
       <tr>
           <th>Nom de domaines</th>
           <th>Etat</th>

       </tr>
       @foreach($ndd as $n)
       <tr>
           <td>{{$n->ndd}}</td>
           <td>{{$n->status}}</td>
       </tr>
       @endforeach
   </table>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

  <h2>Etat des demandes de nouveau domaines</h2>

   <!--- form ajout domain-->
</div>

</div>
@endsection
