@extends('template')

@section('content')

    <h4>Statistique de {!!$editorName!!}</h4>
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <span>Nombre d'adresses ayant matché avec notre base de données</span>
            <canvas id="mailMatched"></canvas>
        </div>
        <div class="col-sm-6">
            <span>Nombre de Cookies déposées chez le destinataire</span>
            <canvas id="cookies"></canvas>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <span>Nombre de hits sur les noms de domaines</span>
            <canvas id="hitsndd"></canvas>
        </div>
        <div class="col-sm-6">
            <span>Nombre de match sur les sites partneraires</span>
            <canvas id="sitepartners"></canvas>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-6">
            <span>Nombre de relanch (global dev)</span>
            <canvas id="relauncheday"></canvas>
        </div>
        <div class="col-sm-6">
          *** debug
        </div>

    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <script>

        var colorArray,label_array,data_array;
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        <!--  -->
        function createGraph(typeGraph,domElement,labelArray,colorArray,dataArray,labelName){
            return new Chart(domElement,{
                type: typeGraph,
                data: {
                    labels:labelArray,
                    datasets: [{
                        label: labelName,
                        backgroundColor: getRandomColor(),
                        data: dataArray,
                    }],
                },
            });
        }

        label_array = [];
        data_array = [];
        colorArray = [];
        //Nombre de match
        @foreach($matched as $mail)
            label_array.push("{!! $mail->date !!}");
            data_array.push({{$mail->number}});
            colorArray.push(getRandomColor());
        @endforeach

        var mailsMatched = document.getElementById('mailMatched').getContext('2d');
        createGraph("line",mailsMatched,label_array,colorArray,data_array,"Période du {{$dateIn}} au {{$dateOut}}");

        label_array2 = [];
        data_array2 = [];
        colorArray2 = [];
        //Nombre de cookies déposées
        @foreach($cookie as $pixel)
            label_array2.push("{!! $pixel->date !!}");
            data_array2.push({{$pixel->number}});
            colorArray2.push(getRandomColor());
        @endforeach

        var cookiesDeposit = document.getElementById('cookies').getContext('2d');
        createGraph("line",cookiesDeposit,label_array2,colorArray2,data_array2,"Période du {{$dateIn}} au {{$dateOut}}");


        label_array3 = [];
        data_array3 = [];
        colorArray3 = [];

        @foreach($ndd as $n)
            label_array3.push("{!! $n->ndd !!}");
            data_array3.push({{$n->hit}});
            colorArray3.push(getRandomColor());
        @endforeach

        var hitsndd = document.getElementById('hitsndd').getContext('2d');
        createGraph("horizontalBar",hitsndd,label_array3,colorArray3,data_array3,"Hits totaux");

        label_array4 = [];
        data_array4 = [];
        colorArray4 = [];

        @foreach($hitbysitebyeditor as $hit)
            label_array4.push("{!! $hit['name'] !!}");
            data_array4.push({{$hit['count']}});
            colorArray4.push(getRandomColor());
        @endforeach

        var sitepartners = document.getElementById('sitepartners').getContext('2d');
        createGraph("horizontalBar",sitepartners,label_array4,colorArray4,data_array4,"Match sur les sites partenaires");

        label_array5 = [];
        data_array5 = [];
        colorArray5 = [];

         @foreach($relaunch as $rla)

             label_array5.push("{!! $rla->date !!}");
             data_array5.push("{!! $rla->number !!}");
             colorArray5.push(getRandomColor());

         @endforeach

         var relauncheday = document.getElementById('relauncheday').getContext('2d');
         createGraph("line",relauncheday,label_array5,colorArray5,data_array5,"Nombre de relances par jour");


    </script>

@endsection
