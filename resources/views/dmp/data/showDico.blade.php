@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Home <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-xs-12 table-responsive">
                         <table class="table table-bordered table-striped table-hover table-condensed">
                              <tr>
                                   <th>ID</th>
                                   <th>Mail</th>
                                   <th>HASH</th>
                                   <th>Source</th>
                              </tr>

                              @foreach($dico as $d)
                                   <tr>
                                        <td>{{ $d->id }}</td>
                                        <td>{{ $d->mail }}</td>
                                        <td>{{ $d->hash }}</td>
                                        <td>{{ $d->source }}</td>

                                   </tr>
                              @endforeach
                         </table>
                         <div class="col-xs-12 text-center">
                              {!! $dico->render() !!}
                         </div>


                  </div>


      </div>
      </div>

@endsection
