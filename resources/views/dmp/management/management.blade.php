@extends('template')

@section('content')
<h4>La dmp relance des campagnes toutes les {{$relance or '2h'}}</h4>
<a class="btn btn-warning">Modifier</a>
{{--Ajouter la variable lorsque ce sera prêt et l'input--}}
<table class="table table-striped table-hover">
	<tr>
		<th>Nom</th>
		<th>Quota relance DMP</th>
		<th>Action</th>
	</tr>
	@foreach($fais as $fai)
	<tr>
		<td>{{$fai->nom}}</td>
		<td id="dmp_pression--{{$fai->id}}--">{{$fai->dmp_pression}}</td>
		<td>
			<input type="text" id="newQuota[{{$fai->id}}]">
			<a id="update--{{$fai->id}}--" class="btn btn-warning btn-modif">Modifier</a>
		</td>
	</tr>
	@endforeach
</table>

<script>

$(document).ready(function(){
	$.ajaxSetup({
   		 headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  		  }
	});
	$('.btn-modif').click(function(){
		var id = $(this).attr('id').split('--')[1];
		var quota = document.getElementById('newQuota['+id+']').value;
		console.log("Id:"+id);
		console.log("Quota:"+quota);
		if(quota == null || quota != '' || quota == 'undefined'){
			$.post("/admin/fais/"+id+"/quota_pression",{"newQuota": quota},function(data){
				var response = JSON.parse(data);
				if(response.status == "Ok"){
					toastr['success'](response.message);
					document.getElementById('dmp_pression--'+id+'--').innerHTML = quota;
					return;
				}
				toastr['error'](response.message);
				return;
			});
		}
	});
});
</script>
@endsection
