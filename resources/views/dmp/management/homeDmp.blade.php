@extends('template')

@section('content')
<h4>Nombre d'adresses relancées aujourd'hui :{{$nbUsers}} </h4>
<h4>Nombre de campagnes relancées aujourd'hui: {{$nbPlanningsRelaunched}}</h4>

<table class="table table-striped table-hover">
<tr>
	<th>Base</th>
	<th>Nom</th>
	<th>Référence</th>
	<th>Date de création</th>
	<th>Thème</th>
	<th>Volume Relancé</th>
</tr>
@foreach($plannings as $planning )
<tr>
	<td>{{$planning->campagne->base->nom}}</td>
	<td>{{$planning->campagne->nom}}</td>
	<td>{{$planning->campagne->ref}}</td>
	<td>{{$planning->created_at}}</td>
	<td>{{$planning->campagne->theme->nom}}</td>
	<td>{{$planning->volume_selected}}</td>
</tr>
@endforeach
</table>
<script>
</script>
@endsection
