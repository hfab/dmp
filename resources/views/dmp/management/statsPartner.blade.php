@extends('template')

@section('content')

	<div class="row">
		  <div class="col-sm-6">
				<span>Nombre de campagnes lancées par sites partenaires </span>
				<canvas id="campaignLaunched"></canvas>
          </div>
		  <div class="col-sm-6">
				<span>Nombre de mails relancées par campagnes</span>
				<canvas id="mailsRelaunched"></canvas>
		  </div>
	</div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>


<script>

function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

var colorArray,label_array,data_array;

var campaignsLaunched = document.getElementById('campaignLaunched').getContext('2d');
var mailsRelaunched  = document.getElementById('mailsRelaunched').getContext('2d');
var datasetsCampaign = [];
var datasetsMail = [];

@foreach($partners as $partner)
    var data = [];
    datasetsCampaign.push({
                            label:"{!! $partner->name!!}",
                            backgroundColor:getRandomColor(),
                            data:{{$partner->nbCampaigns}}
                           });
@endforeach
@foreach($campaigns as $campaign)

    datasetsMail.push({
        label:"{!!$campaign->nom!!}",
         backgroundColor:getRandomColor(),
        data:{{$campaign->number}}
        });
@endforeach

//Campagnes relancées
new Chart(campaignsLaunched,{
            type: 'horizontalBar',
            data: {
                    labels:["Période du {!! $dateIn !!} au {!!$dateOut!!}"],
                    datasets: datasetsCampaign,
            },
    });

//Mails relancées par campagne
new Chart(mailsRelaunched,{
            type: 'bar',
            data: {
                    labels:["Période du {!! $dateIn !!} au {!!$dateOut!!}"],
                    datasets: datasetsMail,
            },
    });

</script>
@endsection
