@extends('template')

@section('content')

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="x_title">
							<h4>Liste des Editeurs Partenaires<small></small></h4>

							<div class="clearfix"></div>
						</div>
						<div class="x_content">

							<div class="row">

							<hr>
							<a href="/admin/editor/create" class="btn btn-success" id="validPartner"><i class="glyphicon glyphicon-plus"></i></a>
							@if(!empty($editors))
							<table class="table table-hover table-striped">
								<tr>
									<th>Editeur</th>
									<th>Nom de domaine</th>
									<th>Nombre d'utilisateur rattaché à cet éditeur</th>
								</tr>
								@foreach($editors as $editor)
									<tr>
										<td>{!!$editor->nom !!}</td>
										<td>{!!$editor->domains!!}</td>
										<td>{!!$editor->number!!}</td>
									</tr>
									@endforeach
							@endif
						</table>
					</div>
				</div>
			</div>
		</div>
@endsection
