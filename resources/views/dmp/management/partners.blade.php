@extends('template')

@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="x_title">
							<h4>Liste des Annonceurs Partenaires<small></small></h4>

							<div class="clearfix"></div>
						</div>
						<div class="x_content">

							<div class="row">

	<div class="col-sm-8">
								<div class="form-group float-label-control">
										<label for="">Nom</label>
										<input class="form-control" type="text" name="namePartnerSite" id="namePartner" placeholder="Nom annonceur">
								</div>
								<div class="form-group float-label-control">
										<label for="">Domaine</label>
										<input class="form-control" type="text" name="partnerSite" id="partner" placeholder="Domaine">
								</div>
								<div class="form-group float-label-control">
										<label for="">Sous-Domaine</label>
										<input class="form-control" type="text" name="subPartner" id="subPartner" placeholder="Sous-domaine">
								</div>
								<div class="form-group float-label-control">
										<label for="">Themes: </label>
                                        <select id="partnerTheme">
                                            @foreach($themes as $theme)
                                                <option value="{{$theme->id}}">{!!$theme->nom!!}</option>
                                            @endforeach
                                        </select>
								</div>
								<button class="btn btn-success" id="validPartner">Ajouter</button>
							</div>


</div>
@if(!empty($partners))
<table class="table table-hover table-striped" id="listPartner">
	<tr>
		<th>Nom domaine</th>
		<th>Domaine</th>
		<th>Sous-domaine</th>
        <th>Thème</th>
		<td>Actions</td>
	</tr>
	@foreach($partners as $partner)
		<tr id="partnerNb--{{$partner->id}}--">
			<td id="namePartner--{{$partner->id}}--">{{$partner->name}}</td>
			<td id="domain--{{$partner->id}}--">{{$partner->url}}</td>
			<td id="subDomain--{{$partner->id}}--">{{$partner->subDomain}}</td>
            <td id="partnerTheme--{{$partner->id}}--">{!!$partner->theme->nom or 'aucun'!!}</td>
			<td id="actionButtonsPartner--{{$partner->id}}--">
			<div class="actionButtons">
			   <button type="button" class="btn btn-danger partnerDelete" data-id="{{$partner->id}}"><i class="glyphicon glyphicon-remove"></i></button>
			   <button class="btn btn-info partnerModif" data-id="{{$partner->id}}"><i class="glyphicon glyphicon-pencil"></i></button>
			</div>
			</td>
		</tr>
		@endforeach
@endif
</table>


</div>
</div>
</div>

<script>

var domain,subDomain,nameDomain,id;
var domainEditing = false;
function stopUpdate(){
	$('.newDomain').fadeOut();
	$('.newSubDomain').fadeOut();
	$('.newNameDomain').fadeOut();
	$('.actionButtons').fadeIn();
	$('.stopUpdate').fadeOut();
	$('.confUpdate').fadeOut();
	document.getElementById('domain--'+id+'--').innerHTML = domain;
	document.getElementById('subDomain--'+id+'--').innerHTML = subDomain;
	document.getElementById('newDomain--'+id+'--').innerHTML = nameDomain;
	domainEditing = false;
}

function deletePartner(){
	var id = this.getAttribute('data-id');
	if(id != null || id != ""){
		$.ajax({
		type:"DELETE",
			url:"/admin/annonceur/delete",
			data: {"id":id},
			success: function(data){
			}
	}).done(function(data){
		$('#partnerNb--'+id+'--').fadeOut();
	});
	}
}

function addPartner(){
    var partnerName = document.getElementById('namePartner').value;
	var partnerInput =  document.getElementById('partner').value;
	var subPartner = document.getElementById('subPartner').value;
    var select = document.getElementById('partnerTheme');
    var partnerTheme =  select.options[select.selectedIndex].value
	if(partner != null || partner != ""){
		$.post('/admin/annonceur/add',{"partnerName":partnerName,"partnerSite":partnerInput,"subPartner":subPartner,"partnerTheme":partnerTheme},function(data){
			var response = JSON.parse(data);
			if(response.status == "Ok"){
				toastr['success'](response.message);
				//insertTr(response.id,response.url,response.subDomain);
				setTimeout(function(){
					location.reload();
				}, 500);
				return;
            }
			toastr['error'](response.message);
		});
		return;
	}
}

function firstStepUpdate(){
	if(!domainEditing){
		id = this.getAttribute('data-id');
        var tdNameDomain = document.getElementById('namePartner--'+id+'--');
		var tdDomain = document.getElementById('domain--'+id+'--');
		var tdSubDomain = document.getElementById('subDomain--'+id+'--');
		domain = tdDomain.innerHTML;
		subDomain = tdSubDomain.innerHTML;
        nameDomain = tdNameDomain.innerHTML;
		tdDomain.innerHTML = tdNameDomain.innerHTML = tdSubDomain.innerHTML = "";
		//On crée de nouveaux inputs
		var domainInput = document.createElement('input');
		var subDomainInput = document.createElement('input');
		var domainNameInput = document.createElement('input');
		//On crée de nouveaux boutons
		var confModif = document.createElement('button');
		var stopModif = document.createElement('button');
		//On remet l'ancien valeur des td
		domainInput.value = domain;
		subDomainInput.value = subDomain;
        domainNameInput.value =nameDomain;
		//Configuration inputs
		domainInput.className = "newDomain";
		domainInput.name = "newDomainInput";
		subDomainInput.className = "newSubDomain";
		subDomainInput.name = "newSubDomainInput";
        domainNameInput.className ="newNameDomain";
        domainNameInput.name = "newNameDomainInput";
		//On les met dans la bonne colonne
		tdDomain.appendChild(domainInput);
		tdSubDomain.appendChild(subDomainInput);
        tdNameDomain.appendChild(domainNameInput);
		//Configuration button confirmation update
		confModif.classList.add("btn");
		confModif.classList.add("btn-success");
		confModif.classList.add("confUpdate");
		//Configuration button annulation
		stopModif.classList.add("btn");
		stopModif.classList.add("btn-warning");
		stopModif.classList.add("stopUpdate");
		confModif.innerHTML = '<i class="glyphicon glyphicon-ok-sign"></i>';
		stopModif.innerHTML = '<i class="glyphicon glyphicon-remove-circle"></i>';
		stopModif.addEventListener("click",stopUpdate,false);
		confModif.addEventListener("click",validUpdate,false);

		document.getElementById('actionButtonsPartner--'+id+'--').appendChild(confModif);
		document.getElementById('actionButtonsPartner--'+id+'--').appendChild(stopModif);
		domainEditing = true;
		$('.actionButtons').fadeOut();
	}
}

function validUpdate(){
	var newDomain = document.getElementsByClassName('newDomain')[0].value;
	var newNameDomain = document.getElementsByClassName('newNameDomain')[0].value;
	var newSubDomain = document.getElementsByClassName('newSubDomain')[0].value;
    console.log("Domaine nom:"+newNameDomain);
	if(newDomain != null && newDomain != "" || newNameDomain != null && newNameDomain != "" ){
		$.ajax({
		type:"PUT",
			url:"/admin/annonceur/"+id+"/update",
			data:{"newDomain":newDomain,"newSubDomain":newSubDomain,"newNameDomain":newNameDomain},
			success:function(data){
			}
		}).done(function(data){
			$('.newDomain').fadeOut();
			$('.newSubDomain').fadeOut();
            $('.newNameDomain').fadeOut();
			$('.actionButtons').fadeIn();
			$('.stopUpdate').fadeOut();
			$('.confUpdate').fadeOut();
			document.getElementById('domain--'+id+'--').innerHTML = newDomain;
			document.getElementById('subDomain--'+id+'--').innerHTML = newSubDomain;
			document.getElementById('namePartner--'+id+'--').innerHTML = newNameDomain;

			domainEditing = false;
		});
	}
}

function insertTr(idElem,domainValue,subDomainValue){
	var table = document.getElementById("listPartner");
	var row = table.insertRow(table.rows.length);
	console.log(table.rows.length);
	var domainTd = row.insertCell(0);
	var subDomainTd = row.insertCell(1);
	var buttonsTd = row.insertCell(2);
	domainTd.innerHTML = domainValue;
	subDomainTd.innerHTML = subDomainValue;
	buttonsTd.innerHTML =' <div class="actionButtons">'+
		'<button type="button" class="btn btn-danger partnerDelete" data-id="'+idElem+'"><i class="glyphicon glyphicon-remove"></i></button>'+
		'<button class="btn btn-info partnerModif" data-id="'+idElem+'"><i class="glyphicon glyphicon-pencil"></i></button>'+
		'</div>'
		;
	domainTd.setAttribute("id","domain--"+idElem+"--");
	subDomainTd.setAttribute("id","subDomain--"+idElem+"--");
	buttonsTd.setAttribute("id","actionsButtonsPartner--"+idElem+"--");
}


$(document).ready(function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('#validPartner').click(addPartner);
	$('.partnerDelete').click(deletePartner);
	$('.partnerModif').click(firstStepUpdate);
});
</script>
@endsection
