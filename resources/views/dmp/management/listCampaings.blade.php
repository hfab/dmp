@extends('template')

@section('content')



      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Campagne <small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">
                        <div class="span8">
                        <button type="submit" class="btn btn-default">Rechercher</button>
                            <form method>
                                <p>
                                    <select id="base_id" name="base_id" class="form-control input-medium" onchange='filterByBase();'>
                                        <option>Filtrer par partenaire </option>
                                        @foreach ($partners as $partner)
                                            <option value="{{$partner->id}}">{{$partner->name}}</option>
                                        @endforeach
                                    </select>
                                </p>
                            </form>
                    </div>
                    </div>
                    <div class="row">
                      <br />
                    </div>

                    <div style="overflow-x:auto;">
                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th>Site Partenaire</th>
                              <th>Nom</th>
                              <th>Référence</th>
                              <th>Planifications</th>
                              <th>Date de création</th>
                              <th>Actions</th>
                          </tr>
                          @foreach($campagnes as $campagne)
                          <tr>
                              <td class="col-xs-1">@if($campagne->dmp_partnersite_id == 0) Inter-Toto @else {{$campagne->partner->name}} @endif</td>
                              <td class="col-xs-1">{{$campagne->nom}}</td>
                              <td class="col-xs-1">{{$campagne->ref}}</td>
                              <td class="col-xs-1">{{$campagne->plannings->count()}}</td>

                              <td class="col-xs-1">{{$campagne->created_at}}</td>
                              <td class="col-xs-2">
                                  <a title="Editer" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                  <!-- <a title="Statistiques" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/stats"><i class="fa fa-area-chart" aria-hidden="true"></i></a> -->
                                  <a title="Plannifier" class="btn btn-primary btn-sm" href="/campagne/{{$campagne->id}}/planning"><i class="fa fa-clock-o" aria-hidden="true"></i></a>
                                  <a title="Dupliquer" id= 'c{{$campagne->id}}' class="btn btn-success btn-sm" href="/campagne/{{$campagne->id}}/dupliquercampagne"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                              </td>

                          </tr>
                          @endforeach
                      </table>
                    </div>
                  </div>


      </div>
      </div>

<script>
function filterByPartner()
{

}
</script>


@endsection
