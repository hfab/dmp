@extends('template')

@section('content')

    <h4>Statistiques des sites partenaires</h4>
    <hr>
    <div class="row">

    <div class="row">
        <div class="col-sm-6">
            <span>Nombre de hits sur les sites partenaires</span>
            <canvas id="hitsbynddpartner"></canvas>
        </div>
        <div class="col-sm-6">

        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <script>

        var colorArray,label_array,data_array;
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

var period = [];

@foreach($period as $time)
    period.push('{!!$time!!}');
@endforeach

//Nb de cookies déposées chez le destinataire par éditeur
var hitCounter = document.getElementById('hitsbynddpartner').getContext('2d');
var datasetsHitCounter = [];
var dataHitCounter = [];
    <?php $indexName = 0; ?>

    @foreach($nbHit as $arrayresult)
      @foreach($arrayresult as $kndd => $records)

        @for ($index=0;$index<30;$index++)
                dataHitCounter.push({{$records[$index]}});
        @endfor
        datasetsHitCounter.push({ label:"{!!$kndd!!}",backgroundColor:getRandomColor(),data:dataHitCounter});
        dataHitCounter = [];
      @endforeach

    @endforeach

    new Chart(hitCounter,{
        type: "line",
        data: {
            labels:period,
            datasets: datasetsHitCounter,
        },
    });


</script>

@endsection
