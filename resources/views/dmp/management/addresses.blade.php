@extends('template')

@section('content')

<h4>Bases DMP en fonction du thème</h4>
<table class="table table-striped table-hover">
	<tr>
		<th>Nom</th>
		<th>Destinataires</th>
		<th>Action</th>
	</tr>
	@foreach($dmpBases as $base)
		<tr>
			<td>{{$base->name}}</td>
			<td>{{$base->nbDestinataires}}</td>
			<td>
                <form action="/admin/{{$base->theme_id}}/export" method="POST">
                    {{csrf_field()}}
                <button class="btn btn-info getAddr" data-theme="{{$base->theme_id}}">
                    Récupérer les adresses
                </button>
                </form>
            </td>
		</tr>
	@endforeach
</table>

<script>
</script>
@endsection
