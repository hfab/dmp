@extends('template')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_title">
                <h2>Ajouter un domaine <small></small></h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-8">
                        <h4 class="page-header">Informations domaine</h4>
                        <div class="form-group float-label-control">
                            <label for="">Nom de domaine</label>
                            <input type="text" name="nddName" id="nddName" class="form-control" placeholder="Nom du domaine à ajouter">
                        </div>
                        <button class="btn btn-success" id="validNdd" >Valider</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(!empty($listNdd))
        <table class="table table-hover table-striped">
            <tr>
                <th> Nom de domaine </th>
                <th> Status </th>
            </tr>
            @foreach($listNdd as $ndd)
                <tr>
                    <td>{!! $ndd->ndd !!}</td>
                    <td>{!! $ndd->status or "en création"!!}</td>
                </tr>
            @endforeach
        </table>
    @endif
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

function getResult(data)
{
    var response = JSON.parse(data);
    if(response.status !== "Ok"){
        toastr['error'](response.message);
        $('#validNdd').fadeIn();
        return;
    }
    toastr['success'](response.message);
    $('#validNdd').fadeIn();
    location.reload();
    return;
}

$('#validNdd').click(function(){
    var nddName = document.getElementById('nddName').value;
    $(this).fadeOut();
    console.log(nddName);
    if(nddName.length === "" || nddName.length === undefined){
        toastr['error']('L\'un des champs est vide');
        $('#validNdd').fadeIn();
        return;
    }
    $.post('/admin/ndd/create',{'nddName':nddName},getResult);
});
    </script>
@endsection
