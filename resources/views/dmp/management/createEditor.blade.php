@extends('template')

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_title">
                <h2>Ajouter un editeur <small></small></h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div class="row">
                    <div class="col-sm-8">

                        <h4 class="page-header">Informations Editeur</h4>

                        <div class="form-group float-label-control">
                            <label for="">Nom</label>
                            <input type="text" name="editor" id="editor" class="form-control" placeholder="Nom de l'editeur">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">Domaines</label>
                            <input type="text" name="editorDomains" id="editorDomains" class="form-control" placeholder="Entrez les noms de domaines et séparez chacun de ces domaines par un |">
                            <button class="btn btn-info" id="addNdd"><i class="fa fa-plus"></i></button>
                        </div>
                        <div id="listNdd"><div>
                                <button class="btn btn-success" id="validEditor" style="display:none;">Valider</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <script>
                $(document).ready(function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var arrayNdd = [];
                    $('#addNdd').click(function(){
                        var list = document.getElementById('listNdd');
                        var newNdd = document.getElementById('editorDomains').value;
                        var found = false;
                        if(arrayNdd.length == 0){
                            arrayNdd.push(newNdd);
                            var div = document.createElement('div');
                            div.className = 'btn btn-warning deleteNdd';
                            div.innerHTML = newNdd;
                            list.appendChild(div);
                            found = true;
                        }
                        else{
                            arrayNdd.forEach(function(element){
                                if(element == newNdd){
                                    toastr['error']('Nom de domaine déjà crée');
                                    found = true;
                                    document.getElementById('editorDomains').value = "";
                                }
                            });
                        }
                        if(!found){
                            arrayNdd.push(newNdd);
                            var div = document.createElement('div');
                            div.className = 'btn btn-warning deleteNdd';
                            div.append(newNdd);
                            list.appendChild(div);
                        }
                        div.addEventListener("click",deleteElement,false);
                        console.log(arrayNdd);
                        $('#validEditor').fadeIn();
                    });
                    function deleteElement(){
                        var indexTab,indexElementToDelete;
                        var nddToDelete = this.innerHTML;
                        arrayNdd.forEach(function(element){
                            indexTab++;
                            if(element == nddToDelete){
                                indexElementToDelete = indexTab;
                            }
                        });
                        if(indexElementToDelete != undefined){
                            arrayNdd.splice(indexElementToDelete,1);
                            toastr['success']('Nom de domaine supprimé');
                            $(this).fadeOut();
                        }
                        if(arrayNdd.length == 0 || arrayNdd == undefined){
                           $('#validEditor').fadeOut();
                           arrayNdd = [];
                        }
                    };

                    $('#validEditor').click(function(){
                        var editor =  document.getElementById('editor').value;
                        var response = {message :"Erreur: Impossible d'enregister cet éditeur."};
                        if(editor != null || editor != "")
                        {
                            $.post('/admin/editor/add',{"editor":editor,"editorDomains":arrayNdd},function(data){
                                var response = JSON.parse(data);
                                if(response.status == "Ok"){
                                    toastr['success'](response.message);
                                    window.location.href = window.location.origin+"/admin/editor";
                                    return;
                                }
                                toastr['error'](response.message);
                                return;
                            });
                        }
                        console.log("Empty");
                    });
                });
            </script>
        @endsection
