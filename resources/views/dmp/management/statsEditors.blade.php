@extends('template')

@section('content')

    <h4>Dernières relancées</h4>
    <hr>
    <div class="row">

        <div class="col-sm-6">
            <span>Nombre d'adresses ayant matché avec notre base de données</span>
            <canvas id="mailMatched"></canvas>
        </div>
        <div class="col-sm-6">
            <span>Nombre de Cookies déposées chez le destinataire</span>
            <canvas id="cookies"></canvas>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <script>

        var colorArray,label_array,data_array;
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

//Nb de mail matched par éditeur
var mailsMatched = document.getElementById('mailMatched').getContext('2d');
var period = [];
var datasetsMatch = [];
var dataMatched = [];
@foreach($period as $time)
    period.push('{!!$time!!}');
@endforeach
    <?php $indexName = 0; ?>
@foreach($editorsName as $editor)
    @for ($index=0;$index<30;$index++)
            dataMatched.push({{$matched[$indexName][$editor][$index]}});
    @endfor
    <?php $indexName++; ?>
    datasetsMatch.push({ label:"{!!$editor!!}",backgroundColor:getRandomColor(),data:dataMatched});
    dataMatched = [];
@endforeach
new Chart(mailsMatched,{
    type: "line",
    data: {
        labels:period,
        datasets: datasetsMatch,
    },
});

//Nb de cookies déposées chez le destinataire par éditeur
var cookiesDeposit = document.getElementById('cookies').getContext('2d');
var datasetsCookie = [];
var dataCookies = [];
    <?php $indexName = 0; ?>
@foreach($editorsName as $editor)
    @for ($index=0;$index<30;$index++)
            dataCookies.push({{$cookies[$indexName][$editor][$index]}});
    @endfor
    <?php $indexName++; ?>
    datasetsCookie.push({ label:"{!!$editor!!}",backgroundColor:getRandomColor(),data:dataCookies});
    dataCookies = [];
@endforeach

new Chart(cookiesDeposit,{
    type: "line",
    data: {
        labels:period,
        datasets: datasetsCookie,
    },
});


</script>

@endsection
