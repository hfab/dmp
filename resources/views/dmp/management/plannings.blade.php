@extends('template')

@section('content')

<h4>Dernières relancées</h4>
<hr>
	<table class="table table-striped table-hover">
			<tr>
				<th>Id</th>
				<th>Base</th>
				<th>Campagne(référence)</th>
				<th>Volume sélectionné</th>
				<th>Routeur</th>
				<th>Date de relance</th>
				<th>Thème</th>
			</tr>

		@foreach($plannings as $planning)
			<tr>
				<td>{{$planning->id}}</td>
				<td>{{$planning->campagne->base->nom}}</td>
				<td>{{$planning->campagne->ref}}</td>
				<td>{{$planning->volume_selected}}</td>
				<td>{{$planning->routeur->nom}}</td>
				<td>{{$planning->sent_at}}</td>
				<th>{{$planning->campagne->theme->nom}}</th>
			</tr>
		@endforeach
	</table>


	<div class="row">
		  <div class="col-sm-6">
				<span>Thèmes relancés ce mois</span>
				<canvas id="myChart"></canvas>
			</div>
		  <div class="col-sm-6">
				<span>Nombre de mails uniques relancés</span>
				<canvas id="mailsRelaunched"></canvas>
		  </div>
		  <hr>
		  <div class="col-sm-6">
				<span>Pression</span>
				<canvas id="pressionGraph"></canvas>
		  </div>
		  <div class="col-sm-6">
				<span>Nombre de campagne par base</span>
				<canvas id="campaignBase"></canvas>
		  </div>
		  <div class="col-sm-6">
				<span>Nombre de mails relancés par campagnes</span>
				<canvas id="mailsCampaigns"></canvas>
		  </div>
			 <div class="col-sm-12">
					<span>Nombre de mails matched par bases</span>
				<canvas id="mailsMatchedBases"></canvas>
		  </div>
	</div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>


<script>

function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

function createGraph(typeGraph,domElement,labelArray,colorArray,dataArray){
	return new Chart(domElement,{
	type: typeGraph,
		data: {
		labels:labelArray,
			datasets: [{
			label: "Période du {{$a_date['in']}} au {{$a_date['out']}}",
				backgroundColor: colorArray,
				data: data_array,
}],
},
});
}

var colorArray,label_array,data_array;

label_array = [];
data_array = [];
colorArray = [];
//Nombre d'adresses relancées par thèmes dans la période.
@foreach($a_themeVolume as $key => $value)
	label_array.push("{!! $a_themeVolume[$key]['name'] !!}");
data_array.push({{$a_themeVolume[$key]['count']}});
colorArray.push(getRandomColor());
@endforeach

	var ctx = document.getElementById('myChart').getContext('2d');
var chartAddressesThemes = createGraph("horizontalBar",ctx,label_array,colorArray,data_array);
//Pression
label_array = [];
data_array = [];
colorArray = [];
@foreach($countPression as $key => $value)
	label_array.push('Pression:{{$key}} ');
data_array.push('{{$countPression[$key][0]}}');
colorArray.push(getRandomColor());
@endforeach

	var ctx2 = document.getElementById('pressionGraph').getContext('2d');
var chartPression = createGraph("doughnut",ctx2,label_array,colorArray,data_array);

//Mails Relaunched
label_array = [];
data_array = [];
colorArray = [];
@foreach($addresses as $addresse)
	label_array.push('{{$addresse->nom}}');
data_array.push({{$addresse->number}});
colorArray.push(getRandomColor());
@endforeach

	var graphMailRelaunched = document.getElementById('mailsRelaunched').getContext('2d');
var chartMailsRelaunchedByBase = createGraph("doughnut",graphMailRelaunched,label_array,colorArray,data_array);

//Campagnes relancées par Base
label_array = [];
data_array = [];
colorArray = [];
@foreach($campaigns as $campaign)
	label_array.push("{!! $campaign->nom !!}");
data_array.push({{$campaign->number}});
colorArray.push(getRandomColor());
@endforeach

	var graphCampaignBase = document.getElementById('campaignBase').getContext('2d');
var chartCampaignBase = createGraph("horizontalBar",graphCampaignBase,label_array,colorArray,data_array);

//Mails relancés par campagne
label_array = [];
data_array = [];
colorArray = [];
@foreach($mailsCampaign as $campaign)
	label_array.push("{!! $campaign->nom !!}");
data_array.push({{$campaign->number}});
colorArray.push(getRandomColor());
@endforeach
	var graphMailsCampaign = document.getElementById('mailsCampaigns').getContext('2d');
var chartMailsCampaign = createGraph("bar",graphMailsCampaign,label_array,colorArray,data_array);

//Mails matched par bases
label_array = [];
data_array = [];
colorArray = [];
@foreach($nbMailBases as $nbMailBase)
	label_array.push("{!! $nbMailBase->nom !!}");
data_array.push({{$nbMailBase->number}});
colorArray.push(getRandomColor());
@endforeach
	var mailsMatchedBases = document.getElementById('mailsMatchedBases').getContext('2d');
var chartMailsCampaign = createGraph("bar",mailsMatchedBases,label_array,colorArray,data_array);

</script>
@endsection
