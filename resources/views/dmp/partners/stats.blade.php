@extends('template')

@section('content')

    <h4>Dernières Addresses relancées</h4>
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <span>Nombre de mails relancés par campagnes</span>
            <canvas id="mailsCampaigns"></canvas>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <script>

        var colorArray,label_array,data_array;
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function createGraph(typeGraph,domElement,labelArray,colorArray,dataArray){
            return new Chart(domElement,{
                type: typeGraph,
                data: {
                    labels:labelArray,
                    datasets: [{
                        label: "Période du {{$dateIn}} au {{$dateOut}}",
                        backgroundColor: colorArray,
                        data: data_array,
                    }],
                },
            });
        }

        label_array = [];
        data_array = [];
        colorArray = [];
        //Nombre de @ par campagne
        @if(!empty($mails))
        @foreach($mails as $mail)
            label_array.push("{!! $mail->nom !!}");
            data_array.push({{$mail->number}});
            colorArray.push(getRandomColor());
        @endforeach
        @endif

        var mailsMatchedCampaign = document.getElementById('mailsCampaigns').getContext('2d');
        createGraph("bar",mailsMatchedCampaign,label_array,colorArray,data_array);

    </script>

@endsection
