@extends('template')
@section('content')
    <h4>Script à mettre sur toutes les pages</h4>
    <textarea rows="5" style="width:35%;">{{$importScript}}</textarea>
    <h4>Script à mettre sur la page de confirmation d'achat de l'utilisateur</h4>
    <textarea rows="5" style="width:30%;">{{$scriptConfPage}}</textarea>
@endsection
