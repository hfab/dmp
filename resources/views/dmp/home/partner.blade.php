@extends('template')

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_title">
                <h2>Home <small></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <!-- Content home dmp (selon admin / editor a voir) -->
                <div class="row">
                    <div class="col-md-1">
                        <a href="/annonceur/stats">
                            <img width="100%" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU4IDU4IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1OCA1ODsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+CjxnPgoJPHBhdGggc3R5bGU9ImZpbGw6I0YwNzg1QTsiIGQ9Ik0zMSwyNi45NjJoMjYuOTI0QzU2Ljk0LDEyLjU0MSw0NS40MjEsMS4wMjIsMzEsMC4wMzhWMjYuOTYyeiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0YwQzQxOTsiIGQ9Ik01MC4zODYsNDguNjE1YzQuMzQzLTQuNzEsNy4xNTEtMTAuODU4LDcuNjE0LTE3LjY1M0gzMi43MzNMNTAuMzg2LDQ4LjYxNXoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiM1NTYwODA7IiBkPSJNMjcsMjguMTM0VjAuMDM4QzExLjkxOCwxLjA2NywwLDEzLjYxOSwwLDI4Ljk2MkMwLDM2LjI1LDIuNjk1LDQyLjkwNSw3LjEzNCw0OEwyNywyOC4xMzR6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojNzFDMjg1OyIgZD0iTTI4LjQxNCwzMi4zNzZMOS45NjIsNTAuODI4YzUuMDk1LDQuNDM5LDExLjc1LDcuMTM0LDE5LjAzOCw3LjEzNCAgIGM2Ljk5LDAsMTMuMzk2LTIuNDc5LDE4LjQwMS02LjU5OUwyOC40MTQsMzIuMzc2eiIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
                            <hr>
                           <div style="text-align:center">Statistiques</div>
                        </a>
                    </div>
                    <div class="col-md-1">
                        <a href="/annonceur/script">
                            <img width="100%" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA0ODkuOCA0ODkuOCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDg5LjggNDg5Ljg7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGlkPSJYTUxJRF83OThfIiBzdHlsZT0iZmlsbDojQTdBOUFDOyIgZD0iTTQ4MC43LDQ1NC43NXYtMjQuN2MwLTUuMi0yLjMtMTAuMi02LjMtMTMuNWMtMjIuMy0xOC4zLTQ2LjMtMzAuNS01MS4yLTMyLjkgICAgYy0wLjYtMC4zLTAuOS0wLjgtMC45LTEuNHYtMzQuN2M0LjQtMi45LDcuMi03LjksNy4yLTEzLjV2LTM2YzAtMTcuOS0xNC41LTMyLjQtMzIuNC0zMi40aC0zLjloLTMuOWMtMTcuOSwwLTMyLjQsMTQuNS0zMi40LDMyLjQgICAgdjM2YzAsNS42LDIuOSwxMC42LDcuMiwxMy41djM0LjdjMCwwLjYtMC4zLDEuMi0wLjksMS40Yy00LjksMi40LTI4LjksMTQuNS01MS4yLDMyLjljLTQsMy4zLTYuMyw4LjMtNi4zLDEzLjV2MjQuNyIvPgoJCTxwYXRoIGQ9Ik05Ni44LDIzNC4zNWMtNSwwLTkuMSw0LjEtOS4xLDkuMXYzNmMwLDQyLjksMzQuOSw3Ny44LDc3LjgsNzcuOGgxMTQuNGwtMTUsMTVjLTMuNSwzLjUtMy41LDkuMywwLDEyLjggICAgYzEuOCwxLjgsNC4xLDIuNyw2LjQsMi43czQuNi0wLjksNi40LTIuN2wzMC40LTMwLjRjMS43LTEuNywyLjctNCwyLjctNi40cy0xLTQuNy0yLjctNi40bC0zMC40LTMwLjRjLTMuNS0zLjUtOS4zLTMuNS0xMi44LDAgICAgcy0zLjUsOS4zLDAsMTIuOGwxNSwxNUgxNjUuNWMtMzIuOSwwLTU5LjctMjYuOC01OS43LTU5Ljd2LTM2QzEwNS44LDIzOC4zNSwxMDEuOCwyMzQuMzUsOTYuOCwyMzQuMzV6Ii8+CgkJPHBhdGggZD0iTTIxMS43LDE0Ny4wNWMxLjgsMS44LDQuMSwyLjcsNi40LDIuN2MyLjMsMCw0LjYtMC45LDYuNC0yLjdjMy41LTMuNSwzLjUtOS4zLDAtMTIuOGwtMTUtMTVoMTE0LjkgICAgYzMyLjksMCw1OS43LDI2LjgsNTkuNyw1OS43djM2YzAsNSw0LjEsOS4xLDkuMSw5LjFzOS4xLTQuMSw5LjEtOS4xdi0zNmMwLTQyLjktMzQuOS03Ny44LTc3LjgtNzcuOEgyMDkuNmwxNS0xNSAgICBjMy41LTMuNSwzLjUtOS4zLDAtMTIuOHMtOS4zLTMuNS0xMi44LDBsLTMwLjQsMzAuNGMtMS43LDEuNy0yLjcsNC0yLjcsNi40czEsNC43LDIuNyw2LjRMMjExLjcsMTQ3LjA1eiIvPgoJCTxwYXRoIGQ9Ik05LjMsMjMzLjI1YzUsMCw5LjEtNC4xLDkuMS05LjF2LTI0LjdjMC0yLjUsMS4xLTQuOSwzLTYuNWMyMS42LTE3LjcsNDUtMjkuNSw0OS40LTMxLjdjMy42LTEuOCw2LTUuNSw2LTkuNnYtMzQuNyAgICBjMC0zLTEuNS01LjktNC03LjVjLTItMS4zLTMuMi0zLjYtMy4yLTZ2LTM2YzAtMTIuOSwxMC41LTIzLjQsMjMuMy0yMy40aDcuN2MxMi45LDAsMjMuMywxMC41LDIzLjMsMjMuNHYzNmMwLDIuNC0xLjIsNC42LTMuMiw2ICAgIGMtMi41LDEuNy00LDQuNS00LDcuNXYzNC43YzAsNCwyLjMsNy44LDYsOS42YzQuNSwyLjIsMjcuOSwxNCw0OS40LDMxLjdjMS45LDEuNiwzLDMuOSwzLDYuNXYyNC43YzAsNSw0LjEsOS4xLDkuMSw5LjEgICAgczkuMS00LjEsOS4xLTkuMXYtMjQuN2MwLTgtMy41LTE1LjQtOS43LTIwLjVjLTE5LjEtMTUuNy0zOS42LTI3LjEtNDguOC0zMS45di0yNS45YzQuNi00LjcsNy4yLTExLDcuMi0xNy43di0zNiAgICBjMC0yMi45LTE4LjYtNDEuNS00MS41LTQxLjVoLTcuN2MtMjIuOSwwLTQxLjUsMTguNi00MS41LDQxLjV2MzZjMCw2LjcsMi42LDEzLDcuMiwxNy43djI1LjljLTkuMiw0LjgtMjkuNywxNi4yLTQ4LjgsMzEuOSAgICBjLTYuMSw1LTkuNywxMi41LTkuNywyMC41djI0LjdDMC4yLDIyOS4yNSw0LjMsMjMzLjI1LDkuMywyMzMuMjV6Ii8+CgkJPHBhdGggZD0iTTMwNi4zLDQwOS41NWMtNi4xLDUtOS43LDEyLjUtOS43LDIwLjV2MjQuN2MwLDUsNC4xLDkuMSw5LjEsOS4xczkuMS00LjEsOS4xLTkuMXYtMjQuN2MwLTIuNSwxLjEtNC45LDMtNi41ICAgIGMyMS42LTE3LjcsNDUtMjkuNSw0OS40LTMxLjdjMy42LTEuOCw2LTUuNSw2LTkuNnYtMzQuN2MwLTMtMS41LTUuOS00LTcuNWMtMi0xLjMtMy4yLTMuNi0zLjItNnYtMzZjMC0xMi45LDEwLjUtMjMuNCwyMy4zLTIzLjQgICAgaDcuN2MxMi45LDAsMjMuNCwxMC41LDIzLjQsMjMuNHYzNmMwLDIuNC0xLjIsNC42LTMuMiw2Yy0yLjUsMS43LTQsNC41LTQsNy41djM0LjdjMCw0LDIuMyw3LjgsNiw5LjZjNC41LDIuMiwyNy45LDE0LDQ5LjQsMzEuNyAgICBjMS45LDEuNiwzLDMuOSwzLDYuNXYyNC43YzAsNSw0LjEsOS4xLDkuMSw5LjFzOS4xLTQuMSw5LjEtOS4xdi0yNC43YzAtOC0zLjUtMTUuNC05LjctMjAuNWMtMTkuMS0xNS43LTM5LjYtMjcuMS00OC44LTMxLjkgICAgdi0yNS45YzQuNi00LjcsNy4yLTExLDcuMi0xNy43di0zNmMwLTIyLjktMTguNi00MS41LTQxLjUtNDEuNWgtNy43Yy0yMi45LDAtNDEuNSwxOC42LTQxLjUsNDEuNXYzNmMwLDYuNywyLjYsMTMsNy4yLDE3Ljd2MjUuOSAgICBDMzQ1LjksMzgyLjQ1LDMyNS41LDM5My44NSwzMDYuMyw0MDkuNTV6Ii8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" />
                            <hr>
                           <div style="text-align:center">Récupérer le script</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
