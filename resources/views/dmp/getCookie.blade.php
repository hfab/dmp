<?php
    header("Content-Type: application/javascript");

    $cookie_value = '';
    $origin_url = '';
    $to_be_hashed = "DMP-SC2CAP-VISITOR";
    $cookie_name = md5($to_be_hashed);
    if(isset($_COOKIE[$cookie_name])){
        $cookie_value = $_COOKIE[$cookie_name];

        $url = parse_url($_SERVER['HTTP_REFERER'],  PHP_URL_HOST);
        $subdomainPos = strpos($url, '.')+1;
        $origin_url = substr($url, $subdomainPos);

        \Log::info($origin_url);
    }
?>

function setCookieTimeExpiration(){
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 365 * 3600 * 24 * 1000;
    now.setTime(expireTime);
    return now.toGMTString();
}

@if(strlen($cookie_value) > 0)
var get_cookie = '{{$cookie_name}}={{$cookie_value}};expires='+setCookieTimeExpiration()+';path=/;domain={{$origin_url}}';
document.cookie = get_cookie;
@endif
