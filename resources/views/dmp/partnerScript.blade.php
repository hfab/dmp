{{-- code JS a mettre sur les sites partenaires --}}
{{-- CODE PERMETTANT DE RECUPERER LE COOKIE CREE PAR TOR DMP --}}
<script type="text/javascript">
//A mettre sur toutes les pages des sites partenaires ou sur leurs homePage
//Via la création de la balise script avec pour url celle pointant vers la DMP, 
//On crée une copie identique du cookie créée au moment de l'ouverture du mail par le destinataire 
//Il est nécessaire de faire ceci pour pouvoir manipuler ensuite ces données au moment où on souhaite envoyer les infos à la DMP.
    var s =document.createElement('script');
    var u = 'https://tordev.sc2consulting.fr/'+'ajcal_coo'; //Changer par l'url du DMP
    s.setAttribute("type","text/javascript");
    s.setAttribute("src", u);
    document.getElementsByTagName("head")[0].appendChild(s);


//Retourne la valeur du cookie donnée en paramètre
function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

//Crée un objet permettant l'envoi de requête ajax
function getXMLHttpRequest() {
	var xhr = null;
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest();
		}
	} else {
		console.log("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	return xhr;
}

//Envoie à la DMP si le destinataire à réaliser un achat ou non
function hasOrdered(token,hasOrdered){
    var cookieValue = getCookie(token);
    if(cookieValue != ""){
        var xhr = getXMLHttpRequest();
	xhr.onreadystatechange = function(){
    	    var response = xhr.responseText;
	    console.log(response);
	}
        xhr.open("GET","https://tordev.sc2consulting.fr/ajcal_vis?token="+cookieValue+"&has_ordered="+hasOrdered,true); //Changer par l'url du DMP
        xhr.send();
    }
    else{
        console.log("Fail");
    }
}

//A mettre sur toutes les pages pour détecter le départ de l'user
document.addEventListener('DOMContentLoaded', function(){
	//Récupération de tous les liens externes de la page.
        var extLinks = document.querySelectorAll('a[href*="https://"]');
	for(var index = 0;index< extLinks.length;index++){
		//Si le destinataire sur un lien externe, on envoie à la DMP que le destinataire en question n'a pas réalisé d'achat
		extLinks[index].addEventListener('click',function(){
			hasOrdered('f998b468d09d7ca3b2d0c09fea411c7e',0);
		});
	}
}
, false);
//A mettre sur la page de confirmation
//Informe que la DMP que l'utilisateur a réalisé un achat
hasOrdered('f998b468d09d7ca3b2d0c09fea411c7e', 1);
</script>


{{--EXEMPLE SUR VOYANEO--}}
{{--CODE MIS SUR LA HP/SUR TOUTES LES PAGES --}}
<script type="text/javascript">
    var s =document.createElement('script');
{{-- GENERIQUE => tordev.sc2consulting.fr à modifier par un NDD/CNAME plus discret --}}
{{-- PROTOCOLE => ATTENTION AU HTTPS --}}
    var u = 'https://tordev.sc2consulting.fr/'+'ajcal_coo';
    s.setAttribute("type","text/javascript");
    s.setAttribute("src", u);
    document.getElementsByTagName("head")[0].appendChild(s);

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function getXMLHttpRequest() {
        var xhr = null;
        if (window.XMLHttpRequest || window.ActiveXObject) {
            if (window.ActiveXObject) {
                try {
                    xhr = new ActiveXObject("Msxml2.XMLHTTP");
                } catch(e) {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            } else {
                xhr = new XMLHttpRequest();
            }
        } else {
            console.log("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
            return null;
        }
        return xhr;
    }

    function hasOrdered(token,hasOrdered){
        var cookieValue = getCookie(token);
        if(cookieValue != ""){
            var xhr = getXMLHttpRequest();
            xhr.onreadystatechange = function(){
                var response = xhr.responseText;
                console.log(response);
            }
            xhr.open("GET","https://tordev.sc2consulting.fr/ajcal_vis?token="+cookieValue+"&has_ordered="+hasOrdered,
                true);
            xhr.send();
        }
        else{
            console.log("Fail");
        }
    }

document.addEventListener('DOMContentLoaded', function(){
	//Récupération de tous les liens externes de la page.
        var extLinks = document.querySelectorAll('a[href*="https://"]');
	for(var index = 0;index< extLinks.length;index++){
		extLinks[index].addEventListener('click',function(){
			hasOrdered('f998b468d09d7ca3b2d0c09fea411c7e',0);
		});
	}
}
    </script>


{{--CODE MIS SUR LE TUNNEL DE RESA --}}
{{--ATTENTION L'URL DU TUNNEL DOIT ETRE REPERTORIEE DANS NOTRE DB--}}
<!-- SUPADV - 39 -->
<script type="text/javascript">
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function getXMLHttpRequest() {
        var xhr = null;
        if (window.XMLHttpRequest || window.ActiveXObject) {
            if (window.ActiveXObject) {
                try {
                    xhr = new ActiveXObject("Msxml2.XMLHTTP");
                } catch(e) {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            } else {
                xhr = new XMLHttpRequest();
            }
        } else {
            console.log("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
            return null;
        }
        return xhr;
    }

    function hasOrdered(token,hasOrdered){
        var cookieValue = getCookie(token);
        if(cookieValue != ""){
            var xhr = getXMLHttpRequest();
            xhr.onreadystatechange = function(){
                var response = xhr.responseText;
                console.log(response);
            }
{{-- GENERIQUE => tordev.sc2consulting.fr à modifier par un NDD/CNAME plus discret --}}
{{-- PROTOCOLE => ATTENTION AU HTTPS --}}
        xhr.open("GET","https://tordev.sc2consulting.fr/ajcal_vis?token="+cookieValue+"&has_ordered="+hasOrdered,true);
        xhr.send();
        } else{
            console.log("Fail");
        }
    }
    //Fonction lancé lorsque l'utilisateur arrive sur la page informant au client que sa commande est passée
    hasOrdered('f998b468d09d7ca3b2d0c09fea411c7e', 1);
</script>
<!-- END SUPADV-39 -->
