@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Ajouter des destinataires au dictionnaire MD5</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <div id="info" class="text-success" style="display:inline;"></div>  <button id="import" class="btn btn-danger" href="">Importer</button>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="form-group" id="upload-progress" style="opacity: 0;">
                        <label class="col-sm-3 control-label">Upload en cours...</label>
                        <div class="col-sm-9">
                            <div class="fileupload-process">
                                <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="_token" value="{{ csrf_token() }}" />



      </div>
      </div>
      </div>


      <script src="/js/dropzone.js"></script>

      <script>

       // document.getElementById(".col-sm-3 control-label").style.opacity = "0";

          var myDropzone = new Dropzone(document.body, {
              url: "/dmp/upload/inject",
              maxFilesize: 256, // Mo
              acceptedFiles:".csv,.txt",
              createImageThumbnails: false,
              parallelUploads: 1,
              previewTemplate: '<div style="display:none"></div>',
              autoQueue: true,
              clickable: "#import",
              init: function() {
                  this.on("success", function(file, responseText) {
      //                    toastr['success']('Import de '+file.name+' terminé : '+responseText.inserted+' insertions et '+responseText.already+' déjà présents sur '+responseText.total);
                      console.log('Lancement de la FCT');

      //                    $("#container").append("<table> <tr> <th> FAI </th> <th> nombre </th> </tr>");
      //                    $.each(responseText.infos_fais, function(key, val) {
      //                        $("#container").append("<tr> <td>"+key + "</td> <td>" +val+"</td></tr>");
      //                    });
      //                    $("#container").append("</table>");

                  });
              }
          });

          myDropzone.on("totaluploadprogress", function(progress) {
              document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
              if (progress == 100) {
                  // toastr['success']('Fichier uploadé, insertion en cours.');
                  $( "#info" ).html( 'Upload terminé' );

              }
          });

          myDropzone.on("sending", function(file, xhr, formData) {
              formData.append("base_id", $('#base_id').val());
              formData.append("_token", $('#_token').val());

              xhr.setRequestHeader("X-CSRF-TOKEN", $('#_token').val());
      //            xhr.setRequestHeader("X-XSRF-TOKEN", $('#_token').val());

              document.querySelector("#upload-progress").style.opacity = "1";
          });


          myDropzone.on("queuecomplete", function(progress) {
              document.querySelector("#upload-progress").style.opacity = "0";

          });


          $('#import').click(function(e) {
              e.preventDefault();
          })

          $('#formUpload').submit(function() {
              return false;
          })



      </script>



  @endsection
