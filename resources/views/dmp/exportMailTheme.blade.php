@extends('template')

@section('content')

<h4>Dernières relancées</h4>
<hr>
	<table class="table table-striped table-hover">
			<tr>
				<th>Id</th>
				<th>Base</th>
				<th>Campagne(référence)</th>
				<th>Volume sélectionné</th>
				<th>Routeur</th>
				<th>Date de relance</th>
				<th>Thème</th>
			</tr>

		@foreach($plannings as $planning)
			<tr>
				<td>{{$planning->id}}</td>
				<td>{{$planning->campagne->base->nom}}</td>
				<td>{{$planning->campagne->ref}}</td>
				<td>{{$planning->volume_selected}}</td>
				<td>{{$planning->routeur->nom}}</td>
				<td>{{$planning->sent_at}}</td>
				<th>{{$planning->campagne->theme->nom}}</th>
			</tr>
		@endforeach
	</table>


	<div class="row">
		  <div class="col-sm-6">
				<span>Thèmes relancés ce mois</span>
				<canvas id="myChart"></canvas>
			</div>
		  <div class="col-sm-6">
				<span>Nombre de mails uniques relancés</span>
				<canvas id="mailsRelaunched"></canvas>
		  </div>
		  <hr>
		  <div class="col-sm-6">
				<span>Pression</span>
				<canvas id="myChart2"></canvas>
		  </div>
		  <div class="col-sm-6">
				<span>Nombre de campagne par base</span>
				<canvas id="campaignBase"></canvas>
		  </div>

	</div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>


<script>

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}



var ctx = document.getElementById('myChart').getContext('2d');
var graphMailRelaunched = document.getElementById('mailsRelaunched').getContext('2d');
//Pour pression
var colorArray = [];
var label_array =[];
var data_array = [];

@foreach($a_themeVolume as $key => $value)
	label_array.push("{!! $a_themeVolume[$key]['name'] !!}");
	data_array.push({{$a_themeVolume[$key]['count']}});
	colorArray.push(getRandomColor());
@endforeach

//Plannings
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'horizontalBar',

    // The data for our dataset
    data: {
        labels: label_array,
        datasets: [{
            label: "Période du {{$a_date['in']}} au {{$a_date['out']}}",
            backgroundColor: colorArray,
            data: data_array,
        }],
    },
});

//Pression
var ctx2 = document.getElementById('myChart2').getContext('2d');

label_array = [];
data_array = [];
colorArray = [];
@foreach($countPression as $key => $value)
	label_array.push('Pression:{{$key}} ');
	data_array.push('{{$countPression[$key][0]}}');
	colorArray.push(getRandomColor());
@endforeach


var myPieChart = new Chart(ctx2,{
    type: 'doughnut',
		data: {
        datasets: [{
            backgroundColor: colorArray,
            data: data_array,
        }],
	labels: label_array,
    },
});

label_array = [];
data_array = [];
colorArray = [];
@foreach($addresses as $addresse)
	label_array.push('{{$addresse->nom}}');
	data_array.push({{$addresse->number}});
	colorArray.push(getRandomColor());
@endforeach
//Mails Relaunched
var chart = new Chart(graphMailRelaunched, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: label_array,
        datasets: [{
            label: "Période du {{$a_date['in']}} au {{$a_date['out']}}",
            backgroundColor: colorArray,
            data: data_array,
        }],
    },

    // Configuration options go here
    options: {}
});

//Campagnes relancées par Base
label_array = [];
data_array = [];
colorArray = [];
@foreach($campaigns as $campaign)
	label_array.push("{!! $campaign->nom !!}");
	data_array.push({{$campaign->number}});
	colorArray.push(getRandomColor());
@endforeach
var graphCampaignBase = document.getElementById('campaignBase').getContext('2d');
var chart = new Chart(graphCampaignBase, {
    // The type of chart we want to create
    type: 'horizontalBar',

    // The data for our dataset
    data: {
        labels: label_array,
        datasets: [{
            label: "Période du {{$a_date['in']}} au {{$a_date['out']}}",
            backgroundColor: colorArray,
            data: data_array,
        }],
    },

    // Configuration options go here
    options: {}
});


</script>
@endsection
