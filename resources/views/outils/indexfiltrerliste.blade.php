@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Modifier une liste</span>
        </div>
        <div class="actions">
          <div class="btn-group btn-group-devided">
              <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
          </div>
        </div>
    </div>
    <div class="portlet-body">

      <div class="form-group">
        <label for="listename">{{implode($fichier)}}</label>
      </div>

      {!! Form::open(array('url' => 'outils/listemanager/modifier','files' => true, 'method' => 'post')) !!}

      <div class="form-group">
        <label for="listename">Choisir un fichier de Blacklist / Désabonnement : </label>
      </div>

      @foreach ($fichiers as $lefichier)
       <div class="checkbox">
        <label><input type="radio" value="{{basename($lefichier)}}" name="file">{{\File::name($lefichier)}}</label>
       </div>
      @endforeach

        <hr>

        <input type="hidden" name="choix" value="filtrerlisteaction">
        <input type="hidden" name="varfichierbase" value="{{implode($fichier)}}">
       <button type="submit" class="btn btn-success">Valider</button>
      {!! Form::close() !!}

    </div>
</div>
</form>


@endsection

@section('footer')


@endsection
