@extends('common.layout')

@section('content')


<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
                Importer des fichiers liste
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/downl"><button class="btn btn-primary" href="">Retour</button></a>
            </div>
        </div>
    </div>

    <div class="portlet-body">
      <table class="table table-striped table-bordered">
        <tr>
            <th>Nom fichier</th>
            <th>Action</th>
        </tr>
      @foreach ($fichiers as $fichier)

          <tr>
            <td>{{\File::name($fichier)}}</td>
            <td>
              <a href="/downl/deletefile/{{basename($fichier)}}"><span class="label label-danger">Supprimer</span></a> - <a href="/downl/file/{{basename($fichier)}}"><span class="label label-success">Télécharger</span></a>
            </td>
          </tr>



      @endforeach
      </table>
        <hr>

    </div>

</div>
@endsection

@section('footer')


@endsection
