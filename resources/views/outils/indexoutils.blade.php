@extends('template')

@section('content')
     <div class="container">
          <div class="row">
               <div class="col-xs-12">
                   <i class="fa fa-2x fa-cogs text-success"></i>
                   <span class="lead text-success">Outils</span>
                   <br />
                   <br />
               </div>
                    <!-- <div class="actions">
                         <div class="btn-group btn-group-devided"></div>
                    </div> -->
               <div class="col-xs-12 text-center">
                    <a href="listemanager/upload"><button type="button" class="btn btn-default">Importer/Supprimer fichier</button></a>
                    <a href="listemanager/statliste"><button type="button" class="btn btn-success">Stats d'un fichier</button></a>
                    <a href="listemanager/decoupage"><button type="button" class="btn btn-primary">Découper un fichier</button></a>
                    <a href="listemanager/modifier"><button type="button" class="btn btn-warning">Traiter un fichier</button></a>
                    <a href="listemanager/assembler"><button type="button" class="btn btn-danger">Assembler fichiers</button></a>
                    <a href="listemanager/nddfr"><button type="button" class="btn btn-info">Supprimer étrangers</button></a>
               </div>

               <div class="col-xs-12">
                    <h3 class="lead">V2</h3>
               </div>

               <div class="col-xs-12 text-center">
                    <a href="/tool/filterblacklist"><button type="button" class="btn btn-danger">Filtrer blackliste</button></a>
                    <a href="/tool/filterbdd"><button type="button" class="btn btn-warning">Filtrer BDD</button></a>
                    <a href="listemanager/upload"><button type="button" class="btn btn-default">Importer/Supprimer fichier</button></a>
                    <a href="/downl/files"><button type="button" class="btn btn-default">Consulter les fichiers générés</button></a>
               </div>
          </div>
     </div>
@endsection

@section('footer')
@endsection
