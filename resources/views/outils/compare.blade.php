@extends('common.layout')

@section('content')

    <h1>Substract</h1>

    <h2>Comparer deux fichiers</h2>

    <form id="formUpload" class="form-horizontal">

        <button id="add_file" class="btn btn-danger">Ajouter un fichier</button>

        <div id="previews"></div>
        <div class="table table-striped" class="files" id="previews">

            <div id="template" class="file-row">
                <div class="row">
                    {{--<div class="col-md-4">--}}
                        {{--<span class="preview"><img data-dz-thumbnail /></span>--}}
                    {{--</div>--}}
                    <div class="col-md-3">
                        <p class="name" data-dz-name></p>
                        <strong class="error text-danger" data-dz-errormessage></strong>
                        <p class="size" data-dz-size></p>
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                        </div>
                    </div>
                    <div class="col-md-5 upload-form">
                        <div class="media_info"></div>
                    </div>
                    {{--<div class="col-md-2">--}}
                        {{--<button class="btn btn-primary start">--}}
                            {{--<i class="glyphicon glyphicon-upload"></i>--}}
                            {{--<span>Start</span>--}}
                        {{--</button>--}}
                        {{--<button data-dz-remove class="btn btn-warning cancel">--}}
                            {{--<i class="glyphicon glyphicon-ban-circle"></i>--}}
                            {{--<span>Cancel</span>--}}
                        {{--</button>--}}
                        {{--<button data-dz-remove class="btn btn-danger delete">--}}
                            {{--<i class="glyphicon glyphicon-trash"></i>--}}
                            {{--<span>Delete</span>--}}
                        {{--</button>--}}
                    {{--</div>--}}
                </div>
                <hr />
            </div>
        </div>

        <input type="hidden" id="_token" value="{{ csrf_token() }}" />
    </form>

@endsection

@section('footer')
    <script src="/js/dropzone.js"></script>
    <script>

        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        console.log(previewTemplate);

        var myDropzone = new Dropzone(document.body, {
            url: "/compare",
            acceptedFiles:".csv,.txt",
            parallelUploads: 2,
            autoQueue: false,
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: "#add_file",
            init: function() {
                this.on("success", function(file, responseText) {
                    var response = JSON.parse(responseText);
                    toastr['success']('Import terminé : '+response.inserted+' insertions et '+response.already+' déjà présents');
                });
            }
        });

//        myDropzone.on("totaluploadprogress", function(progress) {
//            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
//        });
//
        myDropzone.on("sending", function(file, xhr, formData) {
            formData.append("_token", $('#_token').val());
//            document.querySelector("#upload-progress").style.opacity = "1";
        });
//
//        myDropzone.on("queuecomplete", function(progress) {
//            document.querySelector("#upload-progress").style.opacity = "0";
//        });

        $('.import').click(function(e) {
            e.preventDefault();
        })

        $('#formUpload').submit(function() {
            return false;
        })

    </script>

@endsection
