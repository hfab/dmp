<?php if (!isset($menu)) { $menu = ''; } ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="fr" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="fr" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="fr">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>TOR
      <?php
        if(getenv('CLIENT_TITLE') !== null){
          echo getenv('CLIENT_TITLE');
        } else {
          echo 'TOR';
        }
      ?>
    </title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">

    <meta name="robots" content="noindex,nofollow"/>

    <link rel="icon" type="image/png" href="{{url('/')}}/images/favicon.png" />
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/layout.css">
    <link rel="stylesheet" href="/css/global.css">

    <script src="{{ asset('swal/sweetalert.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('swal/sweetalert.css') }}">


    <script src="/js/base.js"></script>
    <script src="/js/layout.js"></script>
</head>

<body>

<!-- BEGIN HEADER -->
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
              <div class="pull-left">
                    <a href="/" id="top-logo" >
                        <img src="{{url('/')}}/images/logo3.jpg" alt="logo" width="auto" height="75">
                        <h1 id="beta">
                          <?php
                            if(getenv('CLIENT_NAME') !== null){
                              echo getenv('CLIENT_TITLE');
                            } else {
                              echo 'TOR';
                            }
                          ?>
                        </h1>
                    </a>
                </div>
            </div>
            <!-- END LOGO -->

            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">


                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default">{{ \App\Models\Notification::where('user_id', \Auth::User()->id)->where('created_at','>',\Carbon\Carbon::today())->count() }}</span>
                        </a>

                        <?php $notifications = \App\Models\Notification::where('is_important', 1)->orderBy('id', 'desc')->take(6)->get(); ?>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3><strong>Nouvelles</strong> notifications</h3>
                                <a href="/notifications">voir toutes</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    @foreach($notifications as $notification)
                                        <li>
                                            <a href="{{$notification->url}}">
                          <span class="details">
                            <span class="label label-sm label-icon label-{{$notification->color}} {{$notification->color}}">
                              <i class="fa fa-{{$notification->icon}}"></i>
                            </span>
                                                        {{$notification->message}}
                          </span>
                                                <span class="time">{{$notification->created_at->format('H:i')}}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                        <a href='/deconnexion'><button type="button" class="btn btn-danger btn-sm">Se déconnecter</button></a>
                    </li>

                    <li class="droddown dropdown-separator">
                        <a href='/faq'><i class="fa fa-question fa-lg text-success" aria-hidden="true"></i></a>
                    </li>

                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <nav class="navbar navbar-default">
        <div class="container">
        <a href="/mobilehome">
            <button type="button" class="navbar-home" aria-expanded="false">
                <span>Mobile Home</span>
            </button>
        </a>
        <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse hor-menu">
                <ul class="nav navbar-nav">
                    <li><a title="Base" href="/base"><i class="fa fa-database fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Bases</span></a></li>
                    <li><a title="Senders" href="/sender"><i class="fa fa-rocket fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Senders</span></a></li>
                    <li class="dropdown">
                        <a title="Campagnes" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Campagnes</span>&nbsp;&nbsp;<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a title="Campagnes" href="/campagne"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i><span> Campagnes</span></a></li>
                            <li><a title="Thèmes" href="/theme"><i class="fa fa-tags fa-lg" aria-hidden="true"></i><span> Themes</span></a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/campagne/create">Créer une campagne</a></li>
                        </ul>
                    </li>
                    <li><a title="Plannings" href="/planning"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Planning</span></a></li>
                    <li><a title="Statistiques" href="/stats"><i class="fa fa-bar-chart fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Stats</span></a></li>
                    {{--<li @if ($menu == "bounces") class="active" @endif><a href="/stats/bounces">Bounces</a></li>--}}
                    {{--<li @if ($menu == "notifications") class="active" @endif><a href="/notifications">Activité</a></li>--}}
                    @if( \Auth::User()->user_group->name == 'Admin' || \Auth::User()->user_group->name == 'Super Admin' )
                        <li><a title="Utilisateurs" href="/user"><i class="fa fa-users fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Utilisateurs</span></a></li>
                    @endif
                    <li><a title="Outils" href="/outils/listemanager"><i class="fa fa-scissors fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Outils</span></a></li>
                    <li title="Fais"><a href="/fai"><i class="fa fa-cog fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Fais</span></a></li>
                    @if( \Auth::User()->user_group->name == 'Super Admin' )
                        <li><a title="Admin" href="/admin"><i class="fa fa-lock fa-lg" aria-hidden="true"></i><span class="visibleSmart"> Admin </span></a></li>
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
            <div id="indication"></div>
            <script>
            /*
                $(document).mousemove(function(e){
                    $('#indication').css({
                        left:  event.pageX - 35,
                        top: 140
                    });
                });

                */
                $('.navbar-nav li').each(function(){
                    $(this).hover(function(){
                        $('#indication').toggle();
                        $('#indication').text($(this).find('a').attr('title'))
                    })
                })
                $('.navbar li').each(function(){
                    $(this).removeClass('active');
                    if($(this).find('a').prop('href') == location)
                    {
                        $(this).addClass('active');
                    }
                })
            </script>
        </div><!-- /.container-fluid -->
    </nav>
    <!-- END MEGA MENU -->

    </div>
    <!-- END HEADER MENU -->

<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
    @if (isset($page_title))
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{!! $page_title !!}</h1>
                </div>
            </div>
        </div>
    @endif

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">


    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Liste des bases
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                  <a href="/downl/files/" type="submit" class="btn btn-info btn-xs">Liste des fichiers</a>
                </div>
            </div>
        </div>



        <div class="portlet-body">

            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>Identifiant</th>
                    <th>Code</th>
                    <th>Nom</th>

                    <th>Génération de fichier</th>
                </tr>

                @foreach($bases as $b)
                    <tr>
                        <td>{{ $b->id }}</td>
                        <td>{{ $b->code }}</td>
                        <td>{{ $b->nom }}</td>

                        <td>

                          <button id="launch_export_mail_{{$b->id}}" href="/downl/base/{{$b->id}}" type="submit" class="btn btn-success btn-sm">Email</button>
                          <button id="launch_export_all_{{$b->id}}" href="/downl/champs/{{$b->id}}" type="submit" class="btn btn-primary btn-sm">Email et champs</button>
                          <button id="launch_export_desabo_{{$b->id}}" href="/downl/basedesabo/{{$b->id}}" type="submit" class="btn btn-danger btn-sm">Désinscrits</button>
                          <button id="launch_export_md5_{{$b->id}}" href="/downl/basemd5/{{$b->id}}" type="submit" class="btn btn-warning btn-sm">HASH (md5)</button>

                        </td>

                    </tr>
                @endforeach
            </table>




            <!--
              faire un swal ca commence





              faire une notif debut / fin



            -->



        </div>
    </div>



<script>

$("[id*=launch_export_mail_]").click(function(e){


var id_button = $(this).attr('id');
var href_button = $(this).attr('href');

console.log(id_button);
console.log(href_button);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
    }
});

$.ajax({
    url: "http://" + document.domain + href_button,
    type: 'GET',

});
swal({
  title: "<h1>Lancement de l'export EMAIL</h1>",
  text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
  html: true
});
});

$("[id*=launch_export_all_]").click(function(e){


var id_button = $(this).attr('id');
var href_button = $(this).attr('href');

console.log(id_button);
console.log(href_button);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
    }
});

$.ajax({
    url: "http://" + document.domain + href_button,
    type: 'GET',

});
swal({
  title: "<h1>Lancement de l'export EMAIL et Champs</h1>",
  text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
  html: true
});
});


$("[id*=launch_export_desabo_]").click(function(e){


var id_button = $(this).attr('id');
var href_button = $(this).attr('href');

console.log(id_button);
console.log(href_button);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
    }
});

$.ajax({
    url: "http://" + document.domain + href_button,
    type: 'GET',

});
swal({
  title: "<h1>Lancement de l'export unsubscribe</h1>",
  text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
  html: true
});
});

$("[id*=launch_export_md5_]").click(function(e){


var id_button = $(this).attr('id');
var href_button = $(this).attr('href');

console.log(id_button);
console.log(href_button);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
    }
});

$.ajax({
    url: "http://" + document.domain + href_button,
    type: 'GET',

});
swal({
  title: "<h1>Lancement de l'export HASH MD5</h1>",
  text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
  html: true
});
});



</script>


<div><a id="cRetour" class="cInvisible" href="#top-logo"></a></div>
</div>
</div>
</div>
</div>
<div class="page-footer">
<div class="container">
2015 - 2017 &copy; <a href="/">TOR {{APP_VERSION}}</a>
</div>
</div>
<div class="scroll-to-top">
<i class="icon-arrow-up"></i>
</div>
<!--[if lt IE 9]>
<!--<script src="../../assets/global/plugins/respond.min.js"></script>-->
{{--<script src="../../assets/global/plugins/excanvas.min.js"></script>--}}
<![endif]-->


<script>
document.addEventListener('DOMContentLoaded', function() {
    window.onscroll = function(ev) {
        document.getElementById("cRetour").className = (window.pageYOffset > 100) ? "cVisible" : "cInvisible";
    };
});
/*
    jQuery(document).ready(function() {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        UIAlertDialogApi.init();
    });
    */
</script>


</body>

</html>
