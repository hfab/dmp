@extends('template')

@section('content')

     <div class="row">
         <div class="col-xs-12">
               <i class="fa fa-cogs fa-2x text-success"></i>
               <span class="lead text-success">Découper une liste d'email</span>
               <br>
               <br>
          </div>
          <div class="col-xs-12">
               <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
               <hr>
          </div>

          <div class="col-xs-12">
               {!! Form::open(array('url' => 'outils/listemanager/decoupage','files' => true, 'method' => 'post')) !!}
               <label>Nom de la liste : <input type="text" name="nomdecoupage"></label>
               <hr>
               @foreach ($fichiers as $fichier)
                    <div class="checkbox">
                         <label><input type="radio" value="{{basename($fichier)}}" name="file">{{\File::name($fichier)}}</label>
                         <br>
                    </div>
               @endforeach
               <hr>
               <div class="col-xs-12">
                    <label>Nombre de fichier : <input type="number" name="nbrfichier" value="0"></label>
               </div>
               <hr>
               <div class="col-xs-12">
                    <label>Nombre Autre : <input type="number" name="nbrautre" value="0"></label>
                    <br>
               </div>
               <div class="col-xs-12">
                    <label>Nombre de Free : <input type="number" name="nbrfree" value="0"></label>
                    <br>
               </div>
               <div class="col-xs-12">
                    <label>Nombre de SFR/Neuf : <input type="number" name="nbrsfrneuf" value="0"></label>
                    <br>
               </div>
               <div class="col-xs-12">
                    <label>Nombre de LaPoste : <input type="number" name="nbrlaposte" value="0"></label>
                    <br>
               </div>
               <div class="col-xs-12">
                    <label>Nombre de Aol : <input type="number" name="nbraol" value="0"></label>
                    <br>
               </div>
               <div class="col-xs-12">
                    <label>Nombre de Gmail : <input type="number" name="nbrgmail" value="0"></label>
                    <br>
               </div>
               <div class="col-xs-12">
                    <label>Nombre de Hotmail : <input type="number" name="nbrhotmail" value="0"></label>
                    <br>
               </div>
               <div class="col-xs-12">
                    <label>Nombre de Yahoo : <input type="number" name="nbryahoo" value="0"></label>
                    <br>
               </div>
               <hr>
               <div class="col-xs-12">
                   <hr>
                   <button type="submit" class="btn btn-success">Découper</button>
                   {!! Form::close() !!}
               </div>
          </div>
     </div>
@endsection

@section('footer')
@endsection
